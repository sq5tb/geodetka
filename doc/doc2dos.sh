#!/bin/sh
cp ../ABOUT-NLS ./about-nls.txt  
cp ../AUTHORS ./authors.txt
cp ../ChangeLog ./changelog.txt
cp ../COPYING ./copying.txt
cp ../INSTALL ./install.txt
cp ../NEWS ./news.txt
cp ../README ./readme.txt
cp ../TODO ./todo.txt
unix2dos ./*.txt
