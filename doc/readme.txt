-------------------------------------------------------------------------------
GeodeTKa 1.2
Geodezyjny kalkulator napisany w GTK+

Autor programu: Bogusław Ciastek <SQ5TB@tlen.pl>
Program stanowi fragment pracy dyplomowej wykonanej pod kierunkiem
prof. dr hab. Edwarda Nowaka
-------------------------------------------------------------------------------


GeodeTKa to program do podstawowych obliczeń geodezyjnych. Został napisany
w C++ przy użyciu biblioteki GTK2. Jest przeznaczony głównie dla systemu
operacyjnego Linux, dostępna jest jednak również wersja dla środowiska Windows.
Program stanowi fragment pisanej przeze mnie pracy dyplomowej, mam nadzieję,
że okaże się przydatny także szerszej grupie osób związanych z geodezją.

Cechy programu:
* program oparty jest na relacyjnym modelu bazy danych SQLite3 pozwalającym
  na gromadzenie punktów i obserwacji w wielu różnych zbiorach roboczych
* możliwy jest import oraz eksport danych za pomocą pliku tekstowego CSV
* po zakończeniu każdego etapu obliczeń możliwe jest wygenerowanie raportu
  i obejrzenie wyników na szkicu
* realizowane obliczenia obejmują najczęściej spotykane zadania geodezyjne
  (wcięcia, domiary, rzutowanie na linię prostą, przecięcia, transformację
   punktów, obliczenie pola powierzchni) i będą w upływem czasu poszerzane.
* w oknie preferencji możemy ustawić wygodną dla nas jednostkę kąta i układ
  (geodezyjny/matematyczny)
* jest to jedyny znany autorowi tego typu program przeznaczony dla Linuksa
