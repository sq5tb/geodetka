/***************************************************************************
 *            callbacks.h
 *  FUNKCJE WYWOLAN ZWROTNYCH DLA INTERFEJSU
 *  Sun May 29 14:47:05 2005
 *  Copyright  2005  Bogusław Ciastek
 *  ciacho@z.pl
 ****************************************************************************/
#include "winconst.h"


// Funkcje ogolne
gboolean event_get_kod (GtkWidget *widget, GdkEventKey *event, gpointer user_data);

gboolean event_get_pkt (GtkWidget *widget, GdkEventKey *event, gpointer user_data);

// winMain - MENU
void on_mnuMainImportujObserwacje_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainEdytujObserwacje_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainHistoria_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainEksportujObserwacje_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainTachimetria_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_winMain_realize (GtkWidget *widget, gpointer user_data);

void on_mnuMainEdytujPunkty_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainImportujPunkty_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainEksportujPunkty_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainPreferencje_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainPomoc_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainProjekty_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainZakoncz_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainUsunPunkty_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainInformacjeO_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainWciecia_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainZadanieMareka_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainDodajPunkt_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainStatystyka_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainAzymut_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainKat_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainPole_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainDomiary_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainRzutProsta_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainTransformacja_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainTyczenieBiegunowe_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainPrzeciecia_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainOkragWpasowanie_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainOkragStyczna_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainWebFreegis_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainWebGama_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuMainWebGrass_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_winMain_destroy (GtkObject *object, gpointer user_data);

void on_btnMainModyfikuj_clicked (GtkButton *button, gpointer user_data);

void on_cbtnMainUkladLokalny_toggled (GtkToggleButton *togglebutton, gpointer user_data);


// winPunkty
void on_tbtnPunktyRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPunktyWidok_clicked (GtkToolButton   *toolbutton, gpointer user_data);

void on_treePunkty_cursor_changed (GtkTreeView *treeview, gpointer user_data);

void on_tbtnPunktyWyczysc_clicked (GtkButton *button, gpointer user_data);

void on_tbtnPunktyDodaj_clicked (GtkButton *button, gpointer user_data);

void on_tbtnPunktyModyfikacja_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPunktyUsun_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPunktyEksport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPunktyImport_clicked (GtkToolButton *toolbutton, gpointer user_data);

gboolean on_entryPunktyKod_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPunktyKod_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_entryPunktyFiltr_changed (GtkEditable *editable, gpointer user_data);

void on_winPunkty_destroy (GtkObject *object, gpointer user_data);

void on_tbtnPunktyOdswiez_clicked (GtkToolButton *toolbutton, gpointer user_data);


// Okno Wciecia
void on_btnWcieciaKatoweDodajWynik_clicked (GtkButton *button, gpointer user_data);

gboolean on_entryWcieciaKatowePktLNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaKatowePktLNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaKatowePktPNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaKatowePktPNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_cbtnWcieciaKatoweOgolne_toggled (GtkToggleButton *togglebutton, gpointer user_data);

gboolean on_entryWcieciaKatowePktL1Nr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaKatowePktL1Nr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaKatowePktP1Nr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaKatowePktP1Nr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaKod_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaKod_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_winPunkty_realize (GtkWidget *widget, gpointer user_data);

gboolean on_entryWcieciaWsteczPktLNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaWsteczPktLNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaWsteczPktCNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaWsteczPktCNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaWsteczPktPNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaWsteczPktPNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_checkWcieciaWsteczKierunki_toggled (GtkToggleButton *togglebutton, gpointer user_data);

void on_btnWcieciaWsteczDodajWynik_clicked (GtkButton *button, gpointer user_data);

gboolean on_entryWcieciaAWPrzodPktLNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaAWPrzodPktLNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaAWPrzodPktPNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaAWPrzodPktPNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnWcieciaAWPrzodDodajWynik_clicked (GtkButton *button, gpointer user_data);

gboolean on_entryWcieciaAWsteczPktLNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaAWsteczPktLNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaAWsteczPktPNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaAWsteczPktPNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnWcieciaAWsteczDodajWynik_clicked (GtkButton *button, gpointer user_data);

void on_tbtnWcieciaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_winWciecia_destroy (GtkObject *object, gpointer user_data);

gboolean on_entryWcieciaKatoweWynikNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);

gboolean on_entryWcieciaLinioweWynikNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);

gboolean on_entryWcieciaWsteczWynikNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);

gboolean on_entryWcieciaAWPrzodWynikNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);

gboolean on_entryWcieciaAWsteczWynikNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);


// Zadanie Mareka
gboolean on_entryZMarekaSt1Nr_focus_out_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);

gboolean on_entryZMarekaSt2Nr_focus_out_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);

gboolean on_entryZMarekaL1Nr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryZMarekaP1Nr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryZMarekaP2Nr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryZMarekaL2Nr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_tbtnZMarekaWykonaj_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnZMarekaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_btnZMarekaDodajWynik_clicked (GtkButton *button, gpointer user_data);

void on_tbtnZMarekaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

gboolean on_entryZMarekaL1Nr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryZMarekaP1Nr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryZMarekaP2Nr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryZMarekaL2Nr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_tbtnZMarekaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnZMarekaWidok_clicked (GtkToolButton *toolbutton, gpointer user_data);

gboolean on_entryZMarekaKod_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryZMarekaKod_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_winZMareka_destroy (GtkObject *object, gpointer user_data);


// dialog Kod
void on_dialogKod_realize (GtkWidget *widget, gpointer user_data);

void on_entryDialogKodFiltr_changed (GtkEditable *editable, gpointer user_data);

void on_dialogKod_response (GtkDialog *dialog, gint response_id, gpointer user_data);


// dialog Punkty
void on_dialogPunkty_realize (GtkWidget *widget, gpointer user_data);

void on_entryDialogPunktyFiltr_changed (GtkEditable *editable, gpointer user_data);

void on_dialogPunkty_response (GtkDialog *dialog, gint response_id, gpointer user_data);


// Wciecia
void on_tbtnWcieciaWykonaj_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnWcieciaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnWcieciaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnWcieciaWidok_clicked (GtkToolButton *toolbutton, gpointer user_data);

gboolean on_entryWcieciaLiniowePktLNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaLiniowePktLNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaLiniowePktPNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryWcieciaLiniowePktPNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnWcieciaLinioweDodajWynik_clicked (GtkButton *button, gpointer user_data);

void on_cbtnWcieciaWsteczKierunki_toggled (GtkToggleButton *togglebutton, gpointer user_data);


// Okrag Styczna
gboolean on_entryOkragStycznaPunktOkrNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragStycznaPunktOkrNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragStycznaPunktPktNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragStycznaPunktPktNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragStycznaKod_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragStycznaKod_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_tbtnOkragStycznaWykonaj_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnOkragStycznaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnOkragStycznaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnOkragStycznaWidok_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnOkragStycznaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

gboolean on_entryOkragStycznaProstaOkrNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragStycznaProstaOkrNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragStycznaProstaPrNr1_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragStycznaProstaPrNr1_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragStycznaProstaPrNr2_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragStycznaProstaPrNr2_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnOkragStycznaProstaWynikDodaj_clicked (GtkButton *button, gpointer user_data);

gboolean on_entryOkragStyczna2OkregiOkr1Nr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragStyczna2OkregiOkr1Nr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragStyczna2OkregiOkr2Nr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragStyczna2OkregiOkr2Nr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnOkragStyczna2OkregiWynikDodaj_clicked (GtkButton *button, gpointer user_data);

void on_winOkragStyczna_destroy (GtkObject *object, gpointer user_data);

void on_buttonOkragStycznaPunktWynikDodaj_clicked (GtkButton *button, gpointer user_data);


// Okrag Wpasowanie
void on_tbtnOkragWpasowanieWykonaj_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnOkragWpasowanieWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnOkragWpasowanieRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnOkragWpasowanieWidok_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnOkragWpasowanieZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

gboolean on_entryOkragWpasowanie2ProstePLNr1_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragWpasowanie2ProstePLNr1_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragWpasowanie2ProstePLNr2_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragWpasowanie2ProstePLNr2_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragWpasowanie2ProstePPNr1_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragWpasowanie2ProstePPNr1_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragWpasowanie2ProstePPNr2_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragWpasowanie2ProstePPNr2_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnOkragWpasowanie2ProsteWynikDodaj_clicked (GtkButton *button, gpointer user_data);

gboolean on_entryOkragWpasowanieKod_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragWpasowanieKod_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragWpasowanie3PktPNr1_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragWpasowanie3PktPNr1_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragWpasowanie3PktPNr2_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragWpasowanie3PktPNr2_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragWpasowanie3PktPNr3_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryOkragWpasowanie3PktPNr3_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnOkragWpasowanie3PktWynikDodaj_clicked (GtkButton *button, gpointer user_data);

void on_winOkragWpasowanie_destroy (GtkObject *object, gpointer user_data);


// Przeciecia
gboolean on_entryPrzecieciaProstePr1Nr1_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaProstePr1Nr1_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaProstePr1Nr2_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaProstePr1Nr2_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaProstePr2Nr1_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaProstePr2Nr1_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaProstePr2Nr2_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaProstePr2Nr2_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaKod_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaKod_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnPrzecieciaProsteWynikDodaj_clicked (GtkButton *button, gpointer user_data);

void on_tbtnPrzecieciaWykonaj_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPrzecieciaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPrzecieciaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPrzecieciaWidok_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPrzecieciaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

gboolean on_entryPrzecieciaOkragProstaOkrNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaOkragProstaOkrNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaOkragProstaPrNr1_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaOkragProstaPrNr1_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaOkragProstaPrNr2_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaOkragProstaPrNr2_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnPrzecieciaOkragProstaWynikDodaj_clicked (GtkButton *button, gpointer user_data);

gboolean on_entryPrzecieciaOkregiOkr1Nr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaOkregiOkr1Nr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaOkregiOkr2Nr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPrzecieciaOkregiOkr2Nr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnPrzecieciaOkregiWynikDodaj_clicked (GtkButton *button, gpointer user_data);

void on_winPrzeciecia_destroy (GtkObject *object, gpointer user_data);


// Miary biegunowe
void on_winBiegunowe_realize (GtkWidget *widget, gpointer user_data);

void on_tbtnBiegunoweOblicz_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnBiegunoweWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnBiegunoweUsun_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnBiegunoweRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnBiegunoweWidok_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnBiegunoweZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

gboolean on_entryBiegunoweNawiazanieStNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryBiegunoweNawiazanieStNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryBiegunoweNawiazaniePktNNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryBiegunoweNawiazaniePktNNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryBiegunowePktTyczonyNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryBiegunowePktTyczonyNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnBiegunowePktTyczonyDodaj_clicked (GtkButton *button, gpointer user_data);

void on_winBiegunowe_destroy (GtkObject *object, gpointer user_data);

void on_btnBiegunowePktTyczonyDodajWszystkie_clicked (GtkButton *button, gpointer user_data);


// Powierzchnia
void on_tbtnPowierzchniaOblicz_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPowierzchniaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPowierzchniaUsun_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPowierzchniaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPowierzchniaWidok_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPowierzchniaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

gboolean on_entryPowierzchniaPunktNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryPowierzchniaPunktNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnPowierzchniaPunktDodaj_clicked (GtkButton *button, gpointer user_data);

void on_winPowierzchnia_realize (GtkWidget *widget, gpointer user_data);

void on_winPowierzchnia_destroy (GtkObject *object, gpointer user_data);


// Rzut Prosta
void on_winRzutProsta_realize (GtkWidget *widget, gpointer user_data);

void on_tbtnRzutProstaOblicz_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnRzutProstaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnRzutProstaUsun_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnRzutProstaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnRzutProstaWidok_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnRzutProstaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

gboolean on_entryRzutProstaBazaPPNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryRzutProstaBazaPPNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryRzutProstaBazaPKNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryRzutProstaBazaPKNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryRzutProstaPunktNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryRzutProstaPunktNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnRzutProstaPunktDodaj_clicked (GtkButton *button, gpointer user_data);

void on_treeRzutProsta_cursor_changed (GtkTreeView *treeview, gpointer user_data);

void on_winRzutProsta_destroy (GtkObject *object, gpointer user_data);

void on_frameRzutProstaBaza_set_focus_child (GtkContainer *container, GtkWidget *widget, gpointer user_data);

void on_btnRzutProstaPunktDodajWszystkie_clicked (GtkButton *button, gpointer user_data);


// Azymut
void on_winAzymut_realize (GtkWidget *widget, gpointer user_data);

void on_tbtnAzymutWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnAzymutUsun_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnAzymutRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnAzymutWidok_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnAzymutZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_treeAzymut_cursor_changed (GtkTreeView *treeview, gpointer user_data);

gboolean on_entryAzymutPPNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryAzymutPPNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryAzymutPKNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryAzymutPKNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnAzymutDodaj_clicked (GtkButton *button, gpointer user_data);

void on_winAzymut_destroy (GtkObject *object, gpointer user_data);


// Domiary prostokatne
void on_winDomiary_realize (GtkWidget *widget, gpointer user_data);

void on_tbtnDomiaryOblicz_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnDomiaryWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnDomiaryUsun_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnDomiaryRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnDomiaryWidok_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnDomiaryZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

gboolean on_entryDomiaryBazaPPNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryDomiaryBazaPPNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryDomiaryBazaPKNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryDomiaryBazaPKNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryDomiaryKod_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryDomiaryKod_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnDomiaryMiaryDodaj_clicked (GtkButton *button, gpointer user_data);

void on_winDomiary_destroy (GtkObject *object, gpointer user_data);

void on_frameDomiaryBaza_set_focus_child (GtkContainer *container, GtkWidget *widget, gpointer user_data);

void on_btnDomiaryMiaryDodajWszystkie_clicked (GtkButton *button, gpointer user_data);

gboolean on_entryDomiaryMiaryNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);


// Kat
void on_winKat_realize (GtkWidget *widget, gpointer user_data);

void on_tbtnKatWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnKatUsun_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnKatRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnKatWidok_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnKatZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_treeKat_cursor_changed (GtkTreeView *treeview, gpointer user_data);

void on_btnKatDodaj_clicked (GtkButton *button, gpointer user_data);

gboolean on_entryKatCNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryKatCNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryKatLNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryKatLNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryKatPNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryKatPNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_winKat_destroy (GtkObject *object, gpointer user_data);


// Transformacja Helmerta
void on_btnTHelmertaDostDodajWszystkie_clicked (GtkButton *button, gpointer user_data);

void on_btnTHelmertaTransfDodajWszystkie_clicked (GtkButton *button, gpointer user_data);

void on_winTHelmerta_realize (GtkWidget *widget, gpointer user_data);

void on_tbtnTHelmertaOblicz_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnTHelmertaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnTHelmertaUsun_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnTHelmertaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnTHelmertaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

gboolean on_entryTHelmertaDostPNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryTHelmertaDostPNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryTHelmertaDostWNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryTHelmertaDostWNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_btnTHelmertaDostDodaj_clicked (GtkButton *button, gpointer user_data);

void on_btnTHelmertaTransfDodaj_clicked (GtkButton *button, gpointer user_data);

gboolean on_entryTHelmertaTransfNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

gboolean on_entryTHelmertaTransfNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);

void on_winTHelmerta_destroy (GtkObject *object, gpointer user_data);


// window Widok
void on_drawingAreaWidok_realize (GtkWidget *widget, gpointer user_data);

gboolean on_drawingAreaWidok_expose_event (GtkWidget *widget,
	GdkEventExpose *event, gpointer user_data);

void on_btnWidokZamknij_clicked (GtkButton *button, gpointer user_data);


// dialog Statystyka
void on_dialogStatystyka_realize (GtkWidget *widget, gpointer user_data);


// dialog Konfiguracja
void on_dialogKonfiguracja_realize (GtkWidget *widget, gpointer user_data);

void on_dialogKonfiguracja_response (GtkDialog *dialog, gint response_id, gpointer user_data);


// dialog Dodaj Punkt
void on_dialogDodajPunkt_response (GtkDialog *dialog, gint response_id, gpointer user_data);


// dialog Usun Punkty
void on_dialogUsunPunkty_realize (GtkWidget *widget, gpointer user_data);

void on_entryDialogUsunPunktyFiltr_changed (GtkEditable *editable, gpointer user_data);

void on_tbtnDialogUsunPunktyZaznacz_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnDialogUsunPunktyOdznacz_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_dialogUsunPunkty_response (GtkDialog *dialog, gint response_id, gpointer user_data);


// dialog Projekt Wybor
void on_dialogProjektWybor_realize (GtkWidget *widget, gpointer user_data);

void on_treeDialogProjektWybor_cursor_changed (GtkTreeView *treeview, gpointer user_data);

void on_btnDialogProjektWyborNowy_clicked (GtkButton *button, gpointer user_data);

void on_btnDialogProjektWyborUsun_clicked (GtkButton *button, gpointer user_data);

void on_dialogProjektWybor_response (GtkDialog *dialog, gint response_id, gpointer user_data);


// dialog Projakt Edytuj
void on_dialogProjektEdytuj_response (GtkDialog *dialog, gint response_id, gpointer user_data);


// win ImportTxt
void on_btnImportTxtUp_clicked (GtkButton *button, gpointer user_data);

void on_btnImportTxtDown_clicked (GtkButton *button, gpointer user_data);

void on_dialogImportTxt_realize (GtkWidget *widget, gpointer user_data);

void cell_toggled_callback (GtkCellRendererToggle *cell, gchar *path_string, gpointer treeview);

void on_dialogImportTxt_response (GtkDialog *dialog, gint response_id, gpointer user_data);


// win Eksport
void on_winEksportTxt_realize (GtkWidget *widget, gpointer user_data);

void on_winEksportTxt_destroy (GtkObject *object, gpointer user_data);

void on_btnEksportTxtUp_clicked (GtkButton *button, gpointer user_data);

void on_btnEksportTxtDown_clicked (GtkButton *button, gpointer user_data);

void on_btnEksportTxtCancel_clicked (GtkButton *button, gpointer user_data);

void on_btnEksportTxtOk_clicked (GtkButton *button, gpointer user_data);


// dialog Raport
void on_dialogRaport_realize (GtkWidget *widget, gpointer user_data);

void on_mnuDialogRaportZapisz_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_mnuDialogRaportZakoncz_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_btnDialogRaportOk_clicked (GtkButton *button, gpointer user_data);


// winAbout
void on_winAbout_response (GtkDialog *dialog, gint response_id, gpointer user_data);


// winHostoria
void on_winHistoria_realize (GtkWidget *widget, gpointer user_data);

void on_winHistoria_destroy (GtkObject *object, gpointer user_data);

void on_tbtnHistoriaPonow_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnHistoriaUsun_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnHistoriaEdytuj_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnHistoriaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnHistoriaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);


// winObserwacje
void on_winObserwacje_realize (GtkWidget *widget, gpointer user_data);

void on_winObserwacje_destroy (GtkObject *object, gpointer user_data);

void on_tbtnObserwacjeWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnObserwacjeUsun_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnObserwacjeImport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnObserwacjeEksport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnObserwacjeZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnObserwacjeOdswiez_clicked (GtkToolButton *toolbutton, gpointer user_data);


// winTachimetria
void on_winTachimetria_destroy (GtkObject *object, gpointer user_data);

void on_winTachimetria_realize (GtkWidget *widget, gpointer user_data);

void on_tbtnTachimetriaOblicz_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnTachimetriaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnTachimetriaUsun_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnTachimetriaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnTachimetriaWidok_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnTachimetriaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_toggleTachimetriaDaneDsV_toggled (GtkToggleToolButton *toggletoolbutton, gpointer user_data);

void on_toggleTachimetriaDaneDpV_toggled (GtkToggleToolButton *toggletoolbutton, gpointer user_data);

void on_toggleTachimetriaDaneDpH_toggled (GtkToggleToolButton *toggletoolbutton, gpointer user_data);

gboolean on_entryTachimetriaNawiazanieStNr_focus_in_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);

gboolean on_entryTachimetriaNawiazanieStNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);

gboolean on_entryTachimetriaNawiazaniePktN1Nr_focus_in_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);

gboolean on_entryTachimetriaNawiazaniePktN1Nr_focus_out_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);

gboolean on_entryTachimetriaNawiazaniePktN2Nr_focus_in_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);

gboolean on_entryTachimetriaNawiazaniePktN2Nr_focus_out_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);

gboolean on_entryTachimetriaPktNr_focus_out_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);

void on_btnTachimetriaPktDodaj_clicked (GtkButton *button, gpointer user_data);

gboolean on_entryTachimetriaPktKod_focus_in_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);

gboolean on_entryTachimetriaPktKod_focus_out_event (GtkWidget *widget, GdkEventFocus *event,gpointer user_data);


// winHistoriaEdytuj
void on_dialogHistoriaEdytuj_response (GtkDialog *dialog, gint response_id, gpointer user_data);

void on_dialogHistoriaEdytuj_realize (GtkWidget *widget, gpointer user_data);

void dialogHistoriaEdytuj_cell_edited_callback (GtkCellRendererText *cell,
	gchar *path_string, gchar *new_text, gpointer user_data);

// winPoligon
void on_mnuMainPoligon_activate (GtkMenuItem *menuitem, gpointer user_data);

void on_winPoligon_destroy (GtkObject *object, gpointer user_data);

void on_winPoligon_realize (GtkWidget *widget, gpointer user_data);

void on_tbtnPoligonOblicz_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPoligonWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPoligonUsun_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPoligonRaport_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPoligonWidok_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_tbtnPoligonZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data);

void on_framePoligonNawiazanie_set_focus_child (GtkContainer *container,
	GtkWidget *widget, gpointer user_data);

gboolean on_entryPoligonNawiazanieP1Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data);

gboolean on_entryPoligonNawiazanieP1Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data);

gboolean on_entryPoligonNawiazanieP2Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data);

gboolean on_entryPoligonNawiazanieP2Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data);

gboolean on_entryPoligonNawiazanieK1Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data);

gboolean on_entryPoligonNawiazanieK1Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data);

gboolean on_entryPoligonNawiazanieK2Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data);

gboolean on_entryPoligonNawiazanieK2Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data);

gboolean on_entryPoligonMiaryKod_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data);

gboolean on_entryPoligonMiaryKod_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data);

void on_btnPoligonMiaryDodaj_clicked (GtkButton *button, gpointer user_data);

gboolean on_entryPoligonMiaryNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data);

void
on_mnuMainTyczenieOkrag_activate       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_winOkragTyczenie_realize            (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_tbtnOkragTyczenieOblicz_clicked     (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_tbtnOkragTyczenieWyczysc_clicked    (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_tbtnOkragTyczenieUsun_clicked       (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_tbtnOkragTyczenieRaport_clicked     (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_tbtnOkragTyczenieWidok_clicked      (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_tbtnOkragTyczenieZamknij_clicked    (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_frameOkragTyczenieOkrag_set_focus_child
                                        (GtkContainer    *container,
                                        GtkWidget       *widget,
                                        gpointer         user_data);

gboolean
on_entryOkragTyczenieOkragONr_focus_in_event
                                        (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

gboolean
on_entryOkragTyczenieOkragONr_focus_out_event
                                        (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

gboolean
on_entryOkragTyczenieOkragPNr_focus_in_event
                                        (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

gboolean
on_entryOkragTyczenieOkragPNr_focus_out_event
                                        (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_btnOkragTyczeniePunktDodaj_clicked  (GtkButton       *button,
                                        gpointer         user_data);

void
on_winOkragTyczenie_destroy            (GtkObject       *object,
                                        gpointer         user_data);

gboolean
on_entryOkragTyczeniePunktKod_focus_in_event
                                        (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

gboolean
on_entryOkragTyczeniePunktKod_focus_out_event
                                        (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_btnOkragTyczeniePunktWszystkie_clicked
                                        (GtkButton       *button,
                                        gpointer         user_data);

void
on_entryZMarekaP1Nr_editing_done       (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryZMarekaP1Nr_changed            (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_entryZMarekaL1Nr_activate           (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryZMarekaP2Nr_editing_done       (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryZMarekaL2Nr_editing_done       (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryWcieciaKatowePktLNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryWcieciaKatowePktPNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryWcieciaKatowePktL1Nr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryWcieciaKatowePktP1Nr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryWcieciaLiniowePktLNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryWcieciaLiniowePktPNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryWcieciaWsteczPktLNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryWcieciaWsteczPktCNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryWcieciaWsteczPktPNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryWcieciaAWPrzodPktLNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryWcieciaAWPrzodPktPNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryWcieciaAWsteczPktLNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryWcieciaAWsteczPktPNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryTHelmertaDostPNr_editing_done  (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryTHelmertaDostWNr_editing_done  (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryTHelmertaTransfNr_editing_done (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryAzymutPPNr_editing_done        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryAzymutPKNr_editing_done        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryKatCNr_editing_done            (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryKatLNr_editing_done            (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryKatPNr_editing_done            (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryDomiaryBazaPPNr_editing_done   (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryDomiaryBazaPKNr_editing_done   (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryRzutProstaBazaPPNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryRzutProstaBazaPKNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryRzutProstaPunktNr_editing_done (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryPowierzchniaPunktNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryBiegunoweNawiazanieStNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryBiegunoweNawiazaniePktNNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryBiegunowePktTyczonyNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryPrzecieciaProstePr1Nr1_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryPrzecieciaProstePr1Nr2_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryPrzecieciaProstePr2Nr1_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryPrzecieciaProstePr2Nr2_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryPrzecieciaOkragProstaOkrNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryPrzecieciaOkragProstaPrNr1_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryPrzecieciaOkragProstaPrNr2_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryPrzecieciaOkregiOkr1Nr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryPrzecieciaOkregiOkr2Nr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragWpasowanie2ProstePLNr1_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragWpasowanie2ProstePLNr2_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragWpasowanie2ProstePPNr1_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragWpasowanie2ProstePPNr2_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragWpasowanie3PktPNr1_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragWpasowanie3PktPNr2_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragWpasowanie3PktPNr3_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragStycznaPunktOkrNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragStycznaPunktPktNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragStycznaProstaOkrNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragStycznaProstaPrNr1_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragStycznaProstaPrNr2_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragStyczna2OkregiOkr1Nr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragStyczna2OkregiOkr2Nr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryTachimetriaNawiazanieStNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryTachimetriaNawiazaniePktN1Nr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryTachimetriaNawiazaniePktN2Nr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryPoligonNawiazanieP1Nr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryPoligonNawiazanieP2Nr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryPoligonNawiazanieK1Nr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryPoligonNawiazanieK2Nr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragTyczenieOkragONr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_entryOkragTyczenieOkragPNr_editing_done
                                        (GtkCellEditable *celleditable,
                                        gpointer         user_data);

gboolean
on_winMain_delete_event                (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

gboolean
on_winPowierzchnia_delete_event        (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_tbtnPowierzchniaOdswiez_clicked     (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

gboolean
on_winAzymut_delete_event              (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_tbtnAzymutOdswiez_clicked           (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

gboolean
on_winKat_delete_event                 (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_tbtnKatOdswiez_clicked              (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

gboolean
on_winPoligon_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_treePowierzchniaPunkty_row_activated
                                        (GtkTreeView     *treeview,
                                        GtkTreePath     *path,
                                        GtkTreeViewColumn *column,
                                        gpointer         user_data);

void cellPoligon_edited_callback (GtkCellRendererText *cell, gchar *path_string,
	gchar *new_text, gpointer user_data);

void
on_treePoligon_row_activated           (GtkTreeView     *treeview,
                                        GtkTreePath     *path,
                                        GtkTreeViewColumn *column,
                                        gpointer         user_data);

void
on_btnPowierzchniaPunktDodajZakres_clicked
                                        (GtkButton       *button,
                                        gpointer         user_data);


gboolean
event_get_pkt_dpz                      (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);
