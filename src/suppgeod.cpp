/***************************************************************************
 *            suppgeod.cpp
 *
 *  Tue Jul  5 04:43:59 2005
 *  Copyright  2005  Bogusław Ciastek
 *  ciacho@z.pl
 ****************************************************************************/

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#ifdef G_OS_WIN32
	#include <windows.h>
	#include <shellapi.h>
	#include "winconst.h"
#endif

#include "stdgeod.h"
#include "suppgeod.h"
#include "support.h"
#include "dbgeod.h"
#include "interface.h"

void geod_update_entry_xy (GtkWidget* entryNr, const char* nameEntryX,
	const char* nameEntryY, const char* nameNext)
{
	cPunkt pkt_find;
	GtkWidget *entryX = lookup_widget(GTK_WIDGET(entryNr), nameEntryX);
	GtkWidget *entryY = lookup_widget(GTK_WIDGET(entryNr), nameEntryY);
	GtkWidget *entryNext;
	GtkWidget *dialog, *eventEntry;
	char *pkt_nr = NULL;
	char *pkt_x = NULL;
	char *pkt_y = NULL;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	char konfig_auto_addpkt[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	dbgeod_config_get("auto_addpkt", konfig_auto_addpkt);
	pkt_nr = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryNr)));
	
	if (!g_str_equal(pkt_nr, "")) {
		if (!dbgeod_pointt_find(pkt_nr, &pkt_find)) {
			pkt_x = g_strdup_printf("%.*lf", dok_xy_int, pkt_find.getX());
			pkt_y = g_strdup_printf("%.*lf", dok_xy_int, pkt_find.getY());
			
			gtk_entry_set_text(GTK_ENTRY(entryX), pkt_x);
			gtk_entry_set_text(GTK_ENTRY(entryY), pkt_y);
			
			g_free(pkt_x);
			g_free(pkt_y);
			if (nameNext != NULL) {
				entryNext = lookup_widget(GTK_WIDGET(entryNr), nameNext);
				gtk_widget_grab_focus(GTK_WIDGET(entryX)); // poprawic?? - wymusza focus child event w ramce
				gtk_widget_grab_focus(GTK_WIDGET(entryNext));
			}
		}
		else if (g_str_equal(konfig_auto_addpkt, "1")) {
			// wymagane ze wzgledu na blad w GTK
			GtkEntry *entry = GTK_ENTRY(entryNr);
			if (entry->blink_timeout) {
				gtk_timeout_remove (entry->blink_timeout);
				entry->blink_timeout = 0;
			}
			entry->cursor_visible = TRUE;
		
			dialog = create_dialogDodajPunkt();
			g_object_set_data_full (G_OBJECT (dialog), "eventEntry",
				g_object_ref (entryNr), (GDestroyNotify) g_object_unref);
			eventEntry = lookup_widget(GTK_WIDGET(dialog), "entryDialogDodajPunktNr");
			gtk_entry_set_text(GTK_ENTRY(eventEntry), g_strdup(gtk_entry_get_text(GTK_ENTRY(entryNr))));
			gtk_dialog_run(GTK_DIALOG(dialog));
		};
	}
	g_free(pkt_nr);
}


void geod_update_entry_xyh (GtkWidget* entryNr, const char* nameEntryX,
	const char* nameEntryY, const char* nameEntryH, const char* nameNext)
{
	cPunkt pkt_find;
	GtkWidget *entryX = lookup_widget(GTK_WIDGET(entryNr), nameEntryX);
	GtkWidget *entryY = lookup_widget(GTK_WIDGET(entryNr), nameEntryY);
	GtkWidget *entryH = lookup_widget(GTK_WIDGET(entryNr), nameEntryH);
	GtkWidget *entryNext;
	GtkWidget *dialog, *eventEntry;
	char *pkt_nr = NULL;
	char *pkt_x = NULL;
	char *pkt_y = NULL;
	char *pkt_h = NULL;
	char dok_xy[CONFIG_VALUE_MAX_LEN], dok_h[CONFIG_VALUE_MAX_LEN];
	char konfig_auto_addpkt[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3, dok_h_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	if (!dbgeod_config_get("dok_h", dok_h)) dok_h_int = atoi(dok_h);
	dbgeod_config_get("auto_addpkt", konfig_auto_addpkt);	
	pkt_nr = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryNr)));
	
	if (!g_str_equal(pkt_nr, ""))
		if (!dbgeod_pointt_find(pkt_nr, &pkt_find)) {
			pkt_x = g_strdup_printf("%.*lf", dok_xy_int, pkt_find.getX());
			pkt_y = g_strdup_printf("%.*lf", dok_xy_int, pkt_find.getY());
			pkt_h = g_strdup_printf("%.*lf", dok_h_int, pkt_find.getH());
			
			gtk_entry_set_text(GTK_ENTRY(entryX), pkt_x);
			gtk_entry_set_text(GTK_ENTRY(entryY), pkt_y);
			gtk_entry_set_text(GTK_ENTRY(entryH), pkt_h);
			
			g_free(pkt_x);
			g_free(pkt_y);
			g_free(pkt_h);
			if (nameNext != NULL) {
				entryNext = lookup_widget(GTK_WIDGET(entryNr), nameNext);
				gtk_widget_grab_focus(GTK_WIDGET(entryX)); // poprawic?? - wymusza focus child event w ramce
				gtk_widget_grab_focus(GTK_WIDGET(entryNext));
			}
		}
		else if (g_str_equal(konfig_auto_addpkt, "1")) {
			// wymagane ze wzgledu na blad w GTK
			GtkEntry *entry = GTK_ENTRY(entryNr);
			if (entry->blink_timeout) {
				gtk_timeout_remove (entry->blink_timeout);
				entry->blink_timeout = 0;
			}
			entry->cursor_visible = TRUE;
		
			dialog = create_dialogDodajPunkt();
			g_object_set_data_full (G_OBJECT (dialog), "eventEntry",
				g_object_ref (entryNr), (GDestroyNotify) g_object_unref);
			eventEntry = lookup_widget(GTK_WIDGET(dialog), "entryDialogDodajPunktNr");
			gtk_entry_set_text(GTK_ENTRY(eventEntry), g_strdup(gtk_entry_get_text(GTK_ENTRY(entryNr))));
			gtk_dialog_run(GTK_DIALOG(dialog));
		};
	g_free(pkt_nr);
}

int geod_update_entry_obs (GtkWidget* entryObs, const char* nameEntryL,
	const char* nameEntryC,	const char* nameEntryP, const char* nameNext)
{
	GtkWidget *entryC = NULL;
	GtkWidget *entryP = NULL;
	GtkWidget *entryL = NULL;
	GtkWidget *entryNext = NULL;
	char *pkt_l = NULL;
	char *pkt_c = NULL;
	char *pkt_p = NULL;
	double odczyt = 0.0;
	char *txt_odczyt;
	int zmiana = 0;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	
	if (g_str_has_prefix(nameEntryL, "$"))
		pkt_l = g_strdup(nameEntryL);
	else {
		entryL = lookup_widget(GTK_WIDGET(entryObs), nameEntryL);
		pkt_l = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryL)));
	}
	if (g_str_has_prefix(nameEntryC, "$"))
		pkt_c = g_strdup(nameEntryC);
	else {
		entryC = lookup_widget(GTK_WIDGET(entryObs), nameEntryC);
		pkt_c = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryC)));
	}
	if (g_str_has_prefix(nameEntryP, "$"))
		pkt_p = g_strdup(nameEntryP);
	else {
		entryP = lookup_widget(GTK_WIDGET(entryObs), nameEntryP);
		pkt_p = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryP)));
	}
	
	if (!g_str_equal(pkt_l, "") && !g_str_equal(pkt_c, "") && !g_str_equal(pkt_p, ""))
		if (!dbgeod_obs_get(pkt_l, pkt_c, pkt_p, odczyt)) {
			if (dbgeod_obs_is_angle(pkt_l))
				txt_odczyt = geod_kat_from_rad(odczyt, FALSE);
			else
				txt_odczyt = g_strdup_printf("%.*lf", dok_xy_int, odczyt);
			
			gtk_entry_set_text(GTK_ENTRY(entryObs), txt_odczyt);
			g_free(txt_odczyt);
			zmiana = 1;
			if (nameNext != NULL) {
				entryNext = lookup_widget(GTK_WIDGET(entryObs), nameNext);
				gtk_widget_grab_focus(GTK_WIDGET(entryNext));
			}
		};
	g_free(pkt_l);
	g_free(pkt_c);
	g_free(pkt_p);
	return zmiana;
}


void geod_sbarF1Punkt_set (GtkWidget *widget, const char* sbarName)
{
	GtkWidget *statusbar = lookup_widget(widget, sbarName);
	gtk_statusbar_push(GTK_STATUSBAR(statusbar), 10, _("F1 = lista punktów; Insert = dodaj punkt do bazy"));
	GdkColor color;
	gdk_color_parse ("Khaki", &color);
	gtk_widget_modify_bg (statusbar->parent, GTK_STATE_NORMAL, &color);
}

void geod_sbarF1Kod_set (GtkWidget *widget, const char* sbarName)
{
	GtkWidget *statusbar = lookup_widget(widget, sbarName);
	gtk_statusbar_push(GTK_STATUSBAR(statusbar), 10, _("Naciśnij F1 aby uzyskać listę kodów"));
	GdkColor color;
	gdk_color_parse ("Khaki", &color);
	gtk_widget_modify_bg (statusbar->parent, GTK_STATE_NORMAL, &color);
}

void geod_sbarF1_clear (GtkWidget *widget, const char* sbarName)
{
	GtkWidget *statusbar = lookup_widget(widget, sbarName);
	if (GTK_IS_STATUSBAR(statusbar)) {
		gtk_statusbar_pop(GTK_STATUSBAR(statusbar), 10);
		gtk_widget_modify_bg (statusbar->parent, GTK_STATE_NORMAL, NULL);
	}
}

void geod_sbar_set (GtkWidget *widget, const char* sbar_name, const char* sbar_msg)
{
	guint source_id;
	GtkWidget *sbar = lookup_widget(GTK_WIDGET(widget), sbar_name);

	if ( g_object_get_data(G_OBJECT(sbar), "source_id") != NULL )
		geod_sbar_clear(sbar);

	gtk_statusbar_push(GTK_STATUSBAR(sbar), 20, sbar_msg);
	source_id = g_timeout_add(5000, geod_sbar_clear, sbar);
	g_object_set_data(G_OBJECT(sbar), "source_id", GINT_TO_POINTER(source_id));

	GdkColor color;
	gdk_color_parse ("Red", &color);
	gtk_widget_modify_bg (sbar->parent, GTK_STATE_NORMAL, &color);
}

void geod_sbar_set_error (GtkWidget *widget, const char* sbar_name, int geod_err)
{
	char *sbar_msg;
	switch (geod_err) {
		case GEODERR_NO_RESULT:
			sbar_msg = g_strdup(_("Brak rozwiązania"));
			break;
		case GEODERR_NOT_ONE_RESULT:
			sbar_msg = g_strdup(_("Brak jednoznacznego rozwiązania"));
			break;
		case GEODERR_DATA:
			sbar_msg = g_strdup(_("Niepoprawne dane"));
			break;
		case GEODERR_MATH:
			sbar_msg = g_strdup(_("Bład podczas obliczeń"));
			break;
		case GEODERR_ALGORITHM:
			sbar_msg = g_strdup(_("Nie można znaleźć rozwiązania"));
			break;
		case GEODERR_OTHER:
			sbar_msg = g_strdup(_("Nie obliczono - błąd"));
			break;
		case GEODERR_WINDOW_OPEN:
			sbar_msg = g_strdup(_("Okno zostało już otwarte"));
			break;
		default:
			sbar_msg = g_strdup(_("Nie obliczono - błąd"));
	}
	geod_sbar_set (widget, sbar_name, sbar_msg);

	GdkColor color;
	gdk_color_parse ("Red", &color);
	GtkWidget *sbar = lookup_widget(GTK_WIDGET(widget), sbar_name);
	gtk_widget_modify_bg (sbar->parent, GTK_STATE_NORMAL, &color);
	g_free(sbar_msg);
}

gboolean geod_sbar_clear (gpointer sbar)
{
	if (GTK_IS_STATUSBAR(sbar)) {
		gtk_statusbar_pop(GTK_STATUSBAR(sbar), 20);
		g_source_remove(GPOINTER_TO_INT(g_object_get_data(G_OBJECT(sbar), "source_id")));
		g_object_set_data(G_OBJECT(sbar), "source_id", NULL);
		gtk_widget_modify_bg (GTK_WIDGET(sbar)->parent, GTK_STATE_NORMAL, NULL);
	}
	return FALSE;
}


void geod_entry_clear(GtkWidget *widget, const char* entryName)
{
	GtkWidget *entry = lookup_widget(GTK_WIDGET(widget), entryName);
	gtk_editable_delete_text(GTK_EDITABLE(entry), 0, -1);
	gtk_widget_modify_base (entry, GTK_STATE_NORMAL, NULL);
}

void geod_entry_inc(GtkWidget *widget, const char* entryName)
{
	char konfig_auto_num[CONFIG_VALUE_MAX_LEN];
	if (dbgeod_config_get("auto_num", konfig_auto_num) != 0)
		g_strlcpy(konfig_auto_num, "+",CONFIG_VALUE_MAX_LEN);

	GtkWidget *entry = lookup_widget(GTK_WIDGET(widget), entryName);
	const char *entry_txt = gtk_entry_get_text(GTK_ENTRY(entry));

	if (g_str_equal(konfig_auto_num, "+")) {
		char *tekst = geod_inc(entry_txt);
		gtk_entry_set_text(GTK_ENTRY(entry), tekst);
		g_free(tekst);
	}
	else if (g_str_equal(konfig_auto_num, "-")) {
		char *tekst = geod_dec(entry_txt);
		gtk_entry_set_text(GTK_ENTRY(entry), tekst);
		g_free(tekst);
	}
	else
		gtk_editable_delete_text(GTK_EDITABLE(entry), 0, -1);
}

int geod_nr_cmp (const char* nr1, const char* nr2)
{
	char *_nr1 = g_strdup(nr1);
	char *_nr2 = g_strdup(nr2);
		
	for (guint i=0; i <= strlen(_nr1); i++)
		if (g_ascii_islower(_nr1[i]))
			_nr1[i] = g_ascii_toupper(_nr1[i]);
		else if (g_ascii_isupper(_nr1[i]))
			_nr1[i] = g_ascii_tolower(_nr1[i]);

	for (guint i=0; i <= strlen(_nr2); i++)
		if (g_ascii_islower(_nr2[i]))
			_nr2[i] = g_ascii_toupper(_nr2[i]);
		else if (g_ascii_isupper(_nr2[i]))
			_nr2[i] = g_ascii_tolower(_nr2[i]);
	
	int retv = strcmp(_nr1, _nr2);
	g_free(_nr1);
	g_free(_nr2);
	return retv;
}

int geod_nr_type(const char* _txt)
{
	int txt_len = strlen(_txt);
	enum ainc {AINC_DIGIT, AINC_UALPHA, AINC_LALPHA, AINC_ALPHA, AINC_ALL, N_AINC} typ;
	int n_aintc[4] = {0,0,0,0};

	// liczymy znaki
	for (int i=0; i < txt_len; i++) {
		if (g_ascii_isdigit(_txt[i]))
			n_aintc[AINC_DIGIT]++;
		else if (g_ascii_isupper(_txt[i]))
			n_aintc[AINC_UALPHA]++;
		else if (g_ascii_islower(_txt[i]))
			n_aintc[AINC_LALPHA]++;
		else
			n_aintc[3]++;
	}

	// okreslamy typ
	if (n_aintc[AINC_DIGIT]>0 && n_aintc[AINC_LALPHA]==0 && n_aintc[AINC_UALPHA]==0 && n_aintc[AINC_ALPHA]==0)
		typ = AINC_DIGIT;
	else if (n_aintc[AINC_DIGIT]==0 && n_aintc[AINC_LALPHA]>0 && n_aintc[AINC_UALPHA]==0 && n_aintc[AINC_ALPHA]==0)
		typ = AINC_LALPHA;
	else if (n_aintc[AINC_DIGIT]==0 && n_aintc[AINC_LALPHA]==0 && n_aintc[AINC_UALPHA]>0 && n_aintc[AINC_ALPHA]==0)
		typ = AINC_UALPHA;
	else if (n_aintc[AINC_DIGIT]==0 && n_aintc[AINC_LALPHA]>0 && n_aintc[AINC_UALPHA]>0 && n_aintc[AINC_ALPHA]==0)
		typ = AINC_ALPHA;
	else
		typ = AINC_ALL;
	
	return typ;
}

char *geod_dec(const char* _txt, int typ)
{
	enum ainc {AINC_DIGIT, AINC_UALPHA, AINC_LALPHA, AINC_ALPHA, AINC_ALL, N_AINC};
	char *ntxt = g_strdup(_txt);
	char *rtxt = NULL;
	int txt_len = strlen(ntxt);
	bool odejmij = TRUE;
	int i = txt_len;
	if (typ < 0) typ = geod_nr_type(ntxt);
	
	while (i>0 && odejmij) {
		--i;
		switch(typ) {
			case AINC_DIGIT:
				if (ntxt[i]=='0')
					ntxt[i]='9';
				else {
					ntxt[i]--;
					odejmij = FALSE;
				}			
				break;
			case AINC_LALPHA:
				if (ntxt[i]=='a')
					ntxt[i]='z';
				else {
					ntxt[i]--;
					odejmij = FALSE;
				}			
				break;
			case AINC_UALPHA:
				if (ntxt[i]=='A')
					ntxt[i]='Z';
				else {
					ntxt[i]--;
					odejmij = FALSE;
				}
				break;
			case AINC_ALPHA:
				if (ntxt[i]=='a')
					ntxt[i]='Z';
				else if (ntxt[i]=='A') {
					ntxt[i]='z';
					odejmij = FALSE;
				}
				else {
					ntxt[i]--;
					odejmij = FALSE;
				}
				break;
			default:
				if (g_ascii_isalnum(ntxt[i])==FALSE)
					--i;
				else if (ntxt[i]=='0')
					ntxt[i]='Z';
				else if (ntxt[i]=='a') {
					ntxt[i]='9';
					odejmij = FALSE;
				}
				else if (ntxt[i]=='A') {
					ntxt[i]='z';
					odejmij = FALSE;
				}
				else {
					ntxt[i]--;
					odejmij = FALSE;
				}
				break;
		}
	}

	if (odejmij) {
		switch(typ) {
			case AINC_LALPHA:
			case AINC_ALPHA:
			case AINC_UALPHA:
				rtxt = g_strdup(ntxt+1);
				break;
			default:
				rtxt = g_strdup(""); /// puste ???
		}
	}
	else if (g_str_has_prefix(ntxt,"0"))
		rtxt = g_strdup(ntxt+1);
	else
		rtxt = g_strdup(ntxt);

	g_free(ntxt);
	return rtxt;
}


char *geod_inc(const char* _txt, int typ)
{
	enum ainc {AINC_DIGIT, AINC_UALPHA, AINC_LALPHA, AINC_ALPHA, AINC_ALL, N_AINC};
	char *ntxt = g_strdup(_txt);
	char *rtxt = NULL;
	int txt_len = strlen(ntxt);
	bool dodaj = TRUE;
	int i = txt_len;
	if (typ < 0) typ = geod_nr_type(ntxt);

	while (i>0 && dodaj) {
		--i;
		switch(typ) {
			case AINC_DIGIT:
				if (ntxt[i]=='9')
					ntxt[i]='0';
				else {
					ntxt[i]++;
					dodaj = FALSE;
				}			
				break;
			case AINC_LALPHA:
				if (ntxt[i]=='z')
					ntxt[i]='a';
				else {
					ntxt[i]++;
					dodaj = FALSE;
				}			
				break;
			case AINC_UALPHA:
				if (ntxt[i]=='Z')
					ntxt[i]='A';
				else {
					ntxt[i]++;
					dodaj = FALSE;
				}
				break;
			case AINC_ALPHA:
				if (ntxt[i]=='Z')
					ntxt[i]='a';
				else if (ntxt[i]=='z') {
					ntxt[i]='A';
					dodaj = FALSE;
				}
				else {
					ntxt[i]++;
					dodaj = FALSE;
				}
				break;
			default:
				if (g_ascii_isalnum(ntxt[i])==FALSE)
					--i;
				else if (ntxt[i]=='Z')
					ntxt[i]='0';
				else if (ntxt[i]=='z') {
					ntxt[i]='A';
					dodaj = FALSE;
				}
				else if (ntxt[i]=='9') {
					ntxt[i]='a';
					dodaj = FALSE;
				}
				else {
					ntxt[i]++;
					dodaj = FALSE;
				}
				break;
		}
	}

	if (dodaj) {
		switch(typ) {
			case AINC_DIGIT:
			case AINC_ALL:
				rtxt = g_strdup_printf("1%s", ntxt);
				break;
			case AINC_LALPHA:
			case AINC_ALPHA:
				rtxt = g_strdup_printf("a%s", ntxt);
				break;
			case AINC_UALPHA:
				rtxt = g_strdup_printf("A%s", ntxt);
				break;
			case N_AINC:
				break;
		}
	}
	else
		rtxt = g_strdup(ntxt);

	g_free(ntxt);
	return rtxt;
}


int geod_entry_get_pkt_xy(GtkWidget *widget, const char* nameNr,
	const char* nameX, const char* nameY, const char* nameKod, cPunkt & punkt)
{
	int err_code = 0;
	GtkWidget *entryNr;
	GtkWidget *entryX;
	GtkWidget *entryY;
	GtkWidget *entryKod;

	entryNr = lookup_widget(GTK_WIDGET(widget), nameNr);
	entryX = lookup_widget(GTK_WIDGET(widget), nameX);
	entryY = lookup_widget(GTK_WIDGET(widget), nameY);
	const char *nr = gtk_entry_get_text(GTK_ENTRY(entryNr));
	const char *x = gtk_entry_get_text(GTK_ENTRY(entryX));
	const char *y = gtk_entry_get_text(GTK_ENTRY(entryY));

	if (g_str_equal(nr, "")) {
		err_code = -1;
		gtk_widget_grab_focus(GTK_WIDGET(entryNr));
	}
	
	if (g_str_equal(x, "")) {
		if (err_code != -1) gtk_widget_grab_focus(GTK_WIDGET(entryX));
		return 1;
	}

	if (g_str_equal(y, "")) {
		if (err_code != -1) gtk_widget_grab_focus(GTK_WIDGET(entryY));
		return 2;
	}

	punkt.setNr(nr);
	punkt.setXY(g_strtod(x, NULL), g_strtod(y, NULL));
	punkt.setH(0.0);
	
	if (nameKod != NULL) {
		entryKod = lookup_widget(GTK_WIDGET(widget), nameKod);
		const char *kod = gtk_entry_get_text(GTK_ENTRY(entryKod));
		punkt.setKod(kod);
	}
	return err_code;
}


int geod_entry_get_pkt_xyh(GtkWidget *widget, const char* nameNr,
	const char* nameX, const char* nameY, const char* nameH, const char* nameKod,
	cPunkt & punkt)
{
	int err_code = 0;
	GtkWidget *entryH;
	entryH = lookup_widget(GTK_WIDGET(widget), nameH);
	const char *h = gtk_entry_get_text(GTK_ENTRY(entryH));

	err_code = geod_entry_get_pkt_xy(widget, nameNr, nameX, nameY, nameKod, punkt);

	if (g_str_equal(h, "")) {
		err_code = -1;
		gtk_widget_grab_focus(GTK_WIDGET(entryH));
	}

	punkt.setH(g_strtod(h, NULL));
	return err_code;
}


int geod_get_kat(const char* txtkat, double & kat)
{
	enum jednostkiKat { RAD, GRAD, DEG, DEGDEC };
	jednostkiKat jedn = GRAD;
	unsigned int i;

	gchar **kat_split, **bufor;
	char *last_kat;
	char jedn_txt[CONFIG_VALUE_MAX_LEN];

	dbgeod_config_get("jedn_kat", jedn_txt);
	
	if (g_str_equal(jedn_txt, "rad"))
		jedn = RAD;
	else if (g_str_equal(jedn_txt, "grad"))
		jedn = GRAD;
	else if (g_str_equal(jedn_txt, "deg"))
		jedn = DEG;
	else if (g_str_equal(jedn_txt, "degdec"))
		jedn = DEGDEC;

	if (txtkat == NULL || g_str_equal(txtkat, ""))
		return 1;
	
	switch (jedn) {
		case DEG:
			bufor = g_strsplit_set(txtkat, ".,:; ", 4);
			double kat_split_int[4];
			for (i=0; i < 4; i++) kat_split_int[i] = 0;
			
			for (i = 0, kat_split = bufor; *kat_split != NULL && i < 4; kat_split++, i++) {
				if (i == 3) {
					last_kat = g_strdup_printf("0,%s", *kat_split);
					kat_split_int[i-1] += g_strtod(last_kat, NULL);
					g_free(last_kat);
				}
				else
					kat_split_int[i] = g_strtod(*kat_split, NULL);
			}
			kat = stToRad(kat_split_int[0], kat_split_int[1], kat_split_int[2]);
			kat = norm_kat(kat);
			g_strfreev(bufor);
			break;
		case DEGDEC:
			kat = g_strtod(txtkat, NULL)/RO_ST;
			kat = norm_kat(kat);
			break;
		case GRAD:
			kat = g_strtod(txtkat, NULL)/RO_GR;
			kat = norm_kat(kat);
			break;
		case RAD:
			kat = g_strtod(txtkat, NULL);
			kat = norm_kat(kat);
			break;
	}
	return 0;
}

int geod_entry_get_kat(GtkWidget *widget, const char* entryName, double & kat)
{
	GtkWidget *entry = lookup_widget(GTK_WIDGET(widget), entryName);
	const char *kat_entry = gtk_entry_get_text(GTK_ENTRY(entry));
	
	
	if (g_str_equal(kat_entry, "")) {
		gtk_widget_grab_focus(GTK_WIDGET(entry));
		return 1;
	}
	else
		return geod_get_kat(kat_entry, kat);
}

int geod_entry_get_double (GtkWidget *widget, const char* entryName, double &dvaue, bool FOCUS)
{
	GtkWidget *entry = lookup_widget(GTK_WIDGET(widget), entryName);
	const char *entry_value = gtk_entry_get_text(GTK_ENTRY(entry));
	if (g_str_equal(entry_value, "")) {
		if (FOCUS) gtk_widget_grab_focus(GTK_WIDGET(entry));
		dvaue = 0.0;
		return 1;
	}
	
	dvaue = g_strtod(entry_value, NULL);
	return 0;
}

void geod_entry_set_pkt_xy(GtkWidget *widget, const char* nameX,
	const char* nameY, cPunkt punkt)
{
	GtkWidget *entryX = lookup_widget(GTK_WIDGET(widget), nameX);
	GtkWidget *entryY = lookup_widget(GTK_WIDGET(widget), nameY);
	char *txtX = NULL, *txtY = NULL;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	txtX = g_strdup_printf("%.*lf", dok_xy_int, punkt.getX());
	gtk_entry_set_text(GTK_ENTRY(entryX), txtX);
	txtY = g_strdup_printf("%.*lf", dok_xy_int, punkt.getY());
	gtk_entry_set_text(GTK_ENTRY(entryY), txtY);
	
	g_free(txtX);
	g_free(txtY);
}


char* geod_kat_from_rad (double kat, bool jednostki)
{
	enum jednostkiKat { RAD, GRAD, DEG, DEGDEC };
	jednostkiKat jedn = GRAD;
	double stopnie, minuty, sekundy;
	char *txtKat = NULL;
	char jedn_txt[CONFIG_VALUE_MAX_LEN];
	char dok_kat[CONFIG_VALUE_MAX_LEN];
	int dok_kat_int = 5;
	
	if (!dbgeod_config_get("dok_kat", dok_kat)) dok_kat_int = atoi(dok_kat);
	dbgeod_config_get("jedn_kat", jedn_txt);
	
	if (g_str_equal(jedn_txt, "rad"))
		jedn = RAD;
	else if (g_str_equal(jedn_txt, "grad"))
		jedn = GRAD;
	else if (g_str_equal(jedn_txt, "deg"))
		jedn = DEG;
	else if (g_str_equal(jedn_txt, "degdec"))
		jedn = DEGDEC;
	
	switch (jedn) {
		case DEG:
			kat = kat*RO_ST;
			stopnie = trunc(kat);
			minuty = trunc((kat-stopnie)*60);
			sekundy = (kat-stopnie-minuty/60.0)*3600;
			if (fabs(sekundy-60) < 0.00001) { // z powodu bledow zaokraglen
				minuty++;
				sekundy = 0.0;
			}
			if (jednostki)
				txtKat = g_strdup_printf("%.0lfº %.0lf' %.1lf\"", stopnie, fabs(minuty), fabs(sekundy));
			else			
				txtKat = g_strdup_printf("%.0lf %.0lf %.1lf", stopnie, fabs(minuty), fabs(sekundy));
			break;
		case DEGDEC:
			stopnie = kat*RO_ST;
			if (jednostki)
				txtKat = g_strdup_printf("%.*lfº", dok_kat_int, stopnie);
			else
				txtKat = g_strdup_printf("%.*lf", dok_kat_int, stopnie);
			break;
		case GRAD:
			stopnie = kat*RO_GR;
			if (jednostki)
				txtKat = g_strdup_printf("%.*lf g", dok_kat_int, stopnie);
			else
				txtKat = g_strdup_printf("%.*lf", dok_kat_int, stopnie);
			break;
		case RAD:
			if (jednostki)	
				txtKat = g_strdup_printf("%.*lf rad", dok_kat_int+3, kat);
			else
				txtKat = g_strdup_printf("%.*lf", dok_kat_int+3, kat);
			break;
	}
	return txtKat;
}


double geod_view_transform (GtkWidget *darea, cPunkt pktDane[], GdkPoint *pktWidok,
	guint npoints)
{
	enum uklad {GEODEZYJNY, MATEMATYCZNY};
	guint i;
	const gint xmargin = 40;
	const gint ymargin = 10;
	double xmin=0, ymin=0, xmax=0, ymax=0;
	double skalax, skalay, skala;
	enum uklad konfig_uklad = GEODEZYJNY;
	char txt_uklad[CONFIG_VALUE_MAX_LEN];
	
	dbgeod_config_get("uklad", txt_uklad);
	if (g_str_equal(txt_uklad, "geodezyjny"))
		konfig_uklad = GEODEZYJNY;
	else if  (g_str_equal(txt_uklad, "matematyczny"))
		konfig_uklad = MATEMATYCZNY;
	
	switch (konfig_uklad) {
		case GEODEZYJNY:
			xmin = xmax = pktDane[0].getY();
			ymin = ymax = pktDane[0].getX();
			for (i=0; i < npoints; i++)
			{
				if (xmin > pktDane[i].getY()) xmin = pktDane[i].getY();
				if (ymin > pktDane[i].getX()) ymin = pktDane[i].getX();
				if (xmax < pktDane[i].getY()) xmax = pktDane[i].getY();
				if (ymax < pktDane[i].getX()) ymax = pktDane[i].getX();
			}
			break;
		case MATEMATYCZNY:
			xmin = xmax = pktDane[0].getX();
			ymin = ymax = pktDane[0].getY();
			for (i=0; i < npoints; i++)
			{
				if (xmin > pktDane[i].getX()) xmin = pktDane[i].getX();
				if (ymin > pktDane[i].getY()) ymin = pktDane[i].getY();
				if (xmax < pktDane[i].getX()) xmax = pktDane[i].getX();
				if (ymax < pktDane[i].getY()) ymax = pktDane[i].getY();
			}
			break;
	}
	skalax = (darea->allocation.width-2*xmargin)/(xmax-xmin);
	skalay = (darea->allocation.height-2*ymargin)/(ymax-ymin);
	if (skalax > skalay) skala = skalay;
	else skala = skalax;
	
	switch (konfig_uklad) {
		case GEODEZYJNY:
			for (i=0; i < npoints; i++)
			{
				pktWidok[i].x = (gint)round((pktDane[i].getY()-xmin)*skala + xmargin);
				pktWidok[i].y = (gint)round((ymax -pktDane[i].getX())*skala + ymargin);
			}
			break;
		case MATEMATYCZNY:
			for (i=0; i < npoints; i++)
			{
				pktWidok[i].x = (gint)round((pktDane[i].getX()-xmin)*skala + xmargin);
				pktWidok[i].y = (gint)round((ymax -pktDane[i].getY())*skala + ymargin);
			}
			break;
	}
	return skala;
}

void geod_draw_point (GtkWidget *darea, GdkPoint punkt, const char *pkt_id, int typ)
{
	GdkColormap *colormap = gtk_widget_get_colormap(darea);
	PangoLayout *pLayout = gtk_widget_create_pango_layout(GTK_WIDGET(darea), pkt_id);
	GtkWidget *pixmap = lookup_widget(darea, "pixmapWidok");
	GdkGC *gc = gdk_gc_new(darea->window);
	GdkColor czerwony, niebieski, czarny;
	int pLayoutWidth, pLayoutHeight;
	const gint promien = 4;

	gdk_color_parse ("red", &czerwony);
	gdk_color_parse ("blue", &niebieski);
	gdk_color_parse ("black", &czarny);
	gdk_colormap_alloc_color(colormap, &czerwony, FALSE, TRUE);
	gdk_colormap_alloc_color(colormap, &niebieski, FALSE, TRUE);
	gdk_colormap_alloc_color(colormap, &czarny, FALSE, TRUE);
	
	switch (typ) {
		case PKT_OSNOWA:
			gdk_gc_set_foreground(gc, &czerwony);
			break;
		case PKT_WYNIK:
			gdk_gc_set_foreground(gc, &niebieski);
			break;
		default:
			gdk_gc_set_foreground(gc, &czarny);
	}
	// rysujemy punkt
	gdk_draw_arc(GDK_PIXMAP(pixmap), darea->style->white_gc, TRUE, punkt.x-promien, punkt.y-promien, 2*promien, 2*promien, 0, 360*64);
	gdk_draw_arc(GDK_PIXMAP(pixmap), gc, FALSE, punkt.x-promien, punkt.y-promien, 2*promien, 2*promien, 0, 360*64);
	gdk_draw_point(GDK_PIXMAP(pixmap), gc, punkt.x, punkt.y);
	// rysujemy numer punktu
	pango_layout_get_pixel_size(pLayout, &pLayoutWidth, &pLayoutHeight);
	gdk_draw_rectangle (GDK_PIXMAP(pixmap), darea->style->white_gc, TRUE,
		punkt.x+2*promien, punkt.y+2*promien-pLayoutHeight, pLayoutWidth, pLayoutHeight);
	gdk_draw_layout(GDK_PIXMAP(pixmap), gc, punkt.x+2*promien, punkt.y-2*promien, pLayout);
}

void geod_draw_arc (GtkWidget *darea, GdkPoint pktS, GdkPoint pktP, GdkPoint pktL, int typ)
{
	GdkColormap *colormap = gtk_widget_get_colormap(darea);
	GtkWidget *pixmap = lookup_widget(darea, "pixmapWidok");
	GdkGC *gc = gdk_gc_new(darea->window);
	GdkColor czerwony, czarny;
	double kierP, kierL;
	const gint promien = 50;

	gdk_color_parse ("red", &czerwony);
	gdk_color_parse ("black", &czarny);
	gdk_colormap_alloc_color(colormap, &czerwony, FALSE, TRUE);
	gdk_colormap_alloc_color(colormap, &czarny, FALSE, TRUE);
	
	switch (typ) {
		case 1:
			gdk_gc_set_foreground(gc, &czerwony);
			gdk_gc_set_line_attributes(gc, 2, GDK_LINE_SOLID, GDK_CAP_NOT_LAST, GDK_JOIN_MITER);
			break;
		default:
			gdk_gc_set_line_attributes(gc, 1, GDK_LINE_SOLID, GDK_CAP_NOT_LAST, GDK_JOIN_MITER);
			gdk_gc_set_foreground(gc, &czarny);
	}
	azymut(pktS.y-pktL.y, pktL.x-pktS.x, kierL);
	azymut(pktS.y-pktP.y, pktP.x-pktS.x, kierP);
	
	gdk_draw_arc(GDK_PIXMAP(pixmap), gc, FALSE, pktS.x-promien, pktS.y-promien,
		2*promien, 2*promien, (guint)round(kierP*RO_ST*64), (guint)round(norm_kat(kierL-kierP)*RO_ST*64));
}


void geod_draw_line (GtkWidget *darea, GdkPoint pktP, GdkPoint pktK, int typ)
{
	GdkColormap *colormap = gtk_widget_get_colormap(darea);
	GtkWidget *pixmap = lookup_widget(darea, "pixmapWidok");
	GdkGC *gc = gdk_gc_new(darea->window);
	GdkColor czerwony, czarny;

	gdk_color_parse ("red", &czerwony);
	gdk_color_parse ("black", &czarny);
	gdk_colormap_alloc_color(colormap, &czerwony, FALSE, TRUE);
	gdk_colormap_alloc_color(colormap, &czarny, FALSE, TRUE);
	
	switch (typ) {
		case LINE_DLUGOSC: //dlugosc
			gdk_gc_set_foreground(gc, &czerwony);
			gdk_gc_set_line_attributes(gc, 2, GDK_LINE_SOLID, GDK_CAP_NOT_LAST, GDK_JOIN_MITER);
			gdk_draw_line(GDK_PIXMAP(pixmap), gc, pktP.x, pktP.y, pktK.x, pktK.y);
			break;
		case LINE_KIERUNEK: //kierunek
			gdk_gc_set_foreground(gc, &czarny);
			gdk_gc_set_line_attributes(gc, 1, GDK_LINE_SOLID, GDK_CAP_NOT_LAST, GDK_JOIN_MITER);
			gdk_draw_line(GDK_PIXMAP(pixmap), gc, pktP.x, pktP.y, (gint)round(0.5*(pktP.x+pktK.x)), (gint)round(0.5*(pktP.y+pktK.y)));
			gdk_gc_set_line_attributes(gc, 1, GDK_LINE_ON_OFF_DASH, GDK_CAP_NOT_LAST, GDK_JOIN_MITER);
			gdk_draw_line(GDK_PIXMAP(pixmap), gc, (gint)round(0.5*(pktP.x+pktK.x)), (gint)round(0.5*(pktP.y+pktK.y)), pktK.x, pktK.y);
			break;
		case LINE_POLNOC: //kierunek
			gdk_gc_set_foreground(gc, &czarny);
			gdk_gc_set_line_attributes(gc, 1, GDK_LINE_ON_OFF_DASH, GDK_CAP_NOT_LAST, GDK_JOIN_MITER);
			gdk_draw_line(GDK_PIXMAP(pixmap), gc, pktP.x, pktP.y, pktK.x, pktK.y);
			break;
		default:
			gdk_gc_set_foreground(gc, &czarny);
			gdk_gc_set_line_attributes(gc, 1, GDK_LINE_SOLID, GDK_CAP_NOT_LAST, GDK_JOIN_MITER);
			gdk_draw_line(GDK_PIXMAP(pixmap), gc, pktP.x, pktP.y, pktK.x, pktK.y);
	}
}

void geod_draw_circle (GtkWidget *darea, GdkPoint pktS, int promien)
{
	GtkWidget *pixmap = lookup_widget(darea, "pixmapWidok");
	gdk_draw_arc(GDK_PIXMAP(pixmap), darea->style->black_gc, FALSE, pktS.x-promien, pktS.y-promien, 2*promien, 2*promien, 0, 360*64);
}

GSList* geod_slist_remove_string (GSList *list, const char *str)
{
	GSList *iter;
  	iter = list;
	while (iter != NULL)
    {
		const char *s = (char*)iter->data;
		if (g_str_equal(s, str))
        	return g_slist_remove (list, iter->data);
		iter = iter->next;
	}
	return list;
}


void geod_raport_table (GtkWidget *textview, char **cell,
	guint ncol, guint nrow, const guint* colsize, const char* naglowek)
{
	GtkTextBuffer *textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
	GtkTextIter textiter;
	guint col, row, table_size = 0;
	char *buf;
	char *line;
	
	gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(textbuffer), &textiter);

	for (col=0; col < ncol; col++)
		table_size += colsize[col];
	
	line = g_strnfill(table_size, '_');

	// naglowek
	if (naglowek != NULL) {
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, "\n", -1, "bold", NULL);
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, naglowek, -1, "bold", NULL);
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, "\n", -1, "bold", NULL);
	}
	buf = g_strdup_printf("%s\n", line);
	gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "bold", NULL);
	g_free(buf);

	for (col=0; col<ncol; col++) {
		buf = geod_nstr(cell[col], colsize[col]);
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "bold", NULL);
		g_free(buf);
	}
	gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, "\n", -1, "bold", NULL);

	buf = g_strdup_printf("%s\n", line);
	gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "bold", NULL);
	g_free(buf);

	for (row=1; row<=nrow; row++) {
		for (col=0; col<ncol; col++) {
			buf = geod_nstr(cell[row*ncol+col], colsize[col]);
			gtk_text_buffer_insert (textbuffer, &textiter, buf, -1);
			g_free(buf);
		}
		gtk_text_buffer_insert (textbuffer, &textiter, "\n", -1);
	}
	
	buf = g_strdup_printf("%s\n", line);
	gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "bold", NULL);
	g_free(buf);
	g_free(line);
}


void geod_raport_table_pkt (GtkWidget *textview, GSList *punkty, bool col_h, const char* naglowek)
{
	GtkTextBuffer *textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
	GtkTextIter textiter;
	cPunkt *punkt = NULL;
	char *buf;
	char *line;
	guint i, npkt;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	char dok_h[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	int dok_h_int = 3;

	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	if (!dbgeod_config_get("dok_h", dok_h)) dok_h_int = atoi(dok_h);
	
	gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(textbuffer), &textiter);

	if (col_h)
		line = g_strnfill(64, '_');
	else
		line = g_strnfill(49, '_');

	// naglowek
	if (naglowek != NULL) {
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, "\n", -1, "bold", NULL);
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, naglowek, -1, "bold", NULL);
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, "\n", -1, "bold", NULL);
	}
	buf = g_strdup_printf("%s\n", line);
	gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "bold", NULL);
	g_free(buf);
	
	if (col_h)
		buf = g_strdup_printf("%10s %15s %15s %15s %5s\n", _("Nr"), _("X"), _("Y"), _("H"), _("Kod"));
	else
		buf = g_strdup_printf("%10s %15s %15s %5s\n", _("Nr"), _("X"), _("Y"), _("Kod"));
	gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "bold", NULL);
	g_free(buf);

	buf = g_strdup_printf("%s\n", line);
	gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "bold", NULL);
	g_free(buf);

	npkt = g_slist_length(punkty);
	for (i=0; i < npkt; i++) {
		punkt = (cPunkt*)g_slist_nth_data(punkty, i);
		if (col_h)
			buf = g_strdup_printf("%10s %15.*lf %15.*lf %15.*lf %5s\n", punkt->getNr(), dok_xy_int, punkt->getX(), dok_xy_int, punkt->getY(), dok_h_int, punkt->getH(), punkt->getKod());
		else
			buf = g_strdup_printf("%10s %15.*lf %15.*lf %5s\n", punkt->getNr(), dok_xy_int, punkt->getX(), dok_xy_int, punkt->getY(), punkt->getKod());
		
		gtk_text_buffer_insert (textbuffer, &textiter, buf, -1);
		g_free(buf);
	}
	
	buf = g_strdup_printf("%s\n", line);
	gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "bold", NULL);
	g_free(buf);
	g_free(line);
}

void geod_raport_naglowek (GtkWidget *textview, const char* naglowek)
{
	GtkTextBuffer *textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
	GtkTextIter textiter;
	sProjekt projekt;
	char *buf = NULL;

	gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(textbuffer), &textiter);	
	dbgeod_projekt_get(dbgeod_projekt(), projekt);

	if (naglowek == NULL)
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, _("RAPORT Z OBLICZEŃ\n"), -1, "center", "bold", "big", NULL);
	else if (g_str_equal(naglowek, ""))
		; // nic nie drukuj
	else {
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, naglowek, -1, "center", "bold", NULL);
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, "\n", -1, "center", "bold", NULL);
	}

	gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, _("Dane o obiekcie:\n"), -1, "bold", "left", NULL);
	buf = g_strdup_printf(_("Nazwa: %s\nMiejscowość: %s\nUlica: %s\nOznaczenie nieruchomości: %s\n"),
		projekt.nazwa, projekt.miejscowosc, projekt.ulica, projekt.ozn_nier);
	gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "left", NULL);
	g_free(buf);
}

void geod_raport_stopka (GtkWidget *textview)
{
	GtkTextBuffer *textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
	GtkTextIter textiter;
	char wNazwisko[CONFIG_VALUE_MAX_LEN];
	char wNazwa[CONFIG_VALUE_MAX_LEN];
	char wUprawnienia[CONFIG_VALUE_MAX_LEN];
	char *data = NULL;
	char *buf = NULL;
	
	gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(textbuffer), &textiter);

	dbgeod_config_get("wykonawca_nazwa", wNazwa);
	dbgeod_config_get("wykonawca_nazwisko", wNazwisko);
	dbgeod_config_get("wykonawca_uprawnienia", wUprawnienia);
	data = podaj_czas();

	buf = g_strdup_printf(_("\nProgramem GeodeTKa %s\nw dniu: %s\nobliczył(a): %s"),
		VERSION, data, wNazwisko);
	gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "left", "italic", NULL);
	g_free(buf);
	g_free(data);
	
	if (!g_str_equal(wUprawnienia, "")) {
		buf = g_strdup_printf(" (nr upr. %s)", wUprawnienia);
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "left", "italic", NULL);
		g_free(buf);
	}
	if (!g_str_equal(wNazwa, "")) {
		buf = g_strdup_printf("\n\t%s", wNazwa);
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "left", "italic", NULL);
		g_free(buf);
	}
	gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, "\n", -1, "left", "italic", NULL);
}


void geod_raport_line (GtkWidget *textview, const char* opis, const char* wynik, bool bold)
{
	GtkTextBuffer *textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
	GtkTextIter textiter;
	char *buf = NULL;

	gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(textbuffer), &textiter);	

	buf = g_strdup_printf("%s: %s\n", opis, wynik);
	if (bold)
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "left", "bold", NULL);
	else
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "left", NULL);
	g_free(buf);
}


void geod_raport_error (GtkWidget *textview, const char* opis, bool bold)
{
	GtkTextBuffer *textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
	GtkTextIter textiter;
	char *buf = NULL;

	gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(textbuffer), &textiter);	

	buf = g_strdup_printf("%s\n", opis);
	if (bold)
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "left", "bold", "italic", NULL);
	else
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &textiter, buf, -1, "left", "italic", NULL);
	g_free(buf);
}


char* geod_nstr (const char* str, guint nsize)
{
	if (str == NULL)
		return g_strnfill(nsize, ' ');

	char *nstr;
	guint ssize = strlen(str);
	guint ssize_utf = g_utf8_strlen(str, -1);
	guint nsize_utf = nsize+ssize-ssize_utf;
	if (ssize_utf >= nsize)
		return g_strdup(str);
	else {
		nstr = g_strnfill(nsize_utf, ' ');
		for (guint i=0; i < ssize; i++)
			nstr[nsize_utf-ssize+i] = str[i];
		return nstr;
	}
}


/* This is function to find data files. */
gchar* geod_find_data_file (const gchar *filename)
{
	GList *elem;
	GList *data_directories = NULL;
#ifdef HAVE_CONFIG_H
	data_directories = g_list_prepend (data_directories, g_strdup(PACKAGE_DATA_DIR G_DIR_SEPARATOR_S PACKAGE G_DIR_SEPARATOR_S));
	data_directories = g_list_prepend (data_directories, g_strdup(PACKAGE_DATA_DIR G_DIR_SEPARATOR_S PACKAGE G_DIR_SEPARATOR_S "data"));
	data_directories = g_list_prepend (data_directories, g_strdup(PACKAGE_DOC_DIR));
	data_directories = g_list_prepend (data_directories, g_strdup(PACKAGE_SOURCE_DIR G_DIR_SEPARATOR_S "data"));
#endif
	data_directories = g_list_prepend (data_directories, g_strdup("." G_DIR_SEPARATOR_S "data"));
	data_directories = g_list_prepend (data_directories, g_strdup("." G_DIR_SEPARATOR_S));
	data_directories = g_list_prepend (data_directories, g_strdup("." G_DIR_SEPARATOR_S "doc"));

	elem = data_directories;
	while (elem) {
      gchar *pathname = g_strdup_printf ("%s%s%s", (gchar*)elem->data, G_DIR_SEPARATOR_S, filename);
		if (g_file_test (pathname, G_FILE_TEST_EXISTS)) {
			g_list_free(data_directories);
			return pathname;
		}
		g_free (pathname);
		elem = elem->next;
	}
	g_list_free(data_directories);
	g_warning (_("Couldn't find data file: %s"), filename);
	return NULL;
}


void geod_www(const char* web_adress)
{
	char web_browser[CONFIG_VALUE_MAX_LEN];
	int ret;
	
	dbgeod_config_get("przegladarka_www", web_browser);
#ifdef G_OS_UNIX
	if (g_str_equal(web_browser, ""))
		ret = system(g_strdup_printf("htmlview %s &", web_adress));
	else
		ret = system(g_strdup_printf("%s %s &", web_browser, web_adress));
#endif
#ifdef G_OS_WIN32
	ShellExecute(NULL, "open", web_adress, NULL, NULL, SW_SHOWNORMAL);
#endif
}

char* geod_prefix_rem(const char* source)
{
	return g_strdup(source+1);
}
