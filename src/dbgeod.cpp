/***************************************************************************
 *            dbgeod.cpp
 *  OBSŁUGA BAZY DANYCH SQLite W PROGRAMIE
 *  Sun May 29 14:46:18 2005
 *  Copyright  2005  Bogusław Ciastek
 *  ciacho@z.pl
 ****************************************************************************/
#include <gtk/gtk.h>
#include <stdlib.h>

#include "stdgeod.h"
#include "dbgeod.h"
#include "suppgeod.h"
#include "support.h"

extern char *DB_NAME;

// tworzy baze danych z podstawowymi tabelami niezbednymi do dzialania programu
int dbgeod_database_create(GtkProgressBar *pbar)
{
	sqlite3 *dbgeod;
	char *sql_query = NULL;
	char *sql_err = NULL;
	int rc;
	int nrow, ncolumn;
	char **azRezult[255];
	char *data_file = NULL;
	char bufor[512];
	FILE *plik;
	bool create_db_k1_error = FALSE;
	char buff_dok[G_ASCII_DTOSTR_BUF_SIZE];
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	
	// tworzenie tabeli konfiguracja
	gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(pbar), 0.20);
	while (gtk_events_pending ())
  		gtk_main_iteration ();
		
	rc = sqlite3_get_table(dbgeod, "PRAGMA table_info(konfiguracja);", azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("Błąd w SQL: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	else if (nrow == 0 && ncolumn == 0)	{
		gtk_progress_bar_set_text(GTK_PROGRESS_BAR(pbar), _("Tworzenie tabeli konfiguracja..."));
		while (gtk_events_pending ())
  			gtk_main_iteration ();

		sql_query = sqlite3_mprintf("create table konfiguracja (id integer primary key, "
			"key char(25) unique, value char(%d));", CONFIG_VALUE_MAX_LEN);
		rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
		if (rc != SQLITE_OK) {
			fprintf(stderr, _("SQL konfiguracja error: %s\n"), sql_err);
			sqlite3_free(sql_err);
		}
		sqlite3_free(sql_query);
		// ustawienie wartości domyślnych
		dbgeod_config_set("uklad", "geodezyjny");
		dbgeod_config_set("dodaj_pkt", "zapytaj");
		dbgeod_config_set("jedn_kat", "grad");
		dbgeod_config_set("jedn_pow", "m2");
		dbgeod_config_set("auto_addpkt", "1");
		dbgeod_config_set("auto_num", "+");
		dbgeod_config_set("lokalny", "0");
		dbgeod_config_set("dok_xy", "3");
		dbgeod_config_set("dok_h", "3");
		dbgeod_config_set("dok_kat", "5");
		dbgeod_config_set("dok_par", "8");
		dbgeod_config_set("dok_centr", "20");
		dbgeod_config_set("dok_azym", g_ascii_formatd(buff_dok, sizeof(buff_dok), "%.12f", 0.01/RO_GR));
		dbgeod_config_set("dok_kier", g_ascii_formatd(buff_dok, sizeof(buff_dok), "%.12f", 0.002/RO_GR));
		dbgeod_config_set("dok_odl1", "20");
		dbgeod_config_set("dok_odl2", "20");
		dbgeod_config_set("wykonawca_nazwa", "");
		dbgeod_config_set("wykonawca_adres", "");
		dbgeod_config_set("wykonawca_nip", "");
		dbgeod_config_set("wykonawca_regon", "");
		dbgeod_config_set("wykonawca_nazwisko", "");
		dbgeod_config_set("wykonawca_uprawnienia", "");
	}
	sqlite3_free_table(*azRezult);
	
	// tworzenie tabeli punkty
	gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(pbar), 0.30);
	while (gtk_events_pending ())
		gtk_main_iteration ();
	
	rc = sqlite3_get_table(dbgeod, "PRAGMA table_info(punkty);", azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("Błąd w SQL: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	else if (nrow == 0 && ncolumn == 0)	{
		gtk_progress_bar_set_text(GTK_PROGRESS_BAR(pbar), _("Tworzenie tabeli punkty..."));
		while (gtk_events_pending ())
  			gtk_main_iteration ();

		sql_query = sqlite3_mprintf("create table punkty (id integer primary key, "
			"nr char(%d),  x real, y real, h real, kod char(%d), typ integer, "
			"projekt_id integer);", NR_MAX_LEN, KOD_MAX_LEN);
		rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
		if (rc != SQLITE_OK) {
			fprintf(stderr, _("SQL punkty error: %s\n"), sql_err);
			sqlite3_free(sql_err);
		}
		sqlite3_free(sql_query);
	}
	sqlite3_free_table(*azRezult);

	// tworzenie tabeli projekty
	gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(pbar), 0.40);
	while (gtk_events_pending ())
		gtk_main_iteration ();

	rc = sqlite3_get_table(dbgeod, "PRAGMA table_info(projekty);", azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	else if (nrow == 0 && ncolumn == 0)	{
		gtk_progress_bar_set_text(GTK_PROGRESS_BAR(pbar), _("Tworzenie tabeli projekty..."));
		while (gtk_events_pending ())
			gtk_main_iteration ();

		sql_query = sqlite3_mprintf("create table projekty (id integer primary key, "
			"nazwa char(%d), wojewodztwo char(%d), powiat char(%d), gmina char(%d), "
			"miejscowosc char(%d), ulica char(%d), ozn_nier char (%d), cel_pracy char(%d), "
			"uwagi char(%d), data_pom char(%d), data_mod char(%d));",
			PNAZWA_MAX_LEN, PWOJEWODZTWO_MAX_LEN, PPOWIAT_MAX_LEN, PGMINA_MAX_LEN,
			PMIEJSCOWOSC_MAX_LEN, PULICA_MAX_LEN, POZN_NIER_MAX_LEN, PCEL_PRACY_MAX_LEN,
			PUWAGI_MAX_LEN, PDATA_POM_MAX_LEN, PDATA_MOD_MAX_LEN);
		rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
		if (rc != SQLITE_OK) {
			fprintf(stderr, _("SQL projekty error: %s\n"), sql_err);
			sqlite3_free(sql_err);
		}
		sqlite3_free(sql_query);
	}
	sqlite3_free_table(*azRezult);

	// tworzenie tabeli k1
	gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(pbar), 0.50);
	while (gtk_events_pending ())
		gtk_main_iteration ();

	rc = sqlite3_get_table(dbgeod, "PRAGMA table_info(k1);", azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	else if (nrow == 0 && ncolumn == 0)	{
		gtk_progress_bar_set_text(GTK_PROGRESS_BAR(pbar), _("Tworzenie tabeli k1..."));
		while (gtk_events_pending ())
  			gtk_main_iteration ();
		
		data_file = geod_find_data_file("k1.sql");
		if (data_file != NULL) {
			plik = fopen(data_file, "rt");
			if (plik != NULL) {
				while (fgets(bufor, 512, plik) != NULL) {
				rc = sqlite3_exec(dbgeod, bufor, NULL, NULL, &sql_err);
					if (rc != SQLITE_OK) {
						fprintf(stderr, _("SQL konfiguracja error: %s\n"), sql_err);
						sqlite3_free(sql_err);
					}
				}
				fclose(plik);
			}
			else
				create_db_k1_error = TRUE;
			g_free(data_file);
		}
		else
			create_db_k1_error = TRUE;
		
		// w przydaku braku pliku tworze pusta baze kodow
		if (create_db_k1_error) {
			sql_query = sqlite3_mprintf("create table k1 (kod_l char(3), kod_n char(3), "
				"obiekt char(255));");
			rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
			if (rc != SQLITE_OK) {
				fprintf(stderr, _("SQL konfiguracja error: %s\n"), sql_err);
				sqlite3_free(sql_err);
			}		
			sqlite3_free(sql_query);
		}
	}
	sqlite3_free_table(*azRezult);

	/* tworzenie tabeli obserwacje */
	gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(pbar), 0.70);
	while (gtk_events_pending ())
		gtk_main_iteration ();

	rc = sqlite3_get_table(dbgeod, "PRAGMA table_info(obserwacje);", azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	else if (nrow == 0 && ncolumn == 0)	{
		gtk_progress_bar_set_text(GTK_PROGRESS_BAR(pbar), _("Tworzenie tabeli obserwacje..."));
		while (gtk_events_pending ())
  			gtk_main_iteration ();
		
		sql_query = sqlite3_mprintf("create table obserwacje (id integer primary key, "
			"projekt_id integer, punktl char(%d), stanowisko char(%d),  punktp char(%d), "
			"odczyt real, unique(projekt_id, punktl, stanowisko, punktp));", NR_MAX_LEN, NR_MAX_LEN, NR_MAX_LEN);
		rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
		if (rc != SQLITE_OK) {
			fprintf(stderr, _("SQL obserwacje error: %s\n"), sql_err);
			sqlite3_free(sql_err);
		}
		sqlite3_free(sql_query);
	}
	sqlite3_free_table(*azRezult);
	
	/* tworzenie tabeli hist_zadanie */
	gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(pbar), 0.80);
	while (gtk_events_pending ())
		gtk_main_iteration ();

	rc = sqlite3_get_table(dbgeod, "PRAGMA table_info(hist_zadanie);", azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	else if (nrow == 0 && ncolumn == 0)	{
		gtk_progress_bar_set_text(GTK_PROGRESS_BAR(pbar), _("Tworzenie tabeli hist_zadanie..."));
		while (gtk_events_pending ())
  			gtk_main_iteration ();
		
		sql_query = sqlite3_mprintf("create table hist_zadanie (id integer primary key, "
			"projekt_id integer, kolejnosc integer,  funkcja char(%d), data_mod char(%d), unique(kolejnosc, projekt_id));",
			HFUNKCJA_MAX_LEN, HDATA_MOD_MAX_LEN);
		rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
		if (rc != SQLITE_OK) {
			fprintf(stderr, _("SQL hist_zadanie error: %s\n"), sql_err);
			sqlite3_free(sql_err);
		}
		sqlite3_free(sql_query);
	}
	sqlite3_free_table(*azRezult);

	/* tworzenie tabeli hist_parametry */
	gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(pbar), 0.90);
	while (gtk_events_pending ())
  		gtk_main_iteration ();
		
	rc = sqlite3_get_table(dbgeod, "PRAGMA table_info(hist_parametry);", azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	else if (nrow == 0 && ncolumn == 0)	{
		gtk_progress_bar_set_text(GTK_PROGRESS_BAR(pbar), _("Tworzenie tabeli hist_parametry..."));
		while (gtk_events_pending ())
  			gtk_main_iteration ();
		
		sql_query = sqlite3_mprintf("create table hist_parametry (id integer primary key, "
			"zadanie_id integer, nazwa char(%d), wartosc real, wynik integer, "
			"unique (zadanie_id, nazwa));", HNAZWA_MAX_LEN);
		rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
		if (rc != SQLITE_OK) {
			fprintf(stderr, _("SQL hist_parametry error: %s\n"), sql_err);
			sqlite3_free(sql_err);
		}
		sqlite3_free(sql_query);
	}
	sqlite3_free_table(*azRezult);
	
	/* tworzenie tabeli hist_punkty */
	gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(pbar), 1.0);
	while (gtk_events_pending ())
		gtk_main_iteration ();

	rc = sqlite3_get_table(dbgeod, "PRAGMA table_info(hist_punkty);", azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	else if (nrow == 0 && ncolumn == 0)	{
		gtk_progress_bar_set_text(GTK_PROGRESS_BAR(pbar), _("Tworzenie tabeli hist_punkty..."));
		while (gtk_events_pending ())
  			gtk_main_iteration ();
		
		sql_query = sqlite3_mprintf("create table hist_punkty (id integer primary key, "
			"zadanie_id integer, nazwa char(%d), nr_punkt char(%d), wynik integer, "
			"unique (zadanie_id, nazwa));", HNAZWA_MAX_LEN, NR_MAX_LEN);
		rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
		if (rc != SQLITE_OK) {
			fprintf(stderr, _("SQL hist_parametry error: %s\n"), sql_err);
			sqlite3_free(sql_err);
		}
		sqlite3_free(sql_query);
	}
	sqlite3_free_table(*azRezult);
	
	sqlite3_close(dbgeod);
	return 0;
}

// dodanie nowego punktu do bazy danych
int dbgeod_point_add(cPunkt punkt)
{
	sqlite3 *dbgeod;
	char *sql_query = NULL;
	char *sql_err = NULL;
	int rc;
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}

	sql_query = sqlite3_mprintf("insert into punkty(nr, x, y, h, kod, typ, projekt_id) "
		"values('%q', %.8f, %.8f, %.8f, '%q', %d, %d);", punkt.getNr(),
		punkt.getX(), punkt.getY(), punkt.getH(),
		punkt.getKod(), punkt.getTyp(), dbgeod_projekt());
	
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	sqlite3_close(dbgeod);
	return 0;
}

// wprowadzenie nowego punktu do bazy danych zgodnie z ustawieniami w konfiguracji
int dbgeod_point_add_config(cPunkt punkt, bool *wyczysc, char *addkonfig)
{
	GtkWidget *dialog;
	char txt_konfiguracja[CONFIG_VALUE_MAX_LEN];
	GSList *punkty = NULL;
	GSList *punkty_id = NULL;
	guint ile_pkt;
	int err_code = 0, response_id;
	bool zmiana = FALSE;
	char *msg;
	int PktTyp = GEODPKT_ALL;

	if (addkonfig == NULL)
		dbgeod_config_get("dodaj_pkt", txt_konfiguracja);
	else
		g_strlcpy(txt_konfiguracja, addkonfig, CONFIG_VALUE_MAX_LEN);

	if (punkt.getTyp() == GEODPKT_OSNOWA || punkt.getTyp() == GEODPKT_OSNOWAL)
		PktTyp = punkt.getTyp();

	if(!dbgeod_points_find(punkt.getNr(), &punkty, &punkty_id, PktTyp))
		ile_pkt = g_slist_length(punkty);
	else ile_pkt = 0;

	if (ile_pkt == 0) {
		err_code = dbgeod_point_add(punkt);
		zmiana = TRUE;
	}
	else {
		int fpunkt_id = GPOINTER_TO_INT(g_slist_nth_data(punkty_id, 0));
		cPunkt *fpunkt = (cPunkt*)g_slist_nth_data(punkty, 0);
		if (g_str_equal(txt_konfiguracja, "zapytaj")) {
			msg = g_strdup_printf(_("Punkt %s znajduje się już w bazie.\nNadpisać?"), punkt.getNr());
			dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_WARNING, GTK_BUTTONS_YES_NO, msg);
			if (addkonfig != NULL)
				gtk_dialog_add_buttons(GTK_DIALOG(dialog), _("_Wszystkie"), GTK_RESPONSE_APPLY, _("Ża_den"), GTK_RESPONSE_CANCEL, NULL);
			response_id = gtk_dialog_run(GTK_DIALOG(dialog));
			if (response_id == GTK_RESPONSE_YES) {
				err_code = dbgeod_point_update(fpunkt_id, punkt);
				zmiana = TRUE;
			}
			else if (response_id == GTK_RESPONSE_APPLY) {
				err_code = dbgeod_point_update(fpunkt_id, punkt);
				zmiana = TRUE;
				g_strlcpy(addkonfig, "nadpisz", CONFIG_VALUE_MAX_LEN);
			}
			else if (response_id == GTK_RESPONSE_CANCEL) {
				zmiana = FALSE;
				g_strlcpy(addkonfig, "pozostaw", CONFIG_VALUE_MAX_LEN);
			}

			gtk_widget_destroy(dialog);
			g_free(msg);
		}
		else if (g_str_equal(txt_konfiguracja, "nadpisz")) {
			err_code = dbgeod_point_update(fpunkt_id, punkt);
			zmiana = TRUE;
		}
		else if (g_str_equal(txt_konfiguracja, "usrednij")) {
			punkt.setX(0.5*(punkt.getX()+fpunkt->getX()));
			punkt.setY(0.5*(punkt.getY()+fpunkt->getY()));
			err_code = dbgeod_point_update(fpunkt_id, punkt);
			zmiana = TRUE;
		}
		else if (addkonfig == NULL) {
			dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, _("Punkt o takim numerze znajduje się już w bazie."));
			gtk_dialog_run(GTK_DIALOG(dialog));
			gtk_widget_destroy(dialog);
		}
		g_free(fpunkt);
	}
	g_slist_free(punkty_id);
	g_slist_free(punkty);
	if (wyczysc != NULL) *wyczysc = zmiana;
	return err_code;
}
	

// usuniecie punktu z bazy danych na podstawie jego identyfikatora
int dbgeod_point_del(int point_id, const char* pkt_nr)
{
	sqlite3 *dbgeod;
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}

	// usuniecie punktu z bazy danych
	if (pkt_nr != NULL)
		sql_query = sqlite3_mprintf("delete from punkty where nr='%q' and projekt_id=%d;",
			pkt_nr, dbgeod_projekt());
	else
		sql_query = sqlite3_mprintf("delete from punkty where id=%d and projekt_id=%d;",
			point_id, dbgeod_projekt());

	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	sqlite3_close(dbgeod);
	return 0;
}

// modyfikacja punktu z bazy danych na podstawie identyfikatora
int dbgeod_point_update(int point_id, cPunkt punkt, bool pkt_nr)
{
	sqlite3 *dbgeod;
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;
	char *warunek = NULL;

	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}

	// modyfikacja punktu w bazie danych
	if (pkt_nr)
		warunek = sqlite3_mprintf("where nr='%q' and projekt_id=%d", punkt.getNr(), dbgeod_projekt());
	else
		warunek = sqlite3_mprintf("where id='%d' and projekt_id=%d", point_id, dbgeod_projekt());

	sql_query = sqlite3_mprintf("update punkty set nr='%q', x=%.8f, y=%.8f, h=%.8f, "
		"kod='%q', typ=%d %s;",
		punkt.getNr(), punkt.getX(), punkt.getY(), punkt.getH(),
		punkt.getKod(), punkt.getTyp(), warunek);
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	sqlite3_free(warunek);
	sqlite3_close(dbgeod);
	return 0;
}

// funkcja zwraca liste identyfikatorow oraz liste obliektow klasy cPunkt
// posiadajacych oznaczenie podane w zapytaniu
// Pamietaj o zwolnieniu pamieci zajetej przez tworzone obliekty klasy GSList 
int dbgeod_points_find(const char* pkt_nr, GSList** punkty, GSList** punkty_id, int typ)
{
	sqlite3 *dbgeod;
	int rc; int i;
	int id_int;
	char *txt = NULL;
	int nrow = 0, ncolumn = 0;
	char *sql_query = NULL;
	char *sql_err = NULL;
	char **azRezult[255];
	cPunkt *punkt = NULL;
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie punktow i umieszczenie w zwracanej liscie
	if (typ==GEODPKT_ALL)
		if (pkt_nr != NULL)
			sql_query=sqlite3_mprintf("select id, nr, x, y, h, kod, typ from punkty "
			"where nr='%q' and projekt_id=%d;", pkt_nr, dbgeod_projekt());
		else
			sql_query=sqlite3_mprintf("select id, nr, x, y, h, kod, typ from punkty "
			"where projekt_id=%d;", dbgeod_projekt());
	else
		if (pkt_nr != NULL)
			sql_query=sqlite3_mprintf("select id, nr, x, y, h, kod, typ from punkty "
			"where nr='%q' and projekt_id=%d and typ=%d;", pkt_nr, dbgeod_projekt(), typ);
		else
			sql_query=sqlite3_mprintf("select id, nr, x, y, h, kod, typ from punkty "
			"where projekt_id=%d and typ=%d;", dbgeod_projekt(), typ);

	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	if (nrow < 1) {
		sqlite3_free_table(*azRezult);
		sqlite3_close(dbgeod);
		return -3; // brak punktow
	}

	for (i=1; i <= nrow; i++) {
		punkt = new cPunkt;
		if ( (*azRezult)[i*ncolumn+0] != NULL ) {
			txt = g_strdup((*azRezult)[i*ncolumn+0]);
			id_int = atoi(txt);
			*punkty_id = g_slist_append(*punkty_id, GINT_TO_POINTER(id_int));
			g_free(txt);
		}
		if ( (*azRezult)[i*ncolumn+5] != NULL ) {
			txt = g_strdup((*azRezult)[i*ncolumn+5]);
			punkt->setKod(txt);
			g_free(txt);
		}
		if ( (*azRezult)[i*ncolumn+1] != NULL ) {
			txt = g_strdup((*azRezult)[i*ncolumn+1]);
			punkt->setNr(txt);
			g_free(txt);
		}
		if ( (*azRezult)[i*ncolumn+2] != NULL )
			punkt->setX(g_ascii_strtod((*azRezult)[i*ncolumn+2], NULL));
		if ( (*azRezult)[i*ncolumn+3] != NULL )
			punkt->setY(g_ascii_strtod((*azRezult)[i*ncolumn+3], NULL));
		if ( (*azRezult)[i*ncolumn+4] != NULL )
			punkt->setH(g_ascii_strtod((*azRezult)[i*ncolumn+4], NULL));
		if ( (*azRezult)[i*ncolumn+6] != NULL )
			punkt->setTyp(atoi((*azRezult)[i*ncolumn+6]));

		*punkty = g_slist_append(*punkty, punkt);
	}
	sqlite3_free_table(*azRezult);
	sqlite3_close(dbgeod);
	return 0;
}


// funkcja wyszukuje punkt na podstawie jego numeru
int dbgeod_point_find(const char* pkt_nr, cPunkt *punkt, int typ)
{ /// poprawic, optymalizacja poprzez pozbycie sie points_find
	GSList *punkty = NULL;
	GSList *punkty_id = NULL;
	guint ile_pkt;
	int err_code = 0;

	if (pkt_nr == NULL) return -2;
	err_code = dbgeod_points_find(pkt_nr, &punkty, &punkty_id, typ);
	if (err_code) return err_code;
	
	ile_pkt = g_slist_length(punkty);
	if (ile_pkt==0) return -1; // nie znaleziono punktu

	if (punkt != NULL)
		*punkt = *(cPunkt*)g_slist_nth_data(punkty, 0);
	g_slist_free(punkty);
	g_slist_free(punkty_id);
	return 0;
}

// wyszukuje punkt na podstawie numeru i w razie potrzeby go transformuje
int dbgeod_pointt_find(const char* pkt_nr, cPunkt *punkt)
{
	char lokalny[CONFIG_VALUE_MAX_LEN];
	guint transformuj=100, i=0;
	int blad=0;
	GSList *pktDostNr=NULL;
	cPunkt (*pktDost)[2] = NULL;
	cPunkt pkt_up;
	cPunkt *pkt = NULL, *pkt_old = NULL;
	GSList *punkty = NULL;
	GSList *punkty_id = NULL;
	
	dbgeod_config_get("lokalny", lokalny);

	if (pkt_nr == NULL) return -2;
	blad = dbgeod_points_find(pkt_nr, &punkty, &punkty_id);
	if (blad) return blad;
	
	guint n_pkt = g_slist_length(punkty);
	if (n_pkt==0) return -1; // nie znaleziono punktu

	for (i=0; i<n_pkt; i++) {
		pkt_old = pkt;
		pkt = (cPunkt*)g_slist_nth_data(punkty, i);
		
		if (g_str_equal(lokalny, "1")) {
			if ((pkt->getTyp()==GEODPKT_OSNOWAL) || (pkt->getTyp()==GEODPKT_WYNIKL)) {
				transformuj = 0;
				break;
			}
			else if ((pkt->getTyp()==GEODPKT_OSNOWA) || (pkt->getTyp()==GEODPKT_WYNIK))
				transformuj = 1;
			else if (transformuj > 2)
				transformuj = 3;
			else
				pkt = pkt_old;
		}
		else if (g_str_equal(lokalny, "0")) {
			if ((pkt->getTyp()==GEODPKT_OSNOWA) || (pkt->getTyp()==GEODPKT_WYNIK)) {
				transformuj = 0;
				break;
			}
			else if ((pkt->getTyp()==GEODPKT_OSNOWAL) || (pkt->getTyp()==GEODPKT_WYNIKL))
				transformuj = 2;
			else if (transformuj > 2)
				transformuj = 3;
			else
				pkt = pkt_old;
		}
	}
	
	*punkt = *pkt;
	pkt_up = *pkt;
	g_slist_free(punkty);
	g_slist_free(punkty_id);

	if (transformuj==0 || transformuj>2)
		return GEODERR_OK;
	
	dbgeod_points_transf_find(&pktDostNr);
	n_pkt = g_slist_length(pktDostNr);
	
	if (n_pkt>1) {
		pktDost = new cPunkt[n_pkt][2];
		for (i=0; i<n_pkt; i++)
			if (transformuj==1) {
				dbgeod_point_find((char*)g_slist_nth_data(pktDostNr, i), &pktDost[i][0], GEODPKT_OSNOWA);
				dbgeod_point_find((char*)g_slist_nth_data(pktDostNr, i), &pktDost[i][1], GEODPKT_OSNOWAL);
				punkt->setTyp(GEODPKT_WYNIKL);
			}
			else {
				dbgeod_point_find((char*)g_slist_nth_data(pktDostNr, i), &pktDost[i][0], GEODPKT_OSNOWAL);
				dbgeod_point_find((char*)g_slist_nth_data(pktDostNr, i), &pktDost[i][1], GEODPKT_OSNOWA);
				punkt->setTyp(GEODPKT_WYNIK);
			}
		blad = transformacja_helmerta(pktDost, n_pkt, pkt_up, *punkt);
		delete [] pktDost;
		pktDost = NULL;
	}
	else
		blad = GEODERR_ALGORITHM;
	
	g_slist_free(pktDostNr);
	return blad;
}


// informacje o współrzędnych w bazie danych
int dbgeod_points_info (unsigned int &npoints, double &xmin, double &xmax,
	double &ymin, double &ymax)
{
	sqlite3 *dbgeod;
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;
	char **azRezult[255];
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	
	// xmin
	sql_query = sqlite3_mprintf("select min(x) from punkty where projekt_id=%d;", dbgeod_projekt());
	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	if ( (*azRezult)[1] != NULL ) xmin = g_ascii_strtod((*azRezult)[1], NULL); else xmin = NAN;
	sqlite3_free(sql_query);
	sqlite3_free_table(*azRezult);		

	// ymin
	sql_query = sqlite3_mprintf("select min(y) from punkty where projekt_id=%d;", dbgeod_projekt());
	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	if ( (*azRezult)[1] != NULL ) ymin = g_ascii_strtod((*azRezult)[1], NULL); else ymin = NAN;
	sqlite3_free(sql_query);
	sqlite3_free_table(*azRezult);		
	
	// xmax
	sql_query = sqlite3_mprintf("select max(x) from punkty where projekt_id=%d;", dbgeod_projekt());
	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	if ( (*azRezult)[1] != NULL ) xmax = g_ascii_strtod((*azRezult)[1], NULL); else xmax = NAN;
	sqlite3_free(sql_query);
	sqlite3_free_table(*azRezult);		
	
	// ymax
	sql_query = sqlite3_mprintf("select max(y) from punkty where projekt_id=%d;", dbgeod_projekt());
	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	if ( (*azRezult)[1] != NULL ) ymax = g_ascii_strtod((*azRezult)[1], NULL); else ymax = NAN;
	sqlite3_free(sql_query);
	sqlite3_free_table(*azRezult);		
	
	// liczba punktów
	sql_query = sqlite3_mprintf("select count(*) from punkty where projekt_id=%d;", dbgeod_projekt());
	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	if ( (*azRezult)[1] != NULL ) npoints = atoi((*azRezult)[1]); else npoints = 0;
	sqlite3_free(sql_query);
	sqlite3_free_table(*azRezult);		

	sqlite3_close(dbgeod);
	return 0;
}


int treePunkty_callback(void *treeview, int argc, char **argv, char **azColName)
{	
	char *pkt_id = NULL, *pkt_nr = NULL, *pkt_x = NULL, *pkt_y = NULL, *pkt_h = NULL, *pkt_kod = NULL, *pkt_typ = NULL;
	char *pkt_x_loc = NULL, *pkt_y_loc = NULL, *pkt_h_loc = NULL;
	double xf=0.0, yf=0.0, hf=0.0;
	int typ_int=0, id_int=0;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	char dok_h[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	int dok_h_int = 3;

	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	if (!dbgeod_config_get("dok_h", dok_h)) dok_h_int = atoi(dok_h);
	
	if(argv[0]!=NULL) { pkt_id = g_strdup(argv[0]); id_int=atoi(&pkt_id[0]); } //id
	if(argv[1]!=NULL) { pkt_nr = g_strdup(argv[1]); } // nr
	if(argv[2]!=NULL) { pkt_x = g_strdup(argv[2]); xf=g_ascii_strtod(pkt_x, NULL); pkt_x_loc = g_strdup_printf("%.*lf", dok_xy_int, xf); } // x
	if(argv[3]!=NULL) { pkt_y = g_strdup(argv[3]); yf=g_ascii_strtod(pkt_y, NULL); pkt_y_loc = g_strdup_printf("%.*lf", dok_xy_int, yf); } // y
	if(argv[4]!=NULL) { pkt_h = g_strdup(argv[4]); hf=g_ascii_strtod(pkt_h, NULL); pkt_h_loc = g_strdup_printf("%.*lf", dok_h_int, hf); } // h	
	if(argv[5]!=NULL) { pkt_kod = g_strdup(argv[5]); } // kod
	if(argv[6]!=NULL) { pkt_typ = g_strdup(argv[6]); typ_int=atoi(&pkt_typ[0]); } // typ
	
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));

 	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
		COL_PKT_ID, id_int, COL_PKT_NR, pkt_nr,
		COL_PKT_X, pkt_x_loc, COL_PKT_X_DOUBLE, xf,
		COL_PKT_Y, pkt_y_loc, COL_PKT_Y_DOUBLE, yf,
		COL_PKT_H, pkt_h_loc, COL_PKT_H_DOUBLE, hf,
		COL_PKT_KOD, pkt_kod, COL_PKT_TYP, typ_int, -1);
	
	g_free(pkt_id);
	g_free(pkt_nr);
	g_free(pkt_x);
	g_free(pkt_y);
	g_free(pkt_h);
	g_free(pkt_kod);
	g_free(pkt_typ);
	g_free(pkt_x_loc);
	g_free(pkt_y_loc);
	g_free(pkt_h_loc);
	return 0;
}

int treePunkty_update_view(GtkWidget *treeview, const char* pkt_nr)
{
	sqlite3 *dbgeod;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;

	gtk_tree_store_clear(GTK_TREE_STORE(model));
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie punktow i umieszczenie poprzez funkcje callback w tabeli
	// w przypadku gdy nie okreslono zadnych punktow zwracane sa wszystkie
	if (pkt_nr != NULL)
		sql_query = sqlite3_mprintf( "select id, nr, x, y, h, kod, typ from "
			"punkty where nr like '%%%q%%' and projekt_id=%d order by nr;",
			pkt_nr, dbgeod_projekt());
	else
		sql_query = sqlite3_mprintf("select id, nr, x, y, h, kod, typ "
			"from punkty where projekt_id=%d order by nr;", dbgeod_projekt());
	
	rc = sqlite3_exec(dbgeod, sql_query, treePunkty_callback, treeview, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	sqlite3_close(dbgeod);
	return 0;
}

int treeDialogKod_update_view(GtkWidget *treeview, const char* find_str)
{
	sqlite3 *dbgeod;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;

	gtk_tree_store_clear(GTK_TREE_STORE(model));
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie kodow instrukcji K1 i umieszczenie poprzez funkcje callback
	// w tabeli w przypadku gdy nie okreslono zadnych punktow zwracane sa wszystkie
	if (find_str != NULL)
		sql_query = sqlite3_mprintf("select * from k1 where kod_l like '%%%q%%' or kod_n "
			"like '%%%q%%' or obiekt like '%%%q%%' order by kod_l;", find_str,
			find_str, find_str);
	else
		sql_query = sqlite3_mprintf("select * from k1 order by kod_l;");
	
	rc = sqlite3_exec(dbgeod, sql_query, treeDialogKod_callback, treeview, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	sqlite3_close(dbgeod);
	return 0;
}

int treeDialogKod_callback(void *treeview, int argc, char **argv, char **azColName)
{
	char *kod_n = NULL, *kod_l = NULL, *obiekt = NULL;
	
	if(argv[0]!=NULL) kod_l = g_strdup(argv[0]);
	if(argv[1]!=NULL) kod_n = g_strdup(argv[1]);
	if(argv[2]!=NULL) obiekt = g_strdup(argv[2]);
	
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));

 	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
		0, kod_l, 1, kod_n, 2, obiekt, -1);
	
	g_free(kod_l);
	g_free(kod_n);
	g_free(obiekt);
	return 0;
}

int treeProjekty_callback(void *treeview, int argc, char **argv, char **azColName)
{
	struct sProjekt projekt;
	char *id_txt = NULL;
	
	if(argv[0]!=NULL) { id_txt = g_strdup(argv[0]); projekt.id = atoi(id_txt); g_free(id_txt); }
	if(argv[1]!=NULL) g_strlcpy(projekt.nazwa, argv[1], PNAZWA_MAX_LEN);
	if(argv[2]!=NULL) g_strlcpy(projekt.wojewodztwo, argv[2], PWOJEWODZTWO_MAX_LEN);
	if(argv[3]!=NULL) g_strlcpy(projekt.powiat, argv[3], PPOWIAT_MAX_LEN);
	if(argv[4]!=NULL) g_strlcpy(projekt.gmina, argv[4], PGMINA_MAX_LEN);
	if(argv[5]!=NULL) g_strlcpy(projekt.miejscowosc, argv[5], PMIEJSCOWOSC_MAX_LEN);
	if(argv[6]!=NULL) g_strlcpy(projekt.ulica, argv[6], PULICA_MAX_LEN);
	if(argv[7]!=NULL) g_strlcpy(projekt.ozn_nier, argv[7], POZN_NIER_MAX_LEN);
	if(argv[8]!=NULL) g_strlcpy(projekt.cel_pracy, argv[8], PCEL_PRACY_MAX_LEN);
	if(argv[9]!=NULL) g_strlcpy(projekt.uwagi, argv[9], PUWAGI_MAX_LEN);
	if(argv[10]!=NULL) g_strlcpy(projekt.data_pom, argv[10], PDATA_POM_MAX_LEN);
	if(argv[11]!=NULL) g_strlcpy(projekt.data_mod, argv[11], PDATA_MOD_MAX_LEN);
	
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));

 	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
		COL_PRJ_ID, projekt.id, COL_PRJ_NAZWA, projekt.nazwa, COL_PRJ_WOJ, projekt.wojewodztwo,
		COL_PRJ_POWIAT, projekt.powiat, COL_PRJ_GMINA, projekt.gmina,
		COL_PRJ_MIEJSCOWOSC, projekt.miejscowosc, COL_PRJ_ULICA, projekt.ulica,
		COL_PRJ_OZNN, projekt.ozn_nier, COL_PRJ_CEL, projekt.cel_pracy,
		COL_PRJ_UWAGI, projekt.uwagi, COL_PRJ_DATAPOM, projekt.data_pom,
		COL_PRJ_DATAMOD, projekt.data_mod, -1);
	return 0;
}

int treeProjekty_update_view(GtkWidget *treeview)
{
	sqlite3 *dbgeod;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	GtkTreeIter iter;
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;

	gtk_tree_store_clear(GTK_TREE_STORE(model));

 	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
		COL_PRJ_ID, 0, COL_PRJ_NAZWA, _("DOMYŚLNY"), COL_PRJ_WOJ, "-",
		COL_PRJ_POWIAT, "-", COL_PRJ_GMINA, "-", COL_PRJ_MIEJSCOWOSC, "-",
		COL_PRJ_ULICA, "-", COL_PRJ_OZNN, "-", COL_PRJ_CEL, "-", COL_PRJ_UWAGI, "-",
		COL_PRJ_DATAPOM, "-", COL_PRJ_DATAMOD, "-", -1);
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie projektow i umieszczenie poprzez funkcje callback
	sql_query = sqlite3_mprintf("select id, nazwa, wojewodztwo, powiat, gmina, "
		"miejscowosc, ulica, ozn_nier, cel_pracy, uwagi, data_pom, data_mod "
		"from projekty order by id;");
	
	rc = sqlite3_exec(dbgeod, sql_query, treeProjekty_callback, treeview, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	sqlite3_close(dbgeod);
	return 0;
}

// aktualizacja podanej wartosci w tabeli konfiguracja
int dbgeod_config_set (const char* key, const char* value)
{
	sqlite3 *dbgeod;
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}

	if (key == NULL || value == NULL)
		return 2;
	
	// modyfikacja konfiguracji w bazie danych
	sql_query = sqlite3_mprintf("replace into konfiguracja (key, value) values "
		"('%q', '%q');", key, value);
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	sqlite3_close(dbgeod);
	return 0;
}

int dbgeod_config_get (const char* key, char* value)
{
	sqlite3 *dbgeod;
	int rc;
	int nrow, ncolumn;
	char *sql_query = NULL;
	char **azRezult[CONFIG_VALUE_MAX_LEN];
	char *sql_err = NULL;

	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie punktow i umieszczenie w tabeli azRezult
	if (key != NULL)
		sql_query = sqlite3_mprintf("select value from konfiguracja where key='%q' limit 1;", key);
	else
		return -1;
	
	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);	
	
	if (nrow < 1) {
		g_strlcpy(value, "", CONFIG_VALUE_MAX_LEN);
		sqlite3_free_table(*azRezult);
		sqlite3_close(dbgeod);
		return -2; // brak takiej wartosci  w konfiguracji
	}

	if ( (*azRezult)[ncolumn] != NULL ) {
		g_strlcpy(value, (*azRezult)[ncolumn], CONFIG_VALUE_MAX_LEN);
	}

	sqlite3_free_table(*azRezult);
	sqlite3_close(dbgeod);
	return 0;
}

int dbgeod_projekt_get (int id_projekt, sProjekt &projekt)
{
	sqlite3 *dbgeod;
	int rc;
	int nrow, ncolumn;
	char *sql_query;
	char *sql_err = NULL;
	char **azRezult[255];
	char *txt = NULL;
	
	if (id_projekt == 0) {
		projekt.id = 0;
		sprintf(projekt.nazwa, _("DOMYŚLNY"));
		sprintf(projekt.wojewodztwo, "-");
		sprintf(projekt.powiat, "-");
		sprintf(projekt.gmina, "-");
		sprintf(projekt.miejscowosc, "-");
		sprintf(projekt.ulica, "-");
		sprintf(projekt.ozn_nier, "-");
		sprintf(projekt.cel_pracy, "-");
		sprintf(projekt.uwagi, "-");
		sprintf(projekt.data_mod, "-");
		sprintf(projekt.data_pom, "-");
		return 0;
	}
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie projektu i przypisanie wartosci do zmiennej
	sql_query = sqlite3_mprintf("select * from projekty where id=%d limit 1;", id_projekt);
	
	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	if (nrow < 1) {
		sqlite3_free_table(*azRezult);
		sqlite3_close(dbgeod);
		return -1; // nie znaleziono projektu
	}
		
	if ( (*azRezult)[ncolumn+0] != NULL ) {
		txt = g_strdup((*azRezult)[ncolumn+0]);
		projekt.id = atoi(txt);
		g_free(txt);
	}
	
	if ( (*azRezult)[ncolumn+1] != NULL )
		g_strlcpy(projekt.nazwa, (*azRezult)[ncolumn+1], PNAZWA_MAX_LEN);

	if ( (*azRezult)[ncolumn+2] != NULL )
		g_strlcpy(projekt.wojewodztwo, (*azRezult)[ncolumn+2], PWOJEWODZTWO_MAX_LEN);

	if ( (*azRezult)[ncolumn+3] != NULL )
		g_strlcpy(projekt.powiat, (*azRezult)[ncolumn+3], PPOWIAT_MAX_LEN);

	if ( (*azRezult)[ncolumn+4] != NULL )
		g_strlcpy(projekt.gmina, (*azRezult)[ncolumn+4], PGMINA_MAX_LEN);

	if ( (*azRezult)[ncolumn+5] != NULL )
		g_strlcpy(projekt.miejscowosc, (*azRezult)[ncolumn+5], PMIEJSCOWOSC_MAX_LEN);

	if ( (*azRezult)[ncolumn+6] != NULL )
		g_strlcpy(projekt.ulica, (*azRezult)[ncolumn+6], PULICA_MAX_LEN);

	if ( (*azRezult)[ncolumn+7] != NULL )
		g_strlcpy(projekt.ozn_nier, (*azRezult)[ncolumn+7], POZN_NIER_MAX_LEN);

	if ( (*azRezult)[ncolumn+8] != NULL )
		g_strlcpy(projekt.cel_pracy, (*azRezult)[ncolumn+8], PCEL_PRACY_MAX_LEN);

	if ( (*azRezult)[ncolumn+9] != NULL )
		g_strlcpy(projekt.uwagi, (*azRezult)[ncolumn+9], PUWAGI_MAX_LEN);

	if ( (*azRezult)[ncolumn+10] != NULL )
		g_strlcpy(projekt.data_pom, (*azRezult)[ncolumn+10], PDATA_POM_MAX_LEN);

	if ( (*azRezult)[ncolumn+11] != NULL )
		g_strlcpy(projekt.data_mod, (*azRezult)[ncolumn+11], PDATA_MOD_MAX_LEN);

	sqlite3_free_table(*azRezult);
	sqlite3_close(dbgeod);
	return 0;
}

int dbgeod_projekt_del(int projekt_id)
{
	sqlite3 *dbgeod;
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;
	GtkWidget *dialog;
	gint response;
	
	if (projekt_id == dbgeod_projekt()) {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, _("Nie można usunąć aktywnego projektu!"));
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
		return 0;
	}
	
	if (projekt_id == 0) {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_WARNING, GTK_BUTTONS_YES_NO, _("Nie można usunąć projektu domyślnego. Czy usunąć wszystkie punkty i obserwacje z nim powiązane?"));
	}
	else {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_WARNING, GTK_BUTTONS_YES_NO, _("Czy na pewno usunąć zaznaczony projekt i wszystkie punkty i obserwacje z nim powiązane?"));
	}
	response = gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
	if (response == GTK_RESPONSE_NO) return 0;
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}

	// usuniecie punktow nalezacych do danego projektu
	sql_query = sqlite3_mprintf("delete from punkty where projekt_id=%d;", projekt_id);
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);

	// usuniecie obserwacji nalezacych do danego projektu
	sql_query = sqlite3_mprintf("delete from obserwacje where projekt_id=%d;", projekt_id);
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);

	// usuniecie parametrow historii nalezacych do danego projektu
	sql_query = sqlite3_mprintf("delete from hist_parametry where hist_zadanie.projekt_id=%d "
		"and hist_parametry.zadanie_id = hist_zadanie.id;", projekt_id);
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	
	// usuniecie punktow historii nalezacych do danego projektu
	sql_query = sqlite3_mprintf("delete from hist_punkty where hist_zadanie.projekt_id=%d "
		"and hist_punkty.zadanie_id = hist_zadanie.id;", projekt_id);
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	
	// usuniecie historii nalezacych do danego projektu
	sql_query = sqlite3_mprintf("delete from hist_zadanie where projekt_id=%d;", projekt_id);
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);

	// usuniecie projektu z bazy danych
	if (projekt_id != 0)
	{
		sql_query = sqlite3_mprintf("delete from projekty where id=%d;", projekt_id);
		rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
		if (rc != SQLITE_OK) {
			fprintf(stderr, _("SQL error: %s\n"), sql_err);
			sqlite3_free(sql_err);
		}
		sqlite3_free(sql_query);
	}
	sqlite3_close(dbgeod);
	return 0;
}

int dbgeod_projekt_replace(sProjekt projekt, const int *projekt_id)
{
	sqlite3 *dbgeod;
	char *sql_query = NULL;
	char *sql_err = NULL;
	int rc;
	char *time_stamp = podaj_czas();

	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	
	if (projekt_id == NULL)
	{
		sql_query = sqlite3_mprintf("insert into projekty(nazwa, wojewodztwo, powiat, "
			"gmina, miejscowosc, ulica, ozn_nier, cel_pracy, uwagi, data_pom, data_mod) "
			"values('%q', '%q', '%q', '%q', '%q', '%q', '%q', '%q', '%q', '%q', '%q');",
			projekt.nazwa, projekt.wojewodztwo, projekt.powiat, projekt.gmina,
			projekt.miejscowosc, projekt.ulica, projekt.ozn_nier, projekt.cel_pracy,
			projekt.uwagi, projekt.data_pom, time_stamp);
	}
	else if (*projekt_id == 0)
	{
		delete [] time_stamp;
		return 0; // projekt domyslny, nie mozliwa modyfikacja		
	}
	else
	{
		sql_query = sqlite3_mprintf("update projekty set nazwa='%q', wojewodztwo='%q', "
			"powiat='%q', gmina='%q', miejscowosc='%q', ulica='%q', ozn_nier='%q', "
			"cel_pracy='%q', uwagi='%q', data_pom='%q', data_mod='%q' where id=%d;",
			projekt.nazwa, projekt.wojewodztwo, projekt.powiat, projekt.gmina,
			projekt.miejscowosc, projekt.ulica, projekt.ozn_nier, projekt.cel_pracy,
			projekt.uwagi, projekt.data_pom, time_stamp, *projekt_id);
	}
	
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	sqlite3_close(dbgeod);
	delete [] time_stamp;
	return 0;
}

// zapis daty modyfikacji aktywnego zbioru
int dbgeod_projekt_data_mod (void)
{
	sqlite3 *dbgeod;
	int rc;
	char *sql_query = NULL;
	char *czas = podaj_czas();
	char *sql_err = NULL;
	
	// zapis daty
	if (dbgeod_projekt() != 0) {
		rc = sqlite3_open(DB_NAME, &dbgeod);
		if (rc) {
			fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
			sqlite3_close(dbgeod);
			return 1;
		}

		sql_query = sqlite3_mprintf("update projekty set data_mod='%q' where id=%d;",
			czas, dbgeod_projekt());
		rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
		if (rc != SQLITE_OK) {
			fprintf(stderr, _("SQL error: %s\n"), sql_err);
			sqlite3_free(sql_err);
		}
		sqlite3_free(sql_query);
		sqlite3_close(dbgeod);
	}
	delete [] czas;
	return 0;
}

int dbgeod_projekt()
{
	char txt[CONFIG_VALUE_MAX_LEN];
	if (!dbgeod_config_get("projekt", txt))
		return atoi(txt);
	else
		return 0;
}


// aktualizacja podanej obserwacji w tabeli obserwacje
int dbgeod_obs_set (const char* punktl, const char* stanowisko, const char* punktp, double odczyt)
{
	sqlite3 *dbgeod;
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;

	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}

	// modyfikacja konfiguracji w bazie danych
	if (punktl != NULL && stanowisko != NULL && punktp != NULL) {
		sql_query = sqlite3_mprintf("replace into obserwacje (projekt_id, stanowisko, punktl,"
			"punktp, odczyt) values (%d, '%q', '%q', '%q', %.11f);",
			dbgeod_projekt(), stanowisko, punktl, punktp, odczyt);
			
		rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
		if (rc != SQLITE_OK) {
			fprintf(stderr, _("SQL error: %s\n"), sql_err);
			sqlite3_free(sql_err);
		}
		sqlite3_free(sql_query);
	}
	sqlite3_close(dbgeod);
	return 0;
}


// pobranie obserwacji z bazy danych
int dbgeod_obs_get (const char* punktl, const char* stanowisko, const char* punktp, double &odczyt)
{
	sqlite3 *dbgeod;
	int rc;
	int nrow, ncolumn;
	char *sql_query = NULL;
	char **azRezult[G_ASCII_DTOSTR_BUF_SIZE];
	char *sql_err = NULL;

	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie punktow i umieszczenie w tabeli azRezult
	if ((stanowisko != NULL) || (punktp != NULL))
		sql_query = sqlite3_mprintf("select odczyt from obserwacje where projekt_id=%d "
		"and stanowisko='%q' and punktl='%q' and punktp='%q' limit 1;",
		dbgeod_projekt(), stanowisko, punktl, punktp);
	else
		return -1;
	
	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	if (nrow < 1) {
		odczyt=0.0;
		sqlite3_free_table(*azRezult);
		sqlite3_close(dbgeod);
		return -2; // brak takiej wartosci  w konfiguracji
	}

	if ( (*azRezult)[ncolumn] != NULL ) {
		odczyt=g_ascii_strtod((*azRezult)[ncolumn], NULL); 
	}

	sqlite3_free_table(*azRezult);
	sqlite3_close(dbgeod);
	return 0;
}


// usuniecie obserwacji z bazy danych
int dbgeod_obs_del (unsigned int obs_id)
{
	sqlite3 *dbgeod;
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}

	// usuniecie obserwacji z bazy danych
	sql_query = sqlite3_mprintf("delete from obserwacje where projekt_id=%d and id=%d;",
		dbgeod_projekt(), obs_id);

	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	sqlite3_close(dbgeod);
	return 0;
}

// usuniecie wszystkich obserwacji z bazy danych
int dbgeod_obs_del_all()
{
	sqlite3 *dbgeod;
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;
	int response;

	GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_WARNING, GTK_BUTTONS_YES_NO, _("Czy na pewno usunąć wszystkie obserwacje powiązane z otwartym projektem?"));
	response = gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	if (response == GTK_RESPONSE_NO)
		return 2;

	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}

	// usuniecie obserwacji z bazy danych
	sql_query =sqlite3_mprintf("delete from obserwacje where projekt_id=%d;", dbgeod_projekt());

	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	sqlite3_close(dbgeod);
	return 0;
}

// sprawdzenie przy obserwacja jest katem
int dbgeod_obs_is_angle (const char* pktl)
{
	if (g_str_equal(pktl, "$Ox") || g_str_equal(pktl, "$Oy") || g_str_equal(pktl, "$Oz") ||
		g_str_equal(pktl, "$az") || g_str_equal(pktl, "$v") || g_str_equal(pktl, "$hz") ||
		!g_str_has_prefix(pktl, "$"))
		return 1;
	else
		return 0;
}


// usuniecie wpisu z historii
int dbgeod_hist_del (int id_zadanie)
{
	sqlite3 *dbgeod;
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;
	char **azRezult[255];
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}

	// gdy zadanie_id<0 usun ostatni wpis
	if (id_zadanie<0) {
		sql_query = sqlite3_mprintf("select max(id) from hist_zadanie where projekt_id=%d;", dbgeod_projekt());
		rc = sqlite3_get_table(dbgeod, sql_query, azRezult, NULL, NULL, &sql_err);
		if (rc != SQLITE_OK) {
			fprintf(stderr, _("SQL error: %s\n"), sql_err);
			sqlite3_free(sql_err);
		}
		sqlite3_free(sql_query);
		if ( (*azRezult)[1] != NULL ) id_zadanie = atoi((*azRezult)[1]);
		sqlite3_free_table(*azRezult);		
	}

	// usuniecie parametrow powiazanych z wpisem historii
	sql_query = sqlite3_mprintf("delete from hist_parametry where zadanie_id=%d;", id_zadanie);
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	
	// usuniecie punktow powiazanych z wpisem historii
	sql_query = sqlite3_mprintf("delete from hist_punkty where zadanie_id=%d;", id_zadanie);
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	
	// usuniecie wpisu historii z bazy danych
	sql_query = sqlite3_mprintf("delete from hist_zadanie where id=%d and projekt_id=%d;", id_zadanie, dbgeod_projekt());
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	
	sqlite3_close(dbgeod);
	return 0;
}
// usuniecie parametru lub punktu z historii
int dbgeod_hist_del (int id_zadanie, const char* nazwa, const char* table)
{
	sqlite3 *dbgeod;
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}

	// usuniecie parametrow powiazanych z wpisem historii
	sql_query = sqlite3_mprintf("delete from %q where zadanie_id=%d and nazwa='%q';", table, id_zadanie, nazwa);
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	
	sqlite3_close(dbgeod);
	return 0;
}

// dodanie parametru do historii
int dbgeod_hist_par_set (int id_zadanie, const char* nazwa, double value, int wynik)
{
	sqlite3 *dbgeod;
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}

	// modyfikacja parametrow powiazanych z wpisem historii
	sql_query = sqlite3_mprintf("replace into hist_parametry (zadanie_id, nazwa, wartosc, wynik) "
		"values (%d, '%q', %.10f, %d);", id_zadanie, nazwa, value, wynik);
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error par: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	
	sqlite3_close(dbgeod);
	return 0;
}


// dodanie punktu do historii
int dbgeod_hist_point_set (int id_zadanie, const char* nazwa, const char* nr_pkt, int wynik)
{
	sqlite3 *dbgeod;
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}

	// modyfikacja parametrow powiazanych z wpisem historii
	sql_query = sqlite3_mprintf("replace into hist_punkty (zadanie_id, nazwa, nr_punkt, wynik) "
		"values (%d, '%q', '%q', %d);", id_zadanie, nazwa, nr_pkt, wynik);
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	
	sqlite3_close(dbgeod);
	return 0;
}

// dodanie pozycji do historii
int dbgeod_hist_add (const char* funkcja, int &zadanie_id, unsigned int kolejnosc)
{
	sqlite3 *dbgeod;
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;
	char **azRezult[255];
	char *czas = podaj_czas();
	unsigned int max_kolejnosc;
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		delete [] czas;
		return 1;
	}

	// maksymalna wartosc kolejnosci
	sql_query = sqlite3_mprintf("select max(kolejnosc) from hist_zadanie where projekt_id=%d;", dbgeod_projekt());
	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	if ( (*azRezult)[1] != NULL )
		max_kolejnosc = atoi((*azRezult)[1]);
	else
		max_kolejnosc = 0;
	sqlite3_free(sql_query);
	sqlite3_free_table(*azRezult);	

	// poprawienie kolejnosci
	if (kolejnosc>max_kolejnosc || kolejnosc == 0)
		kolejnosc = max_kolejnosc+1;
	else {
		sql_query = sqlite3_mprintf("update hist_zadanie set kolejnosc=kolejnosc+1 "
			"where projekt_id=%d and kolejnosc >= %d;", dbgeod_projekt(), kolejnosc);
		rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
		if (rc != SQLITE_OK) {
			fprintf(stderr, _("SQL error: %s\n"), sql_err);
			sqlite3_free(sql_err);
		}
		sqlite3_free(sql_query);
	}
	
	// dodanie pozycji w historii
	sql_query = sqlite3_mprintf("insert into hist_zadanie (projekt_id, kolejnosc, funkcja, data_mod) "
		"values (%d, %d, '%q', '%q');", dbgeod_projekt(), kolejnosc, funkcja, czas);
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	
	// zadanie_id
	sql_query = sqlite3_mprintf("select id from hist_zadanie where projekt_id=%d and kolejnosc=%d;", dbgeod_projekt(), kolejnosc);
	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	if ( (*azRezult)[1] != NULL )
		zadanie_id = atoi((*azRezult)[1]);
	else
		zadanie_id = -1;
	sqlite3_free(sql_query);
	sqlite3_free_table(*azRezult);	

	sqlite3_close(dbgeod);
	delete [] czas;
	return 0;
}


// dodanie pozycji do historii
int dbgeod_hist_modtime (int zadanie_id)
{
	sqlite3 *dbgeod;
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}

	sql_query = sqlite3_mprintf("update hist_zadanie set data_mod='%q' where id=%d;", podaj_czas(), zadanie_id);
	rc = sqlite3_exec(dbgeod, sql_query, NULL, NULL, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	sqlite3_close(dbgeod);
	return 0;
}


// pobranie nr punktu z historii
int dbgeod_hist_point_get (int id_zadanie, const char* nazwa, char* nr_pkt)
{
	sqlite3 *dbgeod;
	int rc;
	int nrow, ncolumn;
	char *sql_query = NULL;
	char *sql_err = NULL;
	char **azRezult[255];

	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie punktow i umieszczenie w tabeli azRezult
	if (nazwa != NULL)
		sql_query = sqlite3_mprintf("select nr_punkt from hist_punkty where zadanie_id=%d "
		"and nazwa='%q' limit 1;", id_zadanie, nazwa);
	else
		return -1;

	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);

	if (nrow < 1) {
		g_strlcpy(nr_pkt, "", NR_MAX_LEN);
		sqlite3_free_table(*azRezult);
		sqlite3_close(dbgeod);
		return -2; // brak takiej wartosci
	}
	
	if ( (*azRezult)[ncolumn] != NULL )
		g_strlcpy(nr_pkt, (*azRezult)[ncolumn], NR_MAX_LEN);
	else
		g_strlcpy(nr_pkt, "", NR_MAX_LEN);

	sqlite3_free_table(*azRezult);
	sqlite3_close(dbgeod);
	return 0;
}


// pobranie opisu z histrii
int dbgeod_hist_get (int id_zadanie, char* nazwa, int & kolejnosc)
{
	sqlite3 *dbgeod;
	int rc;
	int nrow, ncolumn;
	char *sql_query = NULL;
	char *sql_err = NULL;
	char **azRezult[255];

	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie punktow i umieszczenie w tabeli azRezult
	if (nazwa != NULL)
		sql_query = sqlite3_mprintf("select funkcja, kolejnosc from hist_zadanie where id=%d "
		"limit 1;", id_zadanie);
	else
		return -1;

	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);

	if (nrow < 1) {
		sqlite3_free_table(*azRezult);
		sqlite3_close(dbgeod);
		g_strlcpy(nazwa, "", 255);
		kolejnosc = 0;
		return -2; // brak takiej wartosci
	}
	
	if ( (*azRezult)[ncolumn+0] != NULL )
		g_strlcpy(nazwa, (*azRezult)[ncolumn+0], 255);
	else
		g_strlcpy(nazwa, "", 255);

	if ( (*azRezult)[ncolumn+1] != NULL )
		kolejnosc = atoi((*azRezult)[ncolumn+1]);
	else
		kolejnosc = 0;

	sqlite3_free_table(*azRezult);
	sqlite3_close(dbgeod);
	return 0;
}



// pobranie nr punktu z historii
int dbgeod_hist_par_get (int id_zadanie, const char* nazwa, double & value)
{
	sqlite3 *dbgeod;
	int rc;
	int nrow, ncolumn;
	char *sql_query = NULL;
	char *sql_err = NULL;
	char **azRezult[255];

	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie punktow i umieszczenie w tabeli azRezult
	if (nazwa != NULL)
		sql_query = sqlite3_mprintf("select wartosc from hist_parametry where zadanie_id=%d "
		"and nazwa='%q' limit 1;", id_zadanie, nazwa);
	else
		return -1;
	
	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);

	if (nrow > 0) {
		if ( (*azRezult)[1] != NULL )
			value = g_ascii_strtod((*azRezult)[1], NULL);
	}
	else value = 0.0;
		
	sqlite3_free_table(*azRezult);
	sqlite3_close(dbgeod);
	
	if (nrow < 1)
		return -2; //brak takiej wartosci
	else
		return 0;
}

int treeObserwacje_callback(void *treeview, int argc, char **argv, char **azColName)
{	
	char *obs_id = NULL, *obs_l = NULL, *obs_c = NULL, *obs_p = NULL, *obs_v = NULL, *obs_function = NULL;
	char *obs_v_loc = NULL;
	char *obs_temp = NULL;
	double obs_vf =0.0;
	int id_int=0;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;

	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	
	if(argv[0]!=NULL) { obs_id = g_strdup(argv[0]); id_int=atoi(&obs_id[0]); } //id
	if(argv[1]!=NULL) { obs_l = g_strdup(argv[1]); } // punktl
	if(argv[2]!=NULL) { obs_c = g_strdup(argv[2]); } // stanowisko
	if(argv[3]!=NULL) { obs_p = g_strdup(argv[3]); } // punktp
	if(argv[4]!=NULL) { obs_v = g_strdup(argv[4]); obs_vf=g_ascii_strtod(obs_v, NULL); } // odczyt
	
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));

	if (g_str_has_prefix(obs_c,"$") && g_str_has_prefix(obs_p,"$"))
		obs_function = g_strdup(_("odl. rzutu L"));
	else if (g_str_has_prefix(obs_c,"$"))
		obs_function = g_strdup(_("odl. L od prostej"));
	else if (g_str_equal(obs_l, "$x"))
		obs_function = g_strdup(_("współrzędna X"));
	else if (g_str_equal(obs_l, "$y"))
		obs_function = g_strdup(_("współrzędna Y"));
	else if (g_str_equal(obs_l, "$z"))
		obs_function = g_strdup(_("współrzędna Z"));
	else if (g_str_equal(obs_l, "$Ox"))
		obs_function = g_strdup(_("obrót Ox"));
	else if (g_str_equal(obs_l, "$Oy"))
		obs_function = g_strdup(_("obrót Oy"));
	else if (g_str_equal(obs_l, "$Oz"))
		obs_function = g_strdup(_("obrót Oz"));
	else if (g_str_equal(obs_l, "$dl"))
		obs_function = g_strdup(_("odleglość"));
	else if (g_str_equal(obs_l, "$dx"))
		obs_function = g_strdup(_("przyrost wsp. x"));
	else if (g_str_equal(obs_l, "$dy"))
		obs_function = g_strdup(_("przyrost wsp. y"));
	else if (g_str_equal(obs_l, "$dz"))
		obs_function = g_strdup(_("przyrost wsp. z"));
	else if (g_str_equal(obs_l, "$ds"))
		obs_function = g_strdup(_("długość ukośna"));
	else if (g_str_equal(obs_l, "$az"))
		obs_function = g_strdup(_("azymut"));
	else if (g_str_equal(obs_l, "$v"))
		obs_function = g_strdup(_("odczyt V"));
	else if (g_str_equal(obs_l, "$hz"))
		obs_function = g_strdup(_("odczyt Hz"));
	else
		obs_function = g_strdup(_("kąt"));

	if (!g_str_has_prefix(obs_l, "$"))
		obs_v_loc = geod_kat_from_rad(obs_vf, TRUE);
	else if (dbgeod_obs_is_angle(obs_l)) {
		obs_v_loc = geod_kat_from_rad(obs_vf, TRUE);
		g_free(obs_l);
		obs_l=g_strdup("");
	}
	else if (g_str_has_prefix(obs_l, "$") && g_str_has_prefix(obs_c, "$")) {
		obs_v_loc = g_strdup_printf("%.*f", dok_xy_int, obs_vf);
		obs_temp = obs_l;
		obs_l = geod_prefix_rem(obs_temp);
		g_free(obs_temp);
		
		obs_temp = obs_c;
		obs_c = geod_prefix_rem(obs_temp);
		g_free(obs_temp);
		if (g_str_has_prefix(obs_p, "$")) {
			obs_temp = obs_p;
			obs_p = geod_prefix_rem(obs_temp);
			g_free(obs_temp);
		}
	}
	else {
		obs_v_loc = g_strdup_printf("%.*f", dok_xy_int, obs_vf);
		g_free(obs_l);
		obs_l=g_strdup("");
	}		

 	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
		COL_OBS_ID, id_int, COL_OBS_TYP, obs_function,
		COL_OBS_L, obs_l, COL_OBS_C, obs_c, COL_OBS_P, obs_p,
		COL_OBS_V, obs_v_loc, COL_OBS_V_DOUBLE, obs_vf, -1);
	
	g_free(obs_id);
	g_free(obs_l);
	g_free(obs_c);
	g_free(obs_p);
	g_free(obs_v);
	g_free(obs_v_loc);
	g_free(obs_function);
	return 0;
}

int treeObserwacje_update_view(GtkWidget *treeview)
{
	sqlite3 *dbgeod;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;

	gtk_tree_store_clear(GTK_TREE_STORE(model));
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie obserwacji i umieszczenie poprzez funkcje callback w tabeli
	sql_query = sqlite3_mprintf("select id, punktl, stanowisko, punktp, odczyt "
		"from obserwacje where projekt_id='%d' order by id;", dbgeod_projekt());
	
	rc = sqlite3_exec(dbgeod, sql_query, treeObserwacje_callback, treeview, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	sqlite3_close(dbgeod);
	return 0;
}


int treeHistoria_update_view(GtkWidget *treeview)
{
	sqlite3 *dbgeod;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;

	gtk_tree_store_clear(GTK_TREE_STORE(model));
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie zadan w historii i umieszczenie poprzez funkcje callback w tabeli
	sql_query = sqlite3_mprintf("select id, kolejnosc, funkcja, data_mod "
		"from hist_zadanie where projekt_id=%d order by kolejnosc;", dbgeod_projekt());
	
	rc = sqlite3_exec(dbgeod, sql_query, treeHistoria_callback, treeview, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	sqlite3_close(dbgeod);
	return 0;
}

int treeHistoria_callback(void *treeview, int argc, char **argv, char **azColName)
{	
	char *hist_id = NULL, *hist_ord = NULL, *hist_func = NULL, *hist_date = NULL;
	char pkt_wynik[CONFIG_VALUE_MAX_LEN], pkt_dane[CONFIG_VALUE_MAX_LEN];
	int id_int=0, ord_int=0;
	
	if(argv[0]!=NULL) { hist_id = g_strdup(argv[0]); id_int=atoi(&hist_id[0]); } // id
	if(argv[1]!=NULL) { hist_ord = g_strdup(argv[1]); ord_int=atoi(&hist_id[0]);} // kolejnosc
	if(argv[2]!=NULL) { hist_func = g_strdup(argv[2]); } // funkcja
	if(argv[3]!=NULL) { hist_date = g_strdup(argv[3]); } // data

	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));

	dbgeod_hist_points_find(id_int, pkt_dane, 0);
	dbgeod_hist_points_find(id_int, pkt_wynik, 1);
 	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_HIS_ID, id_int,
		COL_HIS_ORDER, ord_int, COL_HIS_FUNCTION, hist_func,
		COL_HIS_DATA, pkt_dane, COL_HIS_REZULT, pkt_wynik, COL_HIS_MOD, hist_date, -1);
	
	g_free(hist_id);
	g_free(hist_ord);
	g_free(hist_func);
	g_free(hist_date);
	return 0;
}

int dbgeod_hist_points_find(unsigned int hist_id, char* punkty, int wynik)
{
	sqlite3 *dbgeod;
	int rc; int i;
	int nrow = 0, ncolumn = 0;
	char *sql_query = NULL;
	char *sql_err = NULL;
	char **azRezult[255];
	char *punkty_old = NULL, *punkty_func = NULL; /// poprawic punkty_func
	
	g_strlcpy(punkty, "", CONFIG_VALUE_MAX_LEN); /// poprawic, mozliwy wyciek pamieci
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie punktow i umieszczenie w zwracanej liscie
	sql_query=sqlite3_mprintf("select nr_punkt from hist_punkty, hist_zadanie "
		" where hist_zadanie.id=%d and hist_zadanie.id=hist_punkty.zadanie_id and wynik=%d;",
		hist_id, wynik);
	
	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	if (nrow < 1) {
		sqlite3_free_table(*azRezult);
		sqlite3_close(dbgeod);
		return -3; // brak punktow
	}

	if ( (*azRezult)[1] != NULL )
		punkty_func = g_strdup((*azRezult)[1]);
	for (i=2; i <= nrow; i++) {
		if ( (*azRezult)[i] != NULL ) {
			punkty_old = punkty_func;
			punkty_func = g_strdup_printf("%s,%s", punkty_old, (*azRezult)[i]);
			g_free(punkty_old);
		}	
	}

	sqlite3_free_table(*azRezult);
	sqlite3_close(dbgeod);
	g_strlcpy(punkty, punkty_func, CONFIG_VALUE_MAX_LEN);
	return 0;
}

int dbgeod_obs_save(FILE *plik, guint &nobs)
{
	sqlite3 *dbgeod;
	int rc; int i;
	int nrow = 0, ncolumn = 0;
	char *sql_query = NULL;
	char *sql_err = NULL;
	char **azRezult[255];
	char *pktl = NULL, *pktc = NULL, *pktp = NULL;
	double value;
	
	nobs = 0;
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie obserwacji i sapisanie ich do pliku
	sql_query=sqlite3_mprintf("select punktl, stanowisko, punktp, odczyt from obserwacje "
		"where projekt_id=%d;", dbgeod_projekt());
	
	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	if (nrow < 1) {
		sqlite3_free_table(*azRezult);
		sqlite3_close(dbgeod);
		nobs = 0;
		return -3; // brak obserwacji
	}
		
	for (i=1; i <= nrow; i++) {
		if ( (*azRezult)[i*ncolumn+0] != NULL )
			pktl = g_strdup((*azRezult)[i*ncolumn+0]);
		else
			pktl = g_strdup("");
		if ( (*azRezult)[i*ncolumn+1] != NULL )
			pktc = g_strdup((*azRezult)[i*ncolumn+1]);
		else
			pktc = g_strdup("");
		if ( (*azRezult)[i*ncolumn+2] != NULL )
			pktp = g_strdup((*azRezult)[i*ncolumn+2]);
		else
			pktp = g_strdup("");
		if ( (*azRezult)[i*ncolumn+3] != NULL )
			value = g_ascii_strtod((*azRezult)[i*ncolumn+3], NULL);
		else
			value = 0.0;

		fprintf(plik, "%s %s %s %.10f\n", pktl, pktc, pktp, value);
		
		g_free(pktl);
		g_free(pktc);
		g_free(pktp);
	}
	nobs = nrow;
	sqlite3_free_table(*azRezult);
	sqlite3_close(dbgeod);
	return 0;
}

// funkcja wyszukuje punkty dla ktorych mozliwe jest wyznaczenie wspolrzednych
// metoda domiarow prostokatnych
int dbgeod_obs_domiary_find(GSList** punkty, const char *bazaPP, const char *bazaPK)
{
	sqlite3 *dbgeod;
	int rc; int i;
	int nrow = 0, ncolumn = 0;
	char *sql_query = NULL;
	char *sql_err = NULL;
	char **azRezult[255];
	char *bPP = NULL, *bPK = NULL;
	
	if (bazaPP == NULL) bPP = g_strdup("%"); else bPP = g_strdup(bazaPP);
	if (bazaPK == NULL) bPK = g_strdup("%"); else bPK = g_strdup(bazaPK);
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie punktow i umieszczenie w zwracanej liscie
	sql_query=sqlite3_mprintf("select punktl from obserwacje where punktl like '$%%' "
		"and stanowisko like '$%q' and punktp like '$%q' and projekt_id=%d and punktl "
		"in (select punktl from obserwacje where punktl like '$%%' and stanowisko "
		"like '$%q' and punktp like '%q' and projekt_id=%d);", /// poprawic ost like
		bPP, bPK, dbgeod_projekt(), bPP, bPK, dbgeod_projekt());
	
	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	if (nrow < 1) {
		sqlite3_free_table(*azRezult);
		sqlite3_close(dbgeod);
		return -3; // brak punktow
	}

	for (i=1; i <= nrow; i++)
		if ( (*azRezult)[i*ncolumn+0] != NULL )
			*punkty = g_slist_append(*punkty, g_strdup((*azRezult)[i*ncolumn+0]));

	sqlite3_free_table(*azRezult);
	sqlite3_close(dbgeod);
	return 0;
}


// funkcja wyszukuje punkty na podstawie ktorych mozliwe jest okreslenie
// parametrow transformacji pomiedzy ukladami
int dbgeod_points_transf_find (GSList** punkty)
{
	sqlite3 *dbgeod;
	int rc; int i;
	int nrow = 0, ncolumn = 0;
	char *sql_query = NULL;
	char *sql_err = NULL;
	char **azRezult[255];
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie punktow i umieszczenie w zwracanej liscie
	sql_query=sqlite3_mprintf("select nr from punkty where typ=%d and projekt_id=%d "
		"and nr in (select nr from punkty where typ=%d and projekt_id=%d);",
		GEODPKT_OSNOWA, dbgeod_projekt(), GEODPKT_OSNOWAL, dbgeod_projekt());
	
	rc = sqlite3_get_table(dbgeod, sql_query, azRezult, &nrow, &ncolumn, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	if (nrow < 1) {
		sqlite3_free_table(*azRezult);
		sqlite3_close(dbgeod);
		return -3; // brak punktow
	}
	
	for (i=1; i <= nrow; i++)
		if ( (*azRezult)[i*ncolumn+0] != NULL )
			*punkty = g_slist_append(*punkty, g_strdup((*azRezult)[i*ncolumn+0]));

	sqlite3_free_table(*azRezult);
	sqlite3_close(dbgeod);
	return 0;
}


int treeDialogHistoriaEdytuj_update_view(GtkWidget *treeview, int zadanie_id)
{
	sqlite3 *dbgeod;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	int rc;
	char *sql_query = NULL;
	char *sql_err = NULL;

	gtk_tree_store_clear(GTK_TREE_STORE(model));
	
	rc = sqlite3_open(DB_NAME, &dbgeod);
	if (rc) {
		fprintf(stderr, _("Nie można otworzyć bazy danych: %s\n"), sqlite3_errmsg(dbgeod));
		sqlite3_close(dbgeod);
		return 1;
	}
	// wyszukanie parametrow i umieszczenie poprzez funkcje callback
	// w tabeli w przypadku gdy nie okreslono zadnych punktow zwracane sa wszystkie
	sql_query = sqlite3_mprintf("select id, nazwa, wartosc from hist_parametry "
		"where zadanie_id=%d and wynik=0;", zadanie_id);
	
	rc = sqlite3_exec(dbgeod, sql_query, treeDialogHistoriaEdytuj_callback, treeview, &sql_err);
	if (rc != SQLITE_OK) {
		fprintf(stderr, _("SQL error: %s\n"), sql_err);
		sqlite3_free(sql_err);
	}
	sqlite3_free(sql_query);
	sqlite3_close(dbgeod);
	return 0;
}


int treeDialogHistoriaEdytuj_callback(void *treeview, int argc, char **argv, char **azColName)
{
	char *nazwa = NULL;
	int id=0;
	double wartosc=0.0;
	
	if(argv[0]!=NULL) id = atoi(g_strdup(argv[0]));
	if(argv[1]!=NULL) nazwa = g_strdup(argv[1]);
	if(argv[2]!=NULL) wartosc = g_strtod(argv[2], NULL);
	
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));

 	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
		COL_HE_ID, id, COL_HE_NAZWA, nazwa, COL_HE_VALUE_DOUBLE, wartosc,
		COL_HE_EDITED, FALSE, -1);
	if (g_str_has_prefix(nazwa, "Kat") || g_str_has_prefix(nazwa, "Azymut") || g_str_has_prefix(nazwa, "Kier"))
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
			COL_HE_ANGLE, TRUE, COL_HE_VALUE, geod_kat_from_rad(wartosc), -1);
	else
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
			COL_HE_ANGLE, FALSE, COL_HE_VALUE, g_strdup_printf("%.10f", wartosc), -1);
	
	g_free(nazwa);
	return 0;
}
