/***************************************************************************
 *            dbgeod.h
 *  OBSŁUGA BAZY DANYCH SQLite W PROGRAMIE
 *  Sun May 29 14:47:05 2005
 *  Copyright  2005  Bogusław Ciastek
 *  ciacho@z.pl
 ****************************************************************************/
#ifndef __DBGEOD_H__
#define __DBGEOD_H__

#include <sqlite3.h>
#include "stdgeod.h"


const unsigned int PNAZWA_MAX_LEN = 255;
const unsigned int PWOJEWODZTWO_MAX_LEN = 25;
const unsigned int PPOWIAT_MAX_LEN = 35;
const unsigned int PGMINA_MAX_LEN = 35;
const unsigned int PMIEJSCOWOSC_MAX_LEN = 50;
const unsigned int PULICA_MAX_LEN = 75;
const unsigned int POZN_NIER_MAX_LEN = 25;
const unsigned int PCEL_PRACY_MAX_LEN = 255;
const unsigned int PUWAGI_MAX_LEN = 255;
const unsigned int PDATA_POM_MAX_LEN = 17;
const unsigned int PDATA_MOD_MAX_LEN = 17;

const unsigned int HFUNKCJA_MAX_LEN = 50;
const unsigned int HDATA_MOD_MAX_LEN = 17;
const unsigned int HNAZWA_MAX_LEN = 11;

struct sProjekt
{
	int id;
	char nazwa[PNAZWA_MAX_LEN];
	char wojewodztwo[PWOJEWODZTWO_MAX_LEN];
	char powiat[PPOWIAT_MAX_LEN];
	char gmina[PGMINA_MAX_LEN];
	char miejscowosc[PMIEJSCOWOSC_MAX_LEN];
	char ulica[PULICA_MAX_LEN];
	char ozn_nier[POZN_NIER_MAX_LEN];
	char cel_pracy[PCEL_PRACY_MAX_LEN];
	char uwagi[PULICA_MAX_LEN];
	char data_pom[PDATA_POM_MAX_LEN];
	char data_mod[PDATA_MOD_MAX_LEN];
};

int dbgeod_database_create(GtkProgressBar *pbar);

// tabela punkty
int dbgeod_point_add(cPunkt punkt);

int dbgeod_point_add_config(cPunkt punkt, bool *zmiana = NULL, char *addkonfig = NULL);

int dbgeod_point_del(int point_id, const char* pkt_nr = NULL);

int dbgeod_point_update(int point_id, cPunkt punkt, bool pkt_nr = FALSE);

int dbgeod_points_find(const char* pkt_nr, GSList** punkty, GSList** punkty_id, int typ = GEODPKT_ALL);

int dbgeod_point_find(const char* pkt_nr, cPunkt *punkt = NULL, int typ = GEODPKT_ALL);

// wyszukuje punkt na podstawie numeru i w razie potrzeby go transformuje
int dbgeod_pointt_find(const char* pkt_nr, cPunkt *punkt);

int dbgeod_points_info (unsigned int &npoints, double &xmin, double &xmax,
	double &ymin, double &ymax);

int dbgeod_points_transf_find (GSList** punkty);


// funkcje ustawiajace zawartosc tabel
int treeHistoria_callback(void *treeview, int argc, char **argv, char **azColName);

int treeHistoria_update_view(GtkWidget *treeview);

int treePunkty_callback(void *treeview, int argc, char **argv, char **azColName);

int treePunkty_update_view(GtkWidget *treeview, const char* pkt_nr = NULL);

int treeDialogKod_callback(void *treeview, int argc, char **argv, char **azColName);

int treeDialogKod_update_view(GtkWidget *treeview, const char* find_str = NULL);

int treeProjekty_callback(void *treeview, int argc, char **argv, char **azColName);

int treeProjekty_update_view(GtkWidget *treeview);

int treeObserwacje_update_view(GtkWidget *treeview);

int treeObserwacje_callback(void *treeview, int argc, char **argv, char **azColName);

int treeDialogHistoriaEdytuj_callback(void *treeview, int argc, char **argv, char **azColName);

int treeDialogHistoriaEdytuj_update_view(GtkWidget *treeview, int zadanie_id);


// konfiguracja
int dbgeod_config_get (const char* key, char* value);

int dbgeod_config_set (const char* key, const char* value);

// tabela projekty
int dbgeod_projekt_get (int id_obiekt, sProjekt &projekt);

int dbgeod_projekt_del(int projekt_id);

int dbgeod_projekt_replace(sProjekt projekt, const int *projekt_id = NULL);

int dbgeod_projekt_data_mod (void);

int dbgeod_projekt();

// obserwacje
int dbgeod_obs_set (const char* punktl, const char* stanowisko, const char* punktp, double odczyt);

int dbgeod_obs_get (const char* punktl, const char* stanowisko, const char* punktp, double &odczyt);

int dbgeod_obs_del (unsigned int obs_id);

int dbgeod_obs_del_all();

int dbgeod_obs_is_angle (const char* pktl);

int dbgeod_obs_save(FILE *plik, guint &nobs);

int dbgeod_obs_domiary_find(GSList** punkty, const char *bazaPP = NULL, const char *bazaPK = NULL);


// historia
int dbgeod_hist_del (int id_zadanie);

int dbgeod_hist_del (int id_zadanie, const char* nazwa, const char* table);

int dbgeod_hist_par_set (int id_zadanie, const char* nazwa, double value, int wynik=0);

int dbgeod_hist_point_set (int id_zadanie, const char* nazwa, const char* nr_pkt, int wynik=0);

int dbgeod_hist_add (const char* funkcja, int &zadanie_id, unsigned int kolejnosc = 0);

int dbgeod_hist_par_get (int id_zadanie, const char* nazwa, double & value);

int dbgeod_hist_point_get (int id_zadanie, const char* nazwa, char* nr_pkt);

int dbgeod_hist_points_find(unsigned int hist_id, char* punkty, int wynik=0);

int dbgeod_hist_get (int id_zadanie, char* nazwa, int & kolejnosc);

int dbgeod_hist_modtime (int zadanie_id);
#endif /* __DBGEOD_H__ */
