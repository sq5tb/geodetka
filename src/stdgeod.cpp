/***************************************************************************
 *            stdgeod.cpp
 *
 *  PODSTAWOWE OBLICZENIA I FUNKCJE GEODEZYJNE
 *
 *  Fri May  6 04:30:28 2005
 *  Copyright  2005  Bogusław Ciastek
 *  ciacho@z.pl
 ****************************************************************************/

#include <time.h>
#include <gtk/gtk.h>
#include "stdgeod.h"
#include "stdio.h"
#include "winconst.h"
#include "dbgeod.h"

cPunkt::cPunkt():
jegoX(0.0),
jegoY(0.0),
jegoH(0.0)
{
	char lokalny[CONFIG_VALUE_MAX_LEN];
	dbgeod_config_get("lokalny", lokalny);
	if (g_str_equal(lokalny, "1"))
		jegoTyp = GEODPKT_WYNIKL;
	else if (g_str_equal(lokalny, "0"))
		jegoTyp = GEODPKT_WYNIK;
	else
		jegoTyp = GEODPKT_OTHER;
	
	g_strlcpy(jegoNr, "", NR_MAX_LEN);
	g_strlcpy(jegoKod, "", KOD_MAX_LEN);
}
cPunkt::cPunkt (const char* Nr, double X, double Y, double H, const char* Kod, int Typ):
jegoX(X),
jegoY(Y),
jegoH(H),
jegoTyp(Typ)
{
	g_strlcpy(jegoNr, Nr, NR_MAX_LEN);
	g_strlcpy(jegoKod, Kod, KOD_MAX_LEN);
}

cPunkt::~cPunkt()
{ }

// POMOCNICZE SYMBOLE RACHUNKOWE
cForma::cForma(unsigned int rozmiar=1) :
jejRozmiar(rozmiar)
{
	jejForma = new tForma[jejRozmiar];
}

cForma::cForma(const cForma & rhs)
{
	jejForma = new tForma[jejRozmiar];
	*jejForma = *rhs.jejForma;
}

cForma::~cForma()
{
	delete [] jejForma;
}	

void cForma::setF(double a, double b, double c, double d,unsigned int poz=1)
{
	if (poz <= jejRozmiar) {
		jejForma[poz-1].a = a;
		jejForma[poz-1].b = b;
		jejForma[poz-1].c = c;
		jejForma[poz-1].d = d;
	}
}

void cForma::setV(double value, tFormaIndex poz=a, unsigned int forma=1)
{
	if (forma <= jejRozmiar)
		switch (poz) {
			case a: jejForma[forma-1].a = value; break;
			case b: jejForma[forma-1].b = value; break;
			case c: jejForma[forma-1].c = value; break;
			case d: jejForma[forma-1].d = value; break;
		}
}

double cForma::getV(tFormaIndex poz=a, unsigned int forma=1) const
{
	double value = 0.0;
	if (forma <= jejRozmiar)
		switch (poz) {
			case a: value = jejForma[forma-1].a; break;
			case b: value = jejForma[forma-1].b; break;
			case c: value = jejForma[forma-1].c; break;
			case d: value = jejForma[forma-1].d; break;
		}
	return value;
}

// funkcja pierwsza, iloczyn wyznacznikowy
double cForma::f1() const
{
	unsigned int i;
	double wynik = 0.0;
	for (i=0; i<jejRozmiar; i++)
		wynik = wynik + jejForma[i].a*jejForma[i].d-jejForma[i].b*jejForma[i].c;
	return wynik;
}

// funkcja druga, albo iloczy kolumnowy
double cForma::f2() const
{
	unsigned int i;
	double wynik = 0.0;
	for (i=0; i<jejRozmiar; i++)
		wynik = wynik + jejForma[i].a*jejForma[i].c + jejForma[i].b*jejForma[i].d;
	return wynik;
}

// funkcja zerowa, albo iloraz glowny
double cForma::f0() const
{
	double licznik, mianownik;
	licznik = f1();
	mianownik = f2();
	if (mianownik == 0.0)
		return 0; //null
	else
		return licznik/mianownik;
}

// funkcja pierwsza prosta dolna
double cForma::f1pd() const
{
	unsigned int i;
	double licznik=0, mianownik=0;

	licznik = f1();
	for (i=0; i<jejRozmiar; i++)
		mianownik = mianownik + jejForma[i].c + jejForma[i].d;
	
	if (mianownik==0.0)
		return 0; //null
	else
		return licznik/mianownik;
}

// funkcja pierwsza prosta gorna
double cForma::f1pg() const
{
	unsigned int i;
	double licznik=0, mianownik=0;

	licznik = f1();
	for (i=0; i<jejRozmiar; i++)
		mianownik = mianownik + jejForma[i].a + jejForma[i].b;
	
	if (mianownik==0.0)
		return 0; //null
	else
		return licznik/mianownik;
}

// funkcja druga prosta dolna
double cForma::f2pd() const
{
	unsigned int i;
	double licznik=0, mianownik=0;

	licznik = f2();
	for (i=0; i<jejRozmiar; i++)
		mianownik = mianownik + jejForma[i].c + jejForma[i].d;
	
	if (mianownik==0.0)
		return 0; //null
	else
		return licznik/mianownik;
}

// funkcja druga prosta gorna
double cForma::f2pg() const
{
	unsigned int i;
	double licznik=0, mianownik=0;

	licznik = f2();
	for (i=0; i<jejRozmiar; i++)
		mianownik = mianownik + jejForma[i].a + jejForma[i].b;
	
	if (mianownik==0.0)
		return 0; //null
	else
		return licznik/mianownik;
}

// funkcja pierwsza kwadratowa dolna
double cForma::f1kd() const
{
	unsigned int i;
	double licznik=0, mianownik=0;

	licznik = f1();
	for (i=0; i<jejRozmiar; i++)
		mianownik = mianownik + pow(jejForma[i].c, 2) + pow(jejForma[i].d, 2);
	
	if (mianownik==0.0)
		return 0; //null
	else
		return licznik/mianownik;
}

// funkcja pierwsza kwadratowa gorna
double cForma::f1kg() const
{
	unsigned int i;
	double licznik=0, mianownik=0;

	licznik = f1();
	for (i=0; i<jejRozmiar; i++)
		mianownik = mianownik + pow(jejForma[i].a, 2) + pow(jejForma[i].b, 2);
	
	if (mianownik==0.0)
		return 0; //null
	else
		return licznik/mianownik;
}

// funkcja druga kwadratowa dolna
double cForma::f2kd() const
{
	unsigned int i;
	double licznik=0, mianownik=0;

	licznik = f2();
	for (i=0; i<jejRozmiar; i++)
		mianownik = mianownik + pow(jejForma[i].c, 2) + pow(jejForma[i].d, 2);
	
	if (mianownik==0.0)
		return 0; //null
	else
		return licznik/mianownik;
}

// funkcja druga kwadratowa gorna
double cForma::f2kg() const
{
	unsigned int i;
	double licznik=0, mianownik=0;

	licznik = f2();
	for (i=0; i<jejRozmiar; i++)
		mianownik = mianownik + pow(jejForma[i].a, 2) + pow(jejForma[i].b, 2);
	
	if (mianownik==0.0)
		return 0; //null
	else
		return licznik/mianownik;
}

////////////////////////////////////////////////////////////////////////////////

// Normalizacja kata do przedzialu <k0-PI,k0+PI), domyslnie <0, 2PI)
double norm_kat (double kat, double kat0)
{
	while (kat-kat0 >= M_PI) { kat=kat-2*M_PI; };
	while (kat0-kat > M_PI) { kat=kat+2*M_PI; };
	return kat;
}

// Dlugosc odcinka na podstawie wspolrzednych badz przyrostow wspolrzednych
double dlugosc (double dx, double dy)
{
	double dl;
	dl=sqrt(dx*dx+dy*dy);
	return dl;
}
double dlugosc (double wsp_xp, double wsp_yp, double wsp_xk, double wsp_yk)
{
	return dlugosc(wsp_xk-wsp_xp,wsp_yk-wsp_yp);
}
double dlugosc (cPunkt pPoczatek, cPunkt pKoniec)
{
	return dlugosc(pKoniec.getX()-pPoczatek.getX(), pKoniec.getY()-pPoczatek.getY());
}

// Obliczenie azymutu na podstawie wspolrzednych
int azymut (double dy, double dx, double &azym)
{
	if (dx == 0.0 && dy == 0.0) {
		azym = 0.0;
		return GEODERR_DATA;
	}
	
	azym = atan2(dy, dx);
	azym = norm_kat(azym);
	return 0;
}
int azymut (cPunkt pPoczatek, cPunkt pKoniec, double & azym)
{
	return azymut(pKoniec.getY()-pPoczatek.getY(), pKoniec.getX()-pPoczatek.getX(), azym);
}


// wspolrzedne punktu koncowego na podstawie wspolrzednych punktu poczatkowego,
// azymutu i dlugosci odcinka
void punkt_biegun(cPunkt pkt_p, double azymut, double dlugosc, cPunkt & wynik)
{
	double xk, yk;
	
	xk = pkt_p.getX() + dlugosc*cos(azymut);
	yk = pkt_p.getY() + dlugosc*sin(azymut); 
	
	wynik.setXY(xk, yk);
}

// Obliczenie wspolrzednych punktu przeciecia dwoch prostych, z ktorych kazda
// wyznaczona jest przez dwa punkty dane.
int przeciecie_prostych (cPunkt pp1, cPunkt pk1, cPunkt pp2, cPunkt pk2,
		cPunkt &wyznaczany)
{
	cForma forma_k;
	cForma forma_s;
	double x_wyzn, y_wyzn;
	
	forma_k.setV(pp2.getX()-pp1.getX(), a);
	forma_k.setV(pp2.getY()-pp1.getY(), b);
	forma_k.setV(pk2.getX()-pp2.getX(), c);
	forma_k.setV(pk2.getY()-pp2.getY(), d);

	forma_s.setV(pk1.getX()-pp1.getX(), a);
	forma_s.setV(pk1.getY()-pp1.getY(), b);
	forma_s.setV(pk2.getX()-pp2.getX(), c);
	forma_s.setV(pk2.getY()-pp2.getY(), d);

	if (forma_s.f1() == 0.0) return GEODERR_NO_RESULT; // proste rownolegle
	
	x_wyzn = pp1.getX() + (forma_k.f1()/forma_s.f1())*(pk1.getX()-pp1.getX());
	y_wyzn = pp1.getY() + (forma_k.f1()/forma_s.f1())*(pk1.getY()-pp1.getY());
	wyznaczany.setXY(x_wyzn, y_wyzn);
	return 0;
}

// Obliczenie kata ze wspolrzednych
double kat (cPunkt stanowisko, cPunkt lewy, cPunkt prawy)
{
	double kat;
	cForma forma_kat;
	forma_kat.setV(lewy.getX()-stanowisko.getX(), a);
	forma_kat.setV(lewy.getY()-stanowisko.getY(), b);
	forma_kat.setV(prawy.getX()-stanowisko.getX(), c);
	forma_kat.setV(prawy.getY()-stanowisko.getY(), d);

	kat = atan2(forma_kat.f1(), forma_kat.f2()); // Uwaga, zwraca 0 gdy takie same punkty
	return norm_kat(kat);
}

// Obliczenie wspolrzednych punktow zdjetych metoda domiarow prostokatnych
// w najprostszej formie obliczenie wspolrzednych punktu lezacego na prostej
// okreslonej przez dwa punkty gdy podana jest odleglosc i kierunek jej
// odlozenia (+/-)
int domiar_prostokatny (cPunkt poczatek, cPunkt koniec, cPunkt &wynik,
	double biezaca, double domiar, double dlugosc_pom)
{
	cForma domiar_forma;
	
	// sprawdzenie czy poczatek != koniec
	if (dlugosc(poczatek, koniec)==0.0) return GEODERR_DATA;
	
	if (dlugosc_pom == 0.0)
		dlugosc_pom = dlugosc(poczatek, koniec);
	
	domiar_forma.setV((koniec.getX()-poczatek.getX())/dlugosc_pom, a);
	domiar_forma.setV((koniec.getY()-poczatek.getY())/dlugosc_pom, b);
	domiar_forma.setV(domiar, c);
	domiar_forma.setV(biezaca, d);
	
	wynik.setX(poczatek.getX() + domiar_forma.f1());
	wynik.setY(poczatek.getY() + domiar_forma.f2());
	return 0;
}

// Rzutowanie punktow na prosta wyznaczona przez dwa punkty
int rzut_prosta (cPunkt poczatek, cPunkt koniec, cPunkt rzutowany,
	double &rzut_biezaca, double &rzut_domiar)
{
	cForma rzutowanie_forma;
	double dlugosc_w;
	
	dlugosc_w = dlugosc(poczatek, koniec);
	
	if (dlugosc_w==0.0) return GEODERR_DATA; // sprawdzenie czy poczatek != koniec
	
	rzutowanie_forma.setV((koniec.getX()-poczatek.getX())/dlugosc_w, a);
	rzutowanie_forma.setV((koniec.getY()-poczatek.getY())/dlugosc_w, b);
	rzutowanie_forma.setV(rzutowany.getX()-poczatek.getX(), c);
	rzutowanie_forma.setV(rzutowany.getY()-poczatek.getY(), d);
	
	rzut_domiar = rzutowanie_forma.f1();
	rzut_biezaca = rzutowanie_forma.f2();
	return 0;
}

// Wciecie katowe w przod
int wciecie_katowe_w_przod (cPunkt pLewy, cPunkt pPrawy, double kLewy,
		double kPrawy, cPunkt &pWyznaczany)
{
	if (kLewy <= 0 || kPrawy <=0 || kLewy+kPrawy >= M_PI)
		return GEODERR_NO_RESULT;
	
	double xWyznaczany, yWyznaczany;
	double azymut_lp;
	double dlugosc_lp = dlugosc(pLewy, pPrawy);
	azymut(pLewy, pPrawy, azymut_lp);
	double dlugosc_lw = sin(kPrawy)/sin(kLewy+kPrawy)*dlugosc_lp;
	
	xWyznaczany = pLewy.getX()+dlugosc_lw*cos(azymut_lp-kLewy);
	yWyznaczany = pLewy.getY()+dlugosc_lw*sin(azymut_lp-kLewy);
	pWyznaczany.setXY(xWyznaczany, yWyznaczany);
	return 0;
}

// Wciecie katowe w przod na podstawie formy
int wciecie_katowe_w_przod_forma (cPunkt pLewy, cPunkt pPrawy, double kLewy,
		double kPrawy, cPunkt &pWyznaczany)
{
	if (kLewy <= 0 || kPrawy <=0 || kLewy+kPrawy >= M_PI)
		return GEODERR_NO_RESULT;
	
	cForma wciecie_forma(2);
	wciecie_forma.setF(pPrawy.getX(), pPrawy.getY(), -1, cos(kLewy)/sin(kLewy), 1);
	wciecie_forma.setF(pLewy.getX(), pLewy.getY(), 1, cos(kPrawy)/sin(kPrawy), 2);
	pWyznaczany.setXY(wciecie_forma.f1pd(), wciecie_forma.f2pd());
	return 0;
}

// Wciecie liniowe
int wciecie_liniowe(cPunkt pLewy, cPunkt pPrawy, double dLewe, double dPrawe,
		cPunkt &pWyznaczany)
{
	double dPodstawa = dlugosc(pLewy, pPrawy);
	
	// warunek trojkata
	if (0.5*(dLewe+dPrawe+dPodstawa) <=0) return GEODERR_DATA;
	if (0.5*(dLewe+dPrawe+dPodstawa)-dLewe <=0) return GEODERR_DATA;
	if (0.5*(dLewe+dPrawe+dPodstawa)-dPrawe <=0) return GEODERR_DATA;
	if (0.5*(dLewe+dPrawe+dPodstawa)-dPodstawa <=0) return GEODERR_DATA;
	if(dPodstawa == 0.0 || dLewe == 0.0 || dPrawe == 0)
		return GEODERR_NO_RESULT;
		
	double cos_kLewy = (pow(dLewe,2)+pow(dPodstawa,2)-pow(dPrawe,2))/(2*dLewe*dPodstawa);
	double azymut_lp;
	double kLewy;
	
	azymut(pLewy, pPrawy, azymut_lp);
	kLewy=acos(cos_kLewy);
	
	double xWyznaczany = pLewy.getX()+dLewe*cos(azymut_lp-kLewy);
	double yWyznaczany = pLewy.getY()+dLewe*sin(azymut_lp-kLewy);
	pWyznaczany.setXY(xWyznaczany, yWyznaczany);
	return 0;
}

// Wciecie liniowe na podstawie formy
int wciecie_liniowe_forma (cPunkt pLewy, cPunkt pPrawy, double dLewe,
		double dPrawe, cPunkt &pWyznaczany)
{
	double dPodstawa = dlugosc(pLewy, pPrawy);
	
	// warunek trojkata
	if (0.5*(dLewe+dPrawe+dPodstawa) <=0) return GEODERR_DATA;
	if (0.5*(dLewe+dPrawe+dPodstawa)-dLewe <=0) return GEODERR_DATA;
	if (0.5*(dLewe+dPrawe+dPodstawa)-dPrawe <=0) return GEODERR_DATA;
	if (0.5*(dLewe+dPrawe+dPodstawa)-dPodstawa <=0) return GEODERR_DATA;
	if(dPodstawa == 0.0 || dLewe == 0.0 || dPrawe == 0)
		return GEODERR_NO_RESULT;
		
	double CP = pow(dPodstawa,2)+pow(dLewe,2)-pow(dPrawe,2);
	double CL = pow(dPodstawa,2)+pow(dPrawe,2)-pow(dLewe,2);
	double CW = pow(dPrawe,2)+pow(dLewe,2)-pow(dPodstawa,2);
	double _4P = CP*CL + CL*CW + CW*CP;
	if (_4P != 0.0) _4P = sqrt(_4P); else return GEODERR_MATH;
		
	cForma wciecie_forma(2);
	wciecie_forma.setF(pPrawy.getX(), pPrawy.getY(), -1*_4P, CP, 1);
	wciecie_forma.setF(pLewy.getX(), pLewy.getY(), _4P, CL, 2);
	pWyznaczany.setXY(wciecie_forma.f1pd(), wciecie_forma.f2pd());
	return 0;
}

// Ogolny przypadek wciecia w przod
	double kIter(double dLewe, cPunkt pPrawyC, cPunkt pPrawyL, cPunkt pLewyC, double azLewe) // funkcja pomocnicza
	{
		cPunkt pBiegun;
		punkt_biegun(pLewyC, azLewe, dLewe, pBiegun);
		return kat(pPrawyC, pPrawyL, pBiegun);
	}


int wciecie_w_przod_ogolne(cPunkt pLewyC, cPunkt pLewyP, double kLewy,
        cPunkt pPrawyC, cPunkt pPrawyL, double kPrawy, cPunkt &pWyznaczany)
{
	/// #define kIter(dLewe) ( kat(pPrawyC, pPrawyL, punkt_biegun(pLewyC, azLewe, (dLewe))) )
	
	// obliczenie niezbednych azymutow
	double azLewyCP;
	double azPrawyCL;
	azymut(pLewyC, pLewyP, azLewyCP);
	azymut(pPrawyC, pPrawyL, azPrawyCL);
	double azLewe = azLewyCP - kLewy;
	
	// kontrola istnienia rozwiazania
	double kPrawyMin = kat(pPrawyC, pPrawyL, pLewyC);
	double kPrawyMax = norm_kat(azLewe-azPrawyCL);
	
	if (kPrawy <= kPrawyMin || kPrawy>= kPrawyMax)
		return GEODERR_NO_RESULT; // polproste nie przecinaja sie
	
	// iteracja algorytmem bisekcji
	const double eps_dl = 0.0001;
	double dMin = 0;
	double dMax = MAXFLOAT;
	double sDlugosc = 0;

	if ((kIter(0.0, pPrawyC, pPrawyL, pLewyC,azLewe)-kPrawy)*(kIter(MAXFLOAT, pPrawyC, pPrawyL, pLewyC,azLewe)-kPrawy) > 0.0)
		return GEODERR_ALGORITHM; // brak rozwiazania w zadanym przedziale

	while (fabs(dMax-dMin)>eps_dl) {
		sDlugosc = 0.5*(dMin+dMax);
		if (kIter(sDlugosc, pPrawyC, pPrawyL, pLewyC,azLewe)-kPrawy>0)
			dMax = sDlugosc;
		else
			dMin = sDlugosc;
	}
	punkt_biegun(pLewyC, azLewe, sDlugosc, pWyznaczany);
	return 0;
}

// Wciecie wstecz
int wciecie_wstecz(cPunkt pLewy, cPunkt pSrodkowy, cPunkt pPrawy, double katLS,
		double katLP, cPunkt &pWyznaczany)
{
	double azLS; azymut(pLewy, pSrodkowy, azLS);
	double azLP; azymut(pLewy, pPrawy, azLP);
	double azLW;
	double dlLS = dlugosc(pLewy, pSrodkowy);
	double dlLP = dlugosc(pLewy, pPrawy);
	double dlLW;
		
	double azLW_licznik = dlLS*sin(katLP)*sin(azLS-katLS)-dlLP*sin(katLS)*sin(azLP-katLP);
	double azLW_mianownik = dlLS*sin(katLP)*cos(azLS-katLS)-dlLP*sin(katLS)*cos(azLP-katLP);
	if (azLW_licznik == 0 && azLW_mianownik == 0)
		return GEODERR_MATH;
	azymut(azLW_licznik, azLW_mianownik, azLW);
	
	if (katLS != 0)
		dlLW = dlLS*sin(katLS+azLW-azLS)/sin(katLS);
	else if (katLP != 0)
		dlLW = dlLP*sin(katLP+azLW-azLP)/sin(katLP);
	else
		return GEODERR_ALGORITHM;
	
	punkt_biegun(pLewy, azLW, dlLW, pWyznaczany);
	return 0;
}

// Wciecie wstecz obliczane na podstawie formy rachunkowej
int wciecie_wstecz_forma(cPunkt pLewy, cPunkt pSrodkowy, cPunkt pPrawy, double katLS,
		double katLP, cPunkt &pWyznaczany)
{
	cForma forma_F(2);
	cForma forma_f(1);
	forma_F.setF(pSrodkowy.getX()-pLewy.getX(), pSrodkowy.getY()-pLewy.getY(), \
		cos(katLS)/sin(katLS), 1.0, 1);
	forma_F.setF(pPrawy.getX()-pLewy.getX(), pPrawy.getY()-pLewy.getY(), \
		-1.0*cos(katLP)/sin(katLP), -1.0, 2);
	forma_f.setF(pSrodkowy.getX()-pLewy.getX(), pSrodkowy.getY()-pLewy.getY(), \
		cos(katLS)/sin(katLS), 1.0, 1);

	cForma forma_k(1);
	if (forma_F.f2() == 0) return GEODERR_MATH; // nie mozna policzyc forma_F.f0
	forma_k.setF(forma_f.f1(), forma_f.f2(), forma_F.f0(), 1.0);
	
	double x_wyzn = pLewy.getX() + forma_k.f1kd();
	double y_wyzn = pLewy.getY() + -1.0*forma_F.f0()*forma_k.f1kd();
	pWyznaczany.setXY(x_wyzn, y_wyzn);
	return 0;
}

/* Wciecie w bok jako szczegolny przypadek wciecia wstecz
       S\ <)SW     /P
         \______ ./ <)LP
         L       W   
*/
int wciecie_w_bok(cPunkt pLewy, cPunkt pSrodkowy, cPunkt pPrawy, double katLP,
		cPunkt &pWyznaczany, double katSW = M_PI)
{
	if (norm_kat(katLP) == 0.0 || norm_kat(katLP) == M_PI)
		return GEODERR_NOT_ONE_RESULT; // brak jednoznacznego rozwiazania
	
	double azLS; azymut(pLewy, pSrodkowy, azLS);
	double azLP; azymut(pLewy, pPrawy, azLP);
	double dlLP = dlugosc(pLewy, pPrawy);
	
	double azLW = azLS+katSW;
	double dlLW = dlLP*sin(azLW-azLP+katLP)/sin(katLP);
	
	punkt_biegun(pLewy, azLW, dlLW, pWyznaczany);
	return 0;
}

/*  Zadanie Mareka, wyznaczenie pary dwoch punktow
    L2\          /P1
    ST2\.______./ST1
       /        \
    P2/          \L1
*/
int zadanie_Mareka(cPunkt st1L, cPunkt st1P, cPunkt st2L, cPunkt st2P,
		double kat1L, double kat1P, double kat2L, double kat2P,
		cPunkt &st1, cPunkt &st2)
{
	bool odwrocony = FALSE;
	const double eps = 0.0000001;
	const double eps_dl = 0.01;
	double kat2Lwyl;
	double mi_min = 0+eps;
	double kat1;
	int blad;
	
	if ((kat1L+kat1P) > M_PI)
		kat1 = 2*M_PI-(kat1L+kat1P);
	else if ((kat1L+kat1P) < M_PI) {
		odwrocony = TRUE;
		kat1 = kat1L+kat1P;
	}
	else
		return GEODERR_ALGORITHM; // nie mozna okreslic ST1 za pomoca wciecia
	
	double mi_max = M_PI-kat1-eps;
	double kat2LwylMIN, kat2LwylMAX, s_mi;

	// poszukiwanie minimum funkcji odległości ST1 ST2 celem okreslenia
	// minimalnej wartosci kata dla dalszej czesci algorytmu
	bool min_koniec;
	double krok = mi_max-mi_min, pstart = mi_min;
	double odl12, odl12_poprzednia;

	if (odwrocony == TRUE)
		blad = wciecie_katowe_w_przod(st1P, st1L, M_PI-kat1-pstart, pstart, st1);
	else
		blad = wciecie_katowe_w_przod(st1L, st1P, pstart, M_PI-kat1-pstart, st1);
	
	if(blad != GEODERR_OK)
		return blad;
	blad = wciecie_w_bok(st1, st1L, st2P, kat2P, st2, kat1L);
	if(blad != GEODERR_OK)
		return blad;
	odl12 = dlugosc(st1, st2);
	
	while (fabs(krok) > eps) {
		min_koniec = FALSE;
		odl12_poprzednia = odl12;
		pstart += krok;
		if (pstart > mi_max) {
			min_koniec = TRUE;
			pstart = mi_max;
		}
		if (pstart < mi_min) {
			min_koniec = TRUE;
			pstart = mi_min;
		}

		if (odwrocony)
			blad = wciecie_katowe_w_przod(st1P, st1L, M_PI-kat1-pstart, pstart, st1);
		else
			blad = wciecie_katowe_w_przod(st1L, st1P, pstart, M_PI-kat1-pstart, st1);
		if(blad != GEODERR_OK) return blad;

		blad = wciecie_w_bok(st1, st1L, st2P, kat2P, st2, kat1L);
		if(blad != GEODERR_OK) return blad;

		odl12 = dlugosc(st1, st2);
		if (odl12_poprzednia<odl12 || min_koniec)
			krok *= -0.3;
	}
	if (!truncf(odl12/eps_dl))
		mi_min = pstart+2.0*eps;
	else
		mi_min = 0.0+2.0*eps;

	// kat minimalny
	if (odwrocony)
		blad = wciecie_katowe_w_przod(st1P, st1L, M_PI-kat1-mi_min, mi_min, st1);
	else
		blad = wciecie_katowe_w_przod(st1L, st1P, mi_min, M_PI-kat1-mi_min, st1);
	if(blad != GEODERR_OK)
		return blad;

	blad = wciecie_w_bok(st1, st1L, st2P, kat2P, st2, kat1L);
	if(blad != GEODERR_OK)
		return blad;

	kat2LwylMIN = kat(st2, st2L, st1);

	// kat maksymalny
	if (odwrocony)
		blad = wciecie_katowe_w_przod(st1P, st1L, M_PI-kat1-mi_max, mi_max, st1);
	else
		blad = wciecie_katowe_w_przod(st1L, st1P, mi_max, M_PI-kat1-mi_max, st1);
	if(blad != GEODERR_OK)
		return blad;

	blad = wciecie_w_bok(st1, st1L, st2P, kat2P, st2, kat1L);
	if(blad != GEODERR_OK)
		return blad;

	kat2LwylMAX = kat(st2, st2L, st1);

	// iteracja algorytmem bisekcji
	if ((kat2LwylMIN-kat2L)*(kat2LwylMAX-kat2L) > 0.0)
		return GEODERR_ALGORITHM; // brak rozwiazania w przedziale
	
	while (fabs(mi_max-mi_min)>eps) {
		s_mi = 0.5*(mi_min+mi_max);
		if (odwrocony)
			blad = wciecie_katowe_w_przod(st1P, st1L, M_PI-kat1-s_mi, s_mi, st1);
		else
			blad = wciecie_katowe_w_przod(st1L, st1P, s_mi, M_PI-kat1-s_mi, st1);
		if(blad != GEODERR_OK) return blad;

		blad = wciecie_w_bok(st1, st1L, st2P, kat2P, st2, kat1L);
		if(blad != GEODERR_OK) return blad;

		kat2Lwyl = norm_kat(kat(st2, st2L, st1));
		
		if ((kat2LwylMIN-kat2L)*(kat2Lwyl-kat2L)<0) {
			mi_max = s_mi;
			kat2LwylMAX = kat2Lwyl;
		}
		else {
			mi_min = s_mi;
			kat2LwylMIN = kat2Lwyl;
		}
	}
	return 0;
}

/*  Zadanie Mareka rozwiazne przy pomocy form rachunkowych
    L2\          /P1
    ST2\.______./ST1
       /        \
    P2/          \L1
*/
int zadanie_Mareka_forma(cPunkt st1L, cPunkt st1P, cPunkt st2L, cPunkt st2P,
		double kat1L, double kat1P, double kat2L, double kat2P,
		cPunkt &st1, cPunkt &st2)
{
	cForma forma_F(2);
	cForma forma_fi(2);
	
	if (norm_kat(kat1L) == 0 || norm_kat(kat1L) == M_PI)
		return GEODERR_MATH;
	if (norm_kat(kat1P) == 0 || norm_kat(kat1P) == M_PI)
		return GEODERR_MATH;
	if (norm_kat(kat2L) == 0 || norm_kat(kat2L) == M_PI)
		return GEODERR_MATH;
	if (norm_kat(kat2P) == 0 || norm_kat(kat2P) == M_PI)
		return GEODERR_MATH;

	forma_F.setF(st1P.getX(), st1P.getY(), cos(kat1P)/sin(kat1P), 1.0, 1);
	forma_F.setF(st1L.getX(), st1L.getY(), -1.0*cos(kat1L)/sin(-kat1L), -1.0, 2);
	
	forma_fi.setF(st2L.getX(), st2L.getY(), cos(M_PI-kat2L)/sin(M_PI-kat2L), 1.0, 1);
	forma_fi.setF(st2P.getX(), st2P.getY(), -1.0*cos(kat2P+M_PI)/sin(kat2P+M_PI), -1.0, 2);
	
	double azAB_licznik = forma_F.f1pd() - forma_fi.f1pd();
	double azAB_mianownik = forma_F.f2pd() - forma_fi.f2pd();
	double azAB;
	
	if (azymut(-1.0*azAB_licznik, azAB_mianownik, azAB) != GEODERR_OK)
		return GEODERR_DATA;
	
	double az1L = azAB + kat1P; // a1
	double az1P = azAB - kat1L; // a2
	double az2P = azAB - kat2L + M_PI; // b3
	double az2L = azAB + kat2P + M_PI; // b4 

	if (az1L == 0.5*M_PI || az1L == 3.0/2.0*M_PI)
		return GEODERR_MATH;
	if (az1P == 0.5*M_PI || az1P == 3.0/2.0*M_PI)
		return GEODERR_MATH;
	if (az2P == 0.5*M_PI || az2P == 3.0/2.0*M_PI)
		return GEODERR_MATH;
	if (az2L == 0.5*M_PI || az2L == 3.0/2.0*M_PI)
		return GEODERR_MATH;

	cForma forma_f(1);
	cForma forma_h(1);
	forma_f.setF(st1L.getX()-st1P.getX(), st1L.getY()-st1P.getY(), tan(az1P), -1.0);
	forma_h.setF(st2P.getX()-st2L.getX(), st2P.getY()-st2L.getY(), tan(az2L), -1.0);
	
	if (tan(az1P)-tan(az1L) == 0.0)
		return GEODERR_MATH;
	if (tan(az2L)-tan(az2P) == 0.0)
		return GEODERR_MATH;
	
	double dx1L = forma_f.f2()/(tan(az1P)-tan(az1L));
	double dy1L = dx1L*tan(az1L);
	
	double dx2P = forma_h.f2()/(tan(az2L)-tan(az2P)); // 3b
	double dy2P = dx2P*tan(az2P); // 3b
	
	st1.setXY(st1P.getX()+dx1L, st1P.getY()+dy1L);
	st2.setXY(st2L.getX()+dx2P, st2L.getY()+dy2P);
	return 0;
}

// Wciecie azymutale w przod
int wciecie_azymutalne_w_przod (cPunkt pLewy, cPunkt pPrawy, double az_lw,
		double az_pw, cPunkt &pWyznaczany)
{
	if (sin(az_lw-az_pw)==0)
		return GEODERR_NOT_ONE_RESULT; // kierunki prostopadle badz rownolegle

	double az_lp;
	double dlugosc_lp = dlugosc(pLewy, pPrawy);
	azymut(pLewy, pPrawy, az_lp);
	double dlugosc_pw = sin(az_lp-az_lw)/sin(az_lw-az_pw)*dlugosc_lp;
	
	punkt_biegun(pPrawy, az_pw, dlugosc_pw, pWyznaczany);
	return 0;
}

// Wciecie azymutale wstecz
int wciecie_azymutalne_wstecz (cPunkt pLewy, cPunkt pPrawy, double az_wl,
		double az_wp, cPunkt &pWyznaczany)
{
	if (sin(az_wp-az_wl)==0)
		return GEODERR_NOT_ONE_RESULT; // kierunki prostopadle badz rownolegle

	double az_lp;
	double dlugosc_lp = dlugosc(pLewy, pPrawy);
	
	azymut(pLewy, pPrawy, az_lp);
	double dlugosc_pw = sin(az_lp-az_wl)/sin(az_wp-az_wl)*dlugosc_lp;
		
	punkt_biegun(pPrawy, az_wp-M_PI, dlugosc_pw, pWyznaczany);
	return 0;
}

// Obliczanie pola wieloboku na podstawie wspolrzednych jego wierzcholkow
double pole_wielobok (cPunkt wierzcholek[], unsigned int ilosc_pkt)
{
	double pole2 = 0;
	unsigned int i;
	double yp, yn;
	
	if (ilosc_pkt < 3) return 0; // odcinek lub punkt nie ma powierzchni
	
	for (i=0; i < ilosc_pkt; i++)
	{
		if (i == 0) {
			yn=wierzcholek[i+1].getY();
			yp=wierzcholek[ilosc_pkt-1].getY();
		}
		else if (i == ilosc_pkt-1) {
			yn=wierzcholek[0].getY();
			yp=wierzcholek[i-1].getY();
		}
		else {
			yn=wierzcholek[i+1].getY();
			yp=wierzcholek[i-1].getY();
		}
		pole2 += wierzcholek[i].getX()*(yn-yp);
	}
	return pole2/2;
}

// Transformacja Helmerta dla wielu punktow dostosowania - obliczenie parametrow
int transformacja_helmerta_par (cPunkt pkt_dost[][2], unsigned int ilosc_pkt,
	double *u, double *v, double *b_xp, double *b_yp, double *b_xw, double *b_yw)
{
	if (ilosc_pkt<2)
		return GEODERR_DATA; // wymagane sa min. 2 punkty dostosowania
	
	unsigned int i=0;
	cForma forma_parametry(ilosc_pkt);
	double xp_biegun = 0, yp_biegun = 0;
	double xw_biegun = 0, yw_biegun = 0;
	double par_u = 0 , par_v = 0;
	
	for (i=0; i<ilosc_pkt; i++)
	{
		xp_biegun += pkt_dost[i][0].getX() / ilosc_pkt;
		xw_biegun += pkt_dost[i][1].getX() / ilosc_pkt;
		yp_biegun += pkt_dost[i][0].getY() / ilosc_pkt;
		yw_biegun += pkt_dost[i][1].getY() / ilosc_pkt;
	}
	
	for (i=0; i<ilosc_pkt; i++)
		forma_parametry.setF(
			pkt_dost[i][0].getX()-xp_biegun, \
			pkt_dost[i][0].getY()-yp_biegun, \
			pkt_dost[i][1].getX()-xw_biegun, \
			pkt_dost[i][1].getY()-yw_biegun, i+1 \
	    );
	par_u = forma_parametry.f1kg();
	par_v = forma_parametry.f2kg();
	if (u != NULL) *u = par_u;
	if (v != NULL) *v = par_v;
	if (b_xp != NULL) *b_xp = xp_biegun;
	if (b_yp != NULL) *b_yp = yp_biegun;
	if (b_xw != NULL) *b_xw = xw_biegun;
	if (b_yw != NULL) *b_yw = yw_biegun;
	return 0;	
}

// Transformacja Helmerta dla wielu punktow dostosowania
int transformacja_helmerta (cPunkt pkt_dost[][2], unsigned int ilosc_pkt,
	cPunkt pkt_up, cPunkt & pkt_uw)
{
	int blad;
	double par_u, par_v, xp_biegun, yp_biegun, xw_biegun, yw_biegun;
	
	blad = transformacja_helmerta_par(pkt_dost, ilosc_pkt, &par_u, &par_v,
		&xp_biegun, &yp_biegun, &xw_biegun, &yw_biegun);
	if (blad != GEODERR_OK) return blad;
	
	blad = transformacja_helmerta (par_u, par_v, xp_biegun, yp_biegun,
		xw_biegun, yw_biegun, pkt_up, pkt_uw);
	if (blad != GEODERR_OK) return blad;
	return 0;
}

// Transformacja Helmerta dla wielu punktow dostosowania - przy znanych paramatrach
int transformacja_helmerta (double par_u, double par_v, double xp_biegun,
	double yp_biegun, double xw_biegun, double yw_biegun,
	cPunkt pkt_up, cPunkt & pkt_uw)
{
	cForma forma_przyrosty;
	forma_przyrosty.setF(pkt_up.getX()-xp_biegun, pkt_up.getY()-yp_biegun, par_u, par_v);
	pkt_uw.setXY(xw_biegun+forma_przyrosty.f1(), yw_biegun+forma_przyrosty.f2());
	return 0;
}

// Przeciecie okregu z linia prosta wyznaczona przez 2 punkty
int przeciecie_okrag_prosta (cPunkt pktOkragS, double promien, cPunkt pktProstaP,
	cPunkt pktProstaK, cPunkt & pktWynik1, cPunkt & pktWynik2)
{
	double bs, h, c;
	int blad;
	blad = rzut_prosta(pktProstaP, pktProstaK, pktOkragS, bs, h);
	if (blad != GEODERR_OK) return blad;
	if (fabs(h) > promien) return GEODERR_NO_RESULT;
	
	c = sqrt(pow(promien,2)-pow(h,2));
	domiar_prostokatny(pktProstaP, pktProstaK, pktWynik1, bs-c);
	domiar_prostokatny(pktProstaP, pktProstaK, pktWynik2, bs+c);
	if (c==0) return 1; else return 0; // gdy tylko jedno rozwiazanie zwraca 1
}

// Wyznaczenie srodka okregu o danym promieniu stycznego do dwoch prostych
int okrag_wpasowanie_2proste (cPunkt pktPrLP, cPunkt pktPrLK, cPunkt pktPrPP,
	cPunkt pktPrPK, double promien, cPunkt & pktSrodek)
{
	int blad;
	cPunkt pktPrzeciecie;
	double katPrzeciecie, azL, odlPS;
	cForma kat;

	blad = przeciecie_prostych(pktPrLP, pktPrLK, pktPrPP, pktPrPK, pktPrzeciecie);
	if (blad != GEODERR_OK) return blad;

	azymut(pktPrLP, pktPrLK,azL);
	
	kat.setF(pktPrLK.getX()-pktPrLP.getX(), pktPrLK.getY()-pktPrLP.getY(),
		pktPrPK.getX()-pktPrPP.getX(), pktPrPK.getY()-pktPrPP.getY());
	katPrzeciecie = atan2(kat.f1(), kat.f2());
	
	if (sin(0.5*katPrzeciecie) == 0.0)
		blad = GEODERR_NO_RESULT;
	else {
		odlPS = promien/sin(0.5*katPrzeciecie);
		punkt_biegun(pktPrzeciecie, azL+0.5*katPrzeciecie, odlPS, pktSrodek);
		blad = GEODERR_OK;
	}
	return blad;
}

// Wyznaczenie srodka i promienia okregu na podstawie 3 punktow lezacych na okregu
int okrag_wpasowanie_3punkty (cPunkt pkt1, cPunkt pkt2, cPunkt pkt3,
	cPunkt & pktSrodek, double & promien)
{
	cPunkt pktLewy, pktPrawy;
	double azLewy, azPrawy;
	int blad;
	pktLewy.setX(0.5*(pkt1.getX()+pkt2.getX()));
	pktLewy.setY(0.5*(pkt1.getY()+pkt2.getY()));
	pktPrawy.setX(0.5*(pkt2.getX()+pkt3.getX()));
	pktPrawy.setY(0.5*(pkt2.getY()+pkt3.getY()));
	blad = azymut(pkt1, pkt2, azLewy);
	if (blad != GEODERR_OK) return blad;
	azymut(pkt2, pkt3, azPrawy);
	if (blad != GEODERR_OK) return blad;
	blad = wciecie_azymutalne_w_przod(pktLewy, pktPrawy, azLewy+M_PI_2,
		azPrawy+M_PI_2, pktSrodek);
	if (blad != GEODERR_OK) return blad;
	promien = dlugosc(pktSrodek, pkt1);
	return 0;
}

// Wyznaczenie punktow stycznosci okregu z prosta przechodzaca przez dany punkt
// nie lezacy na okregu
int okrag_styczna_punkt(cPunkt pktSrodek, double promien, cPunkt pktProsta,
	cPunkt &pktWynik1, cPunkt &pktWynik2)
{
	double dlBaza = dlugosc(pktSrodek, pktProsta);
	if (promien >= dlBaza) return GEODERR_NO_RESULT;
	double dlRamie = sqrt(pow(dlBaza,2) - pow(promien,2));
	wciecie_liniowe(pktSrodek, pktProsta, promien, dlRamie, pktWynik1);
	wciecie_liniowe(pktProsta, pktSrodek, dlRamie, promien, pktWynik2);
	return 0;
}

// Wyznaczenie punktow stycznosci okregu z prosta rownolegla do danej
int okrag_styczna_prosta_rownolegla(cPunkt pktSrodek, double promien,
	cPunkt pktProstaP, cPunkt pktProstaK, cPunkt &pktWynik1, cPunkt &pktWynik2)
{
	double azProsta;
	if (azymut(pktProstaP, pktProstaK, azProsta) == GEODERR_DATA)
		return GEODERR_DATA;
	punkt_biegun(pktSrodek, azProsta+M_PI_2, promien, pktWynik1);
	punkt_biegun(pktSrodek, azProsta-M_PI_2, promien, pktWynik2);
	return 0;
}

// Wyznaczenie punktow stycznosci okregu z prosta prostopadla do danej
int okrag_styczna_prosta_prostopadla(cPunkt pktSrodek, double promien,
	cPunkt pktProstaP, cPunkt pktProstaK, cPunkt &pktWynik1, cPunkt &pktWynik2)
{
	double azProsta;
	if (azymut(pktProstaP, pktProstaK, azProsta) == GEODERR_DATA)
		return GEODERR_DATA;
	punkt_biegun(pktSrodek, azProsta, promien, pktWynik1);
	punkt_biegun(pktSrodek, azProsta+M_PI, promien, pktWynik2);
	return 0;
}

// Wyznaczenie punktow stycznosci 2 okregow z prosta
int okrag_styczna_2okregi(cPunkt pktSrodek1, double promien1, cPunkt pktSrodek2,
	double promien2, cPunkt &pktSOkr1a, cPunkt &pktSOkr1b, cPunkt &pktSOkr2a,
	cPunkt &pktSOkr2b)
{
	double odlegloscSrodki = dlugosc(pktSrodek1, pktSrodek2);
	double odlegloscS2Przec;
	cPunkt pktPrzeciecie;
	double azSrodki;
	int blad;
	
	if (azymut(pktSrodek1, pktSrodek2, azSrodki) != GEODERR_OK)
		return GEODERR_DATA;
	
	if (promien1 == promien2) {
		punkt_biegun(pktSrodek1, azSrodki+M_PI_2, promien1, pktSOkr1a);
		punkt_biegun(pktSrodek1, azSrodki+M_PI_2, promien1, pktSOkr1b);
		punkt_biegun(pktSrodek2, azSrodki+M_PI_2, promien2, pktSOkr2a);
		punkt_biegun(pktSrodek2, azSrodki+M_PI_2, promien2, pktSOkr2b);
	}
	else {
		odlegloscS2Przec = promien2*odlegloscSrodki/(promien1-promien2);
		punkt_biegun(pktSrodek2, azSrodki, odlegloscS2Przec, pktPrzeciecie);
		blad = okrag_styczna_punkt(pktSrodek1, promien1, pktPrzeciecie, pktSOkr1a, pktSOkr1b);
		if (blad != GEODERR_OK) return blad;
		blad = okrag_styczna_punkt(pktSrodek2, promien2, pktPrzeciecie, pktSOkr2a, pktSOkr2b);
		if (blad != GEODERR_OK) return blad;
	}
	return 0;
}

char* podaj_czas()
{
	char *time_now = new char[17];
	time_t current_time;
	time(&current_time);
	strftime(time_now, 17, "%Y-%m-%d %H:%M", localtime(&current_time));
	return (time_now);
}

// redukcja dlugosci luku na poziom odniesienia
double d_na_poz_odniesienia(double Dm, double Hp, double Hk)
{
	const double R_ZIEMIA = 6382000.0;
	return Dm*(1-(Hp+Hk)/(2*R_ZIEMIA));
}

// redukcja odleglosci skosnej do luku na poziomie odniesienia
double ds_na_poz_odniesienia(double ds, double Hp, double ip, double Hk, double ik)
{
	const double R_ZIEMIA = 6382000.0;
	double dh = (Hk+ik)-(Hp+ip);
	double sumah = (Hp+ip)+(Hk+ik);
	return sqrt((ds*ds-dh*dh)*(1-sumah/R_ZIEMIA));
}

// obliczenie przewyzszenia na podstawie odleglosci poziomej
double dh_odl_pozioma (double Dp, double alfa)
{
	const double R_ZIEMIA = 6382000.0;
	const double WSP_REFRAKCJI = 0.13;
	return Dp*tan(alfa)+Dp*Dp*(1-WSP_REFRAKCJI)/(2*R_ZIEMIA);
}


// obliczenie przewyzszenia na podstawie odleglosci skosnej
double dh_odl_skosna (double Ds, double alfa)
{
	const double R_ZIEMIA = 6382000.0;
	const double WSP_REFRAKCJI = 0.13;
	return Ds*sin(alfa)+Ds*Ds*(1-WSP_REFRAKCJI)/(2*R_ZIEMIA);
}

double H_cel(double H_st, double dh, double i_st, double i_cel)
{
	return H_st+i_st+dh-i_cel;
}
