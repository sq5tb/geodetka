// missing constants

#ifndef VERSION
#  define VERSION "1.2.3"
#endif

#ifndef MAXFLOAT
#	define MAXFLOAT 3.402823466e+38f
#endif

#ifndef M_PI
#	define M_PI 3.14159265358979323846f
#endif
