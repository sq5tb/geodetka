/***************************************************************************
 *            stdgeod.h
 *
 *  PODSTAWOWE OBLICZENIA I FUNKCJE GEODEZYJNE
 *
 *  Fri May  6 04:30:28 2005
 *  Copyright  2005  Bogusław Ciastek
 *  ciacho@z.pl
 ****************************************************************************/
#ifndef __STDGEOD_H__
#define __STDGEOD_H__


#include <math.h>
#include <glib.h>

#define stToRad(st, min, sek) ( ((st)+(min)/60.0+(sek)/3600.0)/RO_ST )

typedef struct { double a, b, c, d; } tForma;
typedef enum {a, b, c, d} tFormaIndex;

const double RO_ST = 180.0/M_PI;  // wartosc radiana w stopniach
const double RO_GR = 200.0/M_PI;  // wartosc radiana w gradach

const unsigned int NR_MAX_LEN = 11;
const unsigned int KOD_MAX_LEN = 4;
const unsigned int CONFIG_VALUE_MAX_LEN = 255;

enum GEODERR {
	GEODERR_OK = 0,
	GEODERR_NO_RESULT = -1,
	GEODERR_NOT_ONE_RESULT = -2,
	GEODERR_DATA = -3,
	GEODERR_MATH = -4,
	GEODERR_ALGORITHM = -5,
	GEODERR_WINDOW_OPEN = -50,
	GEODERR_OTHER = -100
};

enum GEODPKT {
	GEODPKT_OTHER = 0,
	GEODPKT_OSNOWA,
	GEODPKT_OSNOWAL,
	GEODPKT_WYNIK,
	GEODPKT_WYNIKL,
	N_GEODPKT,
	GEODPKT_ALL = 100
};

class cPunkt
{
	public:
	cPunkt();
	cPunkt(const char* Nr, double X, double Y, double H=0.0, const char* Kod="", int Typ=GEODPKT_OTHER);
	~cPunkt();

	void setNr(const char* Nr) { g_strlcpy(jegoNr, Nr, NR_MAX_LEN); };
	char const *getNr() const { return jegoNr; };
	void setX(double X) { jegoX = X; };
	double getX() const { return jegoX; };
	void setY(double Y) { jegoY = Y; };
	double getY() const { return jegoY; };
	void setH(double H) { jegoH = H; };
	double getH() const { return jegoH; };
	void setKod (const char* Kod) { g_strlcpy(jegoKod, Kod, KOD_MAX_LEN); };
	char const *getKod() const { return jegoKod; };
	void setTyp(int Typ) { jegoTyp = Typ; };
	int getTyp() const { return jegoTyp; };

	void setXY(double X, double Y) { jegoX = X; jegoY = Y; };
	
	private:
	char jegoNr[NR_MAX_LEN];
	double jegoX;
	double jegoY;
	double jegoH;
	char jegoKod[KOD_MAX_LEN];
	int jegoTyp;
};

// POMOCNICZE SYMBOLE RACHUNKOWE - definicja klasy
class cForma
{
	public:
	cForma(unsigned int rozmiar);
	cForma(const cForma &);
	~cForma();	

	void setF(double a, double b, double c, double d,unsigned int poz);
	void setV(double value, tFormaIndex poz, unsigned int forma);
	double getV(tFormaIndex poz, unsigned int forma) const;
	
	double f1() const;    // funkcja pierwsza, iloczyn wyznacznikowy
	double f2() const;    // funkcja druga, albo iloczy kolumnowy

	// UWAGA! - formy ponizej zwarcaja 0 w przypadku bledu, skontroluj
	// poprzez sprawdzenie warunku czy f1!=0 badz f2!=0

	double f0() const;    // funkcja zerowa, albo iloraz glowny

	double f1pd() const;  // funkcja pierwsza prosta dolna
	double f1pg() const;  // funkcja pierwsza prosta gorna
	double f2pd() const;  // funkcja druga prosta dolna
	double f2pg() const;  // funkcja druga prosta gorna

	double f1kd() const;  // funkcja pierwsza kwadratowa dolna
	double f1kg() const;  // funkcja pierwsza kwadratowa gorna
	double f2kd() const;  // funkcja druga kwadratowa dolna
	double f2kg() const;  // funkcja druga kwadratowa gorna

	private:
	unsigned int jejRozmiar;
	tForma *jejForma;
};

// Normalizacja kata do przedzialu <k0-PI,k0+PI), domyslnie <0, 2PI)
double norm_kat (double kat, double kat0=M_PI);

// Dlugosc odcinka na podstawie wspolrzednych badz przyrostow wspolrzednych
double dlugosc (double dx, double dy);
double dlugosc (double wsp_xp, double wsp_yp, double wsp_xk, double wsp_yk);
double dlugosc (cPunkt pPoczatek, cPunkt pKoniec);

// Obliczenie azymutu na podstawie wspolrzednych
int azymut (double dy, double dx, double & azymut);
int azymut (cPunkt pPoczatek, cPunkt pKoniec, double & azym);

// wspolrzedne punktu koncowego na podstawie wspolrzednych punktu poczatkowego,
// azymutu i dlugosci odcinka
void punkt_biegun(cPunkt pkt_pocz, double azymut, double dlugosc, cPunkt & wynik);

// Obliczenie wspolrzednych punktu przeciecia dwoch prostych, z ktorych kazda
// wyznaczona jest przez dwa punkty dane. Zwraca 1 gdy proste rownolegle
int przeciecie_prostych (cPunkt pp1, cPunkt pk1, cPunkt pp2, cPunkt pk2,
		cPunkt &wyznaczany);

// Obliczenie kata ze wspolrzednych
// Uwaga - w przypadku pokrywania sie punktow zwraca 0
double kat (cPunkt stanowisko, cPunkt lewy, cPunkt prawy);

// Obliczenie wspolrzednych punktow zdjetych metoda domiarow prostokatnych
// w najprostszej formie obliczenie wspolrzednych punktu lezacego na prostej
// okreslonej przez dwa punkty gdy podana jest odleglosc i kierunek jej
// odlozenia (+/-)
// Uwaga - w przypadku gdy poczatek != koniec nastepuje przerwanie dzialania
int domiar_prostokatny (cPunkt poczatek, cPunkt koniec, cPunkt & wynik,
	double biezaca, double domiar=0, double dlugosc_pom=0);

// Rzutowanie punktow na prosta wyznaczona przez dwa punkty
int rzut_prosta (cPunkt poczatek, cPunkt koniec, cPunkt rzutowany,
		double &rzut_biezaca, double &rzut_domiar);

// Wcięcie kątowe w przód
int wciecie_katowe_w_przod (cPunkt pLewy, cPunkt pPrawy, double kLewy,
		double kPrawy, cPunkt &pWyznaczany);

// Wciecie katowe w przod na podstawie formy
int wciecie_katowe_w_przod_forma (cPunkt pLewy, cPunkt pPrawy, double kLewy,
		double kPrawy, cPunkt &pWyznaczany);

// Wciecie liniowe
int wciecie_liniowe(cPunkt pLewy, cPunkt pPrawy, double dLewe, double dPrawe,
		cPunkt &pWyznaczany);

// Wciecie liniowe na podstawie formy
int wciecie_liniowe_forma (cPunkt pLewy, cPunkt pPrawy, double dLewe,
		double dPrawe, cPunkt &pWyznaczany);

// Ogolny przypadek wciecia w przod
int wciecie_w_przod_ogolne(cPunkt pLewyC, cPunkt pLewyP, double kLewy,
        cPunkt pPrawyC, cPunkt pPrawyL, double kPrawy, cPunkt &pWyznaczany);

// Wciecie wstecz
int wciecie_wstecz(cPunkt pLewy, cPunkt pSrodkowy, cPunkt pPrawy, double katLS,
		double katLP, cPunkt &pWyznaczany);

// Wciecie wstecz obliczane na podstawie formy rachunkowej
int wciecie_wstecz_forma(cPunkt pLewy, cPunkt pSrodkowy, cPunkt pPrawy, double katLS,
		double katLP, cPunkt &pWyznaczany);

/* Wciecie w bok jako szczegolny przypadek wciecia wstecz
       S\ <)SW     /P
         \______ ./ <)LP
         L       W   
*/
int wciecie_w_bok(cPunkt pLewy, cPunkt pSrodkowy, cPunkt pPrawy, double katLP,
		cPunkt &pWyznaczany, double katSW);

/*  Zadanie Mareka, wyznaczenie pary dwoch punktow
    L2\          /P1
    ST2\.______./ST1
       /        \
    P2/          \L1
*/
int zadanie_Mareka(cPunkt st1L, cPunkt st1P, cPunkt st2L, cPunkt st2P,
		double kat1L, double kat1P, double kat2L, double kat2P,
		cPunkt &st1, cPunkt &st2);


/*  Zadanie Mareka rozwiazne przy pomocy form rachunkowych
    L2\          /P1
    ST2\.______./ST1
       /        \
    P2/          \L1
*/
int zadanie_Mareka_forma(cPunkt st1L, cPunkt st1P, cPunkt st2L, cPunkt st2P,
		double kat1L, double kat1P, double kat2L, double kat2P,
		cPunkt &st1, cPunkt &st2);

// Obliczanie pola wieloboku na podstawie wspolrzednych jego wierzcholkow
double pole_wielobok (cPunkt wierzcholek[], unsigned int ilosc_pkt);

// Wciecie azymutale w przod
int wciecie_azymutalne_w_przod (cPunkt pLewy, cPunkt pPrawy, double az_lw,
		double az_pw, cPunkt &pWyznaczany);

// Wciecie azymutale wstecz
int wciecie_azymutalne_wstecz (cPunkt pLewy, cPunkt pPrawy, double az_wl,
		double az_wp, cPunkt &pWyznaczany);

// Transformacja Helmerta dla wielu punktow dostosowania - obliczenie parametrow
int transformacja_helmerta_par (cPunkt pkt_dost[][2], unsigned int ilosc_pkt,
	double *u, double *v, double *b_xp, double *b_yp, double *b_xw, double *b_yw);

// Transformacja Helmerta dla wielu punktow dostosowania - przy znanych paramatrach
int transformacja_helmerta (double par_u, double par_v, double xp_biegun,
	double yp_biegun, double xw_biegun, double yw_biegun, cPunkt pkt_up, cPunkt & pkt_uw);

// Transformacja Helmerta dla wielu punktow dostosowania
int transformacja_helmerta (cPunkt pkt_dost[][2], unsigned int ilosc_pkt,
	cPunkt pkt_up, cPunkt & pkt_uw);


// Przeciecie okregu z linia prosta wyznaczona przez 2 punkty
int przeciecie_okrag_prosta (cPunkt pktOkragS, double promien, cPunkt pktProstaP,
	cPunkt pktProstaK, cPunkt & pktWynik1, cPunkt & pktWynik2);

// Wyznaczenie srodka okregu o danym promieniu stycznego do dwoch prostych
int okrag_wpasowanie_2proste (cPunkt pktPr1P, cPunkt pktPr1K, cPunkt pktPr2P,
	cPunkt pktPr2K, double promien, cPunkt & pktSrodek);

// Wyznaczenie srodka i promienia okregu na podstawie 3 punktow lezacych na okregu
int okrag_wpasowanie_3punkty (cPunkt pkt1, cPunkt pkt2, cPunkt pkt3,
	cPunkt & pktSrodek, double & promien);

// Wyznaczenie punktow stycznosci okregu z prosta przechodzaca przez dany punkt
// nie lezacy na okregu
int okrag_styczna_punkt(cPunkt pktSrodek, double promien, cPunkt pktProsta,
	cPunkt &pktWynik1, cPunkt &pktWynik2);

// Wyznaczenie punktow stycznosci okregu z prosta rownolegla do danej
int okrag_styczna_prosta_rownolegla(cPunkt pktSrodek, double promien,
	cPunkt pktProstaP, cPunkt pktProstaK, cPunkt &pktWynik1, cPunkt &pktWynik2);

// Wyznaczenie punktow stycznosci okregu z prosta prostopadla do danej
int okrag_styczna_prosta_prostopadla(cPunkt pktSrodek, double promien,
	cPunkt pktProstaP, cPunkt pktProstaK, cPunkt &pktWynik1, cPunkt &pktWynik2);

// Wyznaczenie punktow stycznosci 2 okregow z prosta
int okrag_styczna_2okregi(cPunkt pktSrodek1, double promien1, cPunkt pktSrodek2,
	double promien2, cPunkt &pktSOkr1a, cPunkt &pktSOkr1b, cPunkt &pktSOkr2a,
	cPunkt &pktSOkr2b);

// redukcja dlugosci luku na poziom odniesienia
double d_na_poz_odniesienia(double Dm, double Hp, double Hk);

// redukcja odleglosci skosnej do luku na poziomie odniesienia
double ds_na_poz_odniesienia(double ds, double Hp, double ip, double Hk, double ik);

// obliczenie przewyzszenia na podstawie odleglosci poziomej
double dh_odl_pozioma (double Dp, double alfa);

// obliczenie przewyzszenia na podstawie odleglosci skosnej
double dh_odl_skosna (double Ds, double alfa);

double H_cel(double H_st, double dh, double i_st=0.0, double i_cel=0.0);


// Funkcja zwracajaca aktualna date i czas
char* podaj_czas();

#endif /* __STDGEOD_H__ */
