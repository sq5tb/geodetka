/***************************************************************************
 *            suppgeod.h
 *
 *  Tue Jul  5 04:45:19 2005
 *  Copyright  2005  Bogusław Ciastek
 *  ciacho@z.pl
 ****************************************************************************/
#ifndef __SUPPGEOD_H__
#define __SUPPGEOD_H__


// definicja kolumn tabel
enum treePunkty_col {
	COL_PKT_ID = 0,
	COL_PKT_NR,	COL_PKT_X, COL_PKT_X_DOUBLE, COL_PKT_Y, COL_PKT_Y_DOUBLE,
	COL_PKT_H, COL_PKT_H_DOUBLE, COL_PKT_KOD, COL_PKT_TYP,
	N_PKT_COLUMNS
};

enum treeObserwacje_col {
	COL_OBS_ID = 0,
	COL_OBS_TYP, COL_OBS_L, COL_OBS_C, COL_OBS_P,
	COL_OBS_V, COL_OBS_V_DOUBLE,
	N_OBS_COLUMNS
};

enum treeHistoria_col {
	COL_HIS_ID = 0,
	COL_HIS_ORDER, COL_HIS_FUNCTION, COL_HIS_DATA, COL_HIS_REZULT, COL_HIS_MOD,
	N_HIS_COLUMNS
};

enum treeTHelmertaDost_col {
	COL_THD_NRP = 0, COL_THD_XP, COL_THD_XP_DOUBLE, COL_THD_YP, COL_THD_YP_DOUBLE,
	COL_THD_NRW, COL_THD_XW, COL_THD_XW_DOUBLE, COL_THD_YW, COL_THD_YW_DOUBLE,
	COL_THD_DX, COL_THD_DY, COL_THD_DP,
	N_THD_COLUMNS
};

enum treeTHelmertaTransf_col {
	COL_THT_NRP = 0, COL_THT_XP, COL_THT_XP_DOUBLE, COL_THT_YP, COL_THT_YP_DOUBLE,
	COL_THT_NRW, COL_THT_XW, COL_THT_XW_DOUBLE, COL_THT_YW, COL_THT_YW_DOUBLE,
	COL_THT_DODAJ, COL_THT_TOGGLE, N_THT_COLUMNS
};

enum treeTachimetria_col {
	COL_TACH_NR = 0, COL_TACH_HZ, COL_TACH_HZ_DOUBLE, COL_TACH_ODL, COL_TACH_ODL_DOUBLE,
	COL_TACH_VDH, COL_TACH_VDH_DOUBLE, COL_TACH_HC, COL_TACH_HC_DOUBLE,
	COL_TACH_X, COL_TACH_X_DOUBLE, COL_TACH_Y, COL_TACH_Y_DOUBLE,
	COL_TACH_H, COL_TACH_H_DOUBLE, COL_TACH_KOD, COL_TACH_DODAJ, COL_TACH_TOGGLE,
	N_TACH_COLUMNS
};

enum treeHistoriaEdytuj_col {
	COL_HE_ID,
	COL_HE_NAZWA,
	COL_HE_VALUE,
	COL_HE_VALUE_DOUBLE,
	COL_HE_ANGLE,
	COL_HE_EDITED,
	N_HE_COLUMNS
};

enum treePoligon_col {
	COL_POL_NR,
	COL_POL_KOD,
	COL_POL_KAT,
	COL_POL_KAT_DOUBLE,
	COL_POL_DL,
	COL_POL_DL_DOUBLE,
	COL_POL_X,
	COL_POL_X_DOUBLE,
	COL_POL_Y,
	COL_POL_Y_DOUBLE,
	COL_POL_DODAJ,
	N_POL_COLUMNS
};

enum tree_pkt_dodaj {
	PKT_DODAJ = 1, PKT_NIE_DODAWAJ, PKT_DODANY, PKT_ERROR, PKT_BRAK_DANYCH
};

enum treeAzymut {
	COL_AZ_PPNR = 0, COL_AZ_PPX, COL_AZ_PPY,
	COL_AZ_PKNR, COL_AZ_PKX, COL_AZ_PKY,
	COL_AZ_AZYM, COL_AZ_DL, COL_AZ_DL_DOUBLE,
	N_AZ_COLUMNS
};

enum treeKat {
	COL_KAT_CNR = 0, COL_KAT_CX, COL_KAT_CY,
	COL_KAT_LNR, COL_KAT_LX, COL_KAT_LY,
	COL_KAT_PNR, COL_KAT_PX, COL_KAT_PY,
	COL_KAT_KAT,
	N_KAT_COLUMNS
};

enum treeDomiary {
	COL_DOM_NR = 0,
	COL_DOM_B, COL_DOM_B_DOUBLE,
	COL_DOM_D, COL_DOM_D_DOUBLE,
	COL_DOM_X, COL_DOM_X_DOUBLE,
	COL_DOM_Y, COL_DOM_Y_DOUBLE,
	COL_DOM_X0_DOUBLE, COL_DOM_Y0_DOUBLE,
	COL_DOM_KOD, COL_DOM_DODAJ, COL_DOM_TOGGLE,
	N_DOM_COLUMNS
};

enum treeRzutProsta {
	COL_RZP_NR = 0,
	COL_RZP_X, COL_RZP_X_DOUBLE,
	COL_RZP_Y, COL_RZP_Y_DOUBLE,
	COL_RZP_B, COL_RZP_B_DOUBLE,
	COL_RZP_D, COL_RZP_D_DOUBLE,	
	COL_RZP_XR_DOUBLE, COL_RZP_YR_DOUBLE,
	COL_RZP_NRR, COL_RZP_DODAJ, COL_RZP_TOGGLE,
	N_RZP_COLUMNS
};

enum treePowierzchniaPunkty {
	COL_PWP_DZ = 0, COL_PWP_NR,
	COL_PWP_X, COL_PWP_X_DOUBLE,
	COL_PWP_Y, COL_PWP_Y_DOUBLE,
	COL_PWP_CZ, COL_PWP_CZ_DOUBLE,
	N_PWP_COLUMNS
};

enum treePowierzchniaWynik {
	COL_PWW_DZ = 0,
	COL_PWW_POW, COL_PWW_POW_DOUBLE,
	COL_PWW_OBW, COL_PWW_OBW_DOUBLE,
	N_PWW_COLUMNS
};

enum treeBiegunowe {
	COL_BIE_NR = 0,
	COL_BIE_X, COL_BIE_X_DOUBLE,
	COL_BIE_Y, COL_BIE_Y_DOUBLE,
	COL_BIE_KIER, COL_BIE_ODL, COL_BIE_ODL_DOUBLE,
	N_BIE_COLUMNS
};

enum treeProjekty {
	COL_PRJ_ID = 0, COL_PRJ_NAZWA,
	COL_PRJ_WOJ, COL_PRJ_POWIAT, COL_PRJ_GMINA,
	COL_PRJ_MIEJSCOWOSC, COL_PRJ_ULICA, COL_PRJ_OZNN,
	COL_PRJ_CEL, COL_PRJ_UWAGI, COL_PRJ_DATAPOM, COL_PRJ_DATAMOD,
	N_PRJ_COLUMNS
};

enum fileTxt {
	FTXT_NR = 0, FTXT_X, FTXT_Y, FTXT_H, FTXT_KOD, FTXT_TYP, N_FTXT
};

// definicja zakladek
enum tab_wciecia {WCIECIE_KATOWE, WCIECIE_LINIOWE, WCIECIE_WSTECZ,
	WCIECIE_AZ_W_PRZOD, WCIECIE_AZ_WSTECZ};

enum tab_styczna { STYCZNA_PUNKT, STYCZNA_PROSTA, STYCZNA_OKREGI };

enum tab_przeciecia { PRZECIECIE_PROSTE, PRZECIECIE_OKRAG_PROSTA, PRZECIECIE_OKREGI };

enum geod_draw_style {
	DRAW_DEFAULT,
	LINE_BAZA, LINE_DLUGOSC, LINE_KIERUNEK, LINE_POLNOC,
	PKT_OSNOWA, PKT_WYNIK
};

enum geod_functions {
	GF_BIEGUN, GF_PROSTE_PRZECIECIE, GF_DOMIARY, GF_RZUT_PROSTA,
	GF_WCIECIE_KATOWE, GF_WCIECIE_LINIOWE, GF_WCIECIE_W_PRZOD_O,
	GF_WCIECIE_WSTECZ, GF_WCIECIE_W_BOK, GF_WCIECIE_AZ_W_PRZOD,
	GF_WCIECIE_AZ_WSTECZ, GF_ZADANIE_MAREKA, GF_THELMERTA_PAR, GF_THELMERTA,
	GF_PRZECIECIE_OKRAG_PROSTA, GF_PRZECIECIE_OKREGI, GF_OKRAG_2PROSTE,
	GF_OKRAG_3PKT, GF_STYCZNA_PUNKT, GF_STYCZNA_ROWNOLEGLA, GF_STYCZNA_PROSTOPADLA,
	GF_STYCZNA_2OKREGI
};


// funkcje pomocnicze
void geod_update_entry_xy (GtkWidget* entryNr, const char* nameEntryX,
	const char* nameEntryY, const char* nameNext = NULL);

void geod_update_entry_xyh (GtkWidget* entryNr, const char* nameEntryX,
	const char* nameEntryY, const char* nameEntryH, const char* nameNext);

int geod_update_entry_obs (GtkWidget* entryObs, const char* nameEntryL,
	const char* nameEntryC,	const char* nameEntryP, const char* nameNext);

// sbar
void geod_sbarF1Punkt_set (GtkWidget *widget, const char* sbarName);

void geod_sbarF1Kod_set (GtkWidget *widget, const char* sbarName);

void geod_sbarF1_clear (GtkWidget *widget, const char* sbarName);

void geod_sbar_set (GtkWidget *widget, const char* sbar_name, const char* sbar_msg);

void geod_sbar_set_error (GtkWidget *widget, const char* sbar_name, int geod_err);

gboolean geod_sbar_clear (gpointer statusbar);

// entry
void geod_entry_clear(GtkWidget *widget, const char* entryName);

int geod_entry_get_pkt_xy(GtkWidget *widget, const char* nameNr,
	const char* nameX, const char* nameY, const char* nameKod, cPunkt & punkt);

int geod_entry_get_kat(GtkWidget *widget, const char* entryName, double & kat);

int geod_entry_get_double(GtkWidget *widget, const char* entryName, double & fvaue, bool FOCUS = TRUE);

void geod_entry_set_pkt_xy(GtkWidget *widget, const char* nameX, const char* nameY,
	cPunkt punkt);

int geod_entry_get_pkt_xyh(GtkWidget *widget, const char* nameNr,
	const char* nameX, const char* nameY, const char* nameH, const char* nameKod,
	cPunkt & punkt);


// view
double geod_view_transform(GtkWidget *darea, cPunkt pktDane[], GdkPoint *pktWidok,
	guint npoints);

void geod_draw_point(GtkWidget *darea, GdkPoint punkt, const char *pkt_id, int typ = 0);

void geod_draw_arc(GtkWidget *darea, GdkPoint pktS, GdkPoint pktP, GdkPoint pktL, int typ = 1);

void geod_draw_line(GtkWidget *darea, GdkPoint pktP, GdkPoint pktK, int typ = 0);

void geod_draw_circle(GtkWidget *darea, GdkPoint pktS, int promien);

GSList* geod_slist_remove_string (GSList *list, const char *str);

// raport
void geod_raport_table (GtkWidget *textview, char **cell,
	guint ncol, guint nrow, const guint* colsize, const char* naglowek = NULL);

void geod_raport_table_pkt (GtkWidget *textview, GSList *punkty, bool col_h = FALSE, const char* naglowek = NULL);

void geod_raport_naglowek (GtkWidget *textview, const char* naglowek = NULL);

void geod_raport_stopka (GtkWidget *textview);

void geod_raport_line (GtkWidget *textview, const char* opis, const char* wynik, bool bold = FALSE);

void geod_raport_error (GtkWidget *textview, const char* opis, bool bold = FALSE);

// inne
char* geod_nstr (const char* str, guint nsize);

char* geod_kat_from_rad (double kat, bool jednostki = TRUE);

int geod_get_kat(const char* txtkat, double & kat);

void geod_www(const char* web_adress);

char* geod_prefix_rem(const char* source);

char *geod_inc(const char* _txt, int typ = -1);

char *geod_dec(const char* _txt, int typ = -1);

int geod_nr_type(const char* _txt);

int geod_nr_cmp (const char* nr1, const char* nr2);

void geod_entry_inc(GtkWidget *widget, const char* entryName);

/* This is function to find data files. */
gchar* geod_find_data_file (const gchar *filename);

#endif /* __SUPPGEOD_H__ */
