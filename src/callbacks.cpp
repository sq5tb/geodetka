#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <stdlib.h>
#include <string.h>

#include "stdgeod.h"
#include "suppgeod.h"
#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "dbgeod.h"

GSList *otwarte_okna = NULL;
GtkWidget *geod_open_window (GtkWidget* (*fcreate)(void), const char *win_name);

GtkWidget *geod_open_window (GtkWidget* (*fcreate)(void), const char *win_name)
{
	GtkWidget *win = NULL;
	if (!g_slist_find_custom(otwarte_okna, win_name, (GCompareFunc) g_ascii_strcasecmp)) {
		win = (*fcreate)();
		otwarte_okna = g_slist_append(otwarte_okna, g_strdup(win_name));
		gtk_widget_show (win);
	}
	return win;
}


gboolean event_get_kod (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	if (event->keyval == GDK_F1) {
		GtkWidget *dialogKod = create_dialogKod();
		g_object_set_data_full (G_OBJECT (dialogKod), "eventEntry",
			g_object_ref (widget), (GDestroyNotify) g_object_unref);
		gtk_dialog_run(GTK_DIALOG(dialogKod));
	}
	return FALSE;
}

gboolean event_get_pkt (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	GtkWidget *dialog = NULL;
	GtkWidget *dialog_entry = NULL;
	gboolean retv = FALSE;
	
	switch (event->keyval) {
		case GDK_F1:
			dialog = create_dialogPunkty();
			g_object_set_data_full (G_OBJECT (dialog), "eventEntry",
				g_object_ref (widget), (GDestroyNotify) g_object_unref);
			gtk_dialog_run(GTK_DIALOG(dialog));
			break;
		case GDK_Insert:
			dialog = create_dialogDodajPunkt();
			g_object_set_data_full (G_OBJECT (dialog), "eventEntry",
				g_object_ref (widget), (GDestroyNotify) g_object_unref);
			dialog_entry = lookup_widget(GTK_WIDGET(dialog), "entryDialogDodajPunktNr");
			gtk_entry_set_text(GTK_ENTRY(dialog_entry), g_strdup(gtk_entry_get_text(GTK_ENTRY(widget))));
			gtk_dialog_run(GTK_DIALOG(dialog));
			break;
		case GDK_Return:
		case GDK_Tab:
			gtk_cell_editable_editing_done(GTK_CELL_EDITABLE(widget));
			retv = TRUE;
			break;
	}
	return retv;
}


/* winMain */
void on_winMain_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *labelProjektNazwa = lookup_widget(GTK_WIDGET(widget), "labelMainNazwaV");
	GtkWidget *labelProjektWojewodztwo = lookup_widget(GTK_WIDGET(widget), "labelMainWojewodztwoV");
	GtkWidget *labelProjektPowiat = lookup_widget(GTK_WIDGET(widget), "labelMainPowiatV");
	GtkWidget *labelProjektGmina = lookup_widget(GTK_WIDGET(widget), "labelMainGminaV");
	GtkWidget *labelProjektMiejscowosc = lookup_widget(GTK_WIDGET(widget), "labelMainMiejscowoscV");
	GtkWidget *labelProjektUlica = lookup_widget(GTK_WIDGET(widget), "labelMainUlicaV");
	GtkWidget *labelProjektOznNier = lookup_widget(GTK_WIDGET(widget), "labelMainOznNierV");
	GtkWidget *labelProjektCel = lookup_widget(GTK_WIDGET(widget), "labelMainCelV");
	GtkWidget *labelProjektUwagi = lookup_widget(GTK_WIDGET(widget), "labelMainUwagiV");
	GtkWidget *labelProjektDataMod = lookup_widget(GTK_WIDGET(widget), "labelMainDataModV");
	GtkWidget *labelProjektDataPom = lookup_widget(GTK_WIDGET(widget), "labelMainDataPomV");
	GtkWidget *sbarMain = lookup_widget(GTK_WIDGET(widget), "sbarMain");
	GtkWidget *cbtnMainUkladLokalny = lookup_widget(GTK_WIDGET(widget), "cbtnMainUkladLokalny");
	struct sProjekt projekt;
	char *sbar_msg;
	char konfiguracja_kat[CONFIG_VALUE_MAX_LEN];
	char konfiguracja_uklad[CONFIG_VALUE_MAX_LEN];
	
	// ustawienie paska statusu
	dbgeod_config_get("jedn_kat", konfiguracja_kat);
	if (g_str_equal(konfiguracja_kat, "rad"))
		sbar_msg = g_strdup(_("Jednostka kąta: RADIANY"));
	else if (g_str_equal(konfiguracja_kat, "grad"))
		sbar_msg = g_strdup(_("Jednostka kąta: GRADY"));
	else if (g_str_equal(konfiguracja_kat, "deg"))
		sbar_msg = g_strdup(_("Jednostka kąta: STOPNIE"));
	else if (g_str_equal(konfiguracja_kat, "degdec"))
		sbar_msg = g_strdup(_("Jednostka kąta: STOPNIE (dziesiętnie)"));
	else
		sbar_msg = g_strdup(_("Wybierz Plik->Preferencje aby dostosować program"));
	gtk_statusbar_push(GTK_STATUSBAR(sbarMain), 2, sbar_msg);
	g_free(sbar_msg);
	
	dbgeod_projekt_get(dbgeod_projekt(), projekt);
	gtk_label_set_label(GTK_LABEL(labelProjektNazwa), g_strdup_printf("<b>%s</b>", projekt.nazwa));
	gtk_label_set_text(GTK_LABEL(labelProjektWojewodztwo), projekt.wojewodztwo);
	gtk_label_set_text(GTK_LABEL(labelProjektPowiat), projekt.powiat);
	gtk_label_set_text(GTK_LABEL(labelProjektGmina), projekt.gmina);
	gtk_label_set_text(GTK_LABEL(labelProjektMiejscowosc), projekt.miejscowosc);
	gtk_label_set_text(GTK_LABEL(labelProjektUlica), projekt.ulica);
	gtk_label_set_text(GTK_LABEL(labelProjektOznNier), projekt.ozn_nier);
	gtk_label_set_text(GTK_LABEL(labelProjektCel), projekt.cel_pracy);
	gtk_label_set_text(GTK_LABEL(labelProjektUwagi), projekt.uwagi);
	gtk_label_set_text(GTK_LABEL(labelProjektDataPom), projekt.data_pom);
	gtk_label_set_text(GTK_LABEL(labelProjektDataMod), projekt.data_mod);
	if (!dbgeod_config_get("lokalny", konfiguracja_uklad))
		if (g_str_equal(konfiguracja_uklad, "0"))
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cbtnMainUkladLokalny), FALSE);
		else
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cbtnMainUkladLokalny), TRUE);	
	else {
		dbgeod_config_set("lokalny", "0");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cbtnMainUkladLokalny), FALSE);
	}
}


void on_winMain_destroy (GtkObject *object, gpointer user_data)
{
	dbgeod_projekt_data_mod();
	g_slist_free(otwarte_okna);
	gtk_main_quit();
}


gboolean on_winMain_delete_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	int response = GTK_RESPONSE_YES;
	GtkWidget *dialog;

	if (g_slist_length(otwarte_okna) > 0) {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Istnieją otwarte okna z obliczeniami.\nCzy na pewno zamknąć program?"));
		response = gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
	}
	
	if (response == GTK_RESPONSE_YES)
		return FALSE;
	else
		return TRUE;
}


void on_mnuMainZakoncz_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	GtkWidget *winMain = lookup_widget(GTK_WIDGET(menuitem), "winMain");
	gtk_widget_destroy(winMain);
}


void on_mnuMainProjekty_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	GtkWidget *winMain = lookup_widget(GTK_WIDGET(menuitem), "winMain");
	GtkWidget *dialog;
	GtkWidget *dialogProjektWybor;
	gint response = GTK_RESPONSE_OK;

	if (g_slist_length(otwarte_okna) > 0) {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_INFO, GTK_BUTTONS_OK_CANCEL, _("Zalecane jest zamknięcie wszystkich okien z obliczeniami.\nKontynuować?"));
		response = gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
	}
	if (response == GTK_RESPONSE_OK) {
		dialogProjektWybor = create_dialogProjektWybor();
		g_object_set_data_full (G_OBJECT (dialogProjektWybor), "winMain",
			g_object_ref (winMain), (GDestroyNotify) g_object_unref);
		gtk_dialog_run(GTK_DIALOG(dialogProjektWybor));
		g_signal_emit_by_name(winMain, "realize");
	}
}


void on_mnuMainEdytujPunkty_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	GtkWidget *winPunkty = geod_open_window(create_winPunkty, "winPunkty");
	if (winPunkty != NULL) {
		GtkWidget *tree = lookup_widget(GTK_WIDGET(winPunkty), "treePunkty");
		GtkWidget *winMain = lookup_widget(GTK_WIDGET(menuitem), "winMain");
		g_object_set_data_full (G_OBJECT (winMain), "treePunkty",
			g_object_ref (tree), (GDestroyNotify) g_object_unref);
	}
	else
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}

void on_mnuMainPomoc_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	char *pathname = NULL;
	GtkWidget *dialog;
	
	pathname = geod_find_data_file("geodetka.htm");
	if (pathname != NULL)
		geod_www(g_strdup_printf("file://%s", pathname));	
	else {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_INFO, GTK_BUTTONS_OK, _("Nie odnaleziono dokumentacji"));
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
	}
	g_free (pathname);
}


void on_mnuMainUsunPunkty_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	GtkWidget *dialog = create_dialogUsunPunkty();
	gtk_dialog_run(GTK_DIALOG(dialog));
}

void on_mnuMainInformacjeO_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if (geod_open_window(create_winAbout, "winAbout") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}


void on_mnuMainWciecia_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winWciecia, "winWciecia") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}

void on_mnuMainZadanieMareka_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winZMareka, "winZMareka") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}

void on_mnuMainDodajPunkt_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	GtkWidget *dialog = create_dialogDodajPunkt();
	gtk_dialog_run(GTK_DIALOG(dialog));
}

void on_mnuMainEdytujObserwacje_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winObserwacje, "winObserwacje") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}


void on_mnuMainHistoria_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winHistoria, "winHistoria") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}

void on_mnuMainStatystyka_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	GtkWidget *dialog = create_dialogStatystyka();
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}


void on_mnuMainAzymut_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winAzymut, "winAzymut") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}


void on_mnuMainKat_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winKat, "winKat") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}


void on_mnuMainPole_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winPowierzchnia, "winPowierzchnia") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}


void on_mnuMainDomiary_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winDomiary, "winDomiary") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}


void on_mnuMainRzutProsta_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winRzutProsta, "winRzutProsta") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}


void on_mnuMainTransformacja_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winTHelmerta, "winTHelmerta") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}

void on_mnuMainTyczenieBiegunowe_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winBiegunowe, "winBiegunowe") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}


void on_mnuMainPrzeciecia_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winPrzeciecia, "winPrzeciecia") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}


void on_mnuMainOkragWpasowanie_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winOkragWpasowanie, "winOkragWpasowanie") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}


void on_mnuMainOkragStyczna_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winOkragStyczna, "winOkragStyczna") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}


void on_mnuMainWebFreegis_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	geod_www("http://www.freegis.org/");
}

void on_mnuMainWebGama_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	geod_www("http://www.gnu.org/software/gama/gama.html");
}


void on_mnuMainWebGrass_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	geod_www("http://grass.itc.it/");
}


void on_mnuMainEksportujPunkty_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winEksportTxt, "winEksportTxt") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}


void on_mnuMainImportujPunkty_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	GtkWidget *dialog = create_dialogImportTxt();
	GtkWidget *winMain = lookup_widget(GTK_WIDGET(menuitem), "winMain");
	GtkWidget *tree = (GtkWidget*)g_object_get_data (G_OBJECT(winMain), "treePunkty");
	
	if ((gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK) && (GTK_IS_TREE_STORE(tree)))
		treePunkty_update_view(tree);
	gtk_widget_destroy(GTK_WIDGET(dialog));
}


void on_mnuMainPreferencje_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	GtkWidget *dialog = create_dialogKonfiguracja();
	GtkWidget *sbarMain = lookup_widget(GTK_WIDGET(menuitem), "sbarMain");
	g_object_set_data_full (G_OBJECT (dialog), "sbarMain",
		g_object_ref (sbarMain), (GDestroyNotify) g_object_unref);
	gtk_dialog_run(GTK_DIALOG(dialog));
}


void on_mnuMainImportujObserwacje_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	const guint N_COL = 4;
	GtkWidget *dialog;
	char *line_col[N_COL];
	guint i;
	bool blad;
	FILE *plik;

	char bufor[80];
	char *file_name = NULL;
	char **bufor_split = NULL, **item = NULL;
	double odczyt;
	
	
	dialog = gtk_file_chooser_dialog_new (_("Import pliku z obserwacjami"),
		NULL, GTK_FILE_CHOOSER_ACTION_OPEN, GTK_STOCK_CANCEL,
		GTK_RESPONSE_CANCEL, GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT, NULL);

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		file_name = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
		gtk_widget_destroy(dialog);
	}
	else {
		gtk_widget_destroy(dialog);
		return;
	}
	
	// odczytanie i sprawdzenie nazwy pliku
	if (file_name == NULL) {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Nie wybrano pliku."));
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
		return;
	}
	if(g_file_test(file_name, G_FILE_TEST_IS_DIR)) {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Wybrano katalog zamiast pliku."));
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
		g_free(file_name);
		return;
	}
	
	plik = fopen(file_name, "rt");
		
	if (plik == NULL)	{
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Nie mogę otworzyć pliku, sprawdź uprawnienia."));
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
		g_free(file_name);
		return;
	}
	else {
		while (fgets(bufor, 80, plik) != NULL) {
			bufor_split = g_strsplit_set(bufor, " :;|\t\n", -1);
			
			for (i=0; i < N_COL; i++) line_col[i] = NULL; // wyzerowanie tablicy wskaznikow

			for (i = 0, item = bufor_split; *item != NULL && i < N_COL; item++)
				if (!g_str_equal(*item, "")) {
					line_col[i] = *item;
					i++;
				}

			blad = FALSE;
			for (i=0; i < N_COL;i++) if (line_col[i] == NULL) blad = TRUE;
			if (!blad) {
				if (dbgeod_obs_is_angle(line_col[0]))
					geod_get_kat(line_col[3], odczyt);
				else
					odczyt = g_strtod(line_col[3], NULL);
			 	dbgeod_obs_set(line_col[0], line_col[1], line_col[2], odczyt);
			}
			g_strfreev(bufor_split);
		}
		fclose(plik);
	}
	g_free(file_name);

	dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, _("Wczytano obserwacje."));
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}


void on_mnuMainEksportujObserwacje_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	GtkWidget *dialog;
	guint nobs;
	FILE *plik;
	char *file_name = NULL, *dialog_txt = NULL;
	
	
	dialog = gtk_file_chooser_dialog_new (_("Eksport pliku z obserwacjami"),
		NULL, GTK_FILE_CHOOSER_ACTION_SAVE, GTK_STOCK_CANCEL,
		GTK_RESPONSE_CANCEL, GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT, NULL);

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		file_name = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
		gtk_widget_destroy(dialog);
	}
	else {
		gtk_widget_destroy(dialog);
		return;
	}
	
	// sprawdzenie nazwy i obecnosci pliku
	if (file_name == NULL) {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Nie wybrano pliku."));
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
		return;
	}
	if(g_file_test(file_name, G_FILE_TEST_IS_DIR)) {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Wybrano katalog zamiast pliku"));
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
		g_free(file_name);
		return;
	}
	if(g_file_test(file_name, G_FILE_TEST_EXISTS)) {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_WARNING, GTK_BUTTONS_OK_CANCEL, _("Uwaga podany plik istnieje.\nEksport spowoduje usunięcie istniejącego pliku.\nKontynuować?"));
		if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_CANCEL) {
			gtk_widget_destroy(dialog);
			g_free(file_name);
			return;
		}
		else
			gtk_widget_destroy(dialog);
	}

	plik = fopen(file_name, "wt");
	if (plik == NULL)	{
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Nie mogę utworzyć pliku, sprawdź uprawnienia."));
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
		g_free(file_name);
		return;
	}
	else {
		dbgeod_obs_save(plik, nobs);
		fclose(plik);
	}
	g_free(file_name);

	dialog_txt = g_strdup_printf(_("Zapisano %d obserwacji.\n"), nobs);
	dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, dialog_txt);
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
	g_free(dialog_txt);
}


void on_btnMainModyfikuj_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *dialog;
	GtkWidget *labelProjektId;
	GtkWidget *entryProjektNazwa, *entryProjektWojewodztwo, *entryProjektPowiat, *entryProjektGmina;
	GtkWidget *entryProjektMiejscowosc, *entryProjektUlica, *entryProjektOznNier;
	GtkWidget *entryProjektCel, *entryProjektUwagi, *entryProjektDataPom;
	GtkWidget *winMain = lookup_widget(GTK_WIDGET(button), "winMain");
	char *txt_id = NULL;
	struct sProjekt projekt;

	if (dbgeod_projekt() == 0) {
		dialog = gtk_message_dialog_new(GTK_WINDOW(winMain), GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, _("Nie można edytować projektu domyślnego\nUtwórz najpierw nowy projekt."));
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
	}
	else {
		dialog = create_dialogProjektEdytuj();
		labelProjektId = lookup_widget(GTK_WIDGET(dialog), "labelDialogProjektEdytujIdV");
		entryProjektNazwa = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujNazwa");
		entryProjektWojewodztwo = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujWojewodztwo");
		entryProjektPowiat = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujPowiat");
		entryProjektGmina = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujGmina");
		entryProjektMiejscowosc = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujMiejscowosc");
		entryProjektUlica = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujUlica");
		entryProjektOznNier = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujOznNier");
		entryProjektCel = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujCel");
		entryProjektUwagi = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujUwagi");
		entryProjektDataPom = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujDataPom");
		
		dbgeod_projekt_get(dbgeod_projekt(), projekt);
		txt_id = g_strdup_printf("%d", dbgeod_projekt());
		gtk_label_set_text(GTK_LABEL(labelProjektId), txt_id);
		g_free(txt_id);
		gtk_entry_set_text(GTK_ENTRY(entryProjektNazwa), projekt.nazwa);
		gtk_entry_set_text(GTK_ENTRY(entryProjektWojewodztwo), projekt.wojewodztwo);
		gtk_entry_set_text(GTK_ENTRY(entryProjektPowiat), projekt.powiat);
		gtk_entry_set_text(GTK_ENTRY(entryProjektGmina), projekt.gmina);
		gtk_entry_set_text(GTK_ENTRY(entryProjektMiejscowosc), projekt.miejscowosc);
		gtk_entry_set_text(GTK_ENTRY(entryProjektUlica), projekt.ulica);
		gtk_entry_set_text(GTK_ENTRY(entryProjektOznNier), projekt.ozn_nier);
		gtk_entry_set_text(GTK_ENTRY(entryProjektCel), projekt.cel_pracy);
		gtk_entry_set_text(GTK_ENTRY(entryProjektUwagi), projekt.uwagi);
		gtk_entry_set_text(GTK_ENTRY(entryProjektDataPom), projekt.data_mod);

		gtk_dialog_run(GTK_DIALOG(dialog));
		g_signal_emit_by_name(winMain, "realize");
	}
}


/* winPunkty */
void on_winPunkty_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winPunkty");
}


void on_tbtnPunktyWyczysc_clicked (GtkButton *button, gpointer user_data)
{
	geod_entry_clear(GTK_WIDGET(button), "entryPunktyNr");
	geod_entry_clear(GTK_WIDGET(button), "entryPunktyX");
	geod_entry_clear(GTK_WIDGET(button), "entryPunktyY");
	geod_entry_clear(GTK_WIDGET(button), "entryPunktyH");
	geod_entry_clear(GTK_WIDGET(button), "entryPunktyKod");
}


void on_treePunkty_cursor_changed (GtkTreeView *treeview, gpointer user_data)
{
	GtkWidget *btnWyczysc = lookup_widget(GTK_WIDGET(treeview), "tbtnPunktyWyczysc");
	GtkWidget *entryNr = lookup_widget(GTK_WIDGET(treeview), "entryPunktyNr");
	GtkWidget *entryX = lookup_widget(GTK_WIDGET(treeview), "entryPunktyX");
	GtkWidget *entryY = lookup_widget(GTK_WIDGET(treeview), "entryPunktyY");
	GtkWidget *entryH = lookup_widget(GTK_WIDGET(treeview), "entryPunktyH");
	GtkWidget *entryKod = lookup_widget(GTK_WIDGET(treeview), "entryPunktyKod");
	GtkWidget *entryTyp = lookup_widget(GTK_WIDGET(treeview), "entryPunktyTyp");

	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	GtkTreePath *tpath;
	GtkTreeIter iter;
	char *pkt_nr, *pkt_x, *pkt_y, *pkt_h, *pkt_kod;
	int pkt_typ;
	cPunkt punkt;	
	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(treeview), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_PKT_NR, &pkt_nr, COL_PKT_X, &pkt_x, COL_PKT_Y, &pkt_y,
			COL_PKT_H, &pkt_h, COL_PKT_KOD, &pkt_kod, COL_PKT_TYP, &pkt_typ, -1);
		gtk_entry_set_text(GTK_ENTRY(entryNr), pkt_nr);
		gtk_entry_set_text(GTK_ENTRY(entryX), pkt_x);
		gtk_entry_set_text(GTK_ENTRY(entryY), pkt_y);
		gtk_entry_set_text(GTK_ENTRY(entryH), pkt_h);
		gtk_entry_set_text(GTK_ENTRY(entryKod), pkt_kod);
		if (pkt_typ>=N_GEODPKT) pkt_typ=GEODPKT_OTHER;
		gtk_combo_box_set_active(GTK_COMBO_BOX(entryTyp), pkt_typ);

		g_free(pkt_nr);
		g_free(pkt_x);
		g_free(pkt_y);
		g_free(pkt_h);
		g_free(pkt_kod);
		gtk_tree_path_free(tpath);
	}
	else
		g_signal_emit_by_name(G_OBJECT(btnWyczysc), "clicked");
}


void on_tbtnPunktyDodaj_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *treePunkty = lookup_widget(GTK_WIDGET(button), "treePunkty");
	GtkWidget *entryH = lookup_widget(GTK_WIDGET(button), "entryPunktyH");
	GtkWidget *entryTyp = lookup_widget(GTK_WIDGET(button), "entryPunktyTyp");
	const gchar *H = gtk_entry_get_text(GTK_ENTRY(entryH));
	bool zmiana = FALSE;
	int typ;
	cPunkt punkt;

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryPunktyNr", "entryPunktyX",
		"entryPunktyY", "entryPunktyKod", punkt)) {
		punkt.setH(g_strtod(H, NULL));
		typ = gtk_combo_box_get_active(GTK_COMBO_BOX(entryTyp));
		if (typ < 0) typ = 0;
		punkt.setTyp(typ);
		dbgeod_point_add_config(punkt, &zmiana);
		if (zmiana) {
			geod_entry_inc(GTK_WIDGET(button), "entryPunktyNr");
			geod_entry_clear(GTK_WIDGET(button), "entryPunktyX");
			geod_entry_clear(GTK_WIDGET(button), "entryPunktyY");
			geod_entry_clear(GTK_WIDGET(button), "entryPunktyH");
			geod_entry_clear(GTK_WIDGET(button), "entryPunktyKod");
			treePunkty_update_view(treePunkty);
		}
	}
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarPunkty", _("Musisz obowiązkowo podać Nr, X, Y"));
		return;
	}
}


void on_winPunkty_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treePunkty");
	GtkWidget *statusbar = lookup_widget(GTK_WIDGET(widget), "sbarPunkty");
	GtkTreeStore *punkty_model;
	char konfiguracja_dodaj[CONFIG_VALUE_MAX_LEN];
	char *sbar_msg;
	dbgeod_config_get("dodaj_pkt", konfiguracja_dodaj);
	if (g_str_equal(konfiguracja_dodaj, "dodaj"))
		sbar_msg = g_strdup(_("Tryb dodawania punktów: DODAJ"));
	else if (g_str_equal(konfiguracja_dodaj, "pozostaw"))
		sbar_msg = g_strdup(_("Tryb dodawania punktów: POZOSTAW"));
	else if (g_str_equal(konfiguracja_dodaj, "usrednij"))
		sbar_msg = g_strdup(_("Tryb dodawania punktów: UŚREDNIJ"));
	else if (g_str_equal(konfiguracja_dodaj, "zapytaj"))
		sbar_msg = g_strdup(_("Tryb dodawania punktów: ZAPYTAJ"));
	else
		sbar_msg = g_strdup("");
	
	gtk_statusbar_push(GTK_STATUSBAR(statusbar), 2, sbar_msg);
	g_free(sbar_msg);
	
	punkty_model=gtk_tree_store_new(N_PKT_COLUMNS, G_TYPE_INT, G_TYPE_STRING, \
	G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING,G_TYPE_DOUBLE, 
	G_TYPE_STRING, G_TYPE_INT);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), \
	    GTK_TREE_MODEL(punkty_model));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;
	
	// kolumna nr
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Nr"), renderer, "text", COL_PKT_NR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);
	
	// kolumna x
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("X"), renderer, "text", COL_PKT_X, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 80);
	
	// kolumna y
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("Y"), renderer, "text", COL_PKT_Y, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 80);

	// kolumna h
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("H"), renderer, "text", COL_PKT_H, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna kod
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("Kod"), renderer, "text", COL_PKT_KOD, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// kolumna typ
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
		-1, _("Typ"), renderer, "text", COL_PKT_TYP, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 40);

	// ODCZYTANIE PUNKTOW Z BAZY
	treePunkty_update_view(treeview);
}

void on_tbtnPunktyOdswiez_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treePunkty");
	treePunkty_update_view(treeview);
}


void on_tbtnPunktyModyfikacja_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *treePunkty = lookup_widget(GTK_WIDGET(toolbutton), "treePunkty");
	GtkWidget *btnWyczysc = lookup_widget(GTK_WIDGET(toolbutton), "tbtnPunktyWyczysc");
	GtkWidget *entryH = lookup_widget(GTK_WIDGET(toolbutton), "entryPunktyH");
	GtkWidget *entryTyp = lookup_widget(GTK_WIDGET(toolbutton), "entryPunktyTyp");
	GtkWidget *dialog;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treePunkty));
	GtkTreePath *tpath;
	GtkTreeIter iter;
	int punkt_id, typ;
	char *pkt_nr;
	cPunkt punkt;

	const gchar *H = gtk_entry_get_text(GTK_ENTRY(entryH));

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPunktyNr", "entryPunktyX",
		"entryPunktyY", "entryPunktyKod", punkt)) {
		punkt.setH(g_strtod(H, NULL));
		typ = gtk_combo_box_get_active(GTK_COMBO_BOX(entryTyp));
		if (typ < 0) typ = 0;
		punkt.setTyp(typ);
	}
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPunkty", _("Musisz obowiązkowo podać Nr, X, Y"));
		return;
	}

	gtk_tree_view_get_cursor(GTK_TREE_VIEW(treePunkty), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_PKT_ID, &punkt_id, COL_PKT_NR, &pkt_nr, -1);
		if (!g_str_equal(pkt_nr, punkt.getNr()) && !dbgeod_point_find(punkt.getNr()) ) {
			dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, _("Punkt o takim numerze znajduje się już w bazie"));
			gtk_dialog_run(GTK_DIALOG(dialog));
			gtk_widget_destroy(dialog);
		}
		else {
			dbgeod_point_update(punkt_id, punkt);
			treePunkty_update_view(treePunkty);
			g_signal_emit_by_name(G_OBJECT(btnWyczysc), "clicked");
		}

		g_free(pkt_nr);
		gtk_tree_path_free(tpath);
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPunkty", _("Zaznacz najpierw punkt w tabeli"));
}


void on_tbtnPunktyUsun_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *treePunkty = lookup_widget(GTK_WIDGET(toolbutton), "treePunkty");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treePunkty));
	GtkTreePath *tpath;
	GtkTreeIter iter;
	int punkt_id;
	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(treePunkty), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, 0, &punkt_id, -1);
		dbgeod_point_del(punkt_id);
		treePunkty_update_view(treePunkty);
		gtk_tree_path_free(tpath);
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPunkty", _("Zaznacz najpierw punkt w tabeli"));
}


void on_tbtnPunktyEksport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	if(geod_open_window(create_winEksportTxt, "winEksportTxt") == NULL)
		geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarPunkty", GEODERR_WINDOW_OPEN);
}


void on_tbtnPunktyImport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *dialog = create_dialogImportTxt();
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treePunkty");
	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK)
		treePunkty_update_view(treeview);
	
	gtk_widget_destroy(GTK_WIDGET(dialog));
}

gboolean on_entryPunktyKod_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Kod_set(widget, "sbarPunkty");
	return FALSE;
}


gboolean on_entryPunktyKod_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPunkty");
	return FALSE;
}


void on_entryPunktyFiltr_changed (GtkEditable *editable, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(editable), "treePunkty");
	const char *nr_pkt = gtk_entry_get_text(GTK_ENTRY(editable));
	treePunkty_update_view(treeview, nr_pkt);
}


/* winZMareka */
void on_winZMareka_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winZMareka");
}


gboolean on_entryZMarekaSt1Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	GtkWidget *entryAlfa = lookup_widget(GTK_WIDGET(widget), "entryZMarekaAlfa");
	GtkWidget *entryBeta = lookup_widget(GTK_WIDGET(widget), "entryZMarekaBeta");
	GtkWidget *entryGamma = lookup_widget(GTK_WIDGET(widget), "entryZMarekaGamma");
	GtkWidget *entryDelta = lookup_widget(GTK_WIDGET(widget), "entryZMarekaDelta");
	
	geod_update_entry_obs(GTK_WIDGET(entryAlfa), "entryZMarekaSt2Nr", \
		"entryZMarekaSt1Nr", "entryZMarekaP1Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryBeta), "entryZMarekaL1Nr", \
		"entryZMarekaSt1Nr", "entryZMarekaSt2Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryGamma), "entryZMarekaSt1Nr", \
		"entryZMarekaSt2Nr", "entryZMarekaP2Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryDelta), "entryZMarekaL2Nr", \
		"entryZMarekaSt2Nr", "entryZMarekaSt1Nr", NULL);
	return FALSE;
}


gboolean on_entryZMarekaSt2Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	GtkWidget *entryAlfa = lookup_widget(GTK_WIDGET(widget), "entryZMarekaAlfa");
	GtkWidget *entryBeta = lookup_widget(GTK_WIDGET(widget), "entryZMarekaBeta");
	GtkWidget *entryGamma = lookup_widget(GTK_WIDGET(widget), "entryZMarekaGamma");
	GtkWidget *entryDelta = lookup_widget(GTK_WIDGET(widget), "entryZMarekaDelta");
	
	geod_update_entry_obs(GTK_WIDGET(entryAlfa), "entryZMarekaSt2Nr", \
		"entryZMarekaSt1Nr", "entryZMarekaP1Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryBeta), "entryZMarekaL1Nr", \
		"entryZMarekaSt1Nr", "entryZMarekaSt2Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryGamma), "entryZMarekaSt1Nr", \
		"entryZMarekaSt2Nr", "entryZMarekaP2Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryDelta), "entryZMarekaL2Nr", \
		"entryZMarekaSt2Nr", "entryZMarekaSt1Nr", NULL);
	return FALSE;
}


gboolean on_entryZMarekaL1Nr_focus_out_event (GtkWidget *widget,
		GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarZMareka");
	return FALSE;
}


gboolean on_entryZMarekaP1Nr_focus_out_event (GtkWidget *widget,
		GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarZMareka");
	return FALSE;
}


gboolean on_entryZMarekaP2Nr_focus_out_event (GtkWidget *widget,
		GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarZMareka");
	return FALSE;
}

gboolean on_entryZMarekaL2Nr_focus_out_event (GtkWidget *widget,
		GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarZMareka");
	return FALSE;
}


void on_tbtnZMarekaWykonaj_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *entrySt1Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryZMarekaSt1Nr");
	GtkWidget *entrySt2Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryZMarekaSt2Nr");

	cPunkt stanowisko1, stanowisko2;
	cPunkt stanowisko1L, stanowisko1P;
	cPunkt stanowisko2L, stanowisko2P;
	double kat1L, kat1P, kat2L, kat2P;
	int geod_error;

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaSt1X");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaSt1Y");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaSt2X");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaSt2Y");
	g_object_set_data(G_OBJECT(entrySt1Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
	g_object_set_data(G_OBJECT(entrySt2Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
	gtk_entry_set_editable(GTK_ENTRY(entrySt1Nr), TRUE);
	gtk_entry_set_editable(GTK_ENTRY(entrySt2Nr), TRUE);

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaP1Nr",
		"entryZMarekaP1X", "entryZMarekaP1Y", NULL, stanowisko1P));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarZMareka",
			_("Dla P1 musisz obowiązkowo podać Nr, X, Y"));
		return;
	}
	
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaL1Nr",
		"entryZMarekaL1X", "entryZMarekaL1Y", NULL, stanowisko1L));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarZMareka",
			_("Dla L1 musisz obowiązkowo podać Nr, X, Y"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaP2Nr",
		"entryZMarekaP2X", "entryZMarekaP2Y", NULL, stanowisko2P));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarZMareka",
			_("Dla P2 musisz obowiązkowo podać Nr, X, Y"));
		return;
	}
	
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaL2Nr",
		"entryZMarekaL2X", "entryZMarekaL2Y", NULL, stanowisko2L));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarZMareka",
			_("Dla L2 musisz obowiązkowo podać Nr, X, Y"));
		return;
	}

	if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryZMarekaAlfa", kat1P));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarZMareka",
			_("Musisz obowiązkowo podać kąt alfa"));
		return;
	}

	if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryZMarekaBeta", kat1L));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarZMareka",
			_("Musisz obowiązkowo podać kąt beta"));
		return;
	}

	if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryZMarekaGamma", kat2P));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarZMareka", "Musisz obowiązkowo podać kąt gamma");
		return;
	}

	if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryZMarekaDelta", kat2L));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarZMareka",
			_("Musisz obowiązkowo podać kąt delta"));
		return;
	}

	geod_error = zadanie_Mareka(stanowisko1L, stanowisko1P, stanowisko2L, stanowisko2P,
		kat1L, kat1P, kat2L, kat2P, stanowisko1, stanowisko2);

	if (geod_error == GEODERR_OK) {
		geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaSt1X",
			"entryZMarekaSt1Y", stanowisko1);
		geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaSt2X",
			"entryZMarekaSt2Y", stanowisko2);
		g_object_set_data(G_OBJECT(entrySt1Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
		g_object_set_data(G_OBJECT(entrySt2Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
	}
	else {
		g_object_set_data(G_OBJECT(entrySt1Nr), "status_dodaj", GINT_TO_POINTER(PKT_ERROR));
		g_object_set_data(G_OBJECT(entrySt2Nr), "status_dodaj", GINT_TO_POINTER(PKT_ERROR));
		geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarZMareka", geod_error);
	}
}


void on_tbtnZMarekaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *focus = lookup_widget(GTK_WIDGET(toolbutton), "entryZMarekaP1Nr");
	GtkWidget *entrySt1Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryZMarekaSt1Nr");
	GtkWidget *entrySt2Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryZMarekaSt2Nr");
	
	g_object_set_data(G_OBJECT(entrySt1Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
	g_object_set_data(G_OBJECT(entrySt2Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
	gtk_entry_set_editable(GTK_ENTRY(entrySt2Nr), TRUE);
	gtk_entry_set_editable(GTK_ENTRY(entrySt1Nr), TRUE);

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaP1Nr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaP1X");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaP1Y");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaP2Nr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaP2X");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaP2Y");

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaL1Nr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaL1X");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaL1Y");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaL2Nr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaL2X");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaL2Y");

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaAlfa");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaBeta");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaGamma");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaDelta");

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaSt1Nr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaSt1X");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaSt1Y");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaSt1Kod");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaSt2Nr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaSt2X");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaSt2Y");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryZMarekaSt2Kod");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}

void on_btnZMarekaDodajWynik_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktWynikSt1, pktWynikSt2;
	bool zmiana_st1 = FALSE, zmiana_st2 = FALSE;
	int s_dodaj_st1, s_dodaj_st2, zadanie_id;
	double kat;
	GtkWidget *entrySt1Nr = lookup_widget(GTK_WIDGET(button), "entryZMarekaSt1Nr");
	GtkWidget *entrySt2Nr = lookup_widget(GTK_WIDGET(button), "entryZMarekaSt2Nr");
	GtkWidget *buttonWyczysc = lookup_widget(GTK_WIDGET(button), "tbtnZMarekaWyczysc");

	// gdy nie ustawiono statusu - ustaw brak obliczen
	if (g_object_get_data(G_OBJECT(entrySt1Nr), "status_dodaj") == NULL)
		g_object_set_data(G_OBJECT(entrySt1Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));

	if (g_object_get_data(G_OBJECT(entrySt2Nr), "status_dodaj") == NULL)
		g_object_set_data(G_OBJECT(entrySt2Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));

	s_dodaj_st1 = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entrySt1Nr), "status_dodaj"));
	s_dodaj_st2 = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entrySt2Nr), "status_dodaj"));
	
	// gdy brak obliczen przerwij
	if (s_dodaj_st1 == PKT_BRAK_DANYCH && s_dodaj_st2 == PKT_BRAK_DANYCH) {
		geod_sbar_set(GTK_WIDGET(button), "sbarZMareka", _("Brak obliczeń"));
		return;
	}
	if (s_dodaj_st1 == PKT_ERROR || s_dodaj_st2 == PKT_ERROR) {
		geod_sbar_set(GTK_WIDGET(button), "sbarZMareka", _("Brak rozwiązania"));
		return;
	}

	// kontrola wymaganych danych
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryZMarekaSt1Nr", "entryZMarekaSt1X",
		"entryZMarekaSt1Y",	"entryZMarekaSt1Kod", pktWynikSt1) &&
	    !geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryZMarekaSt2Nr", "entryZMarekaSt2X",
		"entryZMarekaSt2Y",	"entryZMarekaSt2Kod", pktWynikSt2))	{
		
		if (s_dodaj_st1 == PKT_DODAJ) {
			dbgeod_point_add_config(pktWynikSt1, &zmiana_st1);
			if (zmiana_st1) {
				g_object_set_data(G_OBJECT(entrySt1Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODANY));
				gtk_entry_set_editable(GTK_ENTRY(entrySt1Nr), FALSE);
			}
		}
		else
			zmiana_st1 = TRUE;

		if (s_dodaj_st2 == PKT_DODAJ) {
			dbgeod_point_add_config(pktWynikSt2, &zmiana_st2);
			if (zmiana_st2) {
				g_object_set_data(G_OBJECT(entrySt2Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODANY));
				gtk_entry_set_editable(GTK_ENTRY(entrySt2Nr), FALSE);
			}
		}
		else
			zmiana_st2 = TRUE;
		
		if (zmiana_st1 && zmiana_st2) {
			GtkWidget *entryL1 = lookup_widget(GTK_WIDGET(button), "entryZMarekaL1Nr");
			GtkWidget *entryL2 = lookup_widget(GTK_WIDGET(button), "entryZMarekaL2Nr");
			GtkWidget *entryP1 = lookup_widget(GTK_WIDGET(button), "entryZMarekaP1Nr");
			GtkWidget *entryP2 = lookup_widget(GTK_WIDGET(button), "entryZMarekaP2Nr");
			dbgeod_hist_add("ZadanieMareka", zadanie_id);
			dbgeod_hist_point_set(zadanie_id, "St1Nr", pktWynikSt1.getNr(), 1);
			dbgeod_hist_point_set(zadanie_id, "St2Nr", pktWynikSt2.getNr(), 1);
			dbgeod_hist_point_set(zadanie_id, "L1Nr", gtk_entry_get_text(GTK_ENTRY(entryL1)), 0);
			dbgeod_hist_point_set(zadanie_id, "L2Nr", gtk_entry_get_text(GTK_ENTRY(entryL2)), 0);
			dbgeod_hist_point_set(zadanie_id, "P1Nr", gtk_entry_get_text(GTK_ENTRY(entryP1)), 0);
			dbgeod_hist_point_set(zadanie_id, "P2Nr", gtk_entry_get_text(GTK_ENTRY(entryP2)), 0);
			geod_entry_get_kat(GTK_WIDGET(button), "entryZMarekaAlfa", kat);
			dbgeod_hist_par_set(zadanie_id, "KatAlfa", kat, 0);
			geod_entry_get_kat(GTK_WIDGET(button), "entryZMarekaBeta", kat);
			dbgeod_hist_par_set(zadanie_id, "KatBeta", kat, 0);
			geod_entry_get_kat(GTK_WIDGET(button), "entryZMarekaGamma", kat);
			dbgeod_hist_par_set(zadanie_id, "KatGamma", kat, 0);
			geod_entry_get_kat(GTK_WIDGET(button), "entryZMarekaDelta", kat);
			dbgeod_hist_par_set(zadanie_id, "KatDelta", kat, 0);

			
			GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Dalsze obliczenia?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
				g_signal_emit_by_name(buttonWyczysc, "clicked");
			gtk_widget_destroy(dialog);
		}
	}
	else
		geod_sbar_set(GTK_WIDGET(button), "sbarZMareka", _("Nie podano Nr punktów"));
}


void on_tbtnZMarekaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winZMareka");
	gtk_widget_destroy(window);
}

gboolean on_entryZMarekaL1Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarZMareka");
	return FALSE;
}


gboolean on_entryZMarekaP1Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarZMareka");
	return FALSE;
}


gboolean on_entryZMarekaP2Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarZMareka");
	return FALSE;
}


gboolean on_entryZMarekaL2Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarZMareka");
	return FALSE;
}

void on_tbtnZMarekaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GSList *lpunkty = NULL;
	cPunkt *punkt;
	double obs_kat;
	char *txt_kat = NULL;
	bool brak_danych = FALSE;

	GtkWidget *dialogRaport = create_dialogRaport();
	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");
	gtk_widget_show(dialogRaport);
	
	geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nZadanie Mareka"));

	// punkty nawiazania
	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaP1Nr",
		"entryZMarekaP1X", "entryZMarekaP1Y", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		geod_raport_error(textview, _("Dla P1 nie podano wszystkich wymaganych danych!"));
		delete punkt;
	}

	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaL1Nr",
		"entryZMarekaL1X", "entryZMarekaL1Y", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		geod_raport_error(textview, _("Dla L1 nie podano wszystkich wymaganych danych!"));
		delete punkt;
	}

	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaP2Nr",
		"entryZMarekaP2X", "entryZMarekaP2Y", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		geod_raport_error(textview, _("Dla P2 nie podano wszystkich wymaganych danych!"));
		delete punkt;
	}

	punkt = new cPunkt;	
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaL2Nr",
		"entryZMarekaL2X", "entryZMarekaL2Y", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		geod_raport_error(textview, _("Dla L2 nie podano wszystkich wymaganych danych!"));
		delete punkt;
	}
	
	geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty nawiązania"));
	g_slist_free(lpunkty);
	lpunkty = NULL;

	// mierzaone katy
	if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryZMarekaAlfa", obs_kat)) {
		txt_kat = geod_kat_from_rad(obs_kat);
		geod_raport_line(textview, _("\nKąt alfa"), txt_kat);
		g_free(txt_kat);
	}
	else
		geod_raport_error(textview, _("Nie podano kąta alfa!"));

	if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryZMarekaBeta", obs_kat)) {
		txt_kat = geod_kat_from_rad(obs_kat);
		geod_raport_line(textview, _("Kąt beta"), txt_kat);
		g_free(txt_kat);
	}
	else
		geod_raport_error(textview, _("Nie podano kąta beta!"));

	if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryZMarekaGamma", obs_kat)) {
		txt_kat = geod_kat_from_rad(obs_kat);
		geod_raport_line(textview, _("Kąt gamma"), txt_kat);
		g_free(txt_kat);
	}
	else
		geod_raport_error(textview, _("Nie podano kąta gamma!"));

	if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryZMarekaDelta", obs_kat)) {
		txt_kat = geod_kat_from_rad(obs_kat);
		geod_raport_line(textview, _("Kąt delta"), txt_kat);
		g_free(txt_kat);
	}
	else
		geod_raport_error(textview, _("Nie podano kąta delta!"));

	// wynik obliczeń
	brak_danych = FALSE;
	punkt = new cPunkt;
	if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaSt1Nr",
		"entryZMarekaSt1X", "entryZMarekaSt1Y", "entryZMarekaSt1Kod", *punkt) < 1)
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		brak_danych = TRUE;
		delete punkt;
	}

	punkt = new cPunkt;
	if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaSt2Nr",
		"entryZMarekaSt2X", "entryZMarekaSt2Y", "entryZMarekaSt2Kod", *punkt) < 1)
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		brak_danych = TRUE;
		delete punkt;
	}
	
	if (brak_danych)
		geod_raport_error(textview, _("Brak obliczeń lub brak rozwiązania"), TRUE);
	else
		geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty wyznaczane"));
	
	g_slist_free(lpunkty);
	geod_raport_stopka(textview);
}


void on_tbtnZMarekaWidok_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	int npoints = 6;
	cPunkt *pktDane = new cPunkt[npoints];
	GdkPoint *pktWidok = new GdkPoint[npoints+1];

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaP1Nr",
		"entryZMarekaP1X", "entryZMarekaP1Y", NULL, pktDane[0]));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarZMareka",
			_("Dla P1 musisz obowiązkowo podać Nr, X, Y"));
		delete [] pktDane;
		delete [] pktWidok;
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaL1Nr",
		"entryZMarekaL1X", "entryZMarekaL1Y", NULL, pktDane[1]));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarZMareka",
			_("Dla L1 musisz obowiązkowo podać Nr, X, Y"));
		delete [] pktDane;
		delete [] pktWidok;
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaP2Nr",
		"entryZMarekaP2X", "entryZMarekaP2Y", NULL, pktDane[2]));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarZMareka",
			_("Dla P2 musisz obowiązkowo podać Nr, X, Y"));
		delete [] pktDane;
		delete [] pktWidok;
		return;
	}
	
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaL2Nr",
		"entryZMarekaL2X", "entryZMarekaL2Y", NULL, pktDane[3]));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarZMareka",
			_("Dla L2 musisz obowiązkowo podać Nr, X, Y"));
		delete [] pktDane;
		delete [] pktWidok;
		return;
	}

	if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaSt1Nr",
		"entryZMarekaSt1X", "entryZMarekaSt1Y", NULL, pktDane[4]) < 1);
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarZMareka", _("Brak obliczeń"));
		delete [] pktDane;
		delete [] pktWidok;
		return;
	}

	if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryZMarekaSt2Nr",
		"entryZMarekaSt2X", "entryZMarekaSt2Y", NULL, pktDane[5]) < 1);
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarZMareka", _("Brak obliczeń"));
		delete [] pktDane;
		delete [] pktWidok;
		return;
	}

	GtkWidget *winWidok = create_winWidok();
	gtk_widget_show(winWidok);
	GtkWidget *darea = lookup_widget(GTK_WIDGET(winWidok), "drawingAreaWidok");

	geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, npoints);

	geod_draw_line(GTK_WIDGET(darea), pktWidok[4], pktWidok[0], LINE_KIERUNEK);
	geod_draw_line(GTK_WIDGET(darea), pktWidok[4], pktWidok[1], LINE_KIERUNEK);
	geod_draw_line(GTK_WIDGET(darea), pktWidok[5], pktWidok[2], LINE_KIERUNEK);
	geod_draw_line(GTK_WIDGET(darea), pktWidok[5], pktWidok[3], LINE_KIERUNEK);
	geod_draw_line(GTK_WIDGET(darea), pktWidok[4], pktWidok[5], LINE_BAZA);

	geod_draw_arc(GTK_WIDGET(darea), pktWidok[4], pktWidok[5], pktWidok[1]);
	geod_draw_arc(GTK_WIDGET(darea), pktWidok[4], pktWidok[0], pktWidok[5]);
	geod_draw_arc(GTK_WIDGET(darea), pktWidok[5], pktWidok[4], pktWidok[3]);
	geod_draw_arc(GTK_WIDGET(darea), pktWidok[5], pktWidok[2], pktWidok[4]);

	geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
	geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[1].getNr(), PKT_OSNOWA);
	geod_draw_point(GTK_WIDGET(darea), pktWidok[2], pktDane[2].getNr(), PKT_OSNOWA);
	geod_draw_point(GTK_WIDGET(darea), pktWidok[3], pktDane[3].getNr(), PKT_OSNOWA);
	geod_draw_point(GTK_WIDGET(darea), pktWidok[4], pktDane[4].getNr(), PKT_WYNIK);
	geod_draw_point(GTK_WIDGET(darea), pktWidok[5], pktDane[5].getNr(), PKT_WYNIK);
			
	gtk_widget_queue_draw(darea);
	delete [] pktDane;
	delete [] pktWidok;
}


gboolean on_entryZMarekaKod_focus_in_event  (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Kod_set(widget, "sbarZMareka");
	return FALSE;
}


gboolean on_entryZMarekaKod_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarZMareka");
	return FALSE;
}


/* dialogKod */
void on_dialogKod_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeDialogKod");
	GtkTreeStore *punkty_model;
	
	punkty_model=gtk_tree_store_new(3, G_TYPE_STRING, G_TYPE_STRING, \
		G_TYPE_STRING);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), \
	    GTK_TREE_MODEL(punkty_model));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;
	
	// kolumna kod znakowy
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Kod Z"), renderer, "text", 0, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);
	
	// kolumna kod liczbowy
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("Kod L"), renderer, "text", 1, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);
	
	// kolumna obiekt
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("Obiekt"), renderer, "text", 2, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 400);

	// ODCZYTANIE KODOW Z BAZY
	treeDialogKod_update_view(treeview);
}

void on_dialogKod_response (GtkDialog *dialog, gint response_id, gpointer user_data)
{
	if (response_id == GTK_RESPONSE_OK) {
		GtkWidget *treeview = lookup_widget(GTK_WIDGET(dialog), "treeDialogKod");
		GtkWidget *entry = lookup_widget(GTK_WIDGET(dialog), "eventEntry");
		GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
		GtkTreePath *tpath;
		char *pkt_kod = NULL;
		GtkTreeIter iter;

		gtk_tree_view_get_cursor(GTK_TREE_VIEW(treeview), &tpath, NULL);
		if (tpath != NULL) {
			gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
			gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, 0, &pkt_kod, -1);
			gtk_entry_set_text(GTK_ENTRY(entry), pkt_kod);
			g_free(pkt_kod);
			gtk_tree_path_free(tpath);
		}
	}
	gtk_widget_destroy(GTK_WIDGET(dialog));		
}


void on_entryDialogKodFiltr_changed (GtkEditable *editable, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(editable), "treeDialogKod");
	const char *find_str = gtk_entry_get_text(GTK_ENTRY(editable));
	treeDialogKod_update_view(treeview, find_str);
}


/* dialogPunkty */
void on_dialogPunkty_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeDialogPunkty");
	GtkTreeStore *punkty_model;
	
	punkty_model=gtk_tree_store_new(N_PKT_COLUMNS, G_TYPE_INT, G_TYPE_STRING, \
	G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING,G_TYPE_DOUBLE,
	G_TYPE_STRING, G_TYPE_INT);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), \
	    GTK_TREE_MODEL(punkty_model));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;
	
	// kolumna nr
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Nr"), renderer, "text", COL_PKT_NR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);
	
	// kolumna x
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("X"), renderer, "text", COL_PKT_X, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);
	
	// kolumna y
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("Y"), renderer, "text", COL_PKT_Y, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);

	// kolumna h
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("H"), renderer, "text", COL_PKT_H, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);

	// kolumna kod
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("Kod"), renderer, "text", COL_PKT_KOD, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// ODCZYTANIE PUNKTOW Z BAZY
	treePunkty_update_view(treeview);
}

void on_dialogPunkty_response (GtkDialog *dialog, gint response_id, gpointer user_data)
{
	if (response_id == GTK_RESPONSE_OK) {
		GtkWidget *treeview = lookup_widget(GTK_WIDGET(dialog), "treeDialogPunkty");
		GtkWidget *entry = lookup_widget(GTK_WIDGET(dialog), "eventEntry");
		GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
		GtkTreePath *tpath;
		GtkTreeIter iter;
		char *pkt_nr = NULL;

		gtk_tree_view_get_cursor(GTK_TREE_VIEW(treeview), &tpath, NULL);
		if (tpath != NULL) {
			gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
			gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_PKT_NR, &pkt_nr, -1);
			gtk_entry_set_text(GTK_ENTRY(entry), pkt_nr);
			g_free(pkt_nr);
			gtk_tree_path_free(tpath);
		}
	}
	gtk_widget_destroy(GTK_WIDGET(dialog));
}


void on_tbtnPunktyRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GSList *punkty = NULL;
	GSList *punkty_id = NULL;
	GtkWidget *dialogRaport = create_dialogRaport();
	gtk_widget_show(dialogRaport);

	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");

	geod_raport_naglowek(textview, _("WYKAZ PUNKTÓW"));
	if (dbgeod_points_find(NULL, &punkty, &punkty_id) == GEODERR_OK)
		geod_raport_table_pkt(textview, punkty, TRUE);
	else
		geod_raport_error(textview, _("Nie można odczytać punktów z dazy danych!"), TRUE);
	
	geod_raport_stopka(textview);

	g_slist_free(punkty);
	g_slist_free(punkty_id);
}


void on_tbtnPunktyWidok_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt *pktDane = NULL;
	GdkPoint *pktWidok = NULL;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treePunkty");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	guint l_wierszy = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	guint i;
	char *pkt_nr = NULL;
	double pkt_x, pkt_y;

	GtkWidget *winWidok = create_winWidok();
	gtk_widget_show(winWidok);
	GtkWidget *darea = lookup_widget(GTK_WIDGET(winWidok), "drawingAreaWidok");
	
	pktDane = new cPunkt[l_wierszy];
	pktWidok = new GdkPoint[l_wierszy];
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (i = 0; i < l_wierszy; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_PKT_NR, &pkt_nr,
		COL_PKT_X_DOUBLE, &pkt_x, COL_PKT_Y_DOUBLE, &pkt_y, -1);
		pktDane[i].setNr(pkt_nr);
		pktDane[i].setXY(pkt_x, pkt_y);
		g_free(pkt_nr);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, l_wierszy);
	
	// rysowanie punktow
	for (i = 0; i < l_wierszy; i++)
	{
		geod_draw_point(GTK_WIDGET(darea), pktWidok[i], pktDane[i].getNr(), PKT_OSNOWA);
	}
	gtk_widget_queue_draw(darea);
	delete [] pktDane;
	delete [] pktWidok;
}


void on_entryDialogPunktyFiltr_changed (GtkEditable *editable, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(editable), "treeDialogPunkty");
	const char *nr_pkt = gtk_entry_get_text(GTK_ENTRY(editable));
	treePunkty_update_view(treeview, nr_pkt);
}


/* winWidok */
void on_drawingAreaWidok_realize (GtkWidget *widget, gpointer user_data)
{
	GdkPixmap *pixmapWidok = gdk_pixmap_new (widget->window, widget->allocation.width,
		widget->allocation.height, -1);
	GtkWidget *winWidok = lookup_widget(GTK_WIDGET(widget), "winWidok");
	
    g_object_set_data_full (G_OBJECT (winWidok), "pixmapWidok",
    	g_object_ref (pixmapWidok), (GDestroyNotify) g_object_unref);
	gdk_draw_rectangle (pixmapWidok, widget->style->white_gc, TRUE, 0, 0,
		widget->allocation.width, widget->allocation.height);
	gdk_draw_rectangle (pixmapWidok, widget->style->black_gc, FALSE, 0, 0,
		widget->allocation.width-1, widget->allocation.height-1);
}


gboolean on_drawingAreaWidok_expose_event (GtkWidget *widget, GdkEventExpose *event,
	gpointer user_data)
{
	GtkWidget *pixmap = lookup_widget(GTK_WIDGET(widget), "pixmapWidok");
	gdk_draw_drawable (widget->window, widget->style->fg_gc[GTK_WIDGET_STATE(widget)],
		GDK_PIXMAP(pixmap), event->area.x, event->area.y, event->area.x, event->area.y, -1, -1 );
  	return FALSE;
}

void on_btnWidokZamknij_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *winWidok = lookup_widget(GTK_WIDGET(button), "winWidok");
	gtk_widget_destroy(winWidok);
}


/* winWciecia */
void on_winWciecia_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winWciecia");
}


void on_btnWcieciaKatoweDodajWynik_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktWynik;
	bool zmiana = FALSE;
	double kat;
	int zadanie_id;
	GtkWidget *buttonWyczysc = lookup_widget(GTK_WIDGET(button), "tbtnWcieciaWyczysc");
	GtkWidget *entryL = lookup_widget(GTK_WIDGET(button), "entryWcieciaKatowePktLNr");
	GtkWidget *entryP = lookup_widget(GTK_WIDGET(button), "entryWcieciaKatowePktPNr");
	GtkWidget *entryL1 = lookup_widget(GTK_WIDGET(button), "entryWcieciaKatowePktL1Nr");
	GtkWidget *entryP1 = lookup_widget(GTK_WIDGET(button), "entryWcieciaKatowePktP1Nr");
	GtkWidget *cbtnOgolne = lookup_widget(GTK_WIDGET(button), "cbtnWcieciaKatoweOgolne");
	
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryWcieciaKatoweWynikNr",
		"entryWcieciaKatoweWynikX", "entryWcieciaKatoweWynikY",
		"entryWcieciaKatoweWynikKod", pktWynik)) {
		dbgeod_point_add_config(pktWynik, &zmiana);
		if (zmiana) {
			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnOgolne))) {
				dbgeod_hist_add("WciecieKatoweOgolne", zadanie_id);
				dbgeod_hist_point_set(zadanie_id, "L1Nr", gtk_entry_get_text(GTK_ENTRY(entryL1)), 0);
				dbgeod_hist_point_set(zadanie_id, "P1Nr", gtk_entry_get_text(GTK_ENTRY(entryP1)), 0);
			}
			else
				dbgeod_hist_add("WciecieKatowe", zadanie_id);
			
			dbgeod_hist_point_set(zadanie_id, "LNr", gtk_entry_get_text(GTK_ENTRY(entryL)), 0);
			dbgeod_hist_point_set(zadanie_id, "PNr", gtk_entry_get_text(GTK_ENTRY(entryP)), 0);
			dbgeod_hist_point_set(zadanie_id, "WNr", pktWynik.getNr(), 1);
			geod_entry_get_kat(GTK_WIDGET(button), "entryWcieciaKatoweKatL", kat);
			dbgeod_hist_par_set(zadanie_id, "KatL", kat, 0);
			geod_entry_get_kat(GTK_WIDGET(button), "entryWcieciaKatoweKatP", kat);
			dbgeod_hist_par_set(zadanie_id, "KatP", kat, 0);
			
			GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Dalsze obliczenia?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
				g_signal_emit_by_name(buttonWyczysc, "clicked");
			gtk_widget_destroy(dialog);
		}
	}
	else
		geod_sbar_set(GTK_WIDGET(button), "sbarWciecia", 
			_("Nie wykonano obliczeń bądź nie podano Nr punktu"));
}

gboolean on_entryWcieciaKatowePktLNr_focus_in_event (GtkWidget *widget,
		GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarWciecia");
  	return FALSE;
}


gboolean on_entryWcieciaKatowePktLNr_focus_out_event (GtkWidget *widget,
		GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarWciecia");
	return FALSE;
}

gboolean on_entryWcieciaKatowePktPNr_focus_in_event (GtkWidget *widget,
		GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarWciecia");
  	return FALSE;
}


gboolean on_entryWcieciaKatowePktPNr_focus_out_event (GtkWidget *widget,
		GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaKatoweWynikNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	GtkWidget *entryKatL = lookup_widget(GTK_WIDGET(widget), "entryWcieciaKatoweKatL");
	GtkWidget *entryKatP = lookup_widget(GTK_WIDGET(widget), "entryWcieciaKatoweKatP");
	GtkWidget *cbtnOgolne = lookup_widget(GTK_WIDGET(widget), "cbtnWcieciaKatoweOgolne");

	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnOgolne))) {
		geod_update_entry_obs(GTK_WIDGET(entryKatL), "entryWcieciaKatoweWynikNr", \
			"entryWcieciaKatowePktLNr", "entryWcieciaKatowePktL1Nr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKatP), "entryWcieciaKatowePktP1Nr", \
			"entryWcieciaKatowePktPNr", "entryWcieciaKatoweWynikNr", NULL);
	}
	else {
		geod_update_entry_obs(GTK_WIDGET(entryKatL), "entryWcieciaKatoweWynikNr", \
			"entryWcieciaKatowePktLNr", "entryWcieciaKatowePktPNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKatP), "entryWcieciaKatowePktLNr", \
			"entryWcieciaKatowePktPNr", "entryWcieciaKatoweWynikNr", NULL);
	}
	return FALSE;
}


void on_cbtnWcieciaKatoweOgolne_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
	GtkWidget *labelL1Nr = lookup_widget(GTK_WIDGET(togglebutton), "labelWcieciaKatowePktL1");
	GtkWidget *labelL1X = lookup_widget(GTK_WIDGET(togglebutton), "labelWcieciaKatowePktL1X");
	GtkWidget *labelL1Y = lookup_widget(GTK_WIDGET(togglebutton), "labelWcieciaKatowePktL1Y");
	GtkWidget *labelP1Nr = lookup_widget(GTK_WIDGET(togglebutton), "labelWcieciaKatowePktP1");
	GtkWidget *labelP1X = lookup_widget(GTK_WIDGET(togglebutton), "labelWcieciaKatowePktP1X");
	GtkWidget *labelP1Y = lookup_widget(GTK_WIDGET(togglebutton), "labelWcieciaKatowePktP1Y");
	
	GtkWidget *entryL1Nr = lookup_widget(GTK_WIDGET(togglebutton), "entryWcieciaKatowePktL1Nr");
	GtkWidget *entryL1X = lookup_widget(GTK_WIDGET(togglebutton), "entryWcieciaKatowePktL1X");
	GtkWidget *entryL1Y = lookup_widget(GTK_WIDGET(togglebutton), "entryWcieciaKatowePktL1Y");
	GtkWidget *entryP1Nr = lookup_widget(GTK_WIDGET(togglebutton), "entryWcieciaKatowePktP1Nr");
	GtkWidget *entryP1X = lookup_widget(GTK_WIDGET(togglebutton), "entryWcieciaKatowePktP1X");
	GtkWidget *entryP1Y = lookup_widget(GTK_WIDGET(togglebutton), "entryWcieciaKatowePktP1Y");
	GtkWidget *imgWcieciaKatowe = lookup_widget(GTK_WIDGET(togglebutton), "imgWcieciaKatowe");
	char *pathname = NULL;
	bool status;

	if (gtk_toggle_button_get_active(togglebutton)) {
		pathname = find_pixmap_file ("w_katoweo.png");
		status = TRUE;
	}
	else {
		pathname = find_pixmap_file ("w_katowe.png");
		status = FALSE;
	}

	if (pathname != NULL)
		gtk_image_set_from_file (GTK_IMAGE(imgWcieciaKatowe), pathname);

	g_free (pathname);

	gtk_widget_set_sensitive(labelL1Nr, status);
	gtk_widget_set_sensitive(labelL1X, status);
	gtk_widget_set_sensitive(labelL1Y, status);
	gtk_widget_set_sensitive(labelP1Nr, status);
	gtk_widget_set_sensitive(labelP1X, status);
	gtk_widget_set_sensitive(labelP1Y, status);
	
	gtk_widget_set_sensitive(entryL1Nr, status);
	gtk_widget_set_sensitive(entryL1X, status);
	gtk_widget_set_sensitive(entryL1Y, status);
	gtk_widget_set_sensitive(entryP1Nr, status);
	gtk_widget_set_sensitive(entryP1X, status);
	gtk_widget_set_sensitive(entryP1Y, status);
}


gboolean on_entryWcieciaKatowePktL1Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarWciecia");
  	return FALSE;
}


gboolean on_entryWcieciaKatowePktL1Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaKatowePktP1Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarWciecia");
  	return FALSE;
}


gboolean on_entryWcieciaKatowePktP1Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaKod_focus_in_event (GtkWidget *widget,
                GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Kod_set(widget, "sbarWciecia");
  	return FALSE;
}


gboolean on_entryWcieciaKod_focus_out_event (GtkWidget *widget,
		GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarWciecia");
	return FALSE;
}


void on_tbtnWcieciaWykonaj_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	double katLewy, katPrawy;
	double dlugoscLW, dlugoscPW;
	double kierLewy, kierSrodek, kierPrawy;
	double azymutLW, azymutPW;
	double azymutWL, azymutWP;
	int geod_error = GEODERR_OK;
	cPunkt pktLewy, pktPrawy, pktSrodkowy, pktWyznaczany, pktLewy1, pktPrawy1;
	GtkWidget *cbtnKierunki = lookup_widget(GTK_WIDGET(toolbutton), "cbtnWcieciaWsteczKierunki");
	GtkWidget *cbtnOgolne = lookup_widget(GTK_WIDGET(toolbutton), "cbtnWcieciaKatoweOgolne");
	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookWciecia");
	
	switch (gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook)))
	{
		case WCIECIE_KATOWE:
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatoweWynikX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatoweWynikY");

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaKatowePktLNr", "entryWcieciaKatowePktLX",
				"entryWcieciaKatowePktLY", NULL, pktLewy));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia",
					_("Dla L musisz podać Nr, X, Y"));
				break;
			}
		
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaKatowePktPNr", "entryWcieciaKatowePktPX",
				"entryWcieciaKatowePktPY", NULL, pktPrawy));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia",
					_("Dla P musisz podać Nr, X, Y"));
				break;
			}

			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnOgolne))) {
				if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
					"entryWcieciaKatowePktL1Nr", "entryWcieciaKatowePktL1X",
					"entryWcieciaKatowePktL1Y", NULL, pktLewy1));
				else {
					geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia",
						_("Dla L1 musisz podać Nr, X, Y"));
					break;
				}
				if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
					"entryWcieciaKatowePktP1Nr", "entryWcieciaKatowePktP1X",
					"entryWcieciaKatowePktP1Y", NULL, pktPrawy1));
				else {
					geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia",
						_("Dla P1 musisz podać Nr, X, Y"));
					break;
				}
			}

			if (!geod_entry_get_kat(GTK_WIDGET(toolbutton),
				"entryWcieciaKatoweKatL", katLewy));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Musisz podać kąt alfa"));
				break;
			}

			if (!geod_entry_get_kat(GTK_WIDGET(toolbutton),
				"entryWcieciaKatoweKatP", katPrawy));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Musisz podać kąt beta"));
				break;
			}

			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnOgolne)))
				geod_error = wciecie_w_przod_ogolne(pktLewy, pktLewy1, katLewy, pktPrawy, pktPrawy1, katPrawy, pktWyznaczany);
			else
				geod_error = wciecie_katowe_w_przod(pktLewy, pktPrawy, katLewy, katPrawy, pktWyznaczany);
			
			if (geod_error == GEODERR_OK)
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaKatoweWynikX",
				"entryWcieciaKatoweWynikY", pktWyznaczany);
			else
				geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarWciecia", geod_error);
		break;

		case WCIECIE_LINIOWE:
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaLinioweWynikX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaLinioweWynikY");

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaLiniowePktLNr", "entryWcieciaLiniowePktLX",
				"entryWcieciaLiniowePktLY", NULL, pktLewy));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", 
					_("Dla L musisz podać Nr, X, Y"));
				break;
			}
		
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaLiniowePktPNr", "entryWcieciaLiniowePktPX",
				"entryWcieciaLiniowePktPY", NULL, pktPrawy));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia",
					_("Dla P musisz podać Nr, X, Y"));
				break;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton),
				"entryWcieciaLinioweDlugoscLW", dlugoscLW));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia",
					_("Musisz podać długość LW"));
				break;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton),
				"entryWcieciaLinioweDlugoscPW", dlugoscPW));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia",
					_("Musisz podać długość PW"));
				break;
			}

			geod_error = wciecie_liniowe(pktLewy, pktPrawy, dlugoscLW, dlugoscPW, pktWyznaczany);
			if (geod_error == GEODERR_OK)
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaLinioweWynikX",
				"entryWcieciaLinioweWynikY", pktWyznaczany);
			else
				geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarWciecia", geod_error);
		break;

		case WCIECIE_WSTECZ:
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczWynikX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczWynikY");

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaWsteczPktLNr", "entryWcieciaWsteczPktLX",
				"entryWcieciaWsteczPktLY", NULL, pktLewy));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia",
					_("Dla L musisz podać Nr, X, Y"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaWsteczPktCNr", "entryWcieciaWsteczPktCX",
				"entryWcieciaWsteczPktCY", NULL, pktSrodkowy));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia",
					_("Dla C musisz podać Nr, X, Y"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaWsteczPktPNr", "entryWcieciaWsteczPktPX",
				"entryWcieciaWsteczPktPY", NULL, pktPrawy));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia",
					_("Dla P musisz podać Nr, X, Y"));
				break;
			}

			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnKierunki)))
			{
				if (!geod_entry_get_kat(GTK_WIDGET(toolbutton),
					"entryWcieciaWsteczKierL", kierLewy));
				else {
					geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia",
						_("Musisz podać kierunek L"));
					break;
				}
			}
			else kierLewy = 0.0;

			if (!geod_entry_get_kat(GTK_WIDGET(toolbutton),
				"entryWcieciaWsteczKierC", kierSrodek));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia",
					_("Musisz podać kierunek C/kąt alfa"));
				break;
			}

			if (!geod_entry_get_kat(GTK_WIDGET(toolbutton),
				"entryWcieciaWsteczKierP", kierPrawy));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia",
					_("Musisz podać kierunek P/kąt beta"));
				break;
			}

			geod_error = wciecie_wstecz(pktLewy, pktSrodkowy, pktPrawy, kierSrodek-kierLewy, kierPrawy-kierLewy, pktWyznaczany);
			if (geod_error == GEODERR_OK)
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaWsteczWynikX",
				"entryWcieciaWsteczWynikY", pktWyznaczany);
			else
				geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarWciecia", geod_error);
		break;

		case WCIECIE_AZ_W_PRZOD:
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodWynikX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodWynikY");

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaAWPrzodPktLNr", "entryWcieciaAWPrzodPktLX",
				"entryWcieciaAWPrzodPktLY", NULL, pktLewy));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla L musisz podać Nr, X, Y"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaAWPrzodPktPNr", "entryWcieciaAWPrzodPktPX",
				"entryWcieciaAWPrzodPktPY", NULL, pktPrawy));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla P musisz podać Nr, X, Y"));
				break;
			}

			if (!geod_entry_get_kat(GTK_WIDGET(toolbutton),
				"entryWcieciaAWPrzodAzLW", azymutLW));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Musisz podać azymut LW"));
				break;
			}

			if (!geod_entry_get_kat(GTK_WIDGET(toolbutton),
				"entryWcieciaAWPrzodAzPW", azymutPW));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Musisz podać azymut PW"));
				break;
			}

			if (!wciecie_azymutalne_w_przod(pktLewy, pktPrawy, azymutLW, azymutPW,
				pktWyznaczany))
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodWynikX",
					"entryWcieciaAWPrzodWynikY", pktWyznaczany);
			else
				geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarWciecia", geod_error);
		break;

		case WCIECIE_AZ_WSTECZ:
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczWynikX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczWynikY");

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaAWsteczPktLNr", "entryWcieciaAWsteczPktLX",
				"entryWcieciaAWsteczPktLY", NULL, pktLewy));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla L musisz podać Nr, X, Y"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaAWsteczPktPNr", "entryWcieciaAWsteczPktPX",
				"entryWcieciaAWsteczPktPY", NULL, pktPrawy));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla P musisz podać Nr, X, Y"));
				break;
			}

			if (!geod_entry_get_kat(GTK_WIDGET(toolbutton),
				"entryWcieciaAWsteczAzWL", azymutWL));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Musisz podać azymut WL"));
				break;
			}

			if (!geod_entry_get_kat(GTK_WIDGET(toolbutton),
				"entryWcieciaAWsteczAzWP", azymutWP));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Musisz podać azymut WP"));
				break;
			}

			geod_error = wciecie_azymutalne_wstecz(pktLewy, pktPrawy, azymutWL, azymutWP, pktWyznaczany);
			if (geod_error == GEODERR_OK)
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczWynikX",
					"entryWcieciaAWsteczWynikY", pktWyznaczany);
			else
				geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarWciecia", geod_error);
		break;
	}
}


void on_tbtnWcieciaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookWciecia");
	GtkWidget *focus = NULL;
	switch (gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook)))
	{
		case WCIECIE_KATOWE:
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktLNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktLX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktLY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktPNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktPX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktPY");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktL1Nr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktL1X");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktL1Y");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktP1Nr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktP1X");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktP1Y");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatoweKatL");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatoweKatP");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatoweWynikNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatoweWynikX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatoweWynikY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaKatoweWynikKod");
			focus = lookup_widget(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktLNr");
			gtk_widget_grab_focus(GTK_WIDGET(focus));
		break;

		case WCIECIE_LINIOWE:
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaLiniowePktLNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaLiniowePktLX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaLiniowePktLY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaLiniowePktPNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaLiniowePktPX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaLiniowePktPY");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaLinioweDlugoscLW");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaLinioweDlugoscPW");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaLinioweWynikNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaLinioweWynikX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaLinioweWynikY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaLinioweWynikKod");	
			focus = lookup_widget(GTK_WIDGET(toolbutton), "entryWcieciaLiniowePktLNr");
			gtk_widget_grab_focus(GTK_WIDGET(focus));
		break;

		case WCIECIE_WSTECZ:
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczPktLNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczPktLX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczPktLY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczPktCNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczPktCX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczPktCY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczPktPNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczPktPX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczPktPY");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczKierC");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczKierL");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczKierP");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczWynikNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczWynikX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczWynikY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaWsteczWynikKod");	
			focus = lookup_widget(GTK_WIDGET(toolbutton), "entryWcieciaWsteczPktLNr");
			gtk_widget_grab_focus(GTK_WIDGET(focus));
		break;

		case WCIECIE_AZ_W_PRZOD:
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodPktLNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodPktLX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodPktLY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodPktPNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodPktPX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodPktPY");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodAzLW");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodAzPW");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodWynikNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodWynikX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodWynikY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodWynikKod");	
			focus = lookup_widget(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodPktLNr");
			gtk_widget_grab_focus(GTK_WIDGET(focus));
		break;

		case WCIECIE_AZ_WSTECZ:
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczPktLNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczPktLX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczPktLY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczPktPNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczPktPX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczPktPY");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczAzWL");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczAzWP");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczWynikNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczWynikX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczWynikY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczWynikKod");	
			focus = lookup_widget(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczPktLNr");
			gtk_widget_grab_focus(GTK_WIDGET(focus));
		break;
	}
}


void on_tbtnWcieciaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GSList *lpunkty = NULL;
	char *txt_obs = NULL;
	double obs;
	cPunkt *punkt = NULL;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	
	GtkWidget *dialogRaport = create_dialogRaport();
	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");
	gtk_widget_show(dialogRaport);

	GtkWidget *cbtnKierunki = lookup_widget(GTK_WIDGET(toolbutton), "cbtnWcieciaWsteczKierunki");
	GtkWidget *cbtnOgolne = lookup_widget(GTK_WIDGET(toolbutton), "cbtnWcieciaKatoweOgolne");
	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookWciecia");

	switch (gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook))) {
		case WCIECIE_KATOWE:
			geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nWcięcie kątowe"));

			// punkty osnowy
			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaKatowePktLNr", "entryWcieciaKatowePktLX",
				"entryWcieciaKatowePktLY", NULL, *punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla punktu L nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaKatowePktPNr", "entryWcieciaKatowePktPX",
				"entryWcieciaKatowePktPY", NULL, *punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla punktu P nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnOgolne))) {
				punkt = new cPunkt;
				if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
					"entryWcieciaKatowePktL1Nr", "entryWcieciaKatowePktL1X",
					"entryWcieciaKatowePktL1Y", NULL, *punkt))
					lpunkty = g_slist_append(lpunkty, punkt);
				else {
					geod_raport_error(textview, _("Dla punktu L1 nie podano wszystkich wymaganych danych!"));
					delete punkt;
				}

				punkt = new cPunkt;
				if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
					"entryWcieciaKatowePktP1Nr", "entryWcieciaKatowePktP1X",
					"entryWcieciaKatowePktP1Y", NULL, *punkt))
					lpunkty = g_slist_append(lpunkty, punkt);
				else {
					geod_raport_error(textview, _("Dla punktu P1 nie podano wszystkich wymaganych danych!"));
					delete punkt;
				}
			}
			
			geod_raport_table_pkt(textview, lpunkty, FALSE, "Punkty osnowy");
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// obserwacje
			if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryWcieciaKatoweKatL", obs)) {
				txt_obs = geod_kat_from_rad(obs);
				geod_raport_line(textview, _("Kąt lewy"), txt_obs, FALSE);
				g_free(txt_obs);
			}
			else
				geod_raport_error(textview, _("Nie podano kąta alfa"));

			if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryWcieciaKatoweKatP", obs)) {
				txt_obs = geod_kat_from_rad(obs);
				geod_raport_line(textview, _("Kąt prawy"), txt_obs, FALSE);
				g_free(txt_obs);
			}
			else
				geod_raport_error(textview, _("Nie podano kąta beta"));

			// wynik
			punkt = new cPunkt;
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaKatoweWynikNr", "entryWcieciaKatoweWynikX",
				"entryWcieciaKatoweWynikY", "entryWcieciaKatoweWynikKod", *punkt) < 1) {
				lpunkty = g_slist_append(lpunkty, punkt);
				geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkt wcinany"));
				g_slist_free(lpunkty);
				lpunkty = NULL;
			}
			else {
				delete punkt;
				geod_raport_error(textview, _("Brak obliczeń lub brak rozwiązania"), TRUE);
			}
		break;

		case WCIECIE_LINIOWE:
			geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nWcięcie liniowe"));
			
			// punkty osnowy
			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaLiniowePktLNr", "entryWcieciaLiniowePktLX",
				"entryWcieciaLiniowePktLY", NULL, *punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla punktu L nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
		
			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaLiniowePktPNr", "entryWcieciaLiniowePktPX",
				"entryWcieciaLiniowePktPY", NULL, *punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla punktu P nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty osnowy"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// obserwacje
			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryWcieciaLinioweDlugoscLW", obs)) {
				txt_obs = g_strdup_printf("%.*f", dok_xy_int, obs);
				geod_raport_line(textview, _("Długość LW"), txt_obs, FALSE);
				g_free(txt_obs);
			}
			else
				geod_raport_error(textview, _("Nie podano długości LW"));

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryWcieciaLinioweDlugoscPW", obs)) {
				txt_obs = g_strdup_printf("%.*f", dok_xy_int, obs);
				geod_raport_line(textview, _("Długość PW"), txt_obs, FALSE);
				g_free(txt_obs);
			}
			else
				geod_raport_error(textview, _("Nie podano długości PW"));

			// wynik
			punkt = new cPunkt;
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaLinioweWynikNr", "entryWcieciaLinioweWynikX",
				"entryWcieciaLinioweWynikY", "entryWcieciaLinioweWynikKod", *punkt) < 1) {
				lpunkty = g_slist_append(lpunkty, punkt);
				geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkt wcinany"));
				g_slist_free(lpunkty);
				lpunkty = NULL;
			}
			else {
				delete punkt;
				geod_raport_error(textview, _("Brak obliczeń lub brak rozwiązania"), TRUE);
			}
		break;

		case WCIECIE_WSTECZ:
			geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nWcięcie wstecz"));

			// punkty osnowy
			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaWsteczPktLNr", "entryWcieciaWsteczPktLX",
				"entryWcieciaWsteczPktLY", NULL, *punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla punktu L nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaWsteczPktCNr", "entryWcieciaWsteczPktCX",
				"entryWcieciaWsteczPktCY", NULL, *punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla punktu C nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaWsteczPktPNr", "entryWcieciaWsteczPktPX",
				"entryWcieciaWsteczPktPY", NULL, *punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla punktu P nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty osnowy"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// obserwacje
			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnKierunki))) {
				if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryWcieciaWsteczKierL", obs)) {
					txt_obs = geod_kat_from_rad(obs);
					geod_raport_line(textview, _("Kierunek L"), txt_obs, FALSE);
					g_free(txt_obs);
				}
				else
					geod_raport_error(textview, _("Nie podano odczytu kierunku L"));

				if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryWcieciaWsteczKierC", obs)) {
					txt_obs = geod_kat_from_rad(obs);
					geod_raport_line(textview, _("Kierunek C"), txt_obs, FALSE);
					g_free(txt_obs);
				}
				else
					geod_raport_error(textview, _("Nie podano odczytu kierunku C"));

				if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryWcieciaWsteczKierP", obs)) {
					txt_obs = geod_kat_from_rad(obs);
					geod_raport_line(textview, _("Kierunek P"), txt_obs, FALSE);
					g_free(txt_obs);
				}
				else
					geod_raport_error(textview, _("Nie podano odczytu kierunku P"));
			}
			else {
				if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryWcieciaWsteczKierC", obs)) {
					txt_obs = geod_kat_from_rad(obs);
					geod_raport_line(textview, _("Kąt alfa"), txt_obs, FALSE);
					g_free(txt_obs);
				}
				else
					geod_raport_error(textview, _("Nie podano kąta alfa"));

				if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryWcieciaWsteczKierP", obs)) {
					txt_obs = geod_kat_from_rad(obs);
					geod_raport_line(textview, _("Kąt beta"), txt_obs, FALSE);
					g_free(txt_obs);
				}
				else
					geod_raport_error(textview, _("Nie podano kąta beta"));
			}

			// wynik
			punkt = new cPunkt;
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczWynikX",
				"entryWcieciaWsteczWynikY", "entryWcieciaWsteczWynikKod", *punkt) < 1) {
				lpunkty = g_slist_append(lpunkty, punkt);
				geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkt wcinany"));
				g_slist_free(lpunkty);
				lpunkty = NULL;
			}
			else {
				delete punkt;
				geod_raport_error(textview, _("Brak obliczeń lub brak rozwiązania"), TRUE);
			}
		break;

		case WCIECIE_AZ_W_PRZOD:
			geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nWcięcie azymutalne w przód"));

			// punkty osnowy
			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaAWPrzodPktLNr", "entryWcieciaAWPrzodPktLX",
				"entryWcieciaAWPrzodPktLY", NULL, *punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla punktu L nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaAWPrzodPktPNr", "entryWcieciaAWPrzodPktPX",
				"entryWcieciaAWPrzodPktPY", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla punktu P nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty osnowy"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// obserwacje
			if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodAzLW", obs)) {
				txt_obs = geod_kat_from_rad(obs);
				geod_raport_line(textview, _("Azymut LW"), txt_obs, FALSE);
				g_free(txt_obs);
			}
			else
				geod_raport_error(textview, _("Nie podano azymutu LW"));

			if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodAzPW", obs)) {
				txt_obs = geod_kat_from_rad(obs);
				geod_raport_line(textview, _("Azymut PW"), txt_obs, FALSE);
				g_free(txt_obs);
			}
			else
				geod_raport_error(textview, _("Nie podano azymutu PW"));

			// wynik
			punkt = new cPunkt;
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaAWPrzodWynikNr", "entryWcieciaAWPrzodWynikX",
				"entryWcieciaAWPrzodWynikY", "entryWcieciaAWPrzodWynikKod", *punkt) < 1) {
				lpunkty = g_slist_append(lpunkty, punkt);
				geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkt wcinany"));
				g_slist_free(lpunkty);
				lpunkty = NULL;
			}
			else {
				delete punkt;
				geod_raport_error(textview, _("Brak obliczeń lub brak rozwiązania"), TRUE);
			}
		break;

		case WCIECIE_AZ_WSTECZ:
			geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nWcięcie azymutalne wstecz"));

			// punkty osnowy
			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaAWsteczPktLNr", "entryWcieciaAWsteczPktLX",
				"entryWcieciaAWsteczPktLY", NULL, *punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla punktu L nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaAWsteczPktPNr", "entryWcieciaAWsteczPktPX",
				"entryWcieciaAWsteczPktPY", NULL, *punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla punktu P nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty osnowy"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// obserwacje
			if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczAzWL", obs)) {
				txt_obs = geod_kat_from_rad(obs);
				geod_raport_line(textview, _("Azymut WL"), txt_obs, FALSE);
				g_free(txt_obs);
			}
			else
				geod_raport_error(textview, _("Nie podano azymutu WL"));

			if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczAzWP", obs)) {
				txt_obs = geod_kat_from_rad(obs);
				geod_raport_line(textview, _("Azymut WP"), txt_obs, FALSE);
				g_free(txt_obs);
			}
			else
				geod_raport_error(textview, _("Nie podano azymutu WP"));
			
			// wynik
			punkt = new cPunkt;
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
				"entryWcieciaAWsteczWynikNr", "entryWcieciaAWsteczWynikX",
				"entryWcieciaAWsteczWynikY", "entryWcieciaAWsteczWynikKod", *punkt) < 1) {
				lpunkty = g_slist_append(lpunkty, punkt);
				geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkt wcinany"));
				g_slist_free(lpunkty);
				lpunkty = NULL;
			}
			else {
				delete punkt;
				geod_raport_error(textview, _("Brak obliczeń lub brak rozwiązania"), TRUE);
			}
		break;
	}
	geod_raport_stopka(textview);
}


void on_tbtnWcieciaWidok_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	unsigned int npoints;
	cPunkt *pktDane;
	GdkPoint *pktWidok;
		
	GtkWidget *winWidok = create_winWidok();
	gtk_widget_show(winWidok);
	GtkWidget *darea = lookup_widget(GTK_WIDGET(winWidok), "drawingAreaWidok");
	GtkWidget *cbtnOgolne = lookup_widget(GTK_WIDGET(toolbutton), "cbtnWcieciaKatoweOgolne");
	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookWciecia");
	
	switch (gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook)))
	{
		case WCIECIE_KATOWE:
			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnOgolne)))
				npoints = 5;
			else
				npoints = 3;
			
			pktDane = new cPunkt[npoints];
			pktWidok = new GdkPoint[npoints];
			
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktLNr",
				"entryWcieciaKatowePktLX", "entryWcieciaKatowePktLY", NULL, pktDane[0]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla L musisz podać Nr, X, Y"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktPNr",
				"entryWcieciaKatowePktPX", "entryWcieciaKatowePktPY", NULL, pktDane[1]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla P musisz podać Nr, X, Y"));
				break;
			}

			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnOgolne))) {
				if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktL1Nr",
					"entryWcieciaKatowePktL1X", "entryWcieciaKatowePktL1Y", NULL, pktDane[3]));
				else {
					geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla L1 musisz podać Nr, X, Y"));
					break;
				}

				if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaKatowePktP1Nr",
					"entryWcieciaKatowePktP1X", "entryWcieciaKatowePktP1Y", NULL, pktDane[4]));
				else {
					geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla P1 musisz podać Nr, X, Y"));
					break;
				}
			}

			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaKatoweWynikNr",
				"entryWcieciaKatoweWynikX", "entryWcieciaKatoweWynikY", NULL, pktDane[2]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Brak obliczeń"));
				break;
			}

			geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, npoints);
			
			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnOgolne))) {
				geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[3], LINE_BAZA);
				geod_draw_line(GTK_WIDGET(darea), pktWidok[1], pktWidok[4], LINE_BAZA);
			}
			else
				geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[1], LINE_BAZA);
			
			geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[2], LINE_KIERUNEK);
			geod_draw_line(GTK_WIDGET(darea), pktWidok[1], pktWidok[2], LINE_KIERUNEK);
	
			geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[1].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[2], pktDane[2].getNr(), PKT_WYNIK);
	
			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnOgolne))) {
				geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[3].getNr(), PKT_OSNOWA);
				geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[4].getNr(), PKT_OSNOWA);
				geod_draw_arc(GTK_WIDGET(darea), pktWidok[0],pktWidok[3],pktWidok[2], 1);
				geod_draw_arc(GTK_WIDGET(darea), pktWidok[1],pktWidok[2],pktWidok[4], 1);
			}
			else {
				geod_draw_arc(GTK_WIDGET(darea), pktWidok[0],pktWidok[1],pktWidok[2], 1);
				geod_draw_arc(GTK_WIDGET(darea), pktWidok[1],pktWidok[2],pktWidok[0], 1);
			}

			gtk_widget_queue_draw(darea);
			delete [] pktDane;
			delete [] pktWidok;
			break;

		case WCIECIE_LINIOWE:
			npoints = 3;
			pktDane = new cPunkt[npoints];
			pktWidok = new GdkPoint[npoints];
			
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaLiniowePktLNr",
				"entryWcieciaLiniowePktLX", "entryWcieciaLiniowePktLY", NULL, pktDane[0]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla L musisz podać Nr, X, Y"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaLiniowePktPNr",
				"entryWcieciaLiniowePktPX", "entryWcieciaLiniowePktPY", NULL, pktDane[1]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla P musisz podać Nr, X, Y"));
				break;
			}

			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaLinioweWynikNr",
				"entryWcieciaLinioweWynikX", "entryWcieciaLinioweWynikY", NULL, pktDane[2]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Brak obliczeń"));
				break;
			}


			geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, npoints);
			geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[1], LINE_BAZA);
			geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[2], LINE_DLUGOSC);
			geod_draw_line(GTK_WIDGET(darea), pktWidok[1], pktWidok[2], LINE_DLUGOSC);
	
			geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[1].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[2], pktDane[2].getNr(), PKT_WYNIK);
	
			gtk_widget_queue_draw(darea);
			delete [] pktDane;
			delete [] pktWidok;
			break;

		case WCIECIE_WSTECZ:
			npoints = 4;
			pktDane = new cPunkt[npoints];
			pktWidok = new GdkPoint[npoints];
			
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaWsteczPktLNr",
				"entryWcieciaWsteczPktLX", "entryWcieciaWsteczPktLY", NULL, pktDane[0]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla L musisz podać Nr, X, Y"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaWsteczPktCNr",
				"entryWcieciaWsteczPktCX", "entryWcieciaWsteczPktCY", NULL, pktDane[1]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla C musisz podać Nr, X, Y"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaWsteczPktPNr",
				"entryWcieciaWsteczPktPX", "entryWcieciaWsteczPktPY", NULL, pktDane[2]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla P musisz podać Nr, X, Y"));
				break;
			}

			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaWsteczWynikNr",
				"entryWcieciaWsteczWynikX", "entryWcieciaWsteczWynikY", NULL, pktDane[3]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Brak obliczeń"));
				break;
			}

			geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, npoints);
			geod_draw_line(GTK_WIDGET(darea), pktWidok[3], pktWidok[0], LINE_KIERUNEK);
			geod_draw_line(GTK_WIDGET(darea), pktWidok[3], pktWidok[1], LINE_KIERUNEK);
			geod_draw_line(GTK_WIDGET(darea), pktWidok[3], pktWidok[2], LINE_KIERUNEK);
	
			geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[1].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[2], pktDane[2].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[3], pktDane[3].getNr(), PKT_WYNIK);
			
			gtk_widget_queue_draw(darea);
			delete [] pktDane;
			delete [] pktWidok;
			break;

		case WCIECIE_AZ_W_PRZOD:
			npoints = 3;
			pktDane = new cPunkt[npoints];
			pktWidok = new GdkPoint[npoints+2];
			
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodPktLNr",
				"entryWcieciaAWPrzodPktLX", "entryWcieciaAWPrzodPktLY", NULL, pktDane[0]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla L musisz podać Nr, X, Y"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodPktPNr",
				"entryWcieciaAWPrzodPktPX", "entryWcieciaAWPrzodPktPY", NULL, pktDane[1]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla P musisz podać Nr, X, Y"));
				break;
			}

			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaAWPrzodWynikNr",
				"entryWcieciaAWPrzodWynikX", "entryWcieciaAWPrzodWynikY", NULL, pktDane[2]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Brak obliczeń"));
				break;
			}

			geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, npoints);
			pktWidok[0+npoints].x = pktWidok[0].x;
			pktWidok[1+npoints].x = pktWidok[1].x;
			pktWidok[0+npoints].y = 0;
			pktWidok[1+npoints].y = 0;

			geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[2], LINE_KIERUNEK);
			geod_draw_line(GTK_WIDGET(darea), pktWidok[1], pktWidok[2], LINE_KIERUNEK);

			geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[0+npoints], LINE_POLNOC);
			geod_draw_line(GTK_WIDGET(darea), pktWidok[1], pktWidok[1+npoints], LINE_POLNOC);

			geod_draw_arc(GTK_WIDGET(darea), pktWidok[0], pktWidok[2], pktWidok[0+npoints]);
			geod_draw_arc(GTK_WIDGET(darea), pktWidok[1], pktWidok[2], pktWidok[1+npoints]);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[1].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[2], pktDane[2].getNr(), PKT_WYNIK);
			
			gtk_widget_queue_draw(darea);
			delete [] pktDane;
			delete [] pktWidok;
			break;

		case WCIECIE_AZ_WSTECZ:
			npoints = 3;
			pktDane = new cPunkt[npoints];
			pktWidok = new GdkPoint[npoints+1];
			
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczPktLNr",
				"entryWcieciaAWsteczPktLX", "entryWcieciaAWsteczPktLY", NULL, pktDane[0]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla L musisz podać Nr, X, Y"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczPktPNr",
				"entryWcieciaAWsteczPktPX", "entryWcieciaAWsteczPktPY", NULL, pktDane[1]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Dla P musisz podać Nr, X, Y"));
				break;
			}

			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryWcieciaAWsteczWynikNr",
				"entryWcieciaAWsteczWynikX", "entryWcieciaAWsteczWynikY", NULL, pktDane[2]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarWciecia", _("Brak obliczeń"));
				break;
			}

			geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, npoints);
			pktWidok[3].x = pktWidok[2].x;
			pktWidok[3].y = 0;

			geod_draw_line(GTK_WIDGET(darea), pktWidok[2], pktWidok[0], LINE_KIERUNEK);
			geod_draw_line(GTK_WIDGET(darea), pktWidok[2], pktWidok[1], LINE_KIERUNEK);

			geod_draw_line(GTK_WIDGET(darea), pktWidok[2], pktWidok[3], LINE_POLNOC);
			geod_draw_arc(GTK_WIDGET(darea), pktWidok[2], pktWidok[1], pktWidok[3]);

			geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[1].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[2], pktDane[2].getNr(), PKT_WYNIK);
			
			gtk_widget_queue_draw(darea);
			delete [] pktDane;
			delete [] pktWidok;
			break;
		}
}


gboolean on_entryWcieciaLinioweWynikNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	GtkWidget *entryDlugoscLW = lookup_widget(GTK_WIDGET(widget), "entryWcieciaLinioweDlugoscLW");
	GtkWidget *entryDlugoscPW = lookup_widget(GTK_WIDGET(widget), "entryWcieciaLinioweDlugoscPW");

	if (!geod_update_entry_obs(GTK_WIDGET(entryDlugoscLW), "$dl",
		"entryWcieciaLiniowePktLNr", "entryWcieciaLinioweWynikNr", NULL))
		geod_update_entry_obs(GTK_WIDGET(entryDlugoscLW), "$dl",
			"entryWcieciaLinioweWynikNr", "entryWcieciaLiniowePktLNr", NULL);

	if (!geod_update_entry_obs(GTK_WIDGET(entryDlugoscPW), "$dl",
		"entryWcieciaLiniowePktPNr", "entryWcieciaLinioweWynikNr", NULL))
		geod_update_entry_obs(GTK_WIDGET(entryDlugoscPW), "$dl",
			"entryWcieciaLinioweWynikNr", "entryWcieciaLiniowePktPNr", NULL);
	return FALSE;
}


gboolean on_entryWcieciaLiniowePktLNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaLiniowePktLNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaLiniowePktPNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaLiniowePktPNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarWciecia");
	return FALSE;
}


void on_btnWcieciaLinioweDodajWynik_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktWynik;
	bool zmiana = FALSE;
	int zadanie_id;
	double dl;
	GtkWidget *buttonWyczysc = lookup_widget(GTK_WIDGET(button), "tbtnWcieciaWyczysc");
	GtkWidget *entryL = lookup_widget(GTK_WIDGET(button), "entryWcieciaLiniowePktLNr");
	GtkWidget *entryP = lookup_widget(GTK_WIDGET(button), "entryWcieciaLiniowePktPNr");
	
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryWcieciaLinioweWynikNr",
		"entryWcieciaLinioweWynikX", "entryWcieciaLinioweWynikY",
		"entryWcieciaLinioweWynikKod", pktWynik)) {
		dbgeod_point_add_config(pktWynik, &zmiana);
		if (zmiana) {
			dbgeod_hist_add("WciecieLiniowe", zadanie_id);
			
			dbgeod_hist_point_set(zadanie_id, "LNr", gtk_entry_get_text(GTK_ENTRY(entryL)), 0);
			dbgeod_hist_point_set(zadanie_id, "PNr", gtk_entry_get_text(GTK_ENTRY(entryP)), 0);
			dbgeod_hist_point_set(zadanie_id, "WNr", pktWynik.getNr(), 1);
			geod_entry_get_double(GTK_WIDGET(button), "entryWcieciaLinioweDlugoscLW", dl);
			dbgeod_hist_par_set(zadanie_id, "DlugoscLW", dl, 0);
			geod_entry_get_double(GTK_WIDGET(button), "entryWcieciaLinioweDlugoscPW", dl);
			dbgeod_hist_par_set(zadanie_id, "DlugoscPW", dl, 0);
			
			GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Dalsze obliczenia?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
				g_signal_emit_by_name(buttonWyczysc, "clicked");
			gtk_widget_destroy(dialog);
		}
	}
	else
		geod_sbar_set(GTK_WIDGET(button), "sbarWciecia", _("Nie wykonano obliczeń bądź nie podano Nr punktów"));

}


gboolean on_entryWcieciaWsteczWynikNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	GtkWidget *cbtnKierunki = lookup_widget(GTK_WIDGET(widget), "cbtnWcieciaWsteczKierunki");
	GtkWidget *entryKierL = lookup_widget(GTK_WIDGET(widget), "entryWcieciaWsteczKierL");
	GtkWidget *entryKierC = lookup_widget(GTK_WIDGET(widget), "entryWcieciaWsteczKierC");
	GtkWidget *entryKierP = lookup_widget(GTK_WIDGET(widget), "entryWcieciaWsteczKierP");

	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnKierunki))) {
		geod_update_entry_obs(GTK_WIDGET(entryKierL), "$hz",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktLNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKierC), "$hz",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktCNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKierP), "$hz",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktPNr", NULL);	
	}
	else {
		geod_update_entry_obs(GTK_WIDGET(entryKierC), "entryWcieciaWsteczPktLNr",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktCNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKierP), "entryWcieciaWsteczPktLNr",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktPNr", NULL);	
	}
	return FALSE;
}


gboolean on_entryWcieciaWsteczPktLNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaWsteczPktLNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaWsteczPktCNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaWsteczPktCNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaWsteczPktPNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaWsteczPktPNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarWciecia");
	return FALSE;
}


void on_cbtnWcieciaWsteczKierunki_toggled (GtkToggleButton *togglebutton,
	gpointer user_data)
{
	GtkWidget *labelKierLewy = lookup_widget(GTK_WIDGET(togglebutton), "labelWcieciaWsteczKierL");
	GtkWidget *labelKierSrodek = lookup_widget(GTK_WIDGET(togglebutton), "labelWcieciaWsteczKierC");
	GtkWidget *labelKierPrawy = lookup_widget(GTK_WIDGET(togglebutton), "labelWcieciaWsteczKierP");
	GtkWidget *entryKierLewy = lookup_widget(GTK_WIDGET(togglebutton), "entryWcieciaWsteczKierL");

	if (gtk_toggle_button_get_active(togglebutton)) {
		gtk_widget_show(labelKierLewy);
		gtk_widget_show(entryKierLewy);
		gtk_label_set_text(GTK_LABEL(labelKierSrodek), _("Kierunek C:"));
		gtk_label_set_text(GTK_LABEL(labelKierPrawy), _("Kierunek P:"));
	}
	else {
		gtk_widget_hide(labelKierLewy);
		gtk_widget_hide(entryKierLewy);
		gtk_label_set_text(GTK_LABEL(labelKierSrodek), _("Kąt alfa (α)"));
		gtk_label_set_text(GTK_LABEL(labelKierPrawy), _("Kąt beta (β)"));
	}
}


void on_btnWcieciaWsteczDodajWynik_clicked  (GtkButton *button,
	gpointer user_data)
{
	cPunkt pktWynik;
	bool zmiana = FALSE;
	double kat;
	int zadanie_id;
	GtkWidget *entryL = lookup_widget(GTK_WIDGET(button), "entryWcieciaWsteczPktLNr");
	GtkWidget *entryC = lookup_widget(GTK_WIDGET(button), "entryWcieciaWsteczPktCNr");
	GtkWidget *entryP = lookup_widget(GTK_WIDGET(button), "entryWcieciaWsteczPktPNr");
	GtkWidget *cbtnKierunki = lookup_widget(GTK_WIDGET(button), "cbtnWcieciaWsteczKierunki");
	GtkWidget *buttonWyczysc = lookup_widget(GTK_WIDGET(button), "tbtnWcieciaWyczysc");
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryWcieciaWsteczWynikNr",
		"entryWcieciaWsteczWynikX", "entryWcieciaWsteczWynikY",
		"entryWcieciaWsteczWynikKod", pktWynik)) {
		dbgeod_point_add_config(pktWynik, &zmiana);
		if (zmiana) {
			dbgeod_hist_add("WciecieWstecz", zadanie_id);
			
			dbgeod_hist_point_set(zadanie_id, "LNr", gtk_entry_get_text(GTK_ENTRY(entryL)), 0);
			dbgeod_hist_point_set(zadanie_id, "CNr", gtk_entry_get_text(GTK_ENTRY(entryC)), 0);
			dbgeod_hist_point_set(zadanie_id, "PNr", gtk_entry_get_text(GTK_ENTRY(entryP)), 0);
			dbgeod_hist_point_set(zadanie_id, "WNr", pktWynik.getNr(), 1);
			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnKierunki))) {
				geod_entry_get_kat(GTK_WIDGET(button), "entryWcieciaWsteczKierL", kat);
				dbgeod_hist_par_set(zadanie_id, "KierL", kat, 0);
				geod_entry_get_kat(GTK_WIDGET(button), "entryWcieciaWsteczKierC", kat);
				dbgeod_hist_par_set(zadanie_id, "KierC", kat, 0);
				geod_entry_get_kat(GTK_WIDGET(button), "entryWcieciaWsteczKierP", kat);
				dbgeod_hist_par_set(zadanie_id, "KierP", kat, 0);
				dbgeod_hist_par_set(zadanie_id, "kier", 1, 0);
			}
			else {
				geod_entry_get_kat(GTK_WIDGET(button), "entryWcieciaWsteczKierC", kat);
				dbgeod_hist_par_set(zadanie_id, "KatAlfa", kat, 0);
				geod_entry_get_kat(GTK_WIDGET(button), "entryWcieciaWsteczKierP", kat);
				dbgeod_hist_par_set(zadanie_id, "KatBeta", kat, 0);
				dbgeod_hist_par_set(zadanie_id, "kier", 0, 0);
			}

			GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Dalsze obliczenia?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
				g_signal_emit_by_name(buttonWyczysc, "clicked");
			gtk_widget_destroy(dialog);
		}
	}
	else
		geod_sbar_set(GTK_WIDGET(button), "sbarWciecia", _("Nie wykonano obliczeń bądź nie podano Nr punktów"));
}


gboolean on_entryWcieciaAWPrzodWynikNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	GtkWidget *entryAzLW = lookup_widget(GTK_WIDGET(widget), "entryWcieciaAWPrzodAzLW");
	GtkWidget *entryAzPW = lookup_widget(GTK_WIDGET(widget), "entryWcieciaAWPrzodAzPW");

	geod_update_entry_obs(GTK_WIDGET(entryAzLW), "$az",
		"entryWcieciaAWPrzodPktLNr", "entryWcieciaAWPrzodWynikNr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryAzPW), "$az",
		"entryWcieciaAWPrzodPktPNr", "entryWcieciaAWPrzodWynikNr", NULL);	

	return FALSE;
}


gboolean on_entryWcieciaAWPrzodPktLNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaAWPrzodPktLNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaAWPrzodPktPNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaAWPrzodPktPNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaAWPrzodWynikKod_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Kod_set(widget, "sbarWciecia");
	return FALSE;
}


void on_btnWcieciaAWPrzodDodajWynik_clicked (GtkButton *button,
	gpointer user_data)
{
	cPunkt pktWynik;
	bool zmiana = FALSE;
	double kat;
	int zadanie_id;
	GtkWidget *buttonWyczysc = lookup_widget(GTK_WIDGET(button), "tbtnWcieciaWyczysc");
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryWcieciaAWPrzodWynikNr",
		"entryWcieciaAWPrzodWynikX", "entryWcieciaAWPrzodWynikY",
		"entryWcieciaAWPrzodWynikKod", pktWynik)) {
		dbgeod_point_add_config(pktWynik, &zmiana);
		if (zmiana) {
			GtkWidget *entryL = lookup_widget(GTK_WIDGET(button), "entryWcieciaAWPrzodPktLNr");
			GtkWidget *entryP = lookup_widget(GTK_WIDGET(button), "entryWcieciaAWPrzodPktPNr");

			dbgeod_hist_add("WciecieAzymutalneWPrzod", zadanie_id);
			dbgeod_hist_point_set(zadanie_id, "LNr", gtk_entry_get_text(GTK_ENTRY(entryL)), 0);
			dbgeod_hist_point_set(zadanie_id, "PNr", gtk_entry_get_text(GTK_ENTRY(entryP)), 0);
			dbgeod_hist_point_set(zadanie_id, "WNr", pktWynik.getNr(), 1);
			geod_entry_get_kat(GTK_WIDGET(button), "entryWcieciaAWPrzodAzLW", kat);
			dbgeod_hist_par_set(zadanie_id, "AzymutLW", kat, 0);
			geod_entry_get_kat(GTK_WIDGET(button), "entryWcieciaAWPrzodAzPW", kat);
			dbgeod_hist_par_set(zadanie_id, "AzymutPW", kat, 0);

			GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Dalsze obliczenia?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
				g_signal_emit_by_name(buttonWyczysc, "clicked");
			gtk_widget_destroy(dialog);
		}
	}
	else
		geod_sbar_set(GTK_WIDGET(button), "sbarWciecia", _("Nie wykonano obliczeń bądź nie podano Nr punktów"));
}


gboolean on_entryWcieciaAWsteczWynikNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	GtkWidget *entryAzWL = lookup_widget(GTK_WIDGET(widget), "entryWcieciaAWsteczAzWL");
	GtkWidget *entryAzWP = lookup_widget(GTK_WIDGET(widget), "entryWcieciaAWsteczAzWP");

	geod_update_entry_obs(GTK_WIDGET(entryAzWL), "$az",
		"entryWcieciaAWsteczWynikNr", "entryWcieciaAWsteczPktLNr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryAzWP), "$az",
		"entryWcieciaAWsteczWynikNr", "entryWcieciaAWsteczPktPNr", NULL);	

	return FALSE;
}


gboolean on_entryWcieciaAWsteczPktLNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaAWsteczPktLNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaAWsteczPktPNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarWciecia");
	return FALSE;
}


gboolean on_entryWcieciaAWsteczPktPNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarWciecia");
	return FALSE;
}


void on_btnWcieciaAWsteczDodajWynik_clicked (GtkButton *button,
	gpointer user_data)
{
	cPunkt pktWynik;
	bool zmiana = FALSE;
	double kat;
	int zadanie_id;
	GtkWidget *buttonWyczysc = lookup_widget(GTK_WIDGET(button), "tbtnWcieciaWyczysc");
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryWcieciaAWsteczWynikNr",
		"entryWcieciaAWsteczWynikX", "entryWcieciaAWsteczWynikY",
		"entryWcieciaAWsteczWynikKod", pktWynik))
	{
		dbgeod_point_add_config(pktWynik, &zmiana);
		if (zmiana) {
			GtkWidget *entryL = lookup_widget(GTK_WIDGET(button), "entryWcieciaAWsteczPktLNr");
			GtkWidget *entryP = lookup_widget(GTK_WIDGET(button), "entryWcieciaAWsteczPktPNr");

			dbgeod_hist_add("WciecieAzymutalneWstecz", zadanie_id);
			dbgeod_hist_point_set(zadanie_id, "LNr", gtk_entry_get_text(GTK_ENTRY(entryL)), 0);
			dbgeod_hist_point_set(zadanie_id, "PNr", gtk_entry_get_text(GTK_ENTRY(entryP)), 0);
			dbgeod_hist_point_set(zadanie_id, "WNr", pktWynik.getNr(), 1);
			geod_entry_get_kat(GTK_WIDGET(button), "entryWcieciaAWsteczAzWL", kat);
			dbgeod_hist_par_set(zadanie_id, "AzymutWL", kat, 0);
			geod_entry_get_kat(GTK_WIDGET(button), "entryWcieciaAWsteczAzWP", kat);
			dbgeod_hist_par_set(zadanie_id, "AzymutWP", kat, 0);

			GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Dalsze obliczenia?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
				g_signal_emit_by_name(buttonWyczysc, "clicked");
			gtk_widget_destroy(dialog);
		}
	}
	else
		geod_sbar_set(GTK_WIDGET(button), "sbarWciecia", _("Nie wykonano obliczeń bądź nie podano Nr punktów"));

}


void on_tbtnWcieciaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winWciecia");
	gtk_widget_destroy(window);
}


/* winOkragStyczna */
void on_winOkragStyczna_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winOkragStyczna");
}


gboolean on_entryOkragStycznaPunktOkrNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragStyczna");
	return FALSE;
}


gboolean on_entryOkragStycznaPunktOkrNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragStyczna");
	return FALSE;
}


gboolean on_entryOkragStycznaPunktPktNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragStyczna");
	return FALSE;
}


gboolean on_entryOkragStycznaPunktPktNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragStyczna");
	return FALSE;
}


gboolean on_entryOkragStycznaKod_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Kod_set(widget, "sbarOkragStyczna");
	return FALSE;
}


gboolean on_entryOkragStycznaKod_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragStyczna");
	return FALSE;
}


void on_tbtnOkragStycznaWykonaj_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt pktProsta1P, pktProsta1K, pktProsta2P, pktProsta2K;
	cPunkt pktSrodek1, pktSrodek2;
	cPunkt pktWynik1a, pktWynik1b, pktWynik2a, pktWynik2b;
	double promien1, promien2;
	int geod_error = GEODERR_OK;

	GtkWidget *entryWynik1Nr = NULL;
	GtkWidget *entryWynik2Nr = NULL;
	GtkWidget *entryWynik3Nr = NULL;
	GtkWidget *entryWynik4Nr = NULL;

	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookOkragStyczna");
	GtkWidget *rbtnProstaProstopadla = lookup_widget(GTK_WIDGET(toolbutton), "rbtnOkragStycznaProstaStycznaProstopadla");
	GtkWidget *rbtnProstaRownolegla = lookup_widget(GTK_WIDGET(toolbutton), "rbtnOkragStycznaProstaStycznaRownolegla");
	
	switch(gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook)))
	{
		case STYCZNA_PUNKT:
			entryWynik1Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikNr1");
			entryWynik2Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikNr2");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikX1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikY1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikX2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikY2");

			g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			gtk_entry_set_editable(GTK_ENTRY(entryWynik1Nr), TRUE);
			gtk_entry_set_editable(GTK_ENTRY(entryWynik2Nr), TRUE);

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktOkrNr",
				"entryOkragStycznaPunktOkrX", "entryOkragStycznaPunktOkrY", NULL, pktSrodek1));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać Nr, X, Y środka okręgu"));
				break;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktOkrR", promien1));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać promień okręgu"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktPktNr",
				"entryOkragStycznaPunktPktX", "entryOkragStycznaPunktPktY", NULL, pktProsta1P));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Dla Punktu musisz podać Nr, X, Y"));
				break;
			}

			geod_error = okrag_styczna_punkt(pktSrodek1, promien1, pktProsta1P, pktWynik1a, pktWynik1b);
			if (geod_error == GEODERR_OK) {
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikX1",
				"entryOkragStycznaPunktWynikY1", pktWynik1a);
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikX2",
				"entryOkragStycznaPunktWynikY2", pktWynik1b);
				g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
				g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
			}
			else {
				g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_ERROR));
				g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_ERROR));
				geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarOkragStyczna", geod_error);
			}
		break;

		case STYCZNA_PROSTA:
			entryWynik1Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikNr1");
			entryWynik2Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikNr2");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikX1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikY1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikX2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikY2");

			g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			gtk_entry_set_editable(GTK_ENTRY(entryWynik1Nr), TRUE);
			gtk_entry_set_editable(GTK_ENTRY(entryWynik2Nr), TRUE);

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaOkrNr",
				"entryOkragStycznaProstaOkrX", "entryOkragStycznaProstaOkrY", NULL, pktSrodek1));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać Nr, X, Y środka okręgu"));
				break;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaOkrR", promien1));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać promień okręgu"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaPrNr1",
				"entryOkragStycznaProstaPrX1", "entryOkragStycznaProstaPrY1", NULL, pktProsta1P));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać Nr, X, Y pierwszego punktu prostej"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaPrNr2",
				"entryOkragStycznaProstaPrX2", "entryOkragStycznaProstaPrY2", NULL, pktProsta1K));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać Nr, X, Y drugiego punktu prostej"));
				break;
			}

			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnProstaProstopadla)))
			{
				geod_error = okrag_styczna_prosta_prostopadla(pktSrodek1, promien1, pktProsta1P,
					pktProsta1K, pktWynik1a, pktWynik1b);
			}
			else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnProstaRownolegla)))
			{
				geod_error = okrag_styczna_prosta_rownolegla(pktSrodek1, promien1, pktProsta1P,
					pktProsta1K, pktWynik1a, pktWynik1b);
			}
			
			if (geod_error == GEODERR_OK) {
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikX1",
				"entryOkragStycznaProstaWynikY1", pktWynik1a);
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikX2",
				"entryOkragStycznaProstaWynikY2", pktWynik1b);
				g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
				g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
			}
			else {
				g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_ERROR));
				g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_ERROR));
				geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarOkragStyczna", geod_error);
			}
		break;

		case STYCZNA_OKREGI:
			entryWynik1Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr1a");
			entryWynik2Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr1b");
			entryWynik3Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr2a");
			entryWynik4Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr2b");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikX1a");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikY1a");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikX1b");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikY1b");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikX2a");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikY2a");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikX2b");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikY2b");

			g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			g_object_set_data(G_OBJECT(entryWynik3Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			g_object_set_data(G_OBJECT(entryWynik4Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			gtk_entry_set_editable(GTK_ENTRY(entryWynik1Nr), TRUE);
			gtk_entry_set_editable(GTK_ENTRY(entryWynik2Nr), TRUE);
			gtk_entry_set_editable(GTK_ENTRY(entryWynik3Nr), TRUE);
			gtk_entry_set_editable(GTK_ENTRY(entryWynik4Nr), TRUE);

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr1Nr",
				"entryOkragStyczna2OkregiOkr1X", "entryOkragStyczna2OkregiOkr1Y", NULL, pktSrodek1));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać Nr, X, Y środka pierwszego okręgu"));
				break;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr1R", promien1));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać promień pierwszego okręgu"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr2Nr",
				"entryOkragStyczna2OkregiOkr2X", "entryOkragStyczna2OkregiOkr2Y", NULL, pktSrodek2));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać Nr, X, Y środka drugiego okręgu"));
				break;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr2R", promien2));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać promień drugiego okręgu"));
				break;
			}

			geod_error = okrag_styczna_2okregi(pktSrodek1, promien1, pktSrodek2, promien2,
				 pktWynik1a, pktWynik1b, pktWynik2a, pktWynik2b);
			if (geod_error == GEODERR_OK) {
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikX1a",
				"entryOkragStyczna2OkregiWynikY1a", pktWynik1a);
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikX1b",
				"entryOkragStyczna2OkregiWynikY1b", pktWynik1b);
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikX2a",
				"entryOkragStyczna2OkregiWynikY2a", pktWynik2a);
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikX2b",
				"entryOkragStyczna2OkregiWynikY2b", pktWynik2b);
				g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
				g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
				g_object_set_data(G_OBJECT(entryWynik3Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
				g_object_set_data(G_OBJECT(entryWynik4Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
			}
			else {
				g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_ERROR));
				g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_ERROR));
				g_object_set_data(G_OBJECT(entryWynik3Nr), "status_dodaj", GINT_TO_POINTER(PKT_ERROR));
				g_object_set_data(G_OBJECT(entryWynik4Nr), "status_dodaj", GINT_TO_POINTER(PKT_ERROR));
				geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarOkragStyczna", geod_error);
			}
		break;
	}
}


void on_tbtnOkragStycznaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *focus = NULL;
	GtkWidget *entryWynik1Nr = NULL;
	GtkWidget *entryWynik2Nr = NULL;
	GtkWidget *entryWynik3Nr = NULL;
	GtkWidget *entryWynik4Nr = NULL;
	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookOkragStyczna");
	switch(gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook)))
	{
		case STYCZNA_PUNKT:
			entryWynik1Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikNr1");
			entryWynik2Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikNr2");

			g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			gtk_entry_set_editable(GTK_ENTRY(entryWynik1Nr), TRUE);
			gtk_entry_set_editable(GTK_ENTRY(entryWynik2Nr), TRUE);

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktOkrNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktOkrX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktOkrY");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktOkrR");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktPktNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktPktX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktPktY");	

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikNr1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikX1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikY1");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikKod1");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikNr2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikX2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikY2");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikKod2");
			focus = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktOkrNr");
			gtk_widget_grab_focus(focus);
		break;

		case STYCZNA_PROSTA:
			entryWynik1Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikNr1");
			entryWynik2Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikNr2");

			g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			gtk_entry_set_editable(GTK_ENTRY(entryWynik1Nr), TRUE);
			gtk_entry_set_editable(GTK_ENTRY(entryWynik2Nr), TRUE);

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaOkrNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaOkrX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaOkrY");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaOkrR");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaPrNr1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaPrX1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaPrY1");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaPrNr2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaPrX2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaPrY2");	

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikNr1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikX1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikY1");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikKod1");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikNr2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikX2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikY2");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikKod2");	
			focus = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaOkrNr");
			gtk_widget_grab_focus(focus);
		break;

		case STYCZNA_OKREGI:
			entryWynik1Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr1a");
			entryWynik2Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr1b");
			entryWynik3Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr2a");
			entryWynik4Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr2b");

			g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			g_object_set_data(G_OBJECT(entryWynik3Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			g_object_set_data(G_OBJECT(entryWynik4Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			gtk_entry_set_editable(GTK_ENTRY(entryWynik1Nr), TRUE);
			gtk_entry_set_editable(GTK_ENTRY(entryWynik2Nr), TRUE);
			gtk_entry_set_editable(GTK_ENTRY(entryWynik3Nr), TRUE);
			gtk_entry_set_editable(GTK_ENTRY(entryWynik4Nr), TRUE);

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr1Nr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr1X");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr1Y");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr1R");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr2Nr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr2X");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr2Y");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr2R");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr1a");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikX1a");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikY1a");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikKod1a");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr1b");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikX1b");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikY1b");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikKod1b");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr2a");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikX2a");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikY2a");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikKod2a");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr2b");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikX2b");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikY2b");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikKod2b");
			focus = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr1Nr");
			gtk_widget_grab_focus(focus);
		break;
	}
}


void on_tbtnOkragStycznaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GSList *lpunkty = NULL;
	char *txt_promien = NULL;
	double promien;
	cPunkt *punkt = NULL;
	bool brak_danych = FALSE;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	GtkWidget *dialogRaport = create_dialogRaport();
	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");
	gtk_widget_show(dialogRaport);

	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookOkragStyczna");
	GtkWidget *rbtnProstaProstopadla = lookup_widget(GTK_WIDGET(toolbutton), "rbtnOkragStycznaProstaStycznaProstopadla");
	GtkWidget *rbtnProstaRownolegla = lookup_widget(GTK_WIDGET(toolbutton), "rbtnOkragStycznaProstaStycznaRownolegla");
	
	switch(gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook)))
	{
		case STYCZNA_PUNKT:
			geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nStyczna do okręgu przez punkt"));
			
			// okrag
			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktOkrNr",
				"entryOkragStycznaPunktOkrX", "entryOkragStycznaPunktOkrY", NULL, *punkt)) {
				lpunkty = g_slist_append(lpunkty, punkt);
				geod_raport_table_pkt(textview, lpunkty, FALSE, _("Środek okręgu"));
				g_slist_free(lpunkty);
				lpunkty = NULL;
			}
			else {
				geod_raport_error(textview, _("Dla środka okręgu nie podano wszystkich wymaganych danych"));
				delete punkt;
			}

			
			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktOkrR", promien)) {
				txt_promien = g_strdup_printf("%.*f", dok_xy_int, promien);
				geod_raport_line(textview, _("Promień okręgu"), txt_promien);
				g_free(txt_promien);
			}
			else
				geod_raport_error(textview, _("Nie podano promienia okręgu"));

			// punkt na stycznej
			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktPktNr",
				"entryOkragStycznaPunktPktX", "entryOkragStycznaPunktPktY", NULL, *punkt)) {
				lpunkty = g_slist_append(lpunkty, punkt);
				geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkt stycznej"));
				g_slist_free(lpunkty);
				lpunkty = NULL;
			}
			else {
				geod_raport_error(textview, _("Dla punktu na stycznej nie podano wszystkich wymaganych danych"));
				delete punkt;
			}

			// wynik
			brak_danych = FALSE;			
			punkt = new cPunkt;
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikNr1",
				"entryOkragStycznaPunktWynikX1", "entryOkragStycznaPunktWynikY1",
				"entryOkragStycznaPunktWynikKod1", *punkt) < 1) {
				lpunkty = g_slist_append(lpunkty, punkt);
			}
			else {
				brak_danych = TRUE;
				delete punkt;
			}

			punkt = new cPunkt;
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikNr2",
				"entryOkragStycznaPunktWynikX2", "entryOkragStycznaPunktWynikY2",
				"entryOkragStycznaPunktWynikKod2", *punkt) < 1) {
				lpunkty = g_slist_append(lpunkty, punkt);
			}
			else {
				brak_danych = TRUE;
				delete punkt;
			}
			
			if (brak_danych)
				geod_raport_error(textview, _("Brak obliczeń lub brak rozwiązania"), TRUE);
			else
				geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punky styczności"));

			g_slist_free(lpunkty);
			lpunkty = NULL;
		break;

		case STYCZNA_PROSTA:
			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnProstaProstopadla)))
				geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nStyczna do okręgu prostopadła do prostej"));
			else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnProstaRownolegla)))
				geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nStyczna do okręgu równoległa do prostej"));

			// okrag
			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaOkrNr",
				"entryOkragStycznaProstaOkrX", "entryOkragStycznaProstaOkrY", NULL, *punkt)) {
				lpunkty = g_slist_append(lpunkty, punkt);
				geod_raport_table_pkt(textview, lpunkty, FALSE, _("Środek okręgu"));
				g_slist_free(lpunkty);
				lpunkty = NULL;
			}
			else {
				geod_raport_error(textview, _("Dla środka okręgu nie podano wszystkich wymaganych danych"));
				delete punkt;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaOkrR", promien)) {
				txt_promien = g_strdup_printf("%.*f", dok_xy_int, promien);
				geod_raport_line(textview, _("Promień okręgu"), txt_promien);
				g_free(txt_promien);
			}
			else
				geod_raport_error(textview, _("Nie podano promienia okręgu"));

			// prosta
			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaPrNr1",
				"entryOkragStycznaProstaPrX1", "entryOkragStycznaProstaPrY1", NULL, *punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla pierwszego punktu prostej nie podano wszystkich wymaganych danych"));
				delete punkt;
			}
			
			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaPrNr2",
				"entryOkragStycznaProstaPrX2", "entryOkragStycznaProstaPrY2", NULL, *punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla drugiego punktu prostej nie podano wszystkich wymaganych danych"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty prostej"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// wynik
			brak_danych = FALSE;
			punkt = new cPunkt;
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikNr1",
				"entryOkragStycznaProstaWynikX1", "entryOkragStycznaProstaWynikY1",
				"entryOkragStycznaProstaWynikKod1", *punkt) < 1) {
				lpunkty = g_slist_append(lpunkty, punkt);
			}
			else {
				brak_danych = TRUE;
				delete punkt;
			}

			punkt = new cPunkt;
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikNr2",
				"entryOkragStycznaProstaWynikX2", "entryOkragStycznaProstaWynikY2",
				"entryOkragStycznaProstaWynikKod2", *punkt) < 1) {
				lpunkty = g_slist_append(lpunkty, punkt);
			}
			else {
				brak_danych = TRUE;
				delete punkt;
			}
			
			if (brak_danych)
				geod_raport_error(textview, _("Brak obliczeń lub brak rozwiązania"), TRUE);
			else
				geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punky styczności"));
			
			g_slist_free(lpunkty);
			lpunkty = NULL;
		break;

		case STYCZNA_OKREGI:
			geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nStyczna do dwóch okręgów"));
			
			// pierwszy okrag
			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr1Nr",
				"entryOkragStyczna2OkregiOkr1X", "entryOkragStyczna2OkregiOkr1Y", NULL, *punkt)) {
				lpunkty = g_slist_append(lpunkty, punkt);
				geod_raport_table_pkt(textview, lpunkty, FALSE, _("Środek pierwszego okręgu"));
				g_slist_free(lpunkty);
				lpunkty = NULL;
			}
			else {
				geod_raport_error(textview, _("Dla środka pierwszego okręgu nie podano wszystkich wymaganych danych"));
				delete punkt;
			}
			
			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr1R", promien)) {
				txt_promien = g_strdup_printf("%.*f", dok_xy_int, promien);
				geod_raport_line(textview, _("Promień pierwszego okręgu"), txt_promien);
				g_free(txt_promien);
			}
			else
				geod_raport_error(textview, _("Nie podano promienia pierwszego okręgu"));

			// drugi okrag
			punkt = new cPunkt;
			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr2Nr",
				"entryOkragStyczna2OkregiOkr2X", "entryOkragStyczna2OkregiOkr2Y", NULL, *punkt)) {
				lpunkty = g_slist_append(lpunkty, punkt);
				geod_raport_table_pkt(textview, lpunkty, FALSE, _("Środek drugiego okręgu"));
				g_slist_free(lpunkty);
				lpunkty = NULL;
			}
			else {
				geod_raport_error(textview, _("Dla środka drugiego okręgu nie podano wszystkich wymaganych danych"));
				delete punkt;
			}
			
			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr2R", promien)) {
				txt_promien = g_strdup_printf("%.*f", dok_xy_int, promien);
				geod_raport_line(textview, _("Promień drugiego okręgu"), txt_promien);
				g_free(txt_promien);
			}
			else
				geod_raport_error(textview, _("Nie podano promienia drugiego okręgu"));

			// wynik
			brak_danych = FALSE;
			punkt = new cPunkt;
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr1a",
				"entryOkragStyczna2OkregiWynikX1a", "entryOkragStyczna2OkregiWynikY1a",
				"entryOkragStyczna2OkregiWynikKod1a", *punkt) < 1) {
				lpunkty = g_slist_append(lpunkty, punkt);
			}
			else {
				brak_danych = TRUE;
				delete punkt;
			}

			punkt = new cPunkt;
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr1b",
				"entryOkragStyczna2OkregiWynikX1b", "entryOkragStyczna2OkregiWynikY1b",
				"entryOkragStyczna2OkregiWynikKod1b", *punkt) < 1) {
				lpunkty = g_slist_append(lpunkty, punkt);
			}
			else {
				brak_danych = TRUE;
				delete punkt;
			}

			punkt = new cPunkt;
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr2a",
				"entryOkragStyczna2OkregiWynikX2a", "entryOkragStyczna2OkregiWynikY2a",
				"entryOkragStyczna2OkregiWynikKod2a", *punkt) < 1) {
				lpunkty = g_slist_append(lpunkty, punkt);
			}
			else {
				brak_danych = TRUE;
				delete punkt;
			}

			punkt = new cPunkt;
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr2b",
				"entryOkragStyczna2OkregiWynikX2b", "entryOkragStyczna2OkregiWynikY2b",
				"entryOkragStyczna2OkregiWynikKod2b", *punkt) < 1) {
				lpunkty = g_slist_append(lpunkty, punkt);
			}
			else {
				brak_danych = TRUE;
				delete punkt;
			}

			if (brak_danych)
				geod_raport_error(textview, _("Brak obliczeń lub brak rozwiązania"), TRUE);
			else
				geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punky styczności"));
			
			g_slist_free(lpunkty);
			lpunkty = NULL;
		break;
	}
	geod_raport_stopka(textview);
}


void on_tbtnOkragStycznaWidok_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt *pktDane = NULL;
	GdkPoint *pktWidok = NULL;
	double promien1, promien2;
	guint npoints;
	double skala;

	GtkWidget *winWidok = create_winWidok();
	gtk_widget_show(winWidok);
	GtkWidget *darea = lookup_widget(GTK_WIDGET(winWidok), "drawingAreaWidok");

	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookOkragStyczna");
	
	switch(gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook)))
	{
		case STYCZNA_PUNKT:
			npoints = 6;
			pktDane = new cPunkt[npoints];
			pktWidok = new GdkPoint[npoints];

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktOkrNr",
				"entryOkragStycznaPunktOkrX", "entryOkragStycznaPunktOkrY", NULL, pktDane[0]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać Nr, X, Y środka okręgu"));
				break;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktOkrR", promien1));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać promień okręgu"));
				break;
			}

			// maksymalna wartosc x, y
			pktDane[1].setX(pktDane[0].getX()-promien1);
			pktDane[1].setY(pktDane[0].getY()-promien1);
			pktDane[2].setX(pktDane[0].getX()+promien1);
			pktDane[2].setY(pktDane[0].getY()+promien1);

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktPktNr",
				"entryOkragStycznaPunktPktX", "entryOkragStycznaPunktPktY", NULL, pktDane[3]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Dla Punktu musisz podać Nr, X, Y"));
				break;
			}

			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikNr1",
				"entryOkragStycznaPunktWynikX1", "entryOkragStycznaPunktWynikY1", NULL, pktDane[4]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Brak obliczeń"));
				break;
			}
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaPunktWynikNr2",
				"entryOkragStycznaPunktWynikX2", "entryOkragStycznaPunktWynikY2", NULL, pktDane[5]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Brak obliczeń"));
				break;
			}

			skala = geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, npoints);

			geod_draw_line(GTK_WIDGET(darea), pktWidok[3], pktWidok[4], LINE_POLNOC);
			geod_draw_line(GTK_WIDGET(darea), pktWidok[3], pktWidok[5], LINE_POLNOC);
			geod_draw_circle(GTK_WIDGET(darea), pktWidok[0], (int)round(promien1*skala));

			geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[3], pktDane[3].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[4], pktDane[4].getNr(), PKT_WYNIK);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[5], pktDane[5].getNr(), PKT_WYNIK);			
			
			gtk_widget_queue_draw(darea);
		break;

		case STYCZNA_PROSTA:
			npoints = 7;
			pktDane = new cPunkt[npoints];
			pktWidok = new GdkPoint[npoints];

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaOkrNr",
				"entryOkragStycznaProstaOkrX", "entryOkragStycznaProstaOkrY", NULL, pktDane[0]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać Nr, X, Y środka okręgu"));
				break;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaOkrR", promien1));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać promień okręgu"));
				break;
			}

			// maksymalna wartosc x, y
			pktDane[1].setX(pktDane[0].getX()-promien1);
			pktDane[1].setY(pktDane[0].getY()-promien1);
			pktDane[2].setX(pktDane[0].getX()+promien1);
			pktDane[2].setY(pktDane[0].getY()+promien1);

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaPrNr1",
				"entryOkragStycznaProstaPrX1", "entryOkragStycznaProstaPrY1", NULL, pktDane[3]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać Nr, X, Y pierwszego punktu prostej"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaPrNr2",
				"entryOkragStycznaProstaPrX2", "entryOkragStycznaProstaPrY2", NULL, pktDane[4]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać Nr, X, Y drugiego punktu prostej"));
				break;
			}

			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikNr1",
				"entryOkragStycznaProstaWynikX1", "entryOkragStycznaProstaWynikY1", NULL, pktDane[5]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Brak obliczeń"));
				break;
			}
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStycznaProstaWynikNr2",
				"entryOkragStycznaProstaWynikX2", "entryOkragStycznaProstaWynikY2", NULL, pktDane[6]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Brak obliczeń"));
				break;
			}

			skala = geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, npoints);

			geod_draw_line(GTK_WIDGET(darea), pktWidok[3], pktWidok[4], LINE_BAZA);
			geod_draw_circle(GTK_WIDGET(darea), pktWidok[0], (int)round(promien1*skala));

			geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[3], pktDane[3].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[4], pktDane[4].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[5], pktDane[5].getNr(), PKT_WYNIK);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[6], pktDane[6].getNr(), PKT_WYNIK);
			
			gtk_widget_queue_draw(darea);
		break;

		case STYCZNA_OKREGI:
			npoints = 10;
			pktDane = new cPunkt[npoints];
			pktWidok = new GdkPoint[npoints];

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr1Nr",
				"entryOkragStyczna2OkregiOkr1X", "entryOkragStyczna2OkregiOkr1Y", NULL, pktDane[0]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać Nr, X, Y środka pierwszego okręgu"));
				break;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr1R", promien1));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać promień pierwszego okręgu"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr2Nr",
				"entryOkragStyczna2OkregiOkr2X", "entryOkragStyczna2OkregiOkr2Y", NULL, pktDane[1]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać Nr, X, Y środka drugiego okręgu"));
				break;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiOkr2R", promien2));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Musisz podać promień drugiego okręgu"));
				break;
			}

			// maksymalna wartosc x, y
			pktDane[2].setX(pktDane[0].getX()-promien1);
			pktDane[2].setY(pktDane[0].getY()-promien1);
			pktDane[3].setX(pktDane[0].getX()+promien1);
			pktDane[3].setY(pktDane[0].getY()+promien1);
			pktDane[4].setX(pktDane[1].getX()-promien2);
			pktDane[4].setY(pktDane[1].getY()-promien2);
			pktDane[5].setX(pktDane[1].getX()+promien2);
			pktDane[5].setY(pktDane[1].getY()+promien2);
			
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr1a",
				"entryOkragStyczna2OkregiWynikX1a", "entryOkragStyczna2OkregiWynikY1a", NULL, pktDane[6]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Brak obliczeń"));
				break;
			}

			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr1b",
				"entryOkragStyczna2OkregiWynikX1b", "entryOkragStyczna2OkregiWynikY1b", NULL, pktDane[7]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Brak obliczeń"));
				break;
			}

			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr2a",
				"entryOkragStyczna2OkregiWynikX2a", "entryOkragStyczna2OkregiWynikY2a", NULL, pktDane[8]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Brak obliczeń"));
				break;
			}

			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragStyczna2OkregiWynikNr2b",
				"entryOkragStyczna2OkregiWynikX2b", "entryOkragStyczna2OkregiWynikY2b", NULL, pktDane[9]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragStyczna", _("Brak obliczeń"));
				break;
			}

			skala = geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, npoints);

			geod_draw_line(GTK_WIDGET(darea), pktWidok[6], pktWidok[8], LINE_POLNOC);
			geod_draw_line(GTK_WIDGET(darea), pktWidok[7], pktWidok[9], LINE_POLNOC);
			geod_draw_circle(GTK_WIDGET(darea), pktWidok[0], (int)round(promien1*skala));
			geod_draw_circle(GTK_WIDGET(darea), pktWidok[1], (int)round(promien2*skala));

			geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[1].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[6], pktDane[6].getNr(), PKT_WYNIK);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[7], pktDane[7].getNr(), PKT_WYNIK);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[8], pktDane[8].getNr(), PKT_WYNIK);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[9], pktDane[9].getNr(), PKT_WYNIK);
			
			gtk_widget_queue_draw(darea);
		break;
	delete [] pktDane;
	delete [] pktWidok;
	}
}


void on_tbtnOkragStycznaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winOkragStyczna");
	gtk_widget_destroy(window);
}


gboolean on_entryOkragStycznaProstaOkrNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragStyczna");
	return FALSE;
}


gboolean on_entryOkragStycznaProstaOkrNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragStyczna");
	return FALSE;
}


gboolean on_entryOkragStycznaProstaPrNr1_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragStyczna");
	return FALSE;
}


gboolean on_entryOkragStycznaProstaPrNr1_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragStyczna");
	return FALSE;
}


gboolean on_entryOkragStycznaProstaPrNr2_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragStyczna");
	return FALSE;
}


gboolean on_entryOkragStycznaProstaPrNr2_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragStyczna");
	return FALSE;
}


void on_btnOkragStycznaProstaWynikDodaj_clicked (GtkButton *button,
	gpointer user_data)
{
	cPunkt pktWynik1, pktWynik2;
	double okrR;
	int zadanie_id;
	bool zmiana1 = FALSE, zmiana2 = FALSE;
	int s_dodaj1, s_dodaj2;
	GtkWidget *entryWynik1Nr = lookup_widget(GTK_WIDGET(button), "entryOkragStycznaProstaWynikNr1");
	GtkWidget *entryWynik2Nr = lookup_widget(GTK_WIDGET(button), "entryOkragStycznaProstaWynikNr1");
	GtkWidget *buttonWyczysc = lookup_widget(GTK_WIDGET(button), "tbtnOkragStycznaWyczysc");

	// gdy nie ustawiono statusu - ustaw brak obliczen
	if (g_object_get_data(G_OBJECT(entryWynik1Nr), "status_dodaj") == NULL)
		g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));

	if (g_object_get_data(G_OBJECT(entryWynik2Nr), "status_dodaj") == NULL)
		g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));

	s_dodaj1 = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entryWynik1Nr), "status_dodaj"));
	s_dodaj2 = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entryWynik2Nr), "status_dodaj"));
	
	// gdy brak obliczen przerwij
	if (s_dodaj1 == PKT_BRAK_DANYCH && s_dodaj2 == PKT_BRAK_DANYCH) {
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragStyczna", _("Brak obliczeń"));
		return;
	}
	if (s_dodaj1 == PKT_ERROR && s_dodaj2 == PKT_ERROR) {
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragStyczna", _("Brak rozwiązania"));
		return;
	}

	// kontrola wymaganych danych
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryOkragStycznaProstaWynikNr1",
			"entryOkragStycznaProstaWynikX1", "entryOkragStycznaProstaWynikY1",
			"entryOkragStycznaProstaWynikKod1", pktWynik1) &&
		!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryOkragStycznaProstaWynikNr2",
			"entryOkragStycznaProstaWynikX2", "entryOkragStycznaProstaWynikY2",
			"entryOkragStycznaProstaWynikKod2", pktWynik2))
	{
		if (s_dodaj1 == PKT_DODAJ) {
			dbgeod_point_add_config(pktWynik1, &zmiana1);
			if (zmiana1) {
				g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODANY));
				gtk_entry_set_editable(GTK_ENTRY(entryWynik1Nr), FALSE);
			}
		}
		else
			zmiana1 = TRUE;

		if (s_dodaj2 == PKT_DODAJ) {
			dbgeod_point_add_config(pktWynik2, &zmiana2);
			if (zmiana2) {
				g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODANY));
				gtk_entry_set_editable(GTK_ENTRY(entryWynik2Nr), FALSE);
			}
		}
		else
			zmiana2 = TRUE;
		
		if (zmiana1 && zmiana2) {
			GtkWidget *rbtnStycznaProstaRownolegla = lookup_widget(GTK_WIDGET(button), "rbtnOkragStycznaProstaStycznaRownolegla");
			GtkWidget *entryOkrNr = lookup_widget(GTK_WIDGET(button), "entryOkragStycznaProstaOkrNr");
			GtkWidget *entryPrNr1 = lookup_widget(GTK_WIDGET(button), "entryOkragStycznaProstaPrNr1");
			GtkWidget *entryPrNr2 = lookup_widget(GTK_WIDGET(button), "entryOkragStycznaProstaPrNr2");
			
			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnStycznaProstaRownolegla))) 
				dbgeod_hist_add("OkragStycznaProstaRownolegla", zadanie_id);
			else
				dbgeod_hist_add("OkragStycznaProstaProstopadla", zadanie_id);
			
			dbgeod_hist_point_set(zadanie_id, "SNr", gtk_entry_get_text(GTK_ENTRY(entryOkrNr)), 0);
			dbgeod_hist_point_set(zadanie_id, "PrNr1", gtk_entry_get_text(GTK_ENTRY(entryPrNr1)), 0);
			dbgeod_hist_point_set(zadanie_id, "PrNr2", gtk_entry_get_text(GTK_ENTRY(entryPrNr2)), 0);
			
			dbgeod_hist_point_set(zadanie_id, "W1Nr", pktWynik1.getNr(), 1);
			dbgeod_hist_point_set(zadanie_id, "W2Nr", pktWynik2.getNr(), 1);
			
			geod_entry_get_double(GTK_WIDGET(button), "entryOkragStycznaProstaOkrR", okrR);
			dbgeod_hist_par_set(zadanie_id, "Promien", okrR, 0);
			
			GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Dalsze obliczenia?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
				g_signal_emit_by_name(buttonWyczysc, "clicked");
			gtk_widget_destroy(dialog);
		}
	}
	else
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragStyczna", _("Nie podano Nr punktów"));
}


gboolean on_entryOkragStyczna2OkregiOkr1Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragStyczna");
	return FALSE;
}


gboolean on_entryOkragStyczna2OkregiOkr1Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragStyczna");
	return FALSE;
}


gboolean on_entryOkragStyczna2OkregiOkr2Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragStyczna");
	return FALSE;
}


gboolean on_entryOkragStyczna2OkregiOkr2Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragStyczna");
	return FALSE;
}


void on_btnOkragStyczna2OkregiWynikDodaj_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktWynik1a, pktWynik1b, pktWynik2a, pktWynik2b;
	bool zmiana1a = FALSE, zmiana1b = FALSE, zmiana2a = FALSE, zmiana2b = FALSE;
	double okrR;
	int zadanie_id;
	int s_dodaj1a, s_dodaj1b, s_dodaj2a, s_dodaj2b;
	GtkWidget *entryWynik1aNr = lookup_widget(GTK_WIDGET(button), "entryOkragStyczna2OkregiWynikNr1a");
	GtkWidget *entryWynik1bNr = lookup_widget(GTK_WIDGET(button), "entryOkragStyczna2OkregiWynikNr1b");
	GtkWidget *entryWynik2aNr = lookup_widget(GTK_WIDGET(button), "entryOkragStyczna2OkregiWynikNr2a");
	GtkWidget *entryWynik2bNr = lookup_widget(GTK_WIDGET(button), "entryOkragStyczna2OkregiWynikNr2b");

	GtkWidget *buttonWyczysc = lookup_widget(GTK_WIDGET(button), "tbtnOkragStycznaWyczysc");

	// gdy nie ustawiono statusu - ustaw brak obliczen
	if (g_object_get_data(G_OBJECT(entryWynik1aNr), "status_dodaj") == NULL)
		g_object_set_data(G_OBJECT(entryWynik1aNr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));

	if (g_object_get_data(G_OBJECT(entryWynik1bNr), "status_dodaj") == NULL)
		g_object_set_data(G_OBJECT(entryWynik1bNr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));

	if (g_object_get_data(G_OBJECT(entryWynik2aNr), "status_dodaj") == NULL)
		g_object_set_data(G_OBJECT(entryWynik2aNr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));

	if (g_object_get_data(G_OBJECT(entryWynik2bNr), "status_dodaj") == NULL)
		g_object_set_data(G_OBJECT(entryWynik2bNr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));

	s_dodaj1a = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entryWynik1aNr), "status_dodaj"));
	s_dodaj1b = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entryWynik1bNr), "status_dodaj"));
	s_dodaj2a = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entryWynik2aNr), "status_dodaj"));
	s_dodaj2b = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entryWynik2bNr), "status_dodaj"));
	
	// gdy brak obliczen przerwij
	if (s_dodaj1a == PKT_BRAK_DANYCH && s_dodaj1b == PKT_BRAK_DANYCH &&
			s_dodaj2a == PKT_BRAK_DANYCH && s_dodaj2b == PKT_BRAK_DANYCH) {
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragStyczna", _("Brak obliczeń"));
		return;
	}
	if (s_dodaj1a == PKT_ERROR && s_dodaj1b == PKT_ERROR &&
			s_dodaj2a == PKT_ERROR && s_dodaj2b == PKT_ERROR) {
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragStyczna", _("Brak rozwiązania"));
		return;
	}

	// kontrola wymaganych danych
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryOkragStyczna2OkregiWynikNr1a",
			"entryOkragStyczna2OkregiWynikX1a", "entryOkragStyczna2OkregiWynikY1a",
			"entryOkragStyczna2OkregiWynikKod1a", pktWynik1a) &&
		!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryOkragStyczna2OkregiWynikNr1b",
			"entryOkragStyczna2OkregiWynikX1b", "entryOkragStyczna2OkregiWynikY1b",
			"entryOkragStyczna2OkregiWynikKod1b", pktWynik1b) &&
		!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryOkragStyczna2OkregiWynikNr2a",
			"entryOkragStyczna2OkregiWynikX2a", "entryOkragStyczna2OkregiWynikY2a",
			"entryOkragStyczna2OkregiWynikKod2a", pktWynik2a) &&
		!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryOkragStyczna2OkregiWynikNr2b",
			"entryOkragStyczna2OkregiWynikX2b", "entryOkragStyczna2OkregiWynikY2b",
			"entryOkragStyczna2OkregiWynikKod2b", pktWynik2b))
	{
		if (s_dodaj1a == PKT_DODAJ) {
			dbgeod_point_add_config(pktWynik1a, &zmiana1a);
			if (zmiana1a) {
				g_object_set_data(G_OBJECT(entryWynik1aNr), "status_dodaj", GINT_TO_POINTER(PKT_DODANY));
				gtk_entry_set_editable(GTK_ENTRY(entryWynik1aNr), FALSE);
			}
		}
		else
			zmiana1a = TRUE;

		if (s_dodaj1b == PKT_DODAJ) {
			dbgeod_point_add_config(pktWynik1b, &zmiana1b);
			if (zmiana1b) {
				g_object_set_data(G_OBJECT(entryWynik1bNr), "status_dodaj", GINT_TO_POINTER(PKT_DODANY));
				gtk_entry_set_editable(GTK_ENTRY(entryWynik1bNr), FALSE);
			}
		}
		else
			zmiana1b = TRUE;

		if (s_dodaj2a == PKT_DODAJ) {
			dbgeod_point_add_config(pktWynik2a, &zmiana2a);
			if (zmiana2a) {
				g_object_set_data(G_OBJECT(entryWynik2aNr), "status_dodaj", GINT_TO_POINTER(PKT_DODANY));
				gtk_entry_set_editable(GTK_ENTRY(entryWynik2aNr), FALSE);
			}
		}
		else
			zmiana2a = TRUE;

		if (s_dodaj2b == PKT_DODAJ) {
			dbgeod_point_add_config(pktWynik2b, &zmiana2b);
			if (zmiana2b) {
				g_object_set_data(G_OBJECT(entryWynik2bNr), "status_dodaj", GINT_TO_POINTER(PKT_DODANY));
				gtk_entry_set_editable(GTK_ENTRY(entryWynik2bNr), FALSE);
			}
		}
		else
			zmiana2b = TRUE;
		
		if (zmiana1a && zmiana1b && zmiana2a && zmiana2b) {
			GtkWidget *entryOkrNr1 = lookup_widget(GTK_WIDGET(button), "entryOkragStyczna2OkregiOkr1Nr");
			GtkWidget *entryOkrNr2 = lookup_widget(GTK_WIDGET(button), "entryOkragStyczna2OkregiOkr2Nr");
			
			dbgeod_hist_add("OkragStyczna2Okregi", zadanie_id);
			
			dbgeod_hist_point_set(zadanie_id, "S1Nr", gtk_entry_get_text(GTK_ENTRY(entryOkrNr1)), 0);
			dbgeod_hist_point_set(zadanie_id, "S2Nr", gtk_entry_get_text(GTK_ENTRY(entryOkrNr2)), 0);

			geod_entry_get_double(GTK_WIDGET(button), "entryOkragStyczna2OkregiOkr1R", okrR);
			dbgeod_hist_par_set(zadanie_id, "PromienOkr1", okrR, 0);
			geod_entry_get_double(GTK_WIDGET(button), "entryOkragStyczna2OkregiOkr2R", okrR);
			dbgeod_hist_par_set(zadanie_id, "PromienOkr2", okrR, 0);
			
			dbgeod_hist_point_set(zadanie_id, "W1aNr", pktWynik1a.getNr(), 1);
			dbgeod_hist_point_set(zadanie_id, "W1bNr", pktWynik1b.getNr(), 1);
			dbgeod_hist_point_set(zadanie_id, "W2aNr", pktWynik2a.getNr(), 1);
			dbgeod_hist_point_set(zadanie_id, "W2bNr", pktWynik2b.getNr(), 1);
						
			GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Dalsze obliczenia?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
				g_signal_emit_by_name(buttonWyczysc, "clicked");
			gtk_widget_destroy(dialog);
		}
	}
	else
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragStyczna", _("Nie podano Nr punktów"));
}

void on_buttonOkragStycznaPunktWynikDodaj_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktWynik1, pktWynik2;
	double okrR;
	int zadanie_id;
	bool zmiana1 = FALSE, zmiana2 = FALSE;
	int s_dodaj1, s_dodaj2;
	GtkWidget *entryWynik1Nr = lookup_widget(GTK_WIDGET(button), "entryOkragStycznaPunktWynikNr1");
	GtkWidget *entryWynik2Nr = lookup_widget(GTK_WIDGET(button), "entryOkragStycznaPunktWynikNr2");
	GtkWidget *buttonWyczysc = lookup_widget(GTK_WIDGET(button), "tbtnOkragStycznaWyczysc");

	// gdy nie ustawiono statusu - ustaw brak obliczen
	if (g_object_get_data(G_OBJECT(entryWynik1Nr), "status_dodaj") == NULL)
		g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));

	if (g_object_get_data(G_OBJECT(entryWynik2Nr), "status_dodaj") == NULL)
		g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));

	s_dodaj1 = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entryWynik1Nr), "status_dodaj"));
	s_dodaj2 = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entryWynik2Nr), "status_dodaj"));
	
	// gdy brak obliczen przerwij
	if (s_dodaj1 == PKT_BRAK_DANYCH && s_dodaj2 == PKT_BRAK_DANYCH) {
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragStyczna", "Brak obliczeń");
		return;
	}
	if (s_dodaj1 == PKT_ERROR && s_dodaj2 == PKT_ERROR) {
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragStyczna", _("Brak rozwiązania"));
		return;
	}

	// kontrola wymaganych danych
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryOkragStycznaPunktWynikNr1",
			"entryOkragStycznaPunktWynikX1", "entryOkragStycznaPunktWynikY1",
			"entryOkragStycznaPunktWynikKod1", pktWynik1) &&
		!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryOkragStycznaPunktWynikNr2",
			"entryOkragStycznaPunktWynikX2", "entryOkragStycznaPunktWynikY2",
			"entryOkragStycznaPunktWynikKod2", pktWynik2))
	{
		if (s_dodaj1 == PKT_DODAJ) {
			dbgeod_point_add_config(pktWynik1, &zmiana1);
			if (zmiana1) {
				g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODANY));
				gtk_entry_set_editable(GTK_ENTRY(entryWynik1Nr), FALSE);
			}
		}
		else
			zmiana1 = TRUE;

		if (s_dodaj2 == PKT_DODAJ) {
			dbgeod_point_add_config(pktWynik2, &zmiana2);
			if (zmiana2) {
				g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODANY));
				gtk_entry_set_editable(GTK_ENTRY(entryWynik2Nr), FALSE);
			}
		}
		else
			zmiana2 = TRUE;
		
		if (zmiana1 && zmiana2) {
			GtkWidget *entryOkrNr = lookup_widget(GTK_WIDGET(button), "entryOkragStycznaPunktOkrNr");
			GtkWidget *entryPktNr = lookup_widget(GTK_WIDGET(button), "entryOkragStycznaPunktPktNr");
			
			dbgeod_hist_add("OkragStycznaPunkt", zadanie_id);
			
			dbgeod_hist_point_set(zadanie_id, "SNr", gtk_entry_get_text(GTK_ENTRY(entryOkrNr)), 0);
			dbgeod_hist_point_set(zadanie_id, "PktNr", gtk_entry_get_text(GTK_ENTRY(entryPktNr)), 0);

			geod_entry_get_double(GTK_WIDGET(button), "entryOkragStycznaPunktOkrR", okrR);
			dbgeod_hist_par_set(zadanie_id, "Promien", okrR, 0);
			
			dbgeod_hist_point_set(zadanie_id, "W1Nr", pktWynik1.getNr(), 1);
			dbgeod_hist_point_set(zadanie_id, "W2Nr", pktWynik2.getNr(), 1);

			GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Dalsze obliczenia?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
				g_signal_emit_by_name(buttonWyczysc, "clicked");
			gtk_widget_destroy(dialog);
		}
	}
	else
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragStyczna", _("Nie podano Nr punktów"));
}


/* winOkragWpasowanie */
void on_winOkragWpasowanie_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winOkragWpasowanie");
}


void on_tbtnOkragWpasowanieWykonaj_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt pkt1, pkt2, pkt3;
	cPunkt pktPrLP, pktPrLK, pktPrPP, pktPrPK;
	cPunkt pktSrodek;
	double promien;
	int geod_error;
	char *txtPromien = NULL;
	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookOkragWpasowanie");
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	switch(gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook)))
	{
	case 0:
	// Wpasowanie okregu w 2 proste
		geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProsteWynikX");
		geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProsteWynikY");

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePLNr1",
			"entryOkragWpasowanie2ProstePLX1", "entryOkragWpasowanie2ProstePLY1", NULL, pktPrLP));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Musisz podać Nr, X, Y pierwszego punktu prostej l"));
			break;
		}

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePLNr2",
			"entryOkragWpasowanie2ProstePLX2", "entryOkragWpasowanie2ProstePLY2", NULL, pktPrLK));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Musisz podać Nr, X, Y drugiego punktu prostej l"));
			break;
		}

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePPNr1",
			"entryOkragWpasowanie2ProstePPX1", "entryOkragWpasowanie2ProstePPY1", NULL, pktPrPP));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Musisz podać Nr, X, Y pierwszego punktu prostej p"));
			break;
		}

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePPNr2",
			"entryOkragWpasowanie2ProstePPX2", "entryOkragWpasowanie2ProstePPY2", NULL, pktPrPK));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Musisz podać Nr, X, Y drugiego punktu prostej p"));
			break;
		}

		if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProsteWynikR", promien));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Musisz podać promień okręgu"));
			break;
		}


		geod_error = okrag_wpasowanie_2proste(pktPrLP, pktPrLK, pktPrPP, pktPrPK, promien, pktSrodek);
		if (geod_error == GEODERR_OK)
			geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProsteWynikX",
				"entryOkragWpasowanie2ProsteWynikY", pktSrodek);
		else
			geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", geod_error);
	break;

	case 1:
	// Wpasowanie okregu w 3 punkty
		geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktWynikX");
		geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktWynikY");
		geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktWynikR");

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPNr1",
			"entryOkragWpasowanie3PktPX1", "entryOkragWpasowanie3PktPY1", NULL, pkt1));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Dla punktu 1 musisz podać Nr, X, Y"));
			break;
		}

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPNr2",
			"entryOkragWpasowanie3PktPX2", "entryOkragWpasowanie3PktPY2", NULL, pkt2));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Dla punktu 2 musisz podać Nr, X, Y"));
			break;
		}

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPNr3",
			"entryOkragWpasowanie3PktPX3", "entryOkragWpasowanie3PktPY3", NULL, pkt3));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Dla punktu 3 musisz podać Nr, X, Y"));
			break;
		}

		geod_error = okrag_wpasowanie_3punkty(pkt1, pkt2, pkt3, pktSrodek, promien);
		if (geod_error == GEODERR_OK) {
			geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktWynikX",
			"entryOkragWpasowanie3PktWynikY", pktSrodek);
			GtkWidget *entryWynikPromien = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktWynikR");
			txtPromien = g_strdup_printf("%.*f", dok_xy_int, promien);
			gtk_entry_set_text(GTK_ENTRY(entryWynikPromien), txtPromien);
			g_free(txtPromien);
		}
		else
			geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", geod_error);
	break;
	}
}


void on_tbtnOkragWpasowanieWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *focus = NULL;
	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookOkragWpasowanie");
	switch(gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook)))
	{
		case 0:
		// Wpasowanie okregu o danym promieniu w 2 proste
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePLNr1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePLX1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePLY1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePLNr2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePLX2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePLY2");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePPNr1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePPX1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePPY1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePPNr2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePPX2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePPY2");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProsteWynikNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProsteWynikX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProsteWynikY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProsteWynikR");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProsteWynikKod");
			focus = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePLNr1");
			gtk_widget_grab_focus(GTK_WIDGET(focus));
		break;

		case 1:
		// Wpasowanie okregu w 3 punkty
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPNr1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPX1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPY1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPNr2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPX2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPY2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPNr3");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPX3");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPY3");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktWynikNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktWynikX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktWynikY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktWynikR");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktWynikKod");
			focus = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPNr1");
			gtk_widget_grab_focus(GTK_WIDGET(focus));
		break;
	}
}


void on_tbtnOkragWpasowanieRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GSList *lpunkty = NULL;
	char *txt_promien = NULL;
	double promien;
	cPunkt *punkt = NULL;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	GtkWidget *dialogRaport = create_dialogRaport();
	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");
	gtk_widget_show(dialogRaport);

	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookOkragWpasowanie");
	
	switch(gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook))) {
	case 0: // Wpasowanie okregu w 2 proste
		geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nWpasowanie okręgu  w dwie proste"));
		
		// pierwsza posta
		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePLNr1",
			"entryOkragWpasowanie2ProstePLX1", "entryOkragWpasowanie2ProstePLY1", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
		else {
			geod_raport_error(textview, _("Dla pierwszego punktu prostej l nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePLNr2",
			"entryOkragWpasowanie2ProstePLX2", "entryOkragWpasowanie2ProstePLY2", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
		else {
			geod_raport_error(textview, _("Dla drugiego punktu prostej l nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		geod_raport_table_pkt(textview, lpunkty, FALSE, "Punkty prostej l");
		g_slist_free(lpunkty);
		lpunkty = NULL;

		// druga prosta
		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePPNr1",
			"entryOkragWpasowanie2ProstePPX1", "entryOkragWpasowanie2ProstePPY1", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
		else {
			geod_raport_error(textview, _("Dla pierwszego punktu prostej p nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePPNr2",
			"entryOkragWpasowanie2ProstePPX2", "entryOkragWpasowanie2ProstePPY2", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
		else {
			geod_raport_error(textview, _("Dla drugiego punktu prostej p nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		geod_raport_table_pkt(textview, lpunkty, FALSE, "Punkty prostej p");
		g_slist_free(lpunkty);
		lpunkty = NULL;
		
		// promien
		if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProsteWynikR", promien)) {
			txt_promien = g_strdup_printf("%.*f", dok_xy_int, promien);
			geod_raport_line(textview, _("\nPromień okręgu"), txt_promien, TRUE);
			g_free(txt_promien);
		}
		else
			geod_raport_error(textview, _("Nie podano promienia okręgu"));

		// wynik
		punkt = new cPunkt;
		if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
			"entryOkragWpasowanie2ProsteWynikNr", "entryOkragWpasowanie2ProsteWynikX",
			"entryOkragWpasowanie2ProsteWynikY", "entryOkragWpasowanie2ProsteWynikKod", *punkt) < 1) {
			lpunkty = g_slist_append(lpunkty, punkt);
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Środek okręgu"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else {
			geod_raport_error(textview, _("Brak obliczeń lub brak rozwiązania"), TRUE);
			delete punkt;
		}
	break;

	case 1: // Wpasowanie okregu w 3 punkty
		geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nWpasowanie okręgu w trzy punkty należące do okręgu"));
		
		// punkty
		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPNr1",
			"entryOkragWpasowanie3PktPX1", "entryOkragWpasowanie3PktPY1", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
		else {
			geod_raport_error(textview, _("Dla pierwszego punktu nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPNr2",
			"entryOkragWpasowanie3PktPX2", "entryOkragWpasowanie3PktPY2", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
		else {
			geod_raport_error(textview, _("Dla drugiego punktu nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPNr3",
			"entryOkragWpasowanie3PktPX3", "entryOkragWpasowanie3PktPY3", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
		else {
			geod_raport_error(textview, _("Dla trzeciego punktu nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty należące do okręgu"));
		g_slist_free(lpunkty);
		lpunkty = NULL;

		// wynik
		punkt = new cPunkt;
		if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
			"entryOkragWpasowanie3PktWynikNr", "entryOkragWpasowanie3PktWynikX",
			"entryOkragWpasowanie3PktWynikY", "entryOkragWpasowanie3PktWynikKod", *punkt) < 1) {
			lpunkty = g_slist_append(lpunkty, punkt);
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Środek okręgu"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktWynikR", promien)) {
				txt_promien = g_strdup_printf("%.*f", dok_xy_int, promien);
				geod_raport_line(textview, _("\nPromień okręgu"), txt_promien, TRUE);
				g_free(txt_promien);
			}
		}
		else {
			geod_raport_error(textview, _("Brak obliczeń lub brak rozwiązania"), TRUE);
			delete punkt;
		}
	break;
	}
}


void on_tbtnOkragWpasowanieWidok_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt *pktDane = NULL;
	GdkPoint *pktWidok = NULL;
	double promien;
	guint npoints;
	double skala;

	GtkWidget *winWidok = create_winWidok();
	gtk_widget_show(winWidok);
	GtkWidget *darea = lookup_widget(GTK_WIDGET(winWidok), "drawingAreaWidok");

	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookOkragWpasowanie");
	
	switch(gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook)))
	{
		case 0:
			npoints = 7;
			pktDane = new cPunkt[npoints];
			pktWidok = new GdkPoint[npoints];

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePLNr1",
				"entryOkragWpasowanie2ProstePLX1", "entryOkragWpasowanie2ProstePLY1", NULL, pktDane[0]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Musisz podać Nr, X, Y pierwszego punktu prostej l"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePLNr2",
				"entryOkragWpasowanie2ProstePLX2", "entryOkragWpasowanie2ProstePLY2", NULL, pktDane[1]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Musisz podać Nr, X, Y drugiego punktu prostej l"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePPNr1",
				"entryOkragWpasowanie2ProstePPX1", "entryOkragWpasowanie2ProstePPY1", NULL, pktDane[2]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Musisz podać Nr, X, Y pierwszego punktu prostej p"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProstePPNr2",
				"entryOkragWpasowanie2ProstePPX2", "entryOkragWpasowanie2ProstePPY2", NULL, pktDane[3]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Musisz podać Nr, X, Y drugiego punktu prostej p"));
				break;
			}
	
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProsteWynikNr",
				"entryOkragWpasowanie2ProsteWynikX", "entryOkragWpasowanie2ProsteWynikY", NULL, pktDane[4]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Brak obliczeń"));
				break;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragWpasowanie2ProsteWynikR", promien));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Musisz podać promień okręgu"));
				break;
			}

			// maksymalna wartosc x, y
			pktDane[5].setX(pktDane[4].getX()-promien);
			pktDane[5].setY(pktDane[4].getY()-promien);
			pktDane[6].setX(pktDane[4].getX()+promien);
			pktDane[6].setY(pktDane[4].getY()+promien);

			skala = geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, npoints);

			geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[1], LINE_POLNOC);
			geod_draw_line(GTK_WIDGET(darea), pktWidok[2], pktWidok[3], LINE_POLNOC);
			geod_draw_circle(GTK_WIDGET(darea), pktWidok[4], (int)round(promien*skala));

			geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[1].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[2], pktDane[2].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[3], pktDane[3].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[4], pktDane[4].getNr(), PKT_WYNIK);
			
			gtk_widget_queue_draw(darea);
		break;

		case 1:
			npoints = 6;
			pktDane = new cPunkt[npoints];
			pktWidok = new GdkPoint[npoints];

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPNr1",
				"entryOkragWpasowanie3PktPX1", "entryOkragWpasowanie3PktPY1", NULL, pktDane[0]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Dla punktu 1 musisz podać Nr, X, Y"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPNr2",
				"entryOkragWpasowanie3PktPX2", "entryOkragWpasowanie3PktPY2", NULL, pktDane[1]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Dla punktu 2 musisz podać Nr, X, Y"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktPNr3",
				"entryOkragWpasowanie3PktPX3", "entryOkragWpasowanie3PktPY3", NULL, pktDane[2]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Dla punktu 3 musisz podać Nr, X, Y"));
				break;
			}
	
			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktWynikNr",
				"entryOkragWpasowanie3PktWynikX", "entryOkragWpasowanie3PktWynikY", NULL, pktDane[3]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Brak obliczeń"));
				break;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragWpasowanie3PktWynikR", promien));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragWpasowanie", _("Brak obliczeń"));
				break;
			}

			// maksymalna wartosc x, y
			pktDane[4].setX(pktDane[3].getX()-promien);
			pktDane[4].setY(pktDane[3].getY()-promien);
			pktDane[5].setX(pktDane[3].getX()+promien);
			pktDane[5].setY(pktDane[3].getY()+promien);

			skala = geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, npoints);

			geod_draw_circle(GTK_WIDGET(darea), pktWidok[3], (int)round(promien*skala));

			geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[1].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[2], pktDane[2].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[3], pktDane[3].getNr(), PKT_WYNIK);
			
			gtk_widget_queue_draw(darea);
		break;
	}
}


void on_tbtnOkragWpasowanieZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winOkragWpasowanie");
	gtk_widget_destroy(window);
}


gboolean on_entryOkragWpasowanie2ProstePLNr1_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragWpasowanie");
	return FALSE;
}


gboolean on_entryOkragWpasowanie2ProstePLNr1_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragWpasowanie");
	return FALSE;
}


gboolean on_entryOkragWpasowanie2ProstePLNr2_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragWpasowanie");
	return FALSE;
}


gboolean on_entryOkragWpasowanie2ProstePLNr2_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragWpasowanie");
	return FALSE;
}


gboolean on_entryOkragWpasowanie2ProstePPNr1_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragWpasowanie");
	return FALSE;
}


gboolean on_entryOkragWpasowanie2ProstePPNr1_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragWpasowanie");
	return FALSE;
}


gboolean on_entryOkragWpasowanie2ProstePPNr2_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragWpasowanie");
	return FALSE;
}


gboolean on_entryOkragWpasowanie2ProstePPNr2_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragWpasowanie");
	return FALSE;
}


void on_btnOkragWpasowanie2ProsteWynikDodaj_clicked (GtkButton *button,
	gpointer user_data)
{
	cPunkt pktWynikSrodek;
	bool zmiana = FALSE;
	int zadanie_id;
	double okrR;
	
	GtkWidget *buttonWyczysc = lookup_widget(GTK_WIDGET(button), "tbtnOkragWpasowanieWyczysc");
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryOkragWpasowanie2ProsteWynikNr",
			"entryOkragWpasowanie2ProsteWynikX", "entryOkragWpasowanie2ProsteWynikY",
			"entryOkragWpasowanie2ProsteWynikKod", pktWynikSrodek))
		{
			dbgeod_point_add_config(pktWynikSrodek, &zmiana);
			if (zmiana) {
				GtkWidget *entryPLNr1 = lookup_widget(GTK_WIDGET(button), "entryOkragWpasowanie2ProstePLNr1");
				GtkWidget *entryPLNr2 = lookup_widget(GTK_WIDGET(button), "entryOkragWpasowanie2ProstePLNr2");
				GtkWidget *entryPPNr1 = lookup_widget(GTK_WIDGET(button), "entryOkragWpasowanie2ProstePPNr1");
				GtkWidget *entryPPNr2 = lookup_widget(GTK_WIDGET(button), "entryOkragWpasowanie2ProstePPNr2");
						
				dbgeod_hist_add("OkragWpasowanie2Proste", zadanie_id);
			
				dbgeod_hist_point_set(zadanie_id, "PLNr1", gtk_entry_get_text(GTK_ENTRY(entryPLNr1)), 0);
				dbgeod_hist_point_set(zadanie_id, "PLNr2", gtk_entry_get_text(GTK_ENTRY(entryPLNr2)), 0);
				dbgeod_hist_point_set(zadanie_id, "PPNr1", gtk_entry_get_text(GTK_ENTRY(entryPPNr1)), 0);
				dbgeod_hist_point_set(zadanie_id, "PPNr2", gtk_entry_get_text(GTK_ENTRY(entryPPNr2)), 0);
				
				geod_entry_get_double(GTK_WIDGET(button), "entryOkragWpasowanie2ProsteWynikR", okrR);
				dbgeod_hist_par_set(zadanie_id, "Promien", okrR, 0);
			
				dbgeod_hist_point_set(zadanie_id, "WNr", pktWynikSrodek.getNr(), 1);
				
				GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Dalsze obliczenia?"));
				if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
					g_signal_emit_by_name(buttonWyczysc, "clicked");
				gtk_widget_destroy(dialog);
			}
		}
	else
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragWpasowanie", _("Brak obliczeń lub nie podano Nr punktu"));
}


gboolean on_entryOkragWpasowanieKod_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Kod_set(widget, "sbarOkragWpasowanie");
	return FALSE;
}


gboolean on_entryOkragWpasowanieKod_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragWpasowanie");
	return FALSE;
}


gboolean on_entryOkragWpasowanie3PktPNr1_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragWpasowanie");
	return FALSE;
}


gboolean on_entryOkragWpasowanie3PktPNr1_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragWpasowanie");
	return FALSE;
}


gboolean on_entryOkragWpasowanie3PktPNr2_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragWpasowanie");
	return FALSE;
}


gboolean on_entryOkragWpasowanie3PktPNr2_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragWpasowanie");
	return FALSE;
}


gboolean on_entryOkragWpasowanie3PktPNr3_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragWpasowanie");
	return FALSE;
}


gboolean on_entryOkragWpasowanie3PktPNr3_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragWpasowanie");
	return FALSE;
}


void on_btnOkragWpasowanie3PktWynikDodaj_clicked (GtkButton *button,
	gpointer user_data)
{
	cPunkt pktWynikSrodek;
	bool zmiana = FALSE;
	int zadanie_id;
	GtkWidget *buttonWyczysc = lookup_widget(GTK_WIDGET(button), "tbtnOkragWpasowanieWyczysc");
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryOkragWpasowanie3PktWynikNr",
			"entryOkragWpasowanie3PktWynikX", "entryOkragWpasowanie3PktWynikY",
			"entryOkragWpasowanie3PktWynikKod", pktWynikSrodek))
	{
		dbgeod_point_add_config(pktWynikSrodek, &zmiana);
		if (zmiana) {
			GtkWidget *entryPkt1Nr = lookup_widget(GTK_WIDGET(button), "entryOkragWpasowanie3PktPNr1");
			GtkWidget *entryPkt2Nr = lookup_widget(GTK_WIDGET(button), "entryOkragWpasowanie3PktPNr2");
			GtkWidget *entryPkt3Nr = lookup_widget(GTK_WIDGET(button), "entryOkragWpasowanie3PktPNr3");
						
			dbgeod_hist_add("OkragWpasowanie3Punkty", zadanie_id);
			
			dbgeod_hist_point_set(zadanie_id, "Pkt1Nr", gtk_entry_get_text(GTK_ENTRY(entryPkt1Nr)), 0);
			dbgeod_hist_point_set(zadanie_id, "Pkt2Nr", gtk_entry_get_text(GTK_ENTRY(entryPkt2Nr)), 0);
			dbgeod_hist_point_set(zadanie_id, "Pkt3Nr", gtk_entry_get_text(GTK_ENTRY(entryPkt3Nr)), 0);
								
			dbgeod_hist_point_set(zadanie_id, "WNr", pktWynikSrodek.getNr(), 1);

			GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Dalsze obliczenia?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
				g_signal_emit_by_name(buttonWyczysc, "clicked");
			gtk_widget_destroy(dialog);
		}
	}
	else
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragWpasowanie", _("Brak obliczeń lub nie podano Nr punktu"));
}


/* winPrzeciecia */
void on_winPrzeciecia_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winPrzeciecia");
}


gboolean on_entryPrzecieciaProstePr1Nr1_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaProstePr1Nr1_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaProstePr1Nr2_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaProstePr1Nr2_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaProstePr2Nr1_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaProstePr2Nr1_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaProstePr2Nr2_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaProstePr2Nr2_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaKod_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Kod_set(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaKod_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPrzeciecia");
	return FALSE;
}


void on_btnPrzecieciaProsteWynikDodaj_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktWynik;
	bool zmiana = FALSE;
	int zadanie_id;
	GtkWidget *buttonWyczysc = lookup_widget(GTK_WIDGET(button), "tbtnPrzecieciaWyczysc");
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryPrzecieciaProsteWynikNr",
			"entryPrzecieciaProsteWynikX", "entryPrzecieciaProsteWynikY",
			"entryPrzecieciaProsteWynikKod", pktWynik))
	{
		dbgeod_point_add_config(pktWynik, &zmiana);
		if (zmiana) {
			GtkWidget *entryP1Nr1 = lookup_widget(GTK_WIDGET(button), "entryPrzecieciaProstePr1Nr1");
			GtkWidget *entryP1Nr2 = lookup_widget(GTK_WIDGET(button), "entryPrzecieciaProstePr1Nr2");
			GtkWidget *entryP2Nr1 = lookup_widget(GTK_WIDGET(button), "entryPrzecieciaProstePr2Nr1");
			GtkWidget *entryP2Nr2 = lookup_widget(GTK_WIDGET(button), "entryPrzecieciaProstePr2Nr2");
					
			dbgeod_hist_add("PrzeciecieProste", zadanie_id);
		
			dbgeod_hist_point_set(zadanie_id, "Pr1Nr1", gtk_entry_get_text(GTK_ENTRY(entryP1Nr1)), 0);
			dbgeod_hist_point_set(zadanie_id, "Pr1Nr2", gtk_entry_get_text(GTK_ENTRY(entryP1Nr2)), 0);
			dbgeod_hist_point_set(zadanie_id, "Pr2Nr1", gtk_entry_get_text(GTK_ENTRY(entryP2Nr1)), 0);
			dbgeod_hist_point_set(zadanie_id, "Pr2Nr2", gtk_entry_get_text(GTK_ENTRY(entryP2Nr2)), 0);
			dbgeod_hist_point_set(zadanie_id, "WNr", pktWynik.getNr(), 1);

			GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Dalsze obliczenia?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
				g_signal_emit_by_name(buttonWyczysc, "clicked");
			gtk_widget_destroy(dialog);
		}
	}
	else
		geod_sbar_set(GTK_WIDGET(button), "sbarPrzeciecia", _("Brak obliczeń lub nie podano Nr punktu"));
}


void on_tbtnPrzecieciaWykonaj_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt pktProsta1P, pktProsta1K, pktProsta2P, pktProsta2K;
	cPunkt pktSrodek1, pktSrodek2;
	cPunkt pktWynik1, pktWynik2;
	double promien1, promien2;
	int geod_error;
	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookPrzeciecia");
	GtkWidget *entryWynik1Nr = NULL;
	GtkWidget *entryWynik2Nr = NULL;
	
	switch(gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook)))
	{
	case PRZECIECIE_PROSTE:
		geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProsteWynikX");
		geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProsteWynikY");

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr1Nr1",
			"entryPrzecieciaProstePr1X1", "entryPrzecieciaProstePr1Y1", NULL, pktProsta1P));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y pierwszego punktu prostej 1"));
			break;
		}

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr1Nr2",
			"entryPrzecieciaProstePr1X2", "entryPrzecieciaProstePr1Y2", NULL, pktProsta1K));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y drugiego punktu prostej 1"));
			break;
		}

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr2Nr1",
			"entryPrzecieciaProstePr2X1", "entryPrzecieciaProstePr2Y1", NULL, pktProsta2P));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y pierwszego punktu prostej 2"));
			break;
		}

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr1Nr1",
			"entryPrzecieciaProstePr2X2", "entryPrzecieciaProstePr2Y2", NULL, pktProsta2K));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y drugiego punktu prostej 2"));
			break;
		}
		
		geod_error = przeciecie_prostych(pktProsta1P, pktProsta1K, pktProsta2P, pktProsta2K, pktWynik1);
		if (geod_error == GEODERR_OK)
			geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaProsteWynikX",
				"entryPrzecieciaProsteWynikY", pktWynik1);
		else
			geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarPrzeciecia", geod_error);
	break;

	case PRZECIECIE_OKRAG_PROSTA:
		entryWynik1Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikNr1");
		entryWynik2Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikNr2");

		geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikX1");
		geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikY1");
		geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikX2");
		geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikY2");

		g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
		g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
		gtk_entry_set_editable(GTK_ENTRY(entryWynik1Nr), TRUE);
		gtk_entry_set_editable(GTK_ENTRY(entryWynik2Nr), TRUE);

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaOkrNr",
			"entryPrzecieciaOkragProstaOkrX", "entryPrzecieciaOkragProstaOkrY", NULL, pktSrodek1));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y środka okręgu"));
			break;
		}

		if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaOkrR", promien1));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać promień okręgu"));
			break;
		}

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaPrNr1",
			"entryPrzecieciaOkragProstaPrX1", "entryPrzecieciaOkragProstaPrY1", NULL, pktProsta1P));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y pierwszego punktu prostej"));
			break;
		}

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaPrNr2",
			"entryPrzecieciaOkragProstaPrX2", "entryPrzecieciaOkragProstaPrY2", NULL, pktProsta1K));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y drugiego punktu prostej"));
			break;
		}

		geod_error = przeciecie_okrag_prosta(pktSrodek1, promien1, pktProsta1P, pktProsta1K, pktWynik1, pktWynik2);
		switch (geod_error)
		{
		case 0:
			geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikX1",
				"entryPrzecieciaOkragProstaWynikY1", pktWynik1);
			geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikX2",
				"entryPrzecieciaOkragProstaWynikY2", pktWynik2);
			g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
			g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
			break;
		
		case 1:
			geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikX1",
				"entryPrzecieciaOkragProstaWynikY1", pktWynik1);
			g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
			g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_NIE_DODAWAJ));
			break;
		
		default:
			geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarPrzeciecia", geod_error);
			g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_ERROR));
			g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_ERROR));
	 	}
	break;

	case PRZECIECIE_OKREGI:
		entryWynik1Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikNr1");
		entryWynik2Nr = lookup_widget(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikNr2");

		geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikX1");
		geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikY1");
		geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikX2");
		geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikY2");

		g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
		g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
		gtk_entry_set_editable(GTK_ENTRY(entryWynik1Nr), TRUE);
		gtk_entry_set_editable(GTK_ENTRY(entryWynik2Nr), TRUE);

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr1Nr",
			"entryPrzecieciaOkregiOkr1X", "entryPrzecieciaOkregiOkr1Y", NULL, pktSrodek1));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y środka pierwszego okręgu"));
			break;
		}

		if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr1R", promien1));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać promień pierwszego okręgu"));
			break;
		}

		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr2Nr",
			"entryPrzecieciaOkregiOkr2X", "entryPrzecieciaOkregiOkr2Y", NULL, pktSrodek2));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y środka drugiego okręgu"));
			break;
		}

		if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr2R", promien2));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać promień drugiego okręgu"));
			break;
		}

		geod_error = wciecie_liniowe(pktSrodek1, pktSrodek2, promien1, promien2, pktWynik1);
		if (geod_error == GEODERR_OK) {
			if (promien1+promien2 == dlugosc(pktSrodek1, pktSrodek2)) {
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikX1",
					"entryPrzecieciaOkregiWynikY1", pktWynik1);
				g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
				g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_NIE_DODAWAJ));
			}
			else {
				wciecie_liniowe(pktSrodek2, pktSrodek1, promien2, promien1, pktWynik2);
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikX1",
					"entryPrzecieciaOkregiWynikY1", pktWynik1);
				geod_entry_set_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikX2",
					"entryPrzecieciaOkregiWynikY2", pktWynik2);
				g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
				g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_DODAJ));
			}
		}
		else {
			g_object_set_data(G_OBJECT(entryWynik1Nr), "status_dodaj", GINT_TO_POINTER(PKT_ERROR));
			g_object_set_data(G_OBJECT(entryWynik2Nr), "status_dodaj", GINT_TO_POINTER(PKT_ERROR));
			geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarPrzeciecia", geod_error);
		}
	break;
	}
}


void on_tbtnPrzecieciaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *focus = NULL;
	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookPrzeciecia");
	GtkWidget *entryWynikNr1 = NULL;
	GtkWidget *entryWynikNr2 = NULL;
	
	switch(gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook)))
	{
		case PRZECIECIE_PROSTE:
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr1Nr1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr1X1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr1Y1");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr1Nr2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr1X2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr1Y2");	

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr2Nr1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr2X1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr2Y1");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr2Nr2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr2X2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr2Y2");	

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProsteWynikNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProsteWynikX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProsteWynikY");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaProsteWynikKod");
			focus = lookup_widget(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr1Nr1");
			gtk_widget_grab_focus(GTK_WIDGET(focus));
		break;

		case PRZECIECIE_OKRAG_PROSTA:
			entryWynikNr1 = lookup_widget(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikNr1");
			entryWynikNr2 = lookup_widget(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikNr2");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaOkrNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaOkrX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaOkrY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaOkrR");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaPrNr1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaPrX1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaPrY1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaPrNr2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaPrX2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaPrY2");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikNr1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikX1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikY1");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikKod1");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikNr2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikX2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikY2");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikKod2");

			g_object_set_data(G_OBJECT(entryWynikNr1), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			g_object_set_data(G_OBJECT(entryWynikNr2), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			gtk_entry_set_editable(GTK_ENTRY(entryWynikNr1), TRUE);
			gtk_entry_set_editable(GTK_ENTRY(entryWynikNr2), TRUE);

			focus = lookup_widget(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaOkrNr");
			gtk_widget_grab_focus(GTK_WIDGET(focus));
		break;

		case PRZECIECIE_OKREGI:
			entryWynikNr1 = lookup_widget(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikNr1");
			entryWynikNr2 = lookup_widget(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikNr2");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr1Nr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr1X");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr1Y");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr1R");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr2Nr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr2X");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr2Y");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr2R");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikNr1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikX1");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikY1");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikKod1");

			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikNr2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikX2");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikY2");	
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikKod2");

			g_object_set_data(G_OBJECT(entryWynikNr1), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			g_object_set_data(G_OBJECT(entryWynikNr2), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));
			gtk_entry_set_editable(GTK_ENTRY(entryWynikNr1), TRUE);
			gtk_entry_set_editable(GTK_ENTRY(entryWynikNr2), TRUE);

			focus = lookup_widget(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr1Nr");
			gtk_widget_grab_focus(GTK_WIDGET(focus));
		break;
	}
}

void on_tbtnPrzecieciaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GSList *lpunkty = NULL;
	char *txt_promien = NULL;
	double promien;
	cPunkt *punkt = NULL;
	bool brak_danych = FALSE;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	GtkWidget *dialogRaport = create_dialogRaport();
	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");
	gtk_widget_show(dialogRaport);

	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookPrzeciecia");
	
	switch(gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook)))
	{
	case PRZECIECIE_PROSTE:
		geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nPrzecięcie prostych"));

		// pierwsza prosta
		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr1Nr1",
			"entryPrzecieciaProstePr1X1", "entryPrzecieciaProstePr1Y1", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
		else {
			geod_raport_error(textview, _("Dla pierwszego punktu prostej 1 nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr1Nr2",
			"entryPrzecieciaProstePr1X2", "entryPrzecieciaProstePr1Y2", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
		else {
			geod_raport_error(textview, _("Dla drugiego punktu prostej 1 nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		geod_raport_table_pkt(textview, lpunkty, FALSE, _("Pierwsza prosta"));
		g_slist_free(lpunkty);
		lpunkty = NULL;

		// druga prosta
		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr2Nr1",
			"entryPrzecieciaProstePr2X1", "entryPrzecieciaProstePr2Y1", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
		else {
			geod_raport_error(textview, _("Dla pierwszego punktu prostej 2 nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr2Nr2",
			"entryPrzecieciaProstePr2X2", "entryPrzecieciaProstePr2Y2", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
		else {
			geod_raport_error(textview, _("Dla drugiego punktu prostej 2 nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		geod_raport_table_pkt(textview, lpunkty, FALSE, _("Druga prosta"));
		g_slist_free(lpunkty);
		lpunkty = NULL;

		punkt = new cPunkt;
		if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
			"entryPrzecieciaProsteWynikNr", "entryPrzecieciaProsteWynikX",
			"entryPrzecieciaProsteWynikY", "entryPrzecieciaProsteWynikKod", *punkt) < 1) {
			lpunkty = g_slist_append(lpunkty, punkt);
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkt przecięcia prostych"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else {
			geod_raport_error(textview, _("Brak obliczeń lub brak rozwiązania"), TRUE);
			delete punkt;
		}
	break;

	case PRZECIECIE_OKRAG_PROSTA:
		geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nPrzecięcie okręgu z prostą"));

		// okrag
		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaOkrNr",
			"entryPrzecieciaOkragProstaOkrX", "entryPrzecieciaOkragProstaOkrY", NULL, *punkt)) {
			lpunkty = g_slist_append(lpunkty, punkt);
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Środek okręgu"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else {
			geod_raport_error(textview, _("Dla środka okręgu nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaOkrR", promien)) {
			txt_promien = g_strdup_printf("%.*f", dok_xy_int, promien);
			geod_raport_line(textview, _("\nPromień okręgu"), txt_promien, TRUE);
			g_free(txt_promien);
		}
		else
			geod_raport_error(textview, _("Nie podano promienia okręgu"));

		// prosta
		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaPrNr1",
			"entryPrzecieciaOkragProstaPrX1", "entryPrzecieciaOkragProstaPrY1", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
		else {
			geod_raport_error(textview, _("Dla pierwszego punktu prostej nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaPrNr2",
			"entryPrzecieciaOkragProstaPrX2", "entryPrzecieciaOkragProstaPrY2", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
		else {
			geod_raport_error(textview, _("Dla drugiego punktu prostej nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		geod_raport_table_pkt(textview, lpunkty, FALSE, _("Prosta"));
		g_slist_free(lpunkty);
		lpunkty = NULL;

		// wynik
		brak_danych = FALSE;
		punkt = new cPunkt;
		if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
			"entryPrzecieciaOkragProstaWynikNr1", "entryPrzecieciaOkragProstaWynikX1",
			"entryPrzecieciaOkragProstaWynikY1", "entryPrzecieciaOkragProstaWynikKod1", *punkt) < 1) {
			lpunkty = g_slist_append(lpunkty, punkt);
		}
		else {
			brak_danych = TRUE;
			delete punkt;
		}

		punkt = new cPunkt;
		if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
			"entryPrzecieciaOkragProstaWynikNr2", "entryPrzecieciaOkragProstaWynikX2",
			"entryPrzecieciaOkragProstaWynikY2", "entryPrzecieciaOkragProstaWynikKod2", *punkt) < 1) {
			lpunkty = g_slist_append(lpunkty, punkt);
		}
		else {
			delete punkt;
		}

		if (brak_danych)
			geod_raport_error(textview, _("Brak obliczeń lub brak rozwiązania"), TRUE);
		else
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty przecięcia okręgu z prostą"));

		g_slist_free(lpunkty);
		lpunkty = NULL;
	break;

	case PRZECIECIE_OKREGI:
		geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nPrzecięcie okręgu z prostą"));

		// pierwszy okrag
		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr1Nr",
			"entryPrzecieciaOkregiOkr1X", "entryPrzecieciaOkregiOkr1Y", NULL, *punkt)) {
			lpunkty = g_slist_append(lpunkty, punkt);
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Środek pierwszego okręgu"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else {
			geod_raport_error(textview, _("Dla środka pierwszego okręgu nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr1R", promien)) {
			txt_promien = g_strdup_printf("%.*f", dok_xy_int, promien);
			geod_raport_line(textview, _("\nPromień pierwszego okręgu"), txt_promien, FALSE);
			g_free(txt_promien);
		}
		else
			geod_raport_error(textview, _("Nie podano promienia pierwszego okręgu"));

		// drugi okrag
		punkt = new cPunkt;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr2Nr",
			"entryPrzecieciaOkregiOkr2X", "entryPrzecieciaOkregiOkr2Y", NULL, *punkt)) {
			lpunkty = g_slist_append(lpunkty, punkt);
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Środek drugiego okręgu"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else {
			geod_raport_error(textview, _("Dla środka drugiego okręgu nie podano wszystkich wymaganych danych"));
			delete punkt;
		}

		if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr2R", promien)) {
			txt_promien = g_strdup_printf("%.*f", dok_xy_int, promien);
			geod_raport_line(textview, _("\nPromień drugiego okręgu"), txt_promien, FALSE);
			g_free(txt_promien);
		}
		else
			geod_raport_error(textview, _("Nie podano promienia drugiego okręgu"));

		brak_danych = FALSE;
		punkt = new cPunkt;
		if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
			"entryPrzecieciaOkregiWynikNr1", "entryPrzecieciaOkregiWynikX1",
			"entryPrzecieciaOkregiWynikY1", "entryPrzecieciaOkregiWynikKod1", *punkt) < 1) {
			lpunkty = g_slist_append(lpunkty, punkt);
		}
		else {
			brak_danych = TRUE;
			delete punkt;
		}

		punkt = new cPunkt;
		if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton),
			"entryPrzecieciaOkregiWynikNr2", "entryPrzecieciaOkregiWynikX2",
			"entryPrzecieciaOkregiWynikY2", "entryPrzecieciaOkregiWynikKod2", *punkt) < 1) {
			lpunkty = g_slist_append(lpunkty, punkt);
		}
		else {
			delete punkt;
		}
		
		if (brak_danych)
			geod_raport_error(textview, _("Brak obliczeń lub brak rozwiązania"), TRUE);
		else
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty przecięcia okręgów"));
		
		g_slist_free(lpunkty);
		lpunkty = NULL;
	break;
	}
	geod_raport_stopka(textview);
}


void on_tbtnPrzecieciaWidok_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt *pktDane = NULL;
	GdkPoint *pktWidok = NULL;
	double promien1, promien2;
	guint npoints;
	double skala;

	GtkWidget *winWidok = create_winWidok();
	gtk_widget_show(winWidok);
	GtkWidget *darea = lookup_widget(GTK_WIDGET(winWidok), "drawingAreaWidok");

	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookPrzeciecia");
	
	switch(gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook)))
	{
		case PRZECIECIE_PROSTE:
			npoints = 5;
			pktDane = new cPunkt[npoints];
			pktWidok = new GdkPoint[npoints];

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr1Nr1",
				"entryPrzecieciaProstePr1X1", "entryPrzecieciaProstePr1Y1", NULL, pktDane[0]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y pierwszego punktu prostej 1"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr1Nr2",
				"entryPrzecieciaProstePr1X2", "entryPrzecieciaProstePr1Y2", NULL, pktDane[1]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y drugiego punktu prostej 1"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr2Nr1",
				"entryPrzecieciaProstePr2X1", "entryPrzecieciaProstePr2Y1", NULL, pktDane[2]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y pierwszego punktu prostej 2"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaProstePr2Nr2",
				"entryPrzecieciaProstePr2X2", "entryPrzecieciaProstePr2Y2", NULL, pktDane[3]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y drugiego punktu prostej 2"));
				break;
			}

			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaProsteWynikNr",
				"entryPrzecieciaProsteWynikX", "entryPrzecieciaProsteWynikY", NULL, pktDane[4]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Brak obliczeń"));
				break;
			}

			geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, npoints);

			geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[1], LINE_BAZA);
			geod_draw_line(GTK_WIDGET(darea), pktWidok[2], pktWidok[3], LINE_BAZA);

			geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[1].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[2], pktDane[2].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[3], pktDane[3].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[4], pktDane[4].getNr(), PKT_WYNIK);
			
			gtk_widget_queue_draw(darea);
		break;

		case PRZECIECIE_OKRAG_PROSTA:
			npoints = 7;
			pktDane = new cPunkt[npoints];
			pktWidok = new GdkPoint[npoints];

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaPrNr1",
				"entryPrzecieciaOkragProstaPrX1", "entryPrzecieciaOkragProstaPrY1", NULL, pktDane[0]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y pierwszego punktu prostej"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaPrNr2",
				"entryPrzecieciaOkragProstaPrX2", "entryPrzecieciaOkragProstaPrY2", NULL, pktDane[1]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y drugiego punktu prostej"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaOkrNr",
				"entryPrzecieciaOkragProstaOkrX", "entryPrzecieciaOkragProstaOkrY", NULL, pktDane[2]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y środka okręgu"));
				break;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaOkrR", promien1));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać promień okręgu"));
				break;
			}

			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikNr1",
				"entryPrzecieciaOkragProstaWynikX1", "entryPrzecieciaOkragProstaWynikY1", NULL, pktDane[3]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Brak obliczeń"));
				break;
			}

			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkragProstaWynikNr2",
				"entryPrzecieciaOkragProstaWynikX2", "entryPrzecieciaOkragProstaWynikY2", NULL, pktDane[4]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Brak obliczeń"));
				break;
			}

			// maksymalna wartosc x, y
			pktDane[5].setX(pktDane[2].getX()-promien1);
			pktDane[5].setY(pktDane[2].getY()-promien1);
			pktDane[6].setX(pktDane[2].getX()+promien1);
			pktDane[6].setY(pktDane[2].getY()+promien1);

			skala = geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, npoints);

			geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[1], LINE_BAZA);
			geod_draw_circle(GTK_WIDGET(darea), pktWidok[2], (int)round(promien1*skala));

			geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[1].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[2], pktDane[2].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[3], pktDane[3].getNr(), PKT_WYNIK);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[4], pktDane[4].getNr(), PKT_WYNIK);
			
			gtk_widget_queue_draw(darea);
		break;

		case PRZECIECIE_OKREGI:
			npoints = 8;
			pktDane = new cPunkt[npoints];
			pktWidok = new GdkPoint[npoints];

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr1Nr",
				"entryPrzecieciaOkregiOkr1X", "entryPrzecieciaOkregiOkr1Y", NULL, pktDane[0]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y środka pierwszego okręgu"));
				break;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr1R", promien1));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać promień pierwszego okręgu"));
				break;
			}

			if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr2Nr",
				"entryPrzecieciaOkregiOkr2X", "entryPrzecieciaOkregiOkr2Y", NULL, pktDane[3]));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać Nr, X, Y środka drugiego okręgu"));
				break;
			}

			if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiOkr2R", promien2));
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Musisz podać promień drugiego okręgu"));
				break;
			}

			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikNr1",
				"entryPrzecieciaOkregiWynikX1", "entryPrzecieciaOkregiWynikY1", NULL, pktDane[6]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Brak obliczeń"));
				break;
			}

			if (geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPrzecieciaOkregiWynikNr2",
				"entryPrzecieciaOkregiWynikX2", "entryPrzecieciaOkregiWynikY2", NULL, pktDane[7]) < 1);
			else {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPrzeciecia", _("Brak obliczeń"));
				break;
			}

			// maksymalna wartosc x, y
			pktDane[1].setX(pktDane[0].getX()-promien1);
			pktDane[1].setY(pktDane[0].getY()-promien1);
			pktDane[2].setX(pktDane[0].getX()+promien1);
			pktDane[2].setY(pktDane[0].getY()+promien1);
			pktDane[4].setX(pktDane[3].getX()-promien2);
			pktDane[4].setY(pktDane[3].getY()-promien2);
			pktDane[5].setX(pktDane[3].getX()+promien2);
			pktDane[5].setY(pktDane[3].getY()+promien2);

			skala = geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, npoints);

			geod_draw_circle(GTK_WIDGET(darea), pktWidok[0], (int)round(promien1*skala));
			geod_draw_circle(GTK_WIDGET(darea), pktWidok[3], (int)round(promien2*skala));

			geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[3], pktDane[3].getNr(), PKT_OSNOWA);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[6], pktDane[6].getNr(), PKT_WYNIK);
			geod_draw_point(GTK_WIDGET(darea), pktWidok[7], pktDane[7].getNr(), PKT_WYNIK);
			
			gtk_widget_queue_draw(darea);
		break;
	delete [] pktDane;
	delete [] pktWidok;
	}
}


void on_tbtnPrzecieciaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winPrzeciecia");
	gtk_widget_destroy(window);
}


gboolean on_entryPrzecieciaOkragProstaOkrNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaOkragProstaOkrNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaOkragProstaPrNr1_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaOkragProstaPrNr1_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaOkragProstaPrNr2_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaOkragProstaPrNr2_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPrzeciecia");
	return FALSE;
}


void on_btnPrzecieciaOkragProstaWynikDodaj_clicked (GtkButton *button,
	gpointer user_data)
{
	cPunkt pktWynik1, pktWynik2;
	int zadanie_id;
	double okrR;
	bool zmiana1 = FALSE, zmiana2 = FALSE;
	int s_dodaj1, s_dodaj2;
	GtkWidget *entryWynikNr1 = lookup_widget(GTK_WIDGET(button), "entryPrzecieciaOkragProstaWynikNr1");
	GtkWidget *entryWynikNr2 = lookup_widget(GTK_WIDGET(button), "entryPrzecieciaOkragProstaWynikNr2");
	GtkWidget *buttonWyczysc = lookup_widget(GTK_WIDGET(button), "tbtnPrzecieciaWyczysc");

	// gdy nie ustawiono statusu - ustaw brak obliczen
	if (g_object_get_data(G_OBJECT(entryWynikNr1), "status_dodaj") == NULL)
		g_object_set_data(G_OBJECT(entryWynikNr1), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));

	if (g_object_get_data(G_OBJECT(entryWynikNr2), "status_dodaj") == NULL)
		g_object_set_data(G_OBJECT(entryWynikNr2), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));

	s_dodaj1 = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entryWynikNr1), "status_dodaj"));
	s_dodaj2 = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entryWynikNr2), "status_dodaj"));
	
	// gdy brak obliczen przerwij
	if (s_dodaj1 == PKT_BRAK_DANYCH && s_dodaj2 == PKT_BRAK_DANYCH) {
		geod_sbar_set(GTK_WIDGET(button), "sbarPrzeciecia", _("Brak obliczeń"));
		return;
	}
	if (s_dodaj1 == PKT_ERROR || s_dodaj2 == PKT_ERROR) {
		geod_sbar_set(GTK_WIDGET(button), "sbarPrzeciecia", _("Brak rozwiązania"));
		return;
	}

	// kontrola wymaganych danych
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryPrzecieciaOkragProstaWynikNr1",
			"entryPrzecieciaOkragProstaWynikX1", "entryPrzecieciaOkragProstaWynikY1",
			"entryPrzecieciaOkragProstaWynikKod1", pktWynik1) &&
		(!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryPrzecieciaOkragProstaWynikNr2",
			"entryPrzecieciaOkragProstaWynikX2", "entryPrzecieciaOkragProstaWynikY2",
			"entryPrzecieciaOkragProstaWynikKod2", pktWynik2) || s_dodaj2 == PKT_NIE_DODAWAJ))
	{
		if (s_dodaj1 == PKT_DODAJ) {
			dbgeod_point_add_config(pktWynik1, &zmiana1);
			if (zmiana1) {
				g_object_set_data(G_OBJECT(entryWynikNr1), "status_dodaj", GINT_TO_POINTER(PKT_DODANY));
				gtk_entry_set_editable(GTK_ENTRY(entryWynikNr1), FALSE);
			}
		}
		else
			zmiana1 = TRUE;

		if (s_dodaj2 == PKT_DODAJ) {
			dbgeod_point_add_config(pktWynik2, &zmiana2);
			if (zmiana2) {
				g_object_set_data(G_OBJECT(entryWynikNr2), "status_dodaj", GINT_TO_POINTER(PKT_DODANY));
				gtk_entry_set_editable(GTK_ENTRY(entryWynikNr2), FALSE);
			}
		}
		else
			zmiana2 = TRUE;
		
		if (zmiana1 && zmiana2) {
			GtkWidget *entryPrNr1 = lookup_widget(GTK_WIDGET(button), "entryPrzecieciaOkragProstaPrNr1");
			GtkWidget *entryPrNr2 = lookup_widget(GTK_WIDGET(button), "entryPrzecieciaOkragProstaPrNr2");
			GtkWidget *entryOkrNr = lookup_widget(GTK_WIDGET(button), "entryPrzecieciaOkragProstaOkrNr");
					
			dbgeod_hist_add("PrzeciecieOkragProsta", zadanie_id);
		
			dbgeod_hist_point_set(zadanie_id, "PrNr1", gtk_entry_get_text(GTK_ENTRY(entryPrNr1)), 0);
			dbgeod_hist_point_set(zadanie_id, "PrNr2", gtk_entry_get_text(GTK_ENTRY(entryPrNr2)), 0);
			dbgeod_hist_point_set(zadanie_id, "OkrNr", gtk_entry_get_text(GTK_ENTRY(entryOkrNr)), 0);
			
			geod_entry_get_double(GTK_WIDGET(button), "entryPrzecieciaOkragProstaOkrR", okrR);
			dbgeod_hist_par_set(zadanie_id, "Promien", okrR, 0);
		
			dbgeod_hist_point_set(zadanie_id, "W1Nr", pktWynik1.getNr(), 1);
			dbgeod_hist_point_set(zadanie_id, "W2Nr", pktWynik2.getNr(), 1);

			GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Dalsze obliczenia?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
				g_signal_emit_by_name(buttonWyczysc, "clicked");
			gtk_widget_destroy(dialog);
		}
	}
	else
		geod_sbar_set(GTK_WIDGET(button), "sbarPrzeciecia", _("Nie podano Nr punktów"));
}


gboolean on_entryPrzecieciaOkregiOkr1Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaOkregiOkr1Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPrzeciecia");
	return FALSE;
}


gboolean on_entryPrzecieciaOkregiOkr2Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarPrzeciecia");
	return FALSE;
}

gboolean on_entryPrzecieciaOkregiOkr2Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPrzeciecia");
	return FALSE;
}


void on_btnPrzecieciaOkregiWynikDodaj_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktWynik1, pktWynik2;
	bool zmiana1 = FALSE, zmiana2 = FALSE;
	int s_dodaj1, s_dodaj2;
	int zadanie_id;
	double okrR;
	GtkWidget *entryWynikNr1 = lookup_widget(GTK_WIDGET(button), "entryPrzecieciaOkregiWynikNr1");
	GtkWidget *entryWynikNr2 = lookup_widget(GTK_WIDGET(button), "entryPrzecieciaOkregiWynikNr2");
	GtkWidget *buttonWyczysc = lookup_widget(GTK_WIDGET(button), "tbtnPrzecieciaWyczysc");

	// gdy nie ustawiono statusu - ustaw brak obliczen
	if (g_object_get_data(G_OBJECT(entryWynikNr1), "status_dodaj") == NULL)
		g_object_set_data(G_OBJECT(entryWynikNr1), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));

	if (g_object_get_data(G_OBJECT(entryWynikNr2), "status_dodaj") == NULL)
		g_object_set_data(G_OBJECT(entryWynikNr2), "status_dodaj", GINT_TO_POINTER(PKT_BRAK_DANYCH));

	s_dodaj1 = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entryWynikNr1), "status_dodaj"));
	s_dodaj2 = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entryWynikNr2), "status_dodaj"));
	
	// gdy brak obliczen przerwij
	if (s_dodaj1 == PKT_BRAK_DANYCH && s_dodaj2 == PKT_BRAK_DANYCH) {
		geod_sbar_set(GTK_WIDGET(button), "sbarPrzeciecia", _("Brak obliczeń"));
		return;
	}
	if (s_dodaj1 == PKT_ERROR || s_dodaj2 == PKT_ERROR) {
		geod_sbar_set(GTK_WIDGET(button), "sbarPrzeciecia", _("Brak rozwiązania"));
		return;
	}

	// kontrola wymaganych danych
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryPrzecieciaOkregiWynikNr1",
			"entryPrzecieciaOkregiWynikX1", "entryPrzecieciaOkregiWynikY1",
			"entryPrzecieciaOkregiWynikKod1", pktWynik1) &&
		(!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryPrzecieciaOkregiWynikNr2",
			"entryPrzecieciaOkregiWynikX2", "entryPrzecieciaOkregiWynikY2",
			"entryPrzecieciaOkregiWynikKod2", pktWynik2) || s_dodaj2 == PKT_NIE_DODAWAJ))
	{
		if (s_dodaj1 == PKT_DODAJ) {
			dbgeod_point_add_config(pktWynik1, &zmiana1);
			if (zmiana1) {
				g_object_set_data(G_OBJECT(entryWynikNr1), "status_dodaj", GINT_TO_POINTER(PKT_DODANY));
				gtk_entry_set_editable(GTK_ENTRY(entryWynikNr1), FALSE);
			}
		}
		else
			zmiana1 = TRUE;

		if (s_dodaj2 == PKT_DODAJ) {
			dbgeod_point_add_config(pktWynik2, &zmiana2);
			if (zmiana2) {
				g_object_set_data(G_OBJECT(entryWynikNr2), "status_dodaj", GINT_TO_POINTER(PKT_DODANY));
				gtk_entry_set_editable(GTK_ENTRY(entryWynikNr2), FALSE);
			}
		}
		else
			zmiana2 = TRUE;
		
		if (zmiana1 && zmiana2) {
			GtkWidget *entryOkr1Nr = lookup_widget(GTK_WIDGET(button), "entryPrzecieciaOkregiOkr1Nr");
			GtkWidget *entryOkr2Nr = lookup_widget(GTK_WIDGET(button), "entryPrzecieciaOkregiOkr2Nr");
					
			dbgeod_hist_add("PrzeciecieOkregi", zadanie_id);
		
			dbgeod_hist_point_set(zadanie_id, "Okr1Nr", gtk_entry_get_text(GTK_ENTRY(entryOkr1Nr)), 0);
			dbgeod_hist_point_set(zadanie_id, "Okr2Nr", gtk_entry_get_text(GTK_ENTRY(entryOkr2Nr)), 0);
			
			geod_entry_get_double(GTK_WIDGET(button), "entryPrzecieciaOkregiOkr1R", okrR);
			dbgeod_hist_par_set(zadanie_id, "PromienOkr1", okrR, 0);
			geod_entry_get_double(GTK_WIDGET(button), "entryPrzecieciaOkregiOkr2R", okrR);
			dbgeod_hist_par_set(zadanie_id, "PromienOkr2", okrR, 0);
		
			dbgeod_hist_point_set(zadanie_id, "W1Nr", pktWynik1.getNr(), 1);
			dbgeod_hist_point_set(zadanie_id, "W2Nr", pktWynik2.getNr(), 1);

			GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Dalsze obliczenia?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
				g_signal_emit_by_name(buttonWyczysc, "clicked");
			gtk_widget_destroy(dialog);
		}
	}
	else
		geod_sbar_set(GTK_WIDGET(button), "sbarPrzeciecia", _("Nie podano Nr punktów"));
}


/* winBiegunowe */
void on_winBiegunowe_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winBiegunowe");
}


void on_winBiegunowe_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeBiegunowe");
	GtkTreeStore *model;
	
	model=gtk_tree_store_new(N_BIE_COLUMNS, G_TYPE_STRING, \
	G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, \
	G_TYPE_STRING, G_TYPE_STRING, G_TYPE_DOUBLE);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), \
	    GTK_TREE_MODEL(model));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;
	
	// kolumna Nr punkt
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Nr"), renderer, "text", COL_BIE_NR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// kolumna wsp. X
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("X"), renderer, "text", COL_BIE_X, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);

	// kolumna wsp. Y
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Y"), renderer, "text", COL_BIE_Y, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);

	// kolumna Kierunek
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Kierunek"), renderer, "text", COL_BIE_KIER, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);

	// kolumna Odleglosc
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Odległość"), renderer, "text", COL_BIE_ODL, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);
}


void on_tbtnBiegunoweOblicz_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt *pktTyczony = NULL;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeBiegunowe");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	guint l_wierszy = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	guint i;
	char *pkt_nr = NULL;
	double pkt_x, pkt_y;
	double nawiazanieKier, azymutTyczony, azymutNawiazanie;
	double tyczonyKier, tyczonyOdl;
	char *txtTyczonyOdl = NULL;
	char *txtTyczonyKier = NULL;
	cPunkt pktStanowisko, pktNawiazanie;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
			
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazanieStNr",
		"entryBiegunoweNawiazanieStX", "entryBiegunoweNawiazanieStY", NULL, pktStanowisko));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarBiegunowe", _("Musisz podać Nr, X, Y stanowiska"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazaniePktNNr",
		"entryBiegunoweNawiazaniePktNX", "entryBiegunoweNawiazaniePktNY", NULL, pktNawiazanie))
		azymut(pktStanowisko, pktNawiazanie, azymutNawiazanie);
	else
		azymutNawiazanie = 0.0;

	if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazanieKier",
		nawiazanieKier));
	else
		nawiazanieKier = 0.0;
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (i = 0; i < l_wierszy; i++)
	{
		pktTyczony = new cPunkt;
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_BIE_NR, &pkt_nr,
		COL_BIE_X_DOUBLE, &pkt_x, COL_BIE_Y_DOUBLE, &pkt_y, -1);
		pktTyczony->setNr(pkt_nr);
		pktTyczony->setXY(pkt_x, pkt_y);

		azymut(pktStanowisko, *pktTyczony, azymutTyczony);
		tyczonyOdl = dlugosc(pktStanowisko, *pktTyczony);
		tyczonyKier = norm_kat(azymutTyczony-azymutNawiazanie+nawiazanieKier);
		txtTyczonyOdl = g_strdup_printf("%.*f", dok_xy_int, tyczonyOdl);
		txtTyczonyKier = geod_kat_from_rad(tyczonyKier);
 	
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
			COL_BIE_KIER, txtTyczonyKier, COL_BIE_ODL, txtTyczonyOdl,
			COL_BIE_ODL_DOUBLE, tyczonyOdl, -1);

		g_free(pkt_nr);
		g_free(txtTyczonyOdl);
		g_free(txtTyczonyKier);
		delete pktTyczony;
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
}


void on_tbtnBiegunoweWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *focus = lookup_widget(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazanieStNr");
	// czyszczenie listy
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeBiegunowe");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	gtk_tree_store_clear(GTK_TREE_STORE(model));

	// czyszczenie wszystkich pol
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazanieStNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazanieStX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazanieStY");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazaniePktNNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazaniePktNX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazaniePktNY");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazanieKier");

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryBiegunowePktTyczonyNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryBiegunowePktTyczonyX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryBiegunowePktTyczonyY");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


void on_tbtnBiegunoweUsun_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeBiegunowe");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreePath *tpath;
	GtkTreeIter iter;
	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarBiegunowe", _("Zaznacz najpierw pozycję w tabeli"));
	gtk_tree_path_free(tpath);
}


void on_tbtnBiegunoweRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	const guint NCOLS = 5;
	GSList *lpunkty = NULL;
	cPunkt *punkt = NULL;
	double kierunek;
	char *txt_kier = NULL;
	guint i;

	GtkWidget *dialogRaport = create_dialogRaport();
	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");
	gtk_widget_show(dialogRaport);

	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeBiegunowe");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	const guint NROWS = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	
	guint colsize[NCOLS] = { 10, 15, 15, 16, 12 };
	char **tab_cell = new char*[(NROWS+1)*NCOLS];

	geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\ntyczenie - miary biegunowe"));

	// punkty nawiazania
	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazanieStNr",
		"entryBiegunoweNawiazanieStX", "entryBiegunoweNawiazanieStY", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		geod_raport_error(textview, _("Dla stanowiska nie podano wszystkich wymaganych danych"));
		delete punkt;
	}
	
	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazaniePktNNr",
		"entryBiegunoweNawiazaniePktNX", "entryBiegunoweNawiazaniePktNY", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
	else
		delete punkt;

	geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty nawiązania"));
	g_slist_free(lpunkty);
	lpunkty = NULL;
	
	// kierunek nawiazania
	if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazanieKier", kierunek))
		txt_kier = geod_kat_from_rad(kierunek);
	else
		txt_kier = geod_kat_from_rad(0.0);

	geod_raport_line(textview, _("Kierunek nazwiązania"), txt_kier, FALSE);
	g_free(txt_kier);

	// mierzone punkty
	tab_cell[0] = g_strdup(_("Nr"));
	tab_cell[1] = g_strdup(_("X"));
	tab_cell[2] = g_strdup(_("Y"));
	tab_cell[3] = g_strdup(_("Kierunek"));
	tab_cell[4] = g_strdup(_("Odległość"));

	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);	
	for (i=1; i<=NROWS; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_BIE_NR, &tab_cell[i*NCOLS+0],
			COL_BIE_X, &tab_cell[i*NCOLS+1],
			COL_BIE_Y, &tab_cell[i*NCOLS+2],
			COL_BIE_KIER, &tab_cell[i*NCOLS+3],
			COL_BIE_ODL, &tab_cell[i*NCOLS+4], -1);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	
	geod_raport_table(textview, tab_cell, NCOLS, NROWS, colsize, _("\nPunkty tyczone"));
	geod_raport_stopka(textview);

	for (i=0; i<(NROWS+1)*NCOLS; i++)
		g_free(tab_cell[i]);
}


void on_tbtnBiegunoweWidok_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	int nawiazanie = 1;
	cPunkt *pktDane = NULL;
	GdkPoint *pktWidok = NULL;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeBiegunowe");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	guint l_wierszy = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	guint i;
	char *pkt_nr = NULL;
	double pkt_x, pkt_y;

	GtkWidget *winWidok = create_winWidok();
	gtk_widget_show(winWidok);
	GtkWidget *darea = lookup_widget(GTK_WIDGET(winWidok), "drawingAreaWidok");
	
	pktDane = new cPunkt[l_wierszy+2];
	pktWidok = new GdkPoint[l_wierszy+3];
	
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazanieStNr",
		"entryBiegunoweNawiazanieStX", "entryBiegunoweNawiazanieStY", NULL, pktDane[0]));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarBiegunowe", _("Musisz podać Nr, X, Y stanowiska"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryBiegunoweNawiazaniePktNNr",
		"entryBiegunoweNawiazaniePktNX", "entryBiegunoweNawiazaniePktNY", NULL, pktDane[l_wierszy+1]));
	else
		nawiazanie = 0;

	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (i = 0; i < l_wierszy; i++)
	{
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_BIE_NR, &pkt_nr,
		COL_BIE_X_DOUBLE, &pkt_x, COL_BIE_Y_DOUBLE, &pkt_y, -1);
		pktDane[i+1].setNr(pkt_nr);
		pktDane[i+1].setXY(pkt_x, pkt_y);
		g_free(pkt_nr);

		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	if (nawiazanie)
		geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, 2+l_wierszy);
	else
		geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, 1+l_wierszy);
	
	// rysowane polnocy
	pktWidok[l_wierszy+2].x = pktWidok[0].x;
	pktWidok[l_wierszy+2].y = 0;
	if (nawiazanie) geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[1+l_wierszy], LINE_BAZA);
	geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[l_wierszy+2], LINE_POLNOC);
	// rysowanie punktow
	for (i = 0; i < l_wierszy; i++)
	{
		geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[i+1], LINE_KIERUNEK);
		geod_draw_point(GTK_WIDGET(darea), pktWidok[i+1], pktDane[i+1].getNr(), PKT_WYNIK);
	}
	// rysowanie punktow nawiazania
	geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
	if (nawiazanie) geod_draw_point(GTK_WIDGET(darea), pktWidok[1+l_wierszy], pktDane[1+l_wierszy].getNr(), PKT_OSNOWA);

	gtk_widget_queue_draw(darea);
	delete [] pktDane;
	delete [] pktWidok;
}


void on_tbtnBiegunoweZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winBiegunowe");
	gtk_widget_destroy(window);
}


gboolean on_entryBiegunoweNawiazanieStNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarBiegunowe");
	return FALSE;
}


gboolean on_entryBiegunoweNawiazanieStNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarBiegunowe");
	return FALSE;
}


gboolean on_entryBiegunoweNawiazaniePktNNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarBiegunowe");
	return FALSE;
}


gboolean on_entryBiegunoweNawiazaniePktNNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarBiegunowe");
	return FALSE;
}


gboolean on_entryBiegunowePktTyczonyNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarBiegunowe");
	return FALSE;
}


gboolean on_entryBiegunowePktTyczonyNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarBiegunowe");
	return FALSE;
}


void on_btnBiegunowePktTyczonyDodaj_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktTyczony;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeBiegunowe");
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	char *txtX = NULL, *txtY = NULL;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryBiegunowePktTyczonyNr",
		"entryBiegunowePktTyczonyX", "entryBiegunowePktTyczonyY", NULL, pktTyczony));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarBiegunowe", _("Musisz podać Nr, X, Y punktu tyczonego"));
		return;
	}

	txtX = g_strdup_printf("%.*f", dok_xy_int, pktTyczony.getX());
	txtY = g_strdup_printf("%.*f", dok_xy_int, pktTyczony.getY());
	
	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
		COL_BIE_NR, pktTyczony.getNr(),
		COL_BIE_X, txtX, COL_BIE_X_DOUBLE, pktTyczony.getX(),
		COL_BIE_Y, txtY, COL_BIE_Y_DOUBLE, pktTyczony.getY(), -1);
	
	g_free(txtX);
	g_free(txtY);
	
	// wyczyszczenie pol punktu tycznego
	geod_entry_inc(GTK_WIDGET(button), "entryBiegunowePktTyczonyNr");
	geod_entry_clear(GTK_WIDGET(button), "entryBiegunowePktTyczonyX");
	geod_entry_clear(GTK_WIDGET(button), "entryBiegunowePktTyczonyY");
	g_signal_emit_by_name(lookup_widget(GTK_WIDGET(button), "tbtnBiegunoweOblicz"), "clicked");
	gtk_widget_grab_focus(lookup_widget(GTK_WIDGET(button), "entryBiegunowePktTyczonyNr"));
}


/* winPowierzchnia */
void on_winPowierzchnia_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winPowierzchnia");
}


void on_tbtnPowierzchniaOblicz_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt *pktGraniczny = NULL;
	GSList *punkty = NULL;
	cPunkt *pktGraniczne = NULL;
	cPunkt *pkt = NULL;
	char *jedn_pole = NULL;
	double pole_skala = 1.0;

	GtkWidget *treePunkty = lookup_widget(GTK_WIDGET(toolbutton), "treePowierzchniaPunkty");
	GtkTreeModel *modelPunkty = gtk_tree_view_get_model(GTK_TREE_VIEW(treePunkty));
	GtkWidget *treeWynik = lookup_widget(GTK_WIDGET(toolbutton), "treePowierzchniaWynik");
	GtkTreeModel *modelWynik = gtk_tree_view_get_model(GTK_TREE_VIEW(treeWynik));
	GtkTreeIter iterPunkty, iterWynik, iterCzolowka;

	guint n_pkt, l_wierszy = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(modelPunkty), NULL);
	char *pkt_nr = NULL, *txtPole = NULL, *txtCzlowka = NULL, *txtObwod = NULL, *nr_dz = NULL, *nr_dz_nast = NULL;
	double pkt_x, pkt_y, wynik_pole, wynik_obwod = 0.0, wynik_czolowka = 0.0;
	char dok_pole[CONFIG_VALUE_MAX_LEN], dok_xy[CONFIG_VALUE_MAX_LEN], jedn_p[CONFIG_VALUE_MAX_LEN];
	int dok_pole_int = 3, dok_xy_int = 3;
	bool oblicz = FALSE;

	if (!dbgeod_config_get("dok_pole", dok_pole)) dok_pole_int = atoi(dok_pole);
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	dbgeod_config_get("jedn_pow", jedn_p);
	if (g_str_equal(jedn_p, "a")) {
		jedn_pole = g_strdup("a");
		pole_skala = 100.0;
	}
	else if (g_str_equal(jedn_p, "ha")) {
		jedn_pole = g_strdup("ha");
		pole_skala = 10000.0;
	}
	else {
		jedn_pole = g_strdup("m\302\262");
		pole_skala = 1.0;
	}
	
	// czyszczenie listy wynik
	gtk_tree_store_clear(GTK_TREE_STORE(modelWynik));
	if (gtk_tree_model_get_iter_first(GTK_TREE_MODEL(modelPunkty), &iterPunkty) == FALSE)
		return;

	iterCzolowka = iterPunkty;
	for (guint i=0; i<l_wierszy; i++) {
		nr_dz_nast = NULL;
		oblicz = FALSE;
		gtk_tree_model_get(GTK_TREE_MODEL(modelPunkty), &iterPunkty, COL_PWP_DZ, &nr_dz,
			COL_PWP_NR, &pkt_nr, COL_PWP_X_DOUBLE, &pkt_x, COL_PWP_Y_DOUBLE, &pkt_y, -1);

		pktGraniczny = new cPunkt;
		pktGraniczny->setNr(pkt_nr);
		pktGraniczny->setXY(pkt_x, pkt_y);
		punkty = g_slist_append(punkty, pktGraniczny);
		
		if (gtk_tree_model_iter_next(GTK_TREE_MODEL(modelPunkty), &iterPunkty)){
			gtk_tree_model_get(GTK_TREE_MODEL(modelPunkty), &iterPunkty, COL_PWP_DZ, &nr_dz_nast, -1);
			if (!g_str_equal(nr_dz_nast, nr_dz))
				oblicz = TRUE;
		}
		else
			oblicz = TRUE;

		if (oblicz) {
			n_pkt = g_slist_length(punkty);
			pktGraniczne = new cPunkt[n_pkt];
			wynik_obwod = 0.0;

			for (guint k=0; k<n_pkt; k++) {
				pkt = (cPunkt*)g_slist_nth_data(punkty, k);
				pktGraniczne[k].setNr(pkt->getNr());
				pktGraniczne[k].setXY(pkt->getX(), pkt->getY());
			}

			// dodawanie wartosci czolowek
			for (guint k=0; k < n_pkt-1; k++) {
				wynik_czolowka = dlugosc(pktGraniczne[k], pktGraniczne[k+1]);
				wynik_obwod += wynik_czolowka;
				txtCzlowka = g_strdup_printf("%.*f m", dok_xy_int, wynik_czolowka);
				gtk_tree_store_set(GTK_TREE_STORE(modelPunkty), &iterCzolowka, COL_PWP_CZ, txtCzlowka,
					COL_PWP_CZ_DOUBLE, wynik_czolowka, -1);
				g_free(txtCzlowka);
				gtk_tree_model_iter_next(GTK_TREE_MODEL(modelPunkty), &iterCzolowka);
			}
			gtk_tree_model_iter_next(GTK_TREE_MODEL(modelPunkty), &iterCzolowka);
			wynik_czolowka = dlugosc(pktGraniczne[0], pktGraniczne[n_pkt-1]);
			wynik_obwod += wynik_czolowka;
			txtCzlowka = g_strdup_printf("%.*f m", dok_xy_int, wynik_czolowka);
			gtk_tree_store_set(GTK_TREE_STORE(modelPunkty), &iterCzolowka, COL_PWP_CZ, txtCzlowka,
				COL_PWP_CZ_DOUBLE, wynik_czolowka, -1);
			g_free(txtCzlowka);
			
			wynik_pole = pole_wielobok(pktGraniczne, n_pkt);
			txtPole = g_strdup_printf("%.*f %s", dok_pole_int, wynik_pole/pole_skala, jedn_pole);
			txtObwod = g_strdup_printf("%.*f m", dok_pole_int, wynik_obwod);
			
			gtk_tree_store_append(GTK_TREE_STORE(modelWynik), &iterWynik, NULL);
			gtk_tree_store_set(GTK_TREE_STORE(modelWynik), &iterWynik, COL_PWW_DZ, nr_dz,
				COL_PWW_POW_DOUBLE, wynik_pole, COL_PWW_POW, txtPole,
				COL_PWW_OBW_DOUBLE, wynik_obwod, COL_PWW_OBW, txtObwod, -1);

			g_slist_free(punkty);
			punkty = NULL;
			delete [] pktGraniczne;
			g_free(txtPole);
			g_free(txtObwod);
		}
		g_free(nr_dz);
		g_free(nr_dz_nast);
	}
	g_free(jedn_pole);
}


void on_tbtnPowierzchniaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	// Czyszczenie listy punkty
	GtkWidget *treePunkty = lookup_widget(GTK_WIDGET(toolbutton), "treePowierzchniaPunkty");
	GtkTreeModel *modelPunkty = gtk_tree_view_get_model(GTK_TREE_VIEW(treePunkty));
	gtk_tree_store_clear(GTK_TREE_STORE(modelPunkty));

	// Czyszczenie listy wynik
	GtkWidget *treeWynik = lookup_widget(GTK_WIDGET(toolbutton), "treePowierzchniaWynik");
	GtkTreeModel *modelWynik = gtk_tree_view_get_model(GTK_TREE_VIEW(treeWynik));
	gtk_tree_store_clear(GTK_TREE_STORE(modelWynik));
	
	// Czyszczenie wszystkich pol
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPowierzchniaPunktDzialka");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPowierzchniaPunktNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPowierzchniaPunktX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPowierzchniaPunktY");

	GtkWidget *focus = lookup_widget(GTK_WIDGET(toolbutton), "entryPowierzchniaPunktNr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


void on_tbtnPowierzchniaUsun_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treePowierzchniaPunkty");
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(toolbutton), "tbtnPowierzchniaOblicz");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreePath *tpath;
	GtkTreeIter iter;
	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
		g_signal_emit_by_name(btnOblicz, "clicked");
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPowierzchnia", _("Zaznacz najpierw pozycję w tabeli"));

	gtk_tree_path_free(tpath);
}


void on_tbtnPowierzchniaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	char *wynik_obwod = NULL, *wynik_pole = NULL, *wynik_dz = NULL;
	const guint NCOLS = 4;

	GtkWidget *dialogRaport = create_dialogRaport();
	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");
	gtk_widget_show(dialogRaport);

	GtkWidget *treePunkty = lookup_widget(GTK_WIDGET(toolbutton), "treePowierzchniaPunkty");
	GtkTreeModel *modelPunkty = gtk_tree_view_get_model(GTK_TREE_VIEW(treePunkty));
	GtkWidget *treeWynik = lookup_widget(GTK_WIDGET(toolbutton), "treePowierzchniaWynik");
	GtkTreeModel *modelWynik = gtk_tree_view_get_model(GTK_TREE_VIEW(treeWynik));

	const guint NPOINTS = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(modelPunkty), NULL);
	GtkTreeIter iterPunkty, iterWynik;
	char *nr_dz = NULL, *nr_dz_nast = NULL;
	bool rysuj_tabele = FALSE, wynik = FALSE;
	guint n_dzpoints = 0;

	char **tab_cell = new char*[(NPOINTS+1)*NCOLS];
	guint colsize[NCOLS] = { 10, 20, 20, 15 };
	
	tab_cell[0] = g_strdup(_("Nr"));
	tab_cell[1] = g_strdup(_("X"));
	tab_cell[2] = g_strdup(_("Y"));
	tab_cell[3] = g_strdup(_("Czołówka"));

	geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\npole powierzchni"));

	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(modelPunkty), &iterPunkty);
	for (guint i=1; i<=NPOINTS; i++) {
		nr_dz_nast = NULL;
		rysuj_tabele = FALSE;
		n_dzpoints++;

		gtk_tree_model_get(GTK_TREE_MODEL(modelPunkty), &iterPunkty, COL_PWP_DZ, &nr_dz,
			COL_PWP_NR, &tab_cell[n_dzpoints*NCOLS+0],
			COL_PWP_X, &tab_cell[n_dzpoints*NCOLS+1],
			COL_PWP_Y, &tab_cell[n_dzpoints*NCOLS+2],
			COL_PWP_CZ, &tab_cell[n_dzpoints*NCOLS+3], -1);

		if (gtk_tree_model_iter_next(GTK_TREE_MODEL(modelPunkty), &iterPunkty)) {
			gtk_tree_model_get(GTK_TREE_MODEL(modelPunkty), &iterPunkty, COL_PWP_DZ, &nr_dz_nast, -1);
			if (!g_str_equal(nr_dz_nast, nr_dz))
				rysuj_tabele = TRUE;
		}
		else
			rysuj_tabele = TRUE;

		if (rysuj_tabele) {
			// odczytaj powierzchnie i odwod
			wynik_pole = NULL;
			wynik_obwod = NULL;

			if (gtk_tree_model_get_iter_first(GTK_TREE_MODEL(modelWynik), &iterWynik))
				do {
					wynik_dz = NULL;
					wynik = FALSE; 				

					gtk_tree_model_get(GTK_TREE_MODEL(modelWynik), &iterWynik, COL_PWW_DZ, &wynik_dz, -1);
					if (g_str_equal(nr_dz, wynik_dz)) {
						gtk_tree_model_get(GTK_TREE_MODEL(modelWynik), &iterWynik,
							COL_PWW_OBW, &wynik_obwod, COL_PWW_POW, &wynik_pole, -1);
						wynik = TRUE;
					}
					g_free(wynik_dz);
				} while (!wynik && gtk_tree_model_iter_next(GTK_TREE_MODEL(modelWynik), &iterWynik));
			
			// rysuj tabele
			geod_raport_line(textview, _("\nDziałka"), nr_dz, TRUE);
			geod_raport_table(textview, tab_cell, NCOLS, n_dzpoints, colsize, _("Wykaz punktów granicznych"));			
			if (wynik_pole != NULL)
				geod_raport_line(textview, _("Powierzchnia"), wynik_pole, TRUE);
			if (wynik_obwod !=NULL)
				geod_raport_line(textview, _("Obwód"), wynik_obwod, TRUE);
			
			for (guint k=NCOLS; k<(n_dzpoints+1)*NCOLS; k++) {
				g_free(tab_cell[k]);
				tab_cell[k] = NULL;
			}
			n_dzpoints = 0;
			g_free(wynik_pole);
			g_free(wynik_obwod);
		}
		g_free(nr_dz);
		g_free(nr_dz_nast);
	}

	geod_raport_stopka(textview);

	// usun z pamieci naglowek tabeli
	for (guint i=0; i<NCOLS; i++)
		g_free(tab_cell[i]);
}


void on_tbtnPowierzchniaWidok_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt *pktGraniczne = NULL;
	GdkPoint *pktWidok = NULL;

	GtkWidget *treePunkty = lookup_widget(GTK_WIDGET(toolbutton), "treePowierzchniaPunkty");
	GtkTreeModel *modelPunkty = gtk_tree_view_get_model(GTK_TREE_VIEW(treePunkty));
	GtkTreeIter iterPunkty;
	guint l_wierszy = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(modelPunkty), NULL);

	char *pkt_nr = NULL, *nr_dz_nast = NULL, *nr_dz = NULL;
	double pkt_x, pkt_y;
	int *pktNext = NULL;
	int pktPierwszy = 0;
	
	GtkWidget *winWidok = create_winWidok();
	gtk_widget_show(winWidok);
	GtkWidget *darea = lookup_widget(GTK_WIDGET(winWidok), "drawingAreaWidok");

	if (l_wierszy < 2) return;
	
	pktGraniczne = new cPunkt[l_wierszy];
	pktWidok = new GdkPoint[l_wierszy];
	pktNext = new int[l_wierszy];
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(modelPunkty), &iterPunkty);
	for (guint i = 0; i < l_wierszy; i++) {
		nr_dz_nast = NULL;

		gtk_tree_model_get(GTK_TREE_MODEL(modelPunkty), &iterPunkty, COL_PWP_NR,
			&pkt_nr, COL_PWP_DZ, &nr_dz, COL_PWP_X_DOUBLE, &pkt_x, COL_PWP_Y_DOUBLE, &pkt_y, -1);
		pktGraniczne[i].setNr(pkt_nr);
		pktGraniczne[i].setXY(pkt_x, pkt_y);
		g_free(pkt_nr);

		if (gtk_tree_model_iter_next(GTK_TREE_MODEL(modelPunkty), &iterPunkty)){
			gtk_tree_model_get(GTK_TREE_MODEL(modelPunkty), &iterPunkty, COL_PWP_DZ, &nr_dz_nast, -1);
			if (!g_str_equal(nr_dz_nast, nr_dz)) {
				// zamknij dzialke
				pktNext[i] = pktPierwszy;
				pktPierwszy = i+1;
			}
			else
				pktNext[i] = i+1;
		}
		// koniec listy
		else
			pktNext[i] = pktPierwszy;

		g_free(nr_dz);
		g_free(nr_dz_nast);
	}
	geod_view_transform(GTK_WIDGET(darea), pktGraniczne, pktWidok, l_wierszy);
	for (guint i = 0; i < l_wierszy; i++)
		geod_draw_line(GTK_WIDGET(darea), pktWidok[i], pktWidok[pktNext[i]], LINE_BAZA);

	for (guint i = 0; i < l_wierszy; i++)
		geod_draw_point(GTK_WIDGET(darea), pktWidok[i], pktGraniczne[i].getNr());

	delete [] pktGraniczne;
	delete [] pktWidok;
	delete [] pktNext;
}


void on_tbtnPowierzchniaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winPowierzchnia");
	gtk_widget_destroy(window);
}


gboolean on_entryPowierzchniaPunktNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarPowierzchnia");
	return FALSE;
}


gboolean on_entryPowierzchniaPunktNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPowierzchnia");
	return FALSE;
}


void on_btnPowierzchniaPunktDodaj_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktGraniczny;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treePowierzchniaPunkty");
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(button), "tbtnPowierzchniaOblicz");
	GtkWidget *entryDzialka = lookup_widget(GTK_WIDGET(button), "entryPowierzchniaPunktDzialka");
	const char *entry_dz_nr = gtk_entry_get_text(GTK_ENTRY(entryDzialka));
	char *tree_dz_nr, *txtX = NULL, *txtY = NULL;
	GtkTreeIter iter, iter_i;
	GtkTreePath *tpath;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	gboolean nastepny = FALSE, istnieje = FALSE;

	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryPowierzchniaPunktNr",
		"entryPowierzchniaPunktX", "entryPowierzchniaPunktY", NULL, pktGraniczny));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarPowierzchnia", _("Musisz podać Nr, X, Y punktu granicznego"));
		return;
	}

	txtX = g_strdup_printf("%.*f", dok_xy_int, pktGraniczny.getX());
	txtY = g_strdup_printf("%.*f", dok_xy_int, pktGraniczny.getY());

	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter_i, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter_i, COL_PWP_DZ, &tree_dz_nr, -1);
		if (g_str_equal(tree_dz_nr, entry_dz_nr)) {
			istnieje = TRUE;
			nastepny = TRUE;
			gtk_tree_selection_unselect_all(gtk_tree_view_get_selection(GTK_TREE_VIEW(tree)));
		}
	}
	else {
		nastepny = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter_i);
		while (nastepny)
		{
			gtk_tree_model_get(GTK_TREE_MODEL(model), &iter_i, COL_PWP_DZ, &tree_dz_nr, -1);
			if (g_str_equal(tree_dz_nr, entry_dz_nr))
				istnieje = TRUE;

			if (istnieje && !g_str_equal(tree_dz_nr, entry_dz_nr)) {
				g_free(tree_dz_nr);
				break;
			}

			nastepny = gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter_i);
			g_free(tree_dz_nr);
		}
	}
	
	if (istnieje && nastepny)
		gtk_tree_store_insert_before(GTK_TREE_STORE(model), &iter, NULL, &iter_i);
	else
		gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
		COL_PWP_DZ, entry_dz_nr, COL_PWP_NR, pktGraniczny.getNr(),
		COL_PWP_X, txtX, COL_PWP_X_DOUBLE, pktGraniczny.getX(),
		COL_PWP_Y, txtY, COL_PWP_Y_DOUBLE, pktGraniczny.getY(), -1);

	gtk_tree_path_free(tpath);
	g_free(txtX);
	g_free(txtY);

	// wyczyszczenie wszystkich pol
	geod_entry_inc(GTK_WIDGET(button), "entryPowierzchniaPunktNr");
	geod_entry_clear(GTK_WIDGET(button), "entryPowierzchniaPunktX");
	geod_entry_clear(GTK_WIDGET(button), "entryPowierzchniaPunktY");
	g_signal_emit_by_name(btnOblicz, "clicked");
	GtkWidget *focus = lookup_widget(GTK_WIDGET(button), "entryPowierzchniaPunktNr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


void on_winPowierzchnia_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treePunkty = lookup_widget(GTK_WIDGET(widget), "treePowierzchniaPunkty");
	GtkWidget *treeWynik = lookup_widget(GTK_WIDGET(widget), "treePowierzchniaWynik");

	GtkTreeStore *modelPunkty = gtk_tree_store_new(N_PWP_COLUMNS, \
	G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_DOUBLE, \
	G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treePunkty), \
	    GTK_TREE_MODEL(modelPunkty));

	GtkTreeStore *modelWynik = gtk_tree_store_new(N_PWW_COLUMNS, \
	G_TYPE_STRING, G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeWynik), \
	    GTK_TREE_MODEL(modelWynik));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;

	// kolumna Punkty - Działka
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treePunkty), \
	   -1, _("Działka"), renderer, "text", COL_PWP_DZ, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treePunkty), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);
	
	// kolumna Punkty - Nr punkt
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treePunkty), \
	   -1, _("Nr"), renderer, "text", COL_PWP_NR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treePunkty), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// kolumna Punkty - wsp. X
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treePunkty), \
	   -1, _("X"), renderer, "text", COL_PWP_X, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treePunkty), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);

	// kolumna Punkty - wsp. Y
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treePunkty), \
	   -1, _("Y"), renderer, "text", COL_PWP_Y, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treePunkty), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);

	// kolumna Punkty - Czolowka
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treePunkty), \
	   -1, _("Czołówka"), renderer, "text", COL_PWP_CZ, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treePunkty), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);

	// kolumna Wynik - Działka
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeWynik), \
	   -1, _("Działka"), renderer, "text", COL_PWW_DZ, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeWynik), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);
	
	// kolumna Wynik - powierzchnia
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeWynik), \
	   -1, _("Powierzchnia"), renderer, "text", COL_PWW_POW, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeWynik), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 150);

	// kolumna Wynik - obwód
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeWynik), \
	   -1, _("Obwód"), renderer, "text", COL_PWW_OBW, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeWynik), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 150);
}


/* winRzutProsta */ 
void on_winRzutProsta_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winRzutProsta");
}


void on_winRzutProsta_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeRzutProsta");
	GtkTreeStore *model;
	
	model=gtk_tree_store_new(N_RZP_COLUMNS, \
	G_TYPE_STRING, G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, \
	G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE,
	G_TYPE_DOUBLE, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_INT, G_TYPE_BOOLEAN);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), GTK_TREE_MODEL(model));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;
	
	// kolumna Nr punkt
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Nr"), renderer, "text", COL_RZP_NR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// kolumna wsp. X
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("X"), renderer, "text", COL_RZP_X, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 80);

	// kolumna wsp. Y
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Y"), renderer, "text", COL_RZP_Y, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 80);

	// kolumna miara biezaca
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Bieżąca"), renderer, "text", COL_RZP_B, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna domiar
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Domiar"), renderer, "text", COL_RZP_D, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna Nr rzut
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Nr rzut"), renderer, "text", COL_RZP_NRR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// kolumna Toggle
	GtkCellRenderer *rendererToggle = gtk_cell_renderer_toggle_new();
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("D"), rendererToggle, "active", COL_RZP_TOGGLE, NULL);
	GtkTreeViewColumn *toggleColumn = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(toggleColumn), 10);
}


void on_tbtnRzutProstaOblicz_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt *pktRzutowany = NULL;
	cPunkt *pktRzut = NULL;
	cPunkt pktBazaP, pktBazaK;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeRzutProsta");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	guint l_wierszy = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	guint i, geod_error, s_dodaj;
	double biezaca, domiar;
	char *txtB = NULL, *txtD = NULL;
	char *pkt_nr = NULL, *pkt_nrr = NULL;
	double pkt_x, pkt_y;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryRzutProstaBazaPPNr",
		"entryRzutProstaBazaPPX", "entryRzutProstaBazaPPY", NULL, pktBazaP));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarRzutProsta", _("Musisz podać Nr, X, Y pierwszego punktu prostej"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryRzutProstaBazaPKNr",
		"entryRzutProstaBazaPKX", "entryRzutProstaBazaPKY", NULL, pktBazaK));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarRzutProsta", _("Musisz podać Nr, X, Y drugiego punktu prostej"));
		return;
	}
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (i = 0; i < l_wierszy; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_RZP_NR, &pkt_nr,
		COL_RZP_X_DOUBLE, &pkt_x, COL_RZP_Y_DOUBLE, &pkt_y, COL_RZP_NRR, &pkt_nrr,
		COL_RZP_DODAJ, &s_dodaj, -1);
		pktRzutowany = new cPunkt;
		pktRzut = new cPunkt;
		pktRzutowany->setNr(pkt_nr);
		pktRzutowany->setXY(pkt_x, pkt_y);
		pktRzut->setNr(pkt_nrr);

		geod_error = rzut_prosta(pktBazaP, pktBazaK, *pktRzutowany, biezaca, domiar);
		if (geod_error == GEODERR_OK) {
			txtB = g_strdup_printf("%.*f", dok_xy_int, biezaca);
			txtD = g_strdup_printf("%.*f", dok_xy_int, domiar);
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_RZP_B, txtB,
				COL_RZP_B_DOUBLE, biezaca, COL_RZP_D, txtD, COL_RZP_D_DOUBLE, domiar, -1);
			g_free(txtB);
			g_free(txtD);
		}
		else {
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_RZP_B, "",
				COL_RZP_B_DOUBLE, 0.0, COL_RZP_D, "", COL_RZP_D_DOUBLE, 0.0, -1);
			geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarRzutProsta", geod_error);
		}

		geod_error = domiar_prostokatny(pktBazaP, pktBazaK, *pktRzut, biezaca);
		if (geod_error == GEODERR_OK) {
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_RZP_XR_DOUBLE,
				pktRzut->getX(), COL_RZP_YR_DOUBLE, pktRzut->getY(), -1);
			switch (s_dodaj) {
				case PKT_DODAJ:
					if (dbgeod_point_add_config(*pktRzut) != GEODERR_OK) {
						geod_sbar_set(GTK_WIDGET(toolbutton), "sbarRzutProsta",
							_("Nie mogę dodać punktu do bazy danych!"));
						gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
							COL_RZP_DODAJ, PKT_ERROR, -1);
					}
					else
						gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
							COL_RZP_DODAJ, PKT_DODANY, -1);
					break;
				case PKT_DODANY:
					dbgeod_point_update(-1, *pktRzut, TRUE);
					break;
			}
		}
		else {
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_RZP_XR_DOUBLE,
				0.0, COL_RZP_YR_DOUBLE, 0.0, -1);
			geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarRzutProsta", geod_error);
		}

		g_free(pkt_nr);
		g_free(pkt_nrr);
		delete pktRzut;
		delete pktRzutowany;
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
}


void on_tbtnRzutProstaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	// Czyszczenie listy
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeRzutProsta");
	GtkWidget *focus = lookup_widget(GTK_WIDGET(toolbutton), "entryRzutProstaBazaPPNr");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	gtk_tree_store_clear(GTK_TREE_STORE(model));
	
	// Czyszczenie wszystkich pol
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryRzutProstaBazaPPNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryRzutProstaBazaPPX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryRzutProstaBazaPPY");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryRzutProstaBazaPKNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryRzutProstaBazaPKX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryRzutProstaBazaPKY");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryRzutProstaBazaPom");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryRzutProstaBazaWyl");

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryRzutProstaPunktNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryRzutProstaPunktX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryRzutProstaPunktY");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryRzutProstaPunktNrRzut");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


void on_treeRzutProsta_cursor_changed (GtkTreeView *treeview, gpointer user_data)
{
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	GtkTreePath *tpath;
	GtkTreeIter iter;
	char *pkt_nr = NULL, *sbar_msg = NULL;
	double pkt_x, pkt_y;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	gtk_tree_view_get_cursor(GTK_TREE_VIEW(treeview), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_RZP_NRR, &pkt_nr, COL_RZP_XR_DOUBLE, &pkt_x, COL_RZP_YR_DOUBLE, &pkt_y, -1);

		sbar_msg = g_strdup_printf(_("Rzut: %s (%.*f; %.*f)"), pkt_nr, dok_xy_int, pkt_x, dok_xy_int, pkt_y);
		geod_sbar_set(GTK_WIDGET(treeview), "sbarRzutProsta", sbar_msg);
		g_free(pkt_nr);
		g_free(sbar_msg);
		gtk_tree_path_free(tpath);
	}
}


void on_tbtnRzutProstaUsun_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeRzutProsta");
	GtkWidget *winRzutProsta = lookup_widget(GTK_WIDGET(toolbutton), "winRzutProsta");
	GtkWidget *dialog = NULL;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreePath *tpath = NULL;
	GtkTreeIter iter;
	int s_dodaj;
	char *pkt_nr = NULL;
	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_RZP_NRR, &pkt_nr,
				COL_RZP_DODAJ, &s_dodaj, -1);
		if (s_dodaj == PKT_DODANY) {
			dialog = gtk_message_dialog_new(GTK_WINDOW(winRzutProsta), GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Usunąć także punkt z bazy danych?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES) dbgeod_point_del(0, pkt_nr);
			gtk_widget_destroy(dialog);
		}
		g_free(pkt_nr);
		gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarRzutProsta", _("Zaznacz najpierw pozycję w tabeli"));

	gtk_tree_path_free(tpath);
}


void on_tbtnRzutProstaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	const guint NCOLS = 5;
	guint i;
	GSList *lpunkty = NULL;
	cPunkt *punkt = NULL;

	GtkWidget *dialogRaport = create_dialogRaport();
	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");
	gtk_widget_show(dialogRaport);

	GtkWidget *entryBazaDl = lookup_widget(GTK_WIDGET(toolbutton), "entryRzutProstaBazaWyl");
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeRzutProsta");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	const guint NROWS = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	
	guint colsize[NCOLS] = { 10, 15, 15, 13, 13 };
	char **tab_cell = new char*[(NROWS+1)*NCOLS];

	geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nrzutowanie pounktów na prostą"));
	
	// prosta
	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryRzutProstaBazaPPNr",
		"entryRzutProstaBazaPPX", "entryRzutProstaBazaPPY", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		geod_raport_error(textview, _("Dla pierwszego punktu prostej nie podano wszystkich wymaganych danych"));
		delete punkt;
	}

	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryRzutProstaBazaPKNr",
		"entryRzutProstaBazaPKX", "entryRzutProstaBazaPKY", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		geod_raport_error(textview, _("Dla drugiego punktu prostej nie podano wszystkich wymaganych danych"));
		delete punkt;
	}

	geod_raport_table_pkt(textview, lpunkty, FALSE, _("Baza"));
	g_slist_free(lpunkty);
	lpunkty = NULL;

	geod_raport_line(textview, _("Długość wyliczona"), gtk_entry_get_text(GTK_ENTRY(entryBazaDl)), TRUE);
	
	tab_cell[0] = g_strdup(_("Nr"));
	tab_cell[1] = g_strdup(_("X"));
	tab_cell[2] = g_strdup(_("Y"));
	tab_cell[3] = g_strdup(_("Bieżąca"));
	tab_cell[4] = g_strdup(_("Domiar"));

	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);	
	for (i=1; i<=NROWS; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_RZP_NR, &tab_cell[i*NCOLS+0],
			COL_RZP_X, &tab_cell[i*NCOLS+1],
			COL_RZP_Y, &tab_cell[i*NCOLS+2],
			COL_RZP_B, &tab_cell[i*NCOLS+3],
			COL_RZP_D, &tab_cell[i*NCOLS+4], -1);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	
	geod_raport_table(textview, tab_cell, NCOLS, NROWS, colsize, _("\nRzutowane punkty"));
	geod_raport_stopka(textview);

	for (i=0; i<(NROWS+1)*NCOLS; i++)
		g_free(tab_cell[i]);
}


void on_tbtnRzutProstaWidok_clicked  (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt *pktDane = NULL;
	GdkPoint *pktWidok = NULL;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeRzutProsta");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	guint l_wierszy = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	guint i;
	char *pkt_nr = NULL, *pkt_nrr = NULL;
	double pkt_x, pkt_y, pkt_x0, pkt_y0;

	GtkWidget *winWidok = create_winWidok();
	gtk_widget_show(winWidok);
	GtkWidget *darea = lookup_widget(GTK_WIDGET(winWidok), "drawingAreaWidok");
	
	pktDane = new cPunkt[2+2*l_wierszy];
	pktWidok = new GdkPoint[2+2*l_wierszy];
	
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryRzutProstaBazaPPNr",
		"entryRzutProstaBazaPPX", "entryRzutProstaBazaPPY", NULL, pktDane[0]));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarRzutProsta", _("Musisz podać Nr, X, Y pierwszego punktu prostej"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryRzutProstaBazaPKNr",
		"entryRzutProstaBazaPKX", "entryRzutProstaBazaPKY", NULL, pktDane[1]));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarRzutProsta", _("Musisz podać Nr, X, Y drugiego punktu prostej"));
		return;
	}
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (i = 0; i < l_wierszy; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_RZP_NR, &pkt_nr,
		COL_RZP_X_DOUBLE, &pkt_x, COL_RZP_Y_DOUBLE, &pkt_y, COL_RZP_NRR, &pkt_nrr,
		COL_RZP_XR_DOUBLE, &pkt_x0, COL_RZP_YR_DOUBLE, &pkt_y0, -1);
		pktDane[2+i].setNr(pkt_nr);
		pktDane[2+i].setXY(pkt_x, pkt_y);
		pktDane[2+i+l_wierszy].setNr(pkt_nr);
		pktDane[2+i+l_wierszy].setXY(pkt_x0, pkt_y0);
		g_free(pkt_nr);
		g_free(pkt_nrr);

		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, 2+2*l_wierszy);
	
	// rysowanie bazy
	geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[1], LINE_BAZA);
	geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
	geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[1].getNr(), PKT_OSNOWA);
	// rysowanie punktow
	for (i = 0; i < l_wierszy; i++) {
		geod_draw_line(GTK_WIDGET(darea), pktWidok[i+2], pktWidok[i+2+l_wierszy], LINE_POLNOC);
		geod_draw_point(GTK_WIDGET(darea), pktWidok[i+2], pktDane[i+2].getNr(), PKT_OSNOWA);
		geod_draw_point(GTK_WIDGET(darea), pktWidok[i+2+l_wierszy], pktDane[i+2+l_wierszy].getNr(), PKT_WYNIK);
	}
	gtk_widget_queue_draw(darea);
	delete [] pktDane;
	delete [] pktWidok;
}


void on_tbtnRzutProstaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winRzutProsta");
	gtk_widget_destroy(window);
}


gboolean on_entryRzutProstaBazaPPNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarRzutProsta");
	return FALSE;
}


gboolean on_entryRzutProstaBazaPPNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarRzutProsta");
	return FALSE;
}


gboolean on_entryRzutProstaBazaPKNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarRzutProsta");
	return FALSE;
}


gboolean on_entryRzutProstaBazaPKNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarRzutProsta");
	return FALSE;
}


gboolean on_entryRzutProstaPunktNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarRzutProsta");
	return FALSE;
}


gboolean on_entryRzutProstaPunktNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarRzutProsta");
	return FALSE;
}


void on_frameRzutProstaBaza_set_focus_child (GtkContainer *container, GtkWidget *widget, gpointer user_data)
{
	char *txtBazaWyl = NULL;
	GtkWidget *entryBazaWyl = lookup_widget(GTK_WIDGET(container), "entryRzutProstaBazaWyl");
	GtkWidget *entryBazaPPX = lookup_widget(GTK_WIDGET(container), "entryRzutProstaBazaPPX");
	GtkWidget *entryBazaPPY = lookup_widget(GTK_WIDGET(container), "entryRzutProstaBazaPPY");
	GtkWidget *entryBazaPKX = lookup_widget(GTK_WIDGET(container), "entryRzutProstaBazaPKX");
	GtkWidget *entryBazaPKY = lookup_widget(GTK_WIDGET(container), "entryRzutProstaBazaPKY");
	const char *txtPPX = gtk_entry_get_text(GTK_ENTRY(entryBazaPPX));
	const char *txtPPY = gtk_entry_get_text(GTK_ENTRY(entryBazaPPY));
	const char *txtPKX = gtk_entry_get_text(GTK_ENTRY(entryBazaPKX));
	const char *txtPKY = gtk_entry_get_text(GTK_ENTRY(entryBazaPKY));
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	
	if (g_str_equal(txtPPX, "") || g_str_equal(txtPPY, "") || g_str_equal(txtPKX, "") ||
		g_str_equal(txtPKY, ""))
		gtk_editable_delete_text(GTK_EDITABLE(entryBazaWyl), 0, -1);
	else {
		txtBazaWyl = g_strdup_printf("%.*f",  dok_xy_int, dlugosc(g_strtod(txtPPX, NULL), g_strtod(txtPPY, NULL), g_strtod(txtPKX, NULL), g_strtod(txtPKY, NULL)));
		gtk_entry_set_text(GTK_ENTRY(entryBazaWyl), txtBazaWyl);
		g_free(txtBazaWyl);
	}
}


void on_btnRzutProstaPunktDodaj_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktRzutowany;
	int zadanie_id;
	GtkWidget *entryNrRzut = lookup_widget(GTK_WIDGET(button), "entryRzutProstaPunktNrRzut");
	GtkWidget *cbtnNoAdd = lookup_widget(GTK_WIDGET(button), "cbtnRzutProstaPunktDodajLista");
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeRzutProsta");
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	char *txtX = NULL, *txtY = NULL, *txtNrRzut = NULL;
	int s_dodaj;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryRzutProstaPunktNr",
		"entryRzutProstaPunktX", "entryRzutProstaPunktY", NULL, pktRzutowany));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarRzutProsta", _("Musisz podać Nr, X, Y rzutowanego punktu"));
		return;
	}

	if (g_str_equal(gtk_entry_get_text(GTK_ENTRY(entryNrRzut)), "")) {
		if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnNoAdd))) {
			geod_sbar_set(GTK_WIDGET(button), "sbarRzutProsta", _("Musisz podać Nr rzutowanego punktu"));
			gtk_widget_grab_focus(GTK_WIDGET(entryNrRzut));
			return;
		}
		else
			txtNrRzut = g_strdup(pktRzutowany.getNr());
	}
	else if (!dbgeod_point_find(gtk_entry_get_text(GTK_ENTRY(entryNrRzut))) && !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnNoAdd))) {
		geod_sbar_set(GTK_WIDGET(button), "sbarRzutProsta", _("Punkt o takim numerze znajduje się już w bazie"));
		gtk_widget_grab_focus(GTK_WIDGET(entryNrRzut));
		return;
	}
	else
		txtNrRzut = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryNrRzut)));


	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnNoAdd)))
		s_dodaj = PKT_NIE_DODAWAJ;
	else 
		s_dodaj = PKT_DODAJ;

	txtX = g_strdup_printf("%.*f", dok_xy_int, pktRzutowany.getX());
	txtY = g_strdup_printf("%.*f", dok_xy_int, pktRzutowany.getY());
 	
	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
		COL_RZP_NR, pktRzutowany.getNr(),
		COL_RZP_X, txtX, COL_RZP_X_DOUBLE, pktRzutowany.getX(),
		COL_RZP_Y, txtY, COL_RZP_Y_DOUBLE, pktRzutowany.getY(),
		COL_RZP_NRR, txtNrRzut, COL_RZP_DODAJ, s_dodaj, -1);
	if (s_dodaj == PKT_DODAJ) {
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_RZP_TOGGLE, TRUE, -1);
		
		GtkWidget *entryPrPP = lookup_widget(GTK_WIDGET(button), "entryRzutProstaBazaPPNr");
		GtkWidget *entryPrPK = lookup_widget(GTK_WIDGET(button), "entryRzutProstaBazaPKNr");
		dbgeod_hist_add("RzutProsta", zadanie_id);
		dbgeod_hist_point_set(zadanie_id, "PktRzutowanyNr", pktRzutowany.getNr(), 0);
		dbgeod_hist_point_set(zadanie_id, "PrPPNr", gtk_entry_get_text(GTK_ENTRY(entryPrPP)), 0);
		dbgeod_hist_point_set(zadanie_id, "PrPKNr", gtk_entry_get_text(GTK_ENTRY(entryPrPK)), 0);
		dbgeod_hist_point_set(zadanie_id, "PktRzutNr", txtNrRzut, 1);
	}
	else
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_RZP_TOGGLE, FALSE, -1);

	g_free(txtX);
	g_free(txtY);
	g_free(txtNrRzut);
	
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(button), "tbtnRzutProstaOblicz");
	g_signal_emit_by_name(btnOblicz, "clicked");
	
	// wyczyszczenie wszystkich pol
	geod_entry_inc(GTK_WIDGET(button), "entryRzutProstaPunktNr");
	geod_entry_clear(GTK_WIDGET(button), "entryRzutProstaPunktX");
	geod_entry_clear(GTK_WIDGET(button), "entryRzutProstaPunktY");
	if(!g_str_equal(gtk_entry_get_text(GTK_ENTRY(entryNrRzut)), ""))
		geod_entry_inc(GTK_WIDGET(button), "entryRzutProstaPunktNrRzut");

	GtkWidget *focus = lookup_widget(GTK_WIDGET(button), "entryRzutProstaPunktNr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


/* winAzymut */
void on_winAzymut_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winAzymut");
}


void on_winAzymut_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeAzymut");
	GtkTreeStore *azymut_model;
	
	azymut_model=gtk_tree_store_new(N_AZ_COLUMNS, G_TYPE_STRING, G_TYPE_DOUBLE, \
	G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_DOUBLE, G_TYPE_STRING, \
	G_TYPE_STRING, G_TYPE_DOUBLE);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), \
	    GTK_TREE_MODEL(azymut_model));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;
	
	// kolumna PP Nr
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("PP Nr"), renderer, "text", COL_AZ_PPNR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna PK Nr
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("PK Nr"), renderer, "text", COL_AZ_PKNR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna Azymut
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Azymut"), renderer, "text", COL_AZ_AZYM, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 120);

	// kolumna Dlugosc
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Długość"), renderer, "text", COL_AZ_DL, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 120);
}


void on_tbtnAzymutWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	// Czyszczenie listy
	GtkWidget *treeAzymut = lookup_widget(GTK_WIDGET(toolbutton), "treeAzymut");
	GtkTreeModel *modelAzymut = gtk_tree_view_get_model(GTK_TREE_VIEW(treeAzymut));
	gtk_tree_store_clear(GTK_TREE_STORE(modelAzymut));	
	
	// Czyszczenie wszystkich pol
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryAzymutPPNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryAzymutPPX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryAzymutPPY");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryAzymutPKNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryAzymutPKX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryAzymutPKY");
}


void on_tbtnAzymutUsun_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeAzymut");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreePath *tpath;
	GtkTreeIter iter;
	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarAzymut", _("Zaznacz najpierw pozycję w tabeli"));

	gtk_tree_path_free(tpath);
}


void on_tbtnAzymutRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	const guint NCOLS = 4;
	guint i;

	GtkWidget *dialogRaport = create_dialogRaport();
	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");
	gtk_widget_show(dialogRaport);

	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeAzymut");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	const guint NROWS = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	
	guint colsize[NCOLS] = { 10, 10, 20, 15 };
	char **tab_cell = new char*[(NROWS+1)*NCOLS];


	tab_cell[0] = g_strdup(_("Pkt P"));
	tab_cell[1] = g_strdup(_("Pkt K"));
	tab_cell[2] = g_strdup(_("Azymut"));
	tab_cell[3] = g_strdup(_("Długość"));

	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);	
	for (i=1; i<=NROWS; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_AZ_PPNR, &tab_cell[i*NCOLS+0],
			COL_AZ_PKNR, &tab_cell[i*NCOLS+1],
			COL_AZ_AZYM, &tab_cell[i*NCOLS+2],
			COL_AZ_DL, &tab_cell[i*NCOLS+3], -1);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	geod_raport_naglowek(textview);
	geod_raport_table(textview, tab_cell, NCOLS, NROWS, colsize, _("Obliczenie azymutu i odługości odcinka na podstawie współrzędnych"));
	geod_raport_stopka(textview);

	for (i=0; i<(NROWS+1)*NCOLS; i++)
		g_free(tab_cell[i]);
}


void on_tbtnAzymutWidok_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeAzymut");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	GtkTreePath *tpath;
	char *ppnr = NULL, *pknr = NULL;
	double ppx, ppy, pkx, pky;
	GtkTreeIter iter;
	cPunkt pktAzymut[2];
	GdkPoint pktWidok[3];

	GtkWidget *winWidok;
	GtkWidget *darea;

	gtk_tree_view_get_cursor(GTK_TREE_VIEW(treeview), &tpath, NULL);
	if (tpath != NULL)
	{
		winWidok = create_winWidok();
		gtk_widget_show(winWidok);
		darea = lookup_widget(GTK_WIDGET(winWidok), "drawingAreaWidok");

		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_AZ_PPNR, &ppnr, COL_AZ_PPX, &ppx, COL_AZ_PPY, &ppy,
			COL_AZ_PKNR, &pknr, COL_AZ_PKX, &pkx, COL_AZ_PKY, &pky, -1);

		pktAzymut[0].setNr(ppnr);
		pktAzymut[0].setXY(ppx, ppy);
		pktAzymut[1].setNr(pknr);
		pktAzymut[1].setXY(pkx, pky);
		geod_view_transform(GTK_WIDGET(darea), pktAzymut, pktWidok, 2);
		pktWidok[2].x = pktWidok[0].x;
		pktWidok[2].y = 0;
		
		geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[1], LINE_KIERUNEK);
		geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[2], LINE_POLNOC);
		geod_draw_arc(GTK_WIDGET(darea), pktWidok[0], pktWidok[1], pktWidok[2], 1);
		geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktAzymut[0].getNr());
		geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktAzymut[1].getNr());
		g_free(ppnr);
		g_free(pknr);
		gtk_tree_path_free(tpath);
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarAzymut", _("Wybierz pozycję w tabeli"));
}


void on_tbtnAzymutZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winAzymut");
	gtk_widget_destroy(window);
}


void on_treeAzymut_cursor_changed (GtkTreeView *treeview, gpointer user_data)
{
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	GtkTreePath *tpath;
	GtkTreeIter iter;
	char *ppnr = NULL, *pknr = NULL, *sbar_msg = NULL;
	double ppx, ppy, pkx, pky;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	gtk_tree_view_get_cursor(GTK_TREE_VIEW(treeview), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_AZ_PPNR, &ppnr, COL_AZ_PPX, &ppx, COL_AZ_PPY, &ppy,
			COL_AZ_PKNR, &pknr, COL_AZ_PKX, &pkx, COL_AZ_PKY, &pky, -1);

		sbar_msg = g_strdup_printf("%s(%.*f; %.*f)  %s(%.*f; %.*f)", ppnr,
			dok_xy_int, ppx, dok_xy_int, ppy, pknr, dok_xy_int, pkx, dok_xy_int, pky);
		geod_sbar_set(GTK_WIDGET(treeview), "sbarAzymut", sbar_msg);
		g_free(ppnr);
		g_free(pknr);
		g_free(sbar_msg);
		gtk_tree_path_free(tpath);
	}
}


gboolean on_entryAzymutPPNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarAzymut");
	return FALSE;
}


gboolean on_entryAzymutPPNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarAzymut");
	return FALSE;
}


gboolean on_entryAzymutPKNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarAzymut");
	return FALSE;
}


gboolean on_entryAzymutPKNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarAzymut");
	return FALSE;
}


void on_btnAzymutDodaj_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktPoczatek, pktKoniec;
	char *txtAzymut = NULL;
	char *txtDlugosc = NULL;
	double obliczony_azymut, obliczona_dlugosc;
	GtkTreeIter iter;
	GtkWidget *treeAzymut = lookup_widget(GTK_WIDGET(button), "treeAzymut");
	GtkTreeModel *modelAzymut = gtk_tree_view_get_model(GTK_TREE_VIEW(treeAzymut));
	int geod_error;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryAzymutPPNr",
		"entryAzymutPPX", "entryAzymutPPY", NULL, pktPoczatek));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarAzymut", _("Musisz podać Nr, X, Y punktu początkowego"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryAzymutPKNr",
		"entryAzymutPKX", "entryAzymutPKY", NULL, pktKoniec));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarAzymut", _("Musisz podać Nr, X, Y punktu końcowego"));
		return;
	}

	geod_error = azymut(pktPoczatek, pktKoniec, obliczony_azymut);
	if (geod_error != GEODERR_OK) {
		geod_sbar_set_error(GTK_WIDGET(button), "sbarAzymut", geod_error);
		return;
	}

 	obliczona_dlugosc = dlugosc(pktPoczatek, pktKoniec);
	txtDlugosc = g_strdup_printf("%.*f", dok_xy_int, obliczona_dlugosc);
	txtAzymut = geod_kat_from_rad(obliczony_azymut);
	
	gtk_tree_store_append(GTK_TREE_STORE(modelAzymut), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(modelAzymut), &iter,
		COL_AZ_PPNR, pktPoczatek.getNr(), COL_AZ_PPX, pktPoczatek.getX(), COL_AZ_PPY, pktPoczatek.getY(),
		COL_AZ_PKNR, pktKoniec.getNr(), COL_AZ_PKX, pktKoniec.getX(), COL_AZ_PKY, pktKoniec.getY(),
		COL_AZ_AZYM, txtAzymut, COL_AZ_DL_DOUBLE, obliczona_dlugosc, COL_AZ_DL, txtDlugosc, -1);
	g_free(txtAzymut);
	g_free(txtDlugosc);
	
	// wyczyszczenie wszystkich pol
	geod_entry_clear(GTK_WIDGET(button), "entryAzymutPPNr");
	geod_entry_clear(GTK_WIDGET(button), "entryAzymutPPX");
	geod_entry_clear(GTK_WIDGET(button), "entryAzymutPPY");
	geod_entry_clear(GTK_WIDGET(button), "entryAzymutPKNr");
	geod_entry_clear(GTK_WIDGET(button), "entryAzymutPKX");
	geod_entry_clear(GTK_WIDGET(button), "entryAzymutPKY");
	GtkWidget *focus = lookup_widget(GTK_WIDGET(button), "entryAzymutPPNr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


/* winDomiary */
void on_winDomiary_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winDomiary");
}


void on_winDomiary_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeDomiary");
	GtkTreeStore *domiary_model;
	
	domiary_model=gtk_tree_store_new(N_DOM_COLUMNS, \
	G_TYPE_STRING, G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, \
	G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, \
	G_TYPE_DOUBLE, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_INT, G_TYPE_BOOLEAN);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), \
	    GTK_TREE_MODEL(domiary_model));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;
	
	// kolumna Nr
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Nr"), renderer, "text", COL_DOM_NR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// kolumna miara biezaca
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Bieżąca"), renderer, "text", COL_DOM_B, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna domiar
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Domiar"), renderer, "text", COL_DOM_D, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna wsp. X
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("X"), renderer, "text", COL_DOM_X, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 80);

	// kolumna wsp. Y
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Y"), renderer, "text", COL_DOM_Y, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 80);

	// kolumna Kod
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Kod"), renderer, "text", COL_DOM_KOD, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// kolumna Toggle
	GtkCellRenderer *rendererToggle = gtk_cell_renderer_toggle_new();
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("D"), rendererToggle, "active", COL_DOM_TOGGLE, NULL);
	GtkTreeViewColumn *toggleColumn = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(toggleColumn), 10);
}


void on_tbtnDomiaryOblicz_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt *pktDomiar = NULL;
	cPunkt *pktRzut = NULL;
	cPunkt pktBazaP, pktBazaK;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeDomiary");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	guint l_wierszy = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	guint i, s_dodaj;
	double biezaca, domiar, bazaPom;
	char *pkt_nr = NULL, *pkt_kod = NULL, *txtX = NULL, *txtY = NULL;
	int geod_error, zadanie_id;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryDomiaryBazaPPNr",
		"entryDomiaryBazaPPX", "entryDomiaryBazaPPY", NULL, pktBazaP));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarDomiary", _("Musisz podać Nr, X, Y pierwszego punktu bazy"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryDomiaryBazaPKNr",
		"entryDomiaryBazaPKX", "entryDomiaryBazaPKY", NULL, pktBazaK));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarDomiary", _("Musisz podać Nr, X, Y drugiego punktu bazy"));
		return;
	}

	if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryDomiaryBazaPom", bazaPom));
	else bazaPom = 0.0; // do obliczen uzyta bedzie dlugosc wyliczona ze wspolrzednych
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (i = 0; i < l_wierszy; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_DOM_NR, &pkt_nr,
			COL_DOM_B_DOUBLE, &biezaca, COL_DOM_D_DOUBLE, &domiar, COL_DOM_KOD, &pkt_kod,
			COL_DOM_DODAJ, &s_dodaj, -1);

		pktDomiar = new cPunkt;
		pktRzut = new cPunkt;
		pktDomiar->setNr(pkt_nr);
		pktDomiar->setKod(pkt_kod);
		pktRzut->setNr(pkt_nr);
		pktRzut->setKod(pkt_kod);

		geod_error = domiar_prostokatny(pktBazaP, pktBazaK, *pktDomiar, biezaca, domiar, bazaPom);
		if (geod_error == GEODERR_OK)	{
			txtX = g_strdup_printf("%.*f", dok_xy_int, pktDomiar->getX());
			txtY = g_strdup_printf("%.*f", dok_xy_int, pktDomiar->getY());
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
				COL_DOM_X, txtX, COL_DOM_X_DOUBLE, pktDomiar->getX(),
				COL_DOM_Y, txtY, COL_DOM_Y_DOUBLE, pktDomiar->getY(), -1);

			switch (s_dodaj) {
				case PKT_DODAJ:
					if (dbgeod_point_add_config(*pktDomiar) != GEODERR_OK) {
						geod_sbar_set(GTK_WIDGET(toolbutton), "sbarDomiary", _("Nie mogę dodać punktu do bazy danych!"));
						gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_DOM_DODAJ, PKT_ERROR, -1);
					}
					else {
						gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_DOM_DODAJ, PKT_DODANY, -1);
						dbgeod_hist_add("Domiary", zadanie_id);
						dbgeod_hist_point_set(zadanie_id, "BazaPPNr", pktBazaP.getNr(), 0);
						dbgeod_hist_point_set(zadanie_id, "BazaPKNr", pktBazaK.getNr(), 0);
						dbgeod_hist_point_set(zadanie_id, "WNr", pktDomiar->getNr(), 1);
						dbgeod_hist_par_set(zadanie_id, "bazaPomierzona", bazaPom, 0);
						dbgeod_hist_par_set(zadanie_id, "biezaca", biezaca, 0);
						dbgeod_hist_par_set(zadanie_id, "domiar", domiar, 0);
					}
				break;
				
				case PKT_DODANY:
					dbgeod_point_update(-1, *pktDomiar, TRUE);
				break;
			}
			g_free(txtX);
			g_free(txtY);
		}
		else {
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
				COL_DOM_X, "", COL_DOM_X_DOUBLE, 0.0, COL_DOM_Y, "", COL_DOM_Y_DOUBLE, 0.0, -1);
			geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarDomiary", geod_error);
		}
		
		geod_error = domiar_prostokatny(pktBazaP, pktBazaK, *pktRzut, biezaca, 0, bazaPom);
		if (geod_error == GEODERR_OK)
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
				COL_DOM_X0_DOUBLE, pktRzut->getX(), COL_DOM_Y0_DOUBLE, pktRzut->getY(), -1);
		else {
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
				COL_DOM_X0_DOUBLE, 0.0, COL_DOM_Y0_DOUBLE, 0.0, -1);
			geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarDomiary", geod_error);
		}
		
		g_free(pkt_nr);
		g_free(pkt_kod);
		delete pktRzut;
		delete pktDomiar;
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
}


void on_tbtnDomiaryWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	// Czyszczenie listy
	GtkWidget *treeDomiary = lookup_widget(GTK_WIDGET(toolbutton), "treeDomiary");
	GtkTreeModel *modelDomiary = gtk_tree_view_get_model(GTK_TREE_VIEW(treeDomiary));
	gtk_tree_store_clear(GTK_TREE_STORE(modelDomiary));
	
	// Czyszczenie wszystkich pol
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryDomiaryBazaPPNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryDomiaryBazaPPX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryDomiaryBazaPPY");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryDomiaryBazaPKNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryDomiaryBazaPKX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryDomiaryBazaPKY");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryDomiaryBazaPom");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryDomiaryBazaWyl");

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryDomiaryMiaryNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryDomiaryMiaryKod");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryDomiaryMiaryB");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryDomiaryMiaryD");
	GtkWidget *focus = lookup_widget(GTK_WIDGET(toolbutton), "entryDomiaryBazaPPNr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


void on_tbtnDomiaryUsun_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeDomiary");
	GtkWidget *winDomiary = lookup_widget(GTK_WIDGET(toolbutton), "winDomiary");
	GtkWidget *dialog;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreePath *tpath;
	GtkTreeIter iter;
	char *pkt_nr = NULL;
	int s_dodaj;
	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);

	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_DOM_NR, &pkt_nr,
				COL_DOM_DODAJ, &s_dodaj, -1);
		if (s_dodaj == PKT_DODANY) {
			dialog = gtk_message_dialog_new(GTK_WINDOW(winDomiary), GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Usunąć także punkt z bazy danych?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES) dbgeod_point_del(0, pkt_nr);
			gtk_widget_destroy(dialog);
		}
		g_free(pkt_nr);
		gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarDomiary", _("Zaznacz najpierw pozycję w tabeli"));

	gtk_tree_path_free(tpath);
}


void on_tbtnDomiaryRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	const guint NCOLS = 6;
	GSList *lpunkty = NULL;
	cPunkt *punkt = NULL;
	guint i;
	char *txtBazaPom = NULL;
	double bazaPom;

	GtkWidget *dialogRaport = create_dialogRaport();
	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");
	gtk_widget_show(dialogRaport);

	GtkWidget *entryBazaWyl = lookup_widget(GTK_WIDGET(toolbutton), "entryDomiaryBazaWyl");
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeDomiary");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	const guint NROWS = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;

	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	
	guint colsize[NCOLS] = { 10, 13, 13, 15, 15, 4 };
	char **tab_cell = new char*[(NROWS+1)*NCOLS];

	geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\ndomiary prostokątne"));
	
	// baza
	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryDomiaryBazaPPNr",
		"entryDomiaryBazaPPX", "entryDomiaryBazaPPY", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		geod_raport_error(textview, _("Dla pierwszego punktu bazy nie podano wszystkich wymaganych danych"));
		delete punkt;
	}

	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryDomiaryBazaPKNr",
		"entryDomiaryBazaPKX", "entryDomiaryBazaPKY", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		geod_raport_error(textview, _("Dla drugiego punktu bazy nie podano wszystkich wymaganych danych"));
		delete punkt;
	}
	
	geod_raport_table_pkt(textview, lpunkty, FALSE, _("Baza"));
	g_slist_free(lpunkty);
	lpunkty = NULL;	

	geod_raport_line(textview, _("Długość wyliczona"), gtk_entry_get_text(GTK_ENTRY(entryBazaWyl)), TRUE);
	if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryDomiaryBazaPom", bazaPom)) {
		txtBazaPom = g_strdup_printf("%.*f", dok_xy_int, bazaPom);
		geod_raport_line(textview, _("Długość pomierzona"), txtBazaPom, TRUE);
		g_free(txtBazaPom);
	}

	// domiary	
	tab_cell[0] = g_strdup(_("Nr"));
	tab_cell[1] = g_strdup(_("Bieżąca"));
	tab_cell[2] = g_strdup(_("Domiar"));
	tab_cell[3] = g_strdup(_("X"));
	tab_cell[4] = g_strdup(_("Y"));
	tab_cell[5] = g_strdup(_("Kod"));
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);	
	for (i=1; i<=NROWS; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_DOM_NR, &tab_cell[i*NCOLS+0],
			COL_DOM_B, &tab_cell[i*NCOLS+1],
			COL_DOM_D, &tab_cell[i*NCOLS+2],
			COL_DOM_X, &tab_cell[i*NCOLS+3],
			COL_DOM_Y, &tab_cell[i*NCOLS+4],
			COL_DOM_KOD, &tab_cell[i*NCOLS+5], -1);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}

	geod_raport_table(textview, tab_cell, NCOLS, NROWS, colsize, _("\nMierzone punkty"));
	geod_raport_stopka(textview);

	for (i=0; i<(NROWS+1)*NCOLS; i++)
		g_free(tab_cell[i]);
}


void on_tbtnDomiaryWidok_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt *pktDane = NULL;
	GdkPoint *pktWidok = NULL;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeDomiary");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	guint l_wierszy = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	guint i;
	char *pkt_nr = NULL;
	double pkt_x, pkt_y, pkt_x0, pkt_y0;

	GtkWidget *winWidok = create_winWidok();
	gtk_widget_show(winWidok);
	GtkWidget *darea = lookup_widget(GTK_WIDGET(winWidok), "drawingAreaWidok");
	
	pktDane = new cPunkt[2+2*l_wierszy];
	
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryDomiaryBazaPPNr",
		"entryDomiaryBazaPPX", "entryDomiaryBazaPPY", NULL, pktDane[0]));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarDomiary", _("Musisz podać Nr, X, Y pierwszego punktu bazy"));
		delete [] pktDane;
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryDomiaryBazaPKNr",
		"entryDomiaryBazaPKX", "entryDomiaryBazaPKY", NULL, pktDane[1]));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarDomiary", _("Musisz podać Nr, X, Y drugiego punktu bazy"));
		delete [] pktDane;
		return;
	}

	pktWidok = new GdkPoint[2+2*l_wierszy];
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (i = 0; i < l_wierszy; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_DOM_NR, &pkt_nr,
		COL_DOM_X_DOUBLE, &pkt_x, COL_DOM_Y_DOUBLE, &pkt_y,
		COL_DOM_X0_DOUBLE, &pkt_x0, COL_DOM_Y0_DOUBLE, &pkt_y0, -1);
		pktDane[2+i].setNr(pkt_nr);
		pktDane[2+i].setXY(pkt_x, pkt_y);
		pktDane[2+i+l_wierszy].setNr(pkt_nr);
		pktDane[2+i+l_wierszy].setXY(pkt_x0, pkt_y0);
		g_free(pkt_nr);

		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, 2+2*l_wierszy);
	
	// rysowanie bazy
	geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[1], LINE_BAZA);
	geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
	geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[1].getNr(), PKT_OSNOWA);
	// rysowanie punktow
	for (i = 0; i < l_wierszy; i++) {
		geod_draw_line(GTK_WIDGET(darea), pktWidok[i+2], pktWidok[i+2+l_wierszy], LINE_POLNOC);
		geod_draw_point(GTK_WIDGET(darea), pktWidok[i+2], pktDane[i+2].getNr(), PKT_WYNIK);
	}
	gtk_widget_queue_draw(darea);
	delete [] pktDane;
	delete [] pktWidok;
}


void on_tbtnDomiaryZamknij_clicked  (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winDomiary");
	gtk_widget_destroy(window);
}


gboolean on_entryDomiaryBazaPPNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarDomiary");
	return FALSE;
}


gboolean on_entryDomiaryBazaPPNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarDomiary");
	return FALSE;
}


gboolean on_entryDomiaryBazaPKNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarDomiary");
	return FALSE;
}


gboolean on_entryDomiaryBazaPKNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarDomiary");
	return FALSE;
}


gboolean on_entryDomiaryMiaryNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	GtkWidget *entryB = lookup_widget(GTK_WIDGET(widget), "entryDomiaryMiaryB");
	GtkWidget *entryD = lookup_widget(GTK_WIDGET(widget), "entryDomiaryMiaryD");
	GtkWidget *entryPPNr = lookup_widget(GTK_WIDGET(widget), "entryDomiaryBazaPPNr");
	GtkWidget *entryPKNr = lookup_widget(GTK_WIDGET(widget), "entryDomiaryBazaPKNr");
	GtkWidget *entryMiaryNr = lookup_widget(GTK_WIDGET(widget), "entryDomiaryMiaryNr");
	char *par1 = NULL, *par2 = NULL, *par3 = NULL;
	
	par1 = g_strdup_printf("$%s", gtk_entry_get_text(GTK_ENTRY(entryMiaryNr)));
	par2 = g_strdup_printf("$%s", gtk_entry_get_text(GTK_ENTRY(entryPPNr)));
	par3 = g_strdup_printf("$%s", gtk_entry_get_text(GTK_ENTRY(entryPKNr)));
	
	geod_update_entry_obs(GTK_WIDGET(entryB), par1, par2, par3, NULL);
	geod_update_entry_obs(GTK_WIDGET(entryD), par1, par2, "entryDomiaryBazaPKNr", NULL);
	
	g_free(par1);
	g_free(par2);
	g_free(par3);
	return FALSE;
}


gboolean on_entryDomiaryKod_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Kod_set(widget, "sbarDomiary");
	return FALSE;
}


gboolean on_entryDomiaryKod_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarDomiary");
	return FALSE;
}


void on_frameDomiaryBaza_set_focus_child (GtkContainer *container, GtkWidget *widget, gpointer user_data)
{
	char *txtBazaWyl = NULL;
	GtkWidget *entryBazaWyl = lookup_widget(GTK_WIDGET(container), "entryDomiaryBazaWyl");
	GtkWidget *entryBazaPPX = lookup_widget(GTK_WIDGET(container), "entryDomiaryBazaPPX");
	GtkWidget *entryBazaPPY = lookup_widget(GTK_WIDGET(container), "entryDomiaryBazaPPY");
	GtkWidget *entryBazaPKX = lookup_widget(GTK_WIDGET(container), "entryDomiaryBazaPKX");
	GtkWidget *entryBazaPKY = lookup_widget(GTK_WIDGET(container), "entryDomiaryBazaPKY");
	const char *txtPPX = gtk_entry_get_text(GTK_ENTRY(entryBazaPPX));
	const char *txtPPY = gtk_entry_get_text(GTK_ENTRY(entryBazaPPY));
	const char *txtPKX = gtk_entry_get_text(GTK_ENTRY(entryBazaPKX));
	const char *txtPKY = gtk_entry_get_text(GTK_ENTRY(entryBazaPKY));
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if (g_str_equal(txtPPX, "") || g_str_equal(txtPPY, "") || g_str_equal(txtPKX, "") ||
		g_str_equal(txtPKY, ""))
		gtk_editable_delete_text(GTK_EDITABLE(entryBazaWyl), 0, -1);
	else {
		txtBazaWyl = g_strdup_printf("%.*f",  dok_xy_int, dlugosc(g_strtod(txtPPX, NULL), g_strtod(txtPPY, NULL), g_strtod(txtPKX, NULL), g_strtod(txtPKY, NULL)));
		gtk_entry_set_text(GTK_ENTRY(entryBazaWyl), txtBazaWyl);
		g_free(txtBazaWyl);
	}
}


void on_btnDomiaryMiaryDodaj_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *entryMiaryNr = lookup_widget(GTK_WIDGET(button), "entryDomiaryMiaryNr");
	GtkWidget *entryMiaryKod = lookup_widget(GTK_WIDGET(button), "entryDomiaryMiaryKod");
	GtkWidget *cbtnNoAdd = lookup_widget(GTK_WIDGET(button), "cbtnDomiaryMiaryDodajLista");
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeDomiary");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreeIter iter;
	double biezaca, domiar;
	char *txtB = NULL, *txtD = NULL, *txtNr = NULL, *txtKod = NULL;
	int s_dodaj;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if (!geod_entry_get_double(GTK_WIDGET(button), "entryDomiaryMiaryB", biezaca));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarDomiary", _("Musisz podać miarę bieżącą"));
		return;
	}

	if (!geod_entry_get_double(GTK_WIDGET(button), "entryDomiaryMiaryD", domiar));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarDomiary", _("Musisz podać wartość domiaru"));
		return;
	}
	
	txtB = g_strdup_printf("%.*f", dok_xy_int, biezaca);
	txtD = g_strdup_printf("%.*f", dok_xy_int, domiar);
	txtNr = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryMiaryNr)));
	txtKod = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryMiaryKod)));

	if (g_str_equal(txtNr, "")) {
		geod_sbar_set(GTK_WIDGET(button), "sbarDomiary", _("Musisz podać Nr punktu z domiarów"));
		gtk_widget_grab_focus(GTK_WIDGET(entryMiaryNr));
		return;
	}
	else if (!dbgeod_point_find(txtNr) && !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnNoAdd))) {
		geod_sbar_set(GTK_WIDGET(button), "sbarDomiary", _("Punkt o takim numerze znajduje się już w bazie"));
		gtk_widget_grab_focus(GTK_WIDGET(entryMiaryNr));
		return;
	}
	
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnNoAdd)))
		s_dodaj = PKT_NIE_DODAWAJ;
	else 
		s_dodaj = PKT_DODAJ;

	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
		COL_DOM_NR, txtNr,
		COL_DOM_B, txtB, COL_DOM_B_DOUBLE, biezaca,
		COL_DOM_D, txtD, COL_DOM_D_DOUBLE, domiar, 
		COL_DOM_KOD, txtKod, COL_DOM_DODAJ, s_dodaj, -1);

	if (s_dodaj == PKT_DODAJ)
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_DOM_TOGGLE, TRUE, -1);
	else
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_DOM_TOGGLE, FALSE, -1);
	
	g_free(txtB);
	g_free(txtD);
	g_free(txtNr);
	g_free(txtKod);
	
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(button), "tbtnDomiaryOblicz");
	g_signal_emit_by_name(btnOblicz, "clicked");
	
	// wyczyszczenie wszystkich pol
	geod_entry_clear(GTK_WIDGET(button), "entryDomiaryMiaryD");
	geod_entry_clear(GTK_WIDGET(button), "entryDomiaryMiaryB");
	geod_entry_inc(GTK_WIDGET(button), "entryDomiaryMiaryNr");
	geod_entry_clear(GTK_WIDGET(button), "entryDomiaryMiaryKod");
	GtkWidget *focus = lookup_widget(GTK_WIDGET(button), "entryDomiaryMiaryNr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


/* winKat */
void on_winKat_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winKat");
}


void on_winKat_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeKat");
	GtkTreeStore *kat_model;
	
	kat_model=gtk_tree_store_new(N_KAT_COLUMNS, \
	G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_DOUBLE, \
	G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_DOUBLE, \
	G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_DOUBLE, \
	G_TYPE_STRING);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), \
	    GTK_TREE_MODEL(kat_model));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;
	
	// kolumna Pkt C Nr
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("St"), renderer, "text", COL_KAT_CNR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna Pkt Lewy Nr
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Lewy"), renderer, "text", COL_KAT_LNR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna Pkt Prawy Nr
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Prawy"), renderer, "text", COL_KAT_PNR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna Kat
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Kąt"), renderer, "text", COL_KAT_KAT, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 130);
}


void on_tbtnKatWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	// Czyszczenie listy
	GtkWidget *treeKat = lookup_widget(GTK_WIDGET(toolbutton), "treeKat");
	GtkTreeModel *modelKat = gtk_tree_view_get_model(GTK_TREE_VIEW(treeKat));
	gtk_tree_store_clear(GTK_TREE_STORE(modelKat));
	
	// Czyszczenie wszystkich pol
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryKatCNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryKatCX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryKatCY");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryKatLNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryKatLX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryKatLY");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryKatPNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryKatPX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryKatPY");
	GtkWidget *focus = lookup_widget(GTK_WIDGET(toolbutton), "entryKatCNr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


void on_tbtnKatUsun_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeKat");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreePath *tpath;
	GtkTreeIter iter;
	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarKat", _("Zaznacz najpierw pozycję w tabeli"));

	gtk_tree_path_free(tpath);
}


void on_tbtnKatRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	const guint NCOLS = 4;
	guint i;
	
	GtkWidget *dialogRaport = create_dialogRaport();
	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");
	gtk_widget_show(dialogRaport);

	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeKat");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	const guint NROWS = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	
	guint colsize[NCOLS] = { 10, 10, 10, 20 };
	char **tab_cell = new char*[(NROWS+1)*NCOLS];

	tab_cell[0] = g_strdup(_("Pkt C"));
	tab_cell[1] = g_strdup(_("Pkt L"));
	tab_cell[2] = g_strdup(_("Pkt P"));
	tab_cell[3] = g_strdup(_("Kąt"));

	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);	
	for (i=1; i<=NROWS; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_KAT_CNR, &tab_cell[i*NCOLS+0],
			COL_KAT_LNR, &tab_cell[i*NCOLS+1],
			COL_KAT_PNR, &tab_cell[i*NCOLS+2],
			COL_KAT_KAT, &tab_cell[i*NCOLS+3], -1);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	geod_raport_naglowek(textview);
	geod_raport_table(textview, tab_cell, NCOLS, NROWS, colsize, _("Obliczenie kąta na podstawie współrzędnych"));
	geod_raport_stopka(textview);

	for (i=0; i<(NROWS+1)*NCOLS; i++)
		g_free(tab_cell[i]);
}


void on_tbtnKatWidok_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeKat");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	GtkTreePath *tpath;
	char *lnr = NULL, *pnr = NULL, *cnr = NULL;
	double lx, ly, px, py, cx, cy;
	GtkTreeIter iter;
	cPunkt pktAzymut[3];
	GdkPoint pktWidok[3];

	GtkWidget *winWidok;
	GtkWidget *darea;

	gtk_tree_view_get_cursor(GTK_TREE_VIEW(treeview), &tpath, NULL);
	if (tpath != NULL) {
		winWidok = create_winWidok();
		gtk_widget_show(winWidok);
		darea = lookup_widget(GTK_WIDGET(winWidok), "drawingAreaWidok");

		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_KAT_LNR, &lnr, COL_KAT_LX, &lx, COL_KAT_LY, &ly,
			COL_KAT_CNR, &cnr, COL_KAT_CX, &cx, COL_KAT_CY, &cy,
			COL_KAT_PNR, &pnr, COL_KAT_PX, &px, COL_KAT_PY, &py, -1);

		pktAzymut[0].setNr(cnr);
		pktAzymut[0].setXY(cx, cy);
		pktAzymut[1].setNr(lnr);
		pktAzymut[1].setXY(lx, ly);
		pktAzymut[2].setNr(pnr);
		pktAzymut[2].setXY(px, py);
		geod_view_transform(GTK_WIDGET(darea), pktAzymut, pktWidok, 3);
		
		geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[1], LINE_KIERUNEK);
		geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[2], LINE_KIERUNEK);
		geod_draw_arc(GTK_WIDGET(darea), pktWidok[0], pktWidok[2], pktWidok[1], 1);
		geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktAzymut[0].getNr(), PKT_WYNIK);
		geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktAzymut[1].getNr());
		geod_draw_point(GTK_WIDGET(darea), pktWidok[2], pktAzymut[2].getNr());
		g_free(cnr);
		g_free(lnr);
		g_free(pnr);
		gtk_tree_path_free(tpath);
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarKat", _("Wybierz pozycję w tabeli"));
}


void on_tbtnKatZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winKat");
	gtk_widget_destroy(window);
}


void on_treeKat_cursor_changed (GtkTreeView *treeview, gpointer ser_data)
{
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	GtkTreeIter iter;
	GtkTreePath *tpath;
	char *lnr = NULL, *pnr = NULL, *cnr = NULL, *sbar_msg = NULL;
	double lx, ly, px, py, cx, cy;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	gtk_tree_view_get_cursor(GTK_TREE_VIEW(treeview), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_KAT_LNR, &lnr, COL_KAT_LX, &lx, COL_KAT_LY, &ly,
			COL_KAT_CNR, &cnr, COL_KAT_CX, &cx, COL_KAT_CY, &cy,
			COL_KAT_PNR, &pnr, COL_KAT_PX, &px, COL_KAT_PY, &py, -1);

		sbar_msg = g_strdup_printf("%s(%.*f; %.*f)  %s(%.*f; %.*f)  %s(%.*f; %.*f)",
			cnr, dok_xy_int, cx, dok_xy_int, cy, lnr, dok_xy_int, lx, dok_xy_int, ly,
			pnr, dok_xy_int, px, dok_xy_int, py);

		geod_sbar_set(GTK_WIDGET(treeview), "sbarKat", sbar_msg);
		g_free(cnr);
		g_free(lnr);
		g_free(pnr);
		g_free(sbar_msg);
		gtk_tree_path_free(tpath);
	}
}


void on_btnKatDodaj_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktLewy, pktPrawy, pktStanowisko;
	double obliczony_kat;
	char *txtObliczonyKat = NULL;
	GtkTreeIter iter;
	GtkWidget *treeKat = lookup_widget(GTK_WIDGET(button), "treeKat");
	GtkTreeModel *modelKat = gtk_tree_view_get_model(GTK_TREE_VIEW(treeKat));

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryKatCNr",
		"entryKatCX", "entryKatCY", NULL, pktStanowisko));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarKat", _("Musisz podać Nr, X, Y stanowiska"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryKatLNr",
		"entryKatLX", "entryKatLY", NULL, pktLewy));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarKat", _("Musisz podać Nr, X, Y lewego punktu"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryKatPNr",
		"entryKatPX", "entryKatPY", NULL, pktPrawy));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarKat", _("Musisz podać Nr, X, Y prawego punktu"));
		return;
	}

	obliczony_kat = kat(pktStanowisko, pktLewy, pktPrawy);
	txtObliczonyKat = geod_kat_from_rad(obliczony_kat);
 	gtk_tree_store_append(GTK_TREE_STORE(modelKat), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(modelKat), &iter,
		COL_KAT_CNR, pktStanowisko.getNr(), COL_KAT_CX, pktStanowisko.getX(), COL_KAT_CY, pktStanowisko.getY(),
		COL_KAT_LNR, pktLewy.getNr(), COL_KAT_LX, pktLewy.getX(), COL_KAT_LY, pktLewy.getY(),
		COL_KAT_PNR, pktPrawy.getNr(), COL_KAT_PX, pktPrawy.getX(), COL_KAT_PY, pktPrawy.getY(),
		COL_KAT_KAT, txtObliczonyKat, -1);
	delete [] txtObliczonyKat;
	
	// wyczyszczenie wszystkich pol
	geod_entry_clear(GTK_WIDGET(button), "entryKatCNr");
	geod_entry_clear(GTK_WIDGET(button), "entryKatCX");
	geod_entry_clear(GTK_WIDGET(button), "entryKatCY");
	geod_entry_clear(GTK_WIDGET(button), "entryKatLNr");
	geod_entry_clear(GTK_WIDGET(button), "entryKatLX");
	geod_entry_clear(GTK_WIDGET(button), "entryKatLY");
	geod_entry_clear(GTK_WIDGET(button), "entryKatPNr");
	geod_entry_clear(GTK_WIDGET(button), "entryKatPX");
	geod_entry_clear(GTK_WIDGET(button), "entryKatPY");
	GtkWidget *focus = lookup_widget(GTK_WIDGET(button), "entryKatCNr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


gboolean on_entryKatCNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarKat");
	return FALSE;
}


gboolean on_entryKatCNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarKat");
	return FALSE;
}


gboolean on_entryKatLNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarKat");
	return FALSE;
}


gboolean on_entryKatLNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarKat");
	return FALSE;
}


gboolean on_entryKatPNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarKat");
	return FALSE;
}


gboolean on_entryKatPNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarKat");
	return FALSE;
}


/* winTHelmerta */
void on_winTHelmerta_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winTHelmerta");
}


void on_winTHelmerta_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeTHD = lookup_widget(GTK_WIDGET(widget), "treeTHelmertaDost");
	GtkWidget *treeTHT = lookup_widget(GTK_WIDGET(widget), "treeTHelmertaTransf");
	GtkTreeStore *thd_model;
	GtkTreeStore *tht_model;
	
	thd_model=gtk_tree_store_new(N_THD_COLUMNS, \
	G_TYPE_STRING, G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, \
	G_TYPE_STRING, G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, \
	G_TYPE_STRING,  G_TYPE_STRING, G_TYPE_STRING);
	
	tht_model=gtk_tree_store_new(N_THT_COLUMNS, \
	G_TYPE_STRING, G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, \
	G_TYPE_STRING, G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, \
	G_TYPE_INT, G_TYPE_BOOLEAN);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeTHD), GTK_TREE_MODEL(thd_model));
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeTHT), GTK_TREE_MODEL(tht_model));
	
	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;
	
	// Punkty dostosowania - kolumna Nr P
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHD), \
	   -1, _("Nr P"), renderer, "text", COL_THD_NRP, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHD), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);
	
	// Punkty dostosowania - kolumna X P
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHD), \
	   -1, _("X P"), renderer, "text", COL_THD_XP, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHD), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);
	
	// Punkty dostosowania - kolumna Y P
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHD), \
	   -1, _("Y P"), renderer, "text", COL_THD_YP, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHD), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// Punkty dostosowania - kolumna Nr W
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHD), \
	   -1, _("Nr W"), renderer, "text", COL_THD_NRW, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHD), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);
	
	// Punkty dostosowania - kolumna X W
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHD), \
	   -1, _("X W"), renderer, "text", COL_THD_XW, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHD), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);
	
	// Punkty dostosowania - kolumna Y W
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHD), \
	   -1, _("Y W"), renderer, "text", COL_THD_YW, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHD), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// Punkty dostosowania - kolumna dx
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHD), \
	   -1, _("dx"), renderer, "text", COL_THD_DX, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHD), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);
	
	// Punkty dostosowania - kolumna dy
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHD), \
	   -1, _("dy"), renderer, "text", COL_THD_DY, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHD), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);
	
	// Punkty dostosowania - kolumna dp
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHD), \
	   -1, _("dp"), renderer, "text", COL_THD_DP, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHD), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);


	// Punkty do transformacji - kolumna Nr P
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHT), \
	   -1, _("Nr P"), renderer, "text", COL_THT_NRP, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHT), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);
	
	// Punkty do transformacji - kolumna X P
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHT), \
	   -1, _("X P"), renderer, "text", COL_THT_XP, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHT), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 90);
	
	// Punkty do transformacji - kolumna Y P
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHT), \
	   -1, _("Y P"), renderer, "text", COL_THT_YP, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHT), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 90);

	// Punkty do transformacji - kolumna Nr W
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHT), \
	   -1, _("Nr W"), renderer, "text", COL_THT_NRW, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHT), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// Punkty do transformacji - kolumna X W
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHT), \
	   -1, _("X W"), renderer, "text", COL_THT_XW, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHT), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 90);
	
	// Punkty do transformacji - kolumna Y W
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHT), \
	   -1, _("Y W"), renderer, "text", COL_THT_YW, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHT), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 90);

	// kolumna Toggle
	GtkCellRenderer *rendererToggle = gtk_cell_renderer_toggle_new();
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeTHT), \
	   -1, _("D"), rendererToggle, "active", COL_THT_TOGGLE, NULL);
	GtkTreeViewColumn *toggleColumn = gtk_tree_view_get_column (GTK_TREE_VIEW(treeTHT), col_offset -1);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(toggleColumn), 10);
}


void on_tbtnTHelmertaOblicz_clicked  (GtkToolButton *toolbutton, gpointer user_data)
{
	char *nrp = NULL, *nrw = NULL;
	double xp, yp, xw, yw;
	double par_u, par_v, xp_biegun, yp_biegun, xw_biegun, yw_biegun;
	int s_dodaj;
	char *txt_u = NULL, *txt_v = NULL;
	char *txt_xpb = NULL, *txt_ypb = NULL, *txt_xwb = NULL, *txt_ywb = NULL;
	char *txt_xw = NULL, *txt_yw = NULL;
	char *txt_dx = NULL, *txt_dy = NULL, *txt_dp = NULL;
	GtkWidget *tree = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeIter iter;
	unsigned int i;
	cPunkt (*pktDost)[2] = NULL;
	cPunkt pktTransfP, pktTransfW;
	unsigned int n_pkt;
	int geod_error;
	bool tab0 = FALSE;

	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookTHelmerta");
	GtkWidget *entryParU = lookup_widget(GTK_WIDGET(toolbutton), "entryTHelmertaParU");
	GtkWidget *entryParV = lookup_widget(GTK_WIDGET(toolbutton), "entryTHelmertaParV");
	GtkWidget *entryBiegunXp = lookup_widget(GTK_WIDGET(toolbutton), "entryTHelmertaBiegunXp");
	GtkWidget *entryBiegunYp = lookup_widget(GTK_WIDGET(toolbutton), "entryTHelmertaBiegunYp");
	GtkWidget *entryBiegunXw = lookup_widget(GTK_WIDGET(toolbutton), "entryTHelmertaBiegunXw");
	GtkWidget *entryBiegunYw = lookup_widget(GTK_WIDGET(toolbutton), "entryTHelmertaBiegunYw");

	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	char dok_par[CONFIG_VALUE_MAX_LEN];
	int dok_par_int = 11;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	if (!dbgeod_config_get("dok_par", dok_par)) dok_par_int = atoi(dok_par);
		
	switch (gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook))) {
	case 0: // parametry transformacji
		tab0 = TRUE;
		tree = lookup_widget(GTK_WIDGET(toolbutton), "treeTHelmertaDost");
		model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
		n_pkt = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);

		if (n_pkt < 2) {
			gtk_editable_delete_text(GTK_EDITABLE(entryParU), 0,  -1);
			gtk_editable_delete_text(GTK_EDITABLE(entryParV), 0,  -1);
			gtk_editable_delete_text(GTK_EDITABLE(entryBiegunXp), 0,  -1);
			gtk_editable_delete_text(GTK_EDITABLE(entryBiegunYp), 0,  -1);
			gtk_editable_delete_text(GTK_EDITABLE(entryBiegunXw), 0,  -1);
			gtk_editable_delete_text(GTK_EDITABLE(entryBiegunYw), 0,  -1);
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarTHelmerta", _("Musisz podać minimun 2 punkty dostosowania"));
		}
		else {
			pktDost = new cPunkt[n_pkt][2];
			gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
			for (i = 0; i < n_pkt; i++)	{
				gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
				COL_THD_NRP, &nrp, COL_THD_XP_DOUBLE, &xp, COL_THD_YP_DOUBLE, &yp,
				COL_THD_NRW, &nrw, COL_THD_XW_DOUBLE, &xw, COL_THD_YW_DOUBLE, &yw, -1);

				pktDost[i][0].setNr(nrp);
				pktDost[i][1].setNr(nrw);
				pktDost[i][0].setXY(xp, yp);
				pktDost[i][1].setXY(xw, yw);
					
				g_free(nrp);
				g_free(nrw);
				gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
			}

			geod_error = transformacja_helmerta_par(pktDost, n_pkt, &par_u, &par_v,
					&xp_biegun, &yp_biegun, &xw_biegun, &yw_biegun);
			if (geod_error == GEODERR_OK) {
				txt_u = g_strdup_printf("%.*f", dok_par_int, par_u);
				txt_v = g_strdup_printf("%.*f", dok_par_int, par_v);
				txt_xpb = g_strdup_printf("%.*f", dok_xy_int, xp_biegun);
				txt_ypb = g_strdup_printf("%.*f", dok_xy_int, yp_biegun);
				txt_xwb = g_strdup_printf("%.*f", dok_xy_int, xw_biegun);
				txt_ywb = g_strdup_printf("%.*f", dok_xy_int, yw_biegun);
				
				gtk_entry_set_text(GTK_ENTRY(entryParU), txt_u);
				gtk_entry_set_text(GTK_ENTRY(entryParV), txt_v);
				gtk_entry_set_text(GTK_ENTRY(entryBiegunXp), txt_xpb);
				gtk_entry_set_text(GTK_ENTRY(entryBiegunYp), txt_ypb);
				gtk_entry_set_text(GTK_ENTRY(entryBiegunXw), txt_xwb);
				gtk_entry_set_text(GTK_ENTRY(entryBiegunYw), txt_ywb);
				
				// oblicz blad transformacji
				gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
				for (i = 0; i < n_pkt; i++)	{
					gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
					COL_THD_NRP, &nrp, COL_THD_XP_DOUBLE, &xp, COL_THD_YP_DOUBLE, &yp,
					COL_THD_NRW, &nrw, COL_THD_XW_DOUBLE, &xw, COL_THD_YW_DOUBLE, &yw, -1);

					pktTransfP.setNr(nrp);
					pktTransfW.setNr(nrw);
					pktTransfP.setXY(xp, yp);
					if (!transformacja_helmerta(par_u, par_v, xp_biegun, yp_biegun,
						xw_biegun, yw_biegun, pktTransfP, pktTransfW)) {
				
						txt_dx = g_strdup_printf("%.*f", dok_xy_int, xw-pktTransfW.getX());
						txt_dy = g_strdup_printf("%.*f", dok_xy_int, yw-pktTransfW.getY());
						txt_dp = g_strdup_printf("%.*f", dok_xy_int, sqrt(pow(xw-pktTransfW.getX(),2)+pow(yw-pktTransfW.getY(),2)));
						gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
							COL_THD_DX, txt_dx, COL_THD_DY, txt_dy, COL_THD_DP, txt_dp, -1);
					}
					g_free(nrp);
					g_free(nrw);
					g_free(txt_dx);
					g_free(txt_dy);
					g_free(txt_dp);
					gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
				}
				
				g_free(txt_u);
				g_free(txt_v);
				g_free(txt_xpb);
				g_free(txt_ypb);
				g_free(txt_xwb);
				g_free(txt_ywb);
			}
			else
				geod_sbar_set_error(GTK_WIDGET(toolbutton), "sbarTHelmerta", geod_error);
			delete [] pktDost;
			pktDost = NULL;
		}
	// brak break - przelicz takze punkty do transformacji
		
	case 1: // punkty do transformacji
		tree = lookup_widget(GTK_WIDGET(toolbutton), "treeTHelmertaTransf");
		model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
		n_pkt = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);

		txt_u = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryParU)));
		txt_v = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryParV)));
		txt_xpb = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryBiegunXp)));
		txt_ypb = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryBiegunYp)));
		txt_xwb = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryBiegunXw)));
		txt_ywb = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryBiegunYw)));

		geod_error = FALSE;
		if (g_str_equal(txt_u, "")) geod_error = TRUE; else par_u = g_strtod(txt_u, NULL);
		if (g_str_equal(txt_v, "")) geod_error = TRUE; else	par_v = g_strtod(txt_v, NULL);
		if (g_str_equal(txt_xpb, "")) geod_error = TRUE; else xp_biegun = g_strtod(txt_xpb, NULL);
		if (g_str_equal(txt_ypb, "")) geod_error = TRUE; else yp_biegun = g_strtod(txt_ypb, NULL);
		if (g_str_equal(txt_xwb, "")) geod_error = TRUE; else xw_biegun = g_strtod(txt_xwb, NULL);
		if (g_str_equal(txt_ywb, "")) geod_error = TRUE; else yw_biegun = g_strtod(txt_ywb, NULL);

		g_free(txt_u);
		g_free(txt_v);
		g_free(txt_xpb);
		g_free(txt_ypb);
		g_free(txt_xwb);
		g_free(txt_ywb);

		gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
		
		if (geod_error) {
			for (i = 0; i < n_pkt; i++) {
				gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
					COL_THT_XW, "", COL_THT_XW_DOUBLE, 0.0,
					COL_THT_YW, "", COL_THT_YW_DOUBLE, 0.0, -1);
				gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
			}
			if (!tab0)
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarTHelmerta", _("Nie podano wszystkich parametrów transformacji"));
			break;
		}
		
		for (i = 0; i < n_pkt; i++) {
			gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
				COL_THT_NRP, &nrp, COL_THT_XP_DOUBLE, &xp, COL_THT_YP_DOUBLE, &yp,
				COL_THT_NRW, &nrw, COL_THT_DODAJ, &s_dodaj, -1);
			pktTransfP.setNr(nrp);
			pktTransfP.setXY(xp, yp);

			if (!transformacja_helmerta(par_u, par_v, xp_biegun, yp_biegun,
				xw_biegun, yw_biegun, pktTransfP, pktTransfW)) {
				txt_xw = g_strdup_printf("%.*f", dok_xy_int, pktTransfW.getX());
				txt_yw = g_strdup_printf("%.*f", dok_xy_int, pktTransfW.getY());
				gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
					COL_THT_XW, txt_xw, COL_THT_XW_DOUBLE, pktTransfW.getX(),
					COL_THT_YW, txt_yw, COL_THT_YW_DOUBLE, pktTransfW.getY(), -1);

				pktTransfW.setNr(nrw);
				switch (s_dodaj) {
					case PKT_DODAJ:
						if (dbgeod_point_add_config(pktTransfW) != GEODERR_OK) {
							geod_sbar_set(GTK_WIDGET(toolbutton), "sbarTHelmerta",
								_("Nie mogę dodać punktu do bazy danych!"));
							gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
								COL_THT_DODAJ, PKT_ERROR, -1);
						}
						else
							gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
								COL_THT_DODAJ, PKT_DODANY, -1);
						break;
					case PKT_DODANY:
						dbgeod_point_update(-1, pktTransfW, TRUE);
						break;
				}
				g_free(txt_xw);
				g_free(txt_yw);
			}
			g_free(nrp);
			g_free(nrw);
			gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
		}
	break;
	}
}


void on_tbtnTHelmertaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *treeDost = lookup_widget(GTK_WIDGET(toolbutton), "treeTHelmertaDost");
	GtkTreeModel *modelDost = gtk_tree_view_get_model(GTK_TREE_VIEW(treeDost));

	GtkWidget *treeTransf = lookup_widget(GTK_WIDGET(toolbutton), "treeTHelmertaTransf");
	GtkTreeModel *modelTransf = gtk_tree_view_get_model(GTK_TREE_VIEW(treeTransf));

	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookTHelmerta");
	GtkWidget *focus = NULL;

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaParU");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaParV");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaBiegunXp");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaBiegunYp");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaBiegunXw");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaBiegunYw");

	switch (gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook))) {
		case 0:
		// Czyszczenie wszystkich pol w zakladce Punkty Dostosowania
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaDostPNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaDostPX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaDostPY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaDostWNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaDostWX");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaDostWY");
		
			gtk_tree_store_clear(GTK_TREE_STORE(modelDost));
			focus = lookup_widget(GTK_WIDGET(toolbutton), "entryTHelmertaDostPNr");
		break;

		case 1:
		// Czyszczenie wszystkich pol w zakladce Punkty do Transformacji
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaTransfNr");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaTransfNrNowy");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaTransfY");
			geod_entry_clear(GTK_WIDGET(toolbutton), "entryTHelmertaTransfX");
			gtk_tree_store_clear(GTK_TREE_STORE(modelTransf));
			focus = lookup_widget(GTK_WIDGET(toolbutton), "entryTHelmertaParU");
		break;
	}
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


void on_tbtnTHelmertaUsun_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *winTHelmerta = lookup_widget(GTK_WIDGET(toolbutton), "winTHelmerta");
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(toolbutton), "tbtnTHelmertaOblicz");
	GtkWidget *dialog = NULL;
	GtkWidget *tree = NULL;
	GtkTreeModel *model = NULL;
	GtkTreePath *tpath = NULL;
	GtkTreeIter iter;
	int s_dodaj;
	char *pkt_nr = NULL;

	GtkWidget *notebook = lookup_widget(GTK_WIDGET(toolbutton), "notebookTHelmerta");
	gint page = gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook));
	
	if (page == 0)
		tree = lookup_widget(GTK_WIDGET(toolbutton), "treeTHelmertaDost");
	else if (page == 1)
		tree = lookup_widget(GTK_WIDGET(toolbutton), "treeTHelmertaTransf");

	model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);

	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		if (page == 1) {
			gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_THT_NRW, &pkt_nr,
				COL_THT_DODAJ, &s_dodaj, -1);
			if (s_dodaj == PKT_DODANY) {
				dialog = gtk_message_dialog_new(GTK_WINDOW(winTHelmerta), GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Usunąć także punkt z bazy danych?"));
				if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES) dbgeod_point_del(0, pkt_nr);
				gtk_widget_destroy(dialog);
			}
			g_free(pkt_nr);
		}
		gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
		g_signal_emit_by_name(btnOblicz, "clicked");
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarTHelmerta", _("Zaznacz najpierw pozycję w tabeli"));

	gtk_tree_path_free(tpath);
}


void on_tbtnTHelmertaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	const guint NCOLS = 6;
	char **tab_cell = NULL;
	guint i, nrows;
	
	GtkWidget *entryParU2 = lookup_widget(GTK_WIDGET(toolbutton), "entryTHelmertaParU");
	GtkWidget *entryParV2 = lookup_widget(GTK_WIDGET(toolbutton), "entryTHelmertaParV");
	GtkWidget *entryBiegunXp2 = lookup_widget(GTK_WIDGET(toolbutton), "entryTHelmertaBiegunXp");
	GtkWidget *entryBiegunYp2 = lookup_widget(GTK_WIDGET(toolbutton), "entryTHelmertaBiegunYp");
	GtkWidget *entryBiegunXw2 = lookup_widget(GTK_WIDGET(toolbutton), "entryTHelmertaBiegunXw");
	GtkWidget *entryBiegunYw2 = lookup_widget(GTK_WIDGET(toolbutton), "entryTHelmertaBiegunYw");

	GtkWidget *dialogRaport = create_dialogRaport();
	gtk_widget_show(dialogRaport);

	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");

	GtkWidget *treeDost = lookup_widget(GTK_WIDGET(toolbutton), "treeTHelmertaDost");
	GtkTreeModel *modelDost = gtk_tree_view_get_model(GTK_TREE_VIEW(treeDost));
	GtkWidget *treeTransf = lookup_widget(GTK_WIDGET(toolbutton), "treeTHelmertaTransf");
	GtkTreeModel *modelTransf = gtk_tree_view_get_model(GTK_TREE_VIEW(treeTransf));

	GtkTreeIter iter;
	
	guint colsize[NCOLS] = { 10, 15, 15, 10, 15, 15};
	
	nrows = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(modelDost), NULL);
	tab_cell = new char*[(nrows+1)*NCOLS];
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(modelDost), &iter);

	tab_cell[0] = g_strdup(_("Nr p"));
	tab_cell[1] = g_strdup(_("Xp"));
	tab_cell[2] = g_strdup(_("Yp"));
	tab_cell[3] = g_strdup(_("Nr w"));
	tab_cell[4] = g_strdup(_("Xw"));
	tab_cell[5] = g_strdup(_("Yw"));

	for (i=1; i<=nrows; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(modelDost), &iter,
			COL_THD_NRP, &tab_cell[i*NCOLS+0],
			COL_THD_XP, &tab_cell[i*NCOLS+1],
			COL_THD_YP, &tab_cell[i*NCOLS+2],
			COL_THD_NRW, &tab_cell[i*NCOLS+3],
			COL_THD_XW, &tab_cell[i*NCOLS+4],
			COL_THD_YW, &tab_cell[i*NCOLS+5], -1);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(modelDost), &iter);
	}

	geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\ntransforacja współrzędnych"));
	geod_raport_table(textview, tab_cell, NCOLS, nrows, colsize, _("Wykaz punktów dostosowania"));

	geod_raport_line(textview, _("Parametry transformacji"), "", TRUE);
	geod_raport_line(textview, _("Parametr u"), gtk_entry_get_text(GTK_ENTRY(entryParU2)));
	geod_raport_line(textview, _("Parametr v"), gtk_entry_get_text(GTK_ENTRY(entryParV2)));
	geod_raport_line(textview, _("Biegun Xp"), gtk_entry_get_text(GTK_ENTRY(entryBiegunXp2)));
	geod_raport_line(textview, _("Biegun Yp"), gtk_entry_get_text(GTK_ENTRY(entryBiegunYp2)));
	geod_raport_line(textview, _("Biegun Xw"), gtk_entry_get_text(GTK_ENTRY(entryBiegunXw2)));
	geod_raport_line(textview, _("Biegun Yw"), gtk_entry_get_text(GTK_ENTRY(entryBiegunYw2)));

	for (i=0; i<(nrows+1)*NCOLS; i++)
		g_free(tab_cell[i]);
	//poprawic -  moze zwolnic tablice wskaznikow ??
	
	nrows = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(modelTransf), NULL);
	tab_cell = new char*[(nrows+1)*NCOLS];
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(modelTransf), &iter);

	tab_cell[0] = g_strdup(_("Nr p"));
	tab_cell[1] = g_strdup(_("Xp"));
	tab_cell[2] = g_strdup(_("Yp"));
	tab_cell[3] = g_strdup(_("Nr w"));
	tab_cell[4] = g_strdup(_("Xw"));
	tab_cell[5] = g_strdup(_("Yw"));

	for (i=1; i<=nrows; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(modelTransf), &iter,
			COL_THT_NRP, &tab_cell[i*NCOLS+0],
			COL_THT_XP, &tab_cell[i*NCOLS+1],
			COL_THT_YP, &tab_cell[i*NCOLS+2],
			COL_THT_NRW, &tab_cell[i*NCOLS+3],
			COL_THT_XW, &tab_cell[i*NCOLS+4],
			COL_THT_YW, &tab_cell[i*NCOLS+5], -1);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(modelTransf), &iter);
	}

	geod_raport_table(textview, tab_cell, NCOLS, nrows, colsize, _("\nWykaz punktów po transformacji"));
	geod_raport_stopka(textview);
	
	for (i=0; i<(nrows+1)*NCOLS; i++)
		g_free(tab_cell[i]);
}


void on_tbtnTHelmertaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winTHelmerta");
	gtk_widget_destroy(window);
}


gboolean on_entryTHelmertaDostPNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarTHelmerta");
	return FALSE;
}


gboolean on_entryTHelmertaDostPNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarTHelmerta");
	return FALSE;
}


gboolean on_entryTHelmertaDostWNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarTHelmerta");
	return FALSE;
}


gboolean on_entryTHelmertaDostWNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarTHelmerta");
	return FALSE;
}


void on_btnTHelmertaDostDodaj_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktDostP, pktDostW;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeTHelmertaDost");
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(button), "tbtnTHelmertaOblicz");
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	char *txtXp = NULL, *txtYp = NULL, *txtXw = NULL, *txtYw = NULL;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryTHelmertaDostPNr",
		"entryTHelmertaDostPX", "entryTHelmertaDostPY", NULL, pktDostP));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarTHelmerta", _("Musisz podać Nr, X, Y dla punktu w układzie pierwotnym"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryTHelmertaDostWNr",
		"entryTHelmertaDostWX", "entryTHelmertaDostWY", NULL, pktDostW));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarTHelmerta", _("Musisz podać Nr, X, Y dla punktu w układzie wtórnym"));
		return;
	}
	
	txtXp = g_strdup_printf("%.*f", dok_xy_int, pktDostP.getX());
	txtYp = g_strdup_printf("%.*f", dok_xy_int, pktDostP.getY());
	txtXw = g_strdup_printf("%.*f", dok_xy_int, pktDostW.getX());
	txtYw = g_strdup_printf("%.*f", dok_xy_int, pktDostW.getY());
 	
	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
		COL_THD_NRP, pktDostP.getNr(),
		COL_THD_XP, txtXp, COL_THD_XP_DOUBLE, pktDostP.getX(),
		COL_THD_YP, txtYp, COL_THD_YP_DOUBLE, pktDostP.getY(),
		COL_THD_NRW, pktDostW.getNr(),
		COL_THD_XW, txtXw, COL_THD_XW_DOUBLE, pktDostW.getX(),
		COL_THD_YW, txtYw, COL_THD_YW_DOUBLE, pktDostW.getY(), -1);
	
	g_free(txtXp);
	g_free(txtYp);
	g_free(txtXw);
	g_free(txtYw);
	
	// wyczyszczenie wszystkich pol w zakladce Punkty Dostosowania
	geod_entry_clear(GTK_WIDGET(button), "entryTHelmertaDostPNr");
	geod_entry_clear(GTK_WIDGET(button), "entryTHelmertaDostPX");
	geod_entry_clear(GTK_WIDGET(button), "entryTHelmertaDostPY");
	geod_entry_clear(GTK_WIDGET(button), "entryTHelmertaDostWNr");
	geod_entry_clear(GTK_WIDGET(button), "entryTHelmertaDostWX");
	geod_entry_clear(GTK_WIDGET(button), "entryTHelmertaDostWY");

	g_signal_emit_by_name(btnOblicz, "clicked");
	GtkWidget *focus = lookup_widget(GTK_WIDGET(button), "entryTHelmertaDostPNr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


void on_btnTHelmertaTransfDodaj_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktTransf;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeTHelmertaTransf");
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(button), "tbtnTHelmertaOblicz");
	GtkWidget *cbtnNoAdd = lookup_widget(GTK_WIDGET(button), "cbtnTHelmertaTransfDodajLista");
	GtkWidget *entryNrW = lookup_widget(GTK_WIDGET(button), "entryTHelmertaTransfNrNowy");
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	char *txtXt = NULL, *txtYt = NULL, *txtNrNowy = NULL;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	int s_dodaj;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryTHelmertaTransfNr",
		"entryTHelmertaTransfX", "entryTHelmertaTransfY", NULL, pktTransf));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarTHelmerta", _("Musisz podać Nr, X, Y dla punktu do transformacji"));
		return;
	}

	if (g_str_equal(gtk_entry_get_text(GTK_ENTRY(entryNrW)), "")) {
		if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnNoAdd))) {
			geod_sbar_set(GTK_WIDGET(button), "sbarTHelmerta", _("Musisz podać Nr punktu w układzie wtórnym"));
			gtk_widget_grab_focus(GTK_WIDGET(entryNrW));
			return;
		}
		else
			txtNrNowy = g_strdup(pktTransf.getNr());
	}
	else if (!dbgeod_point_find(gtk_entry_get_text(GTK_ENTRY(entryNrW))) && !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnNoAdd))) {
		geod_sbar_set(GTK_WIDGET(button), "sbarTHelmerta", _("Punkt o takim numerze znajduje się już w bazie"));
		gtk_widget_grab_focus(GTK_WIDGET(entryNrW));
		return;
	}
	else
		txtNrNowy = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryNrW)));


	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnNoAdd)))
		s_dodaj = PKT_NIE_DODAWAJ;
	else 
		s_dodaj = PKT_DODAJ;
	
	txtXt = g_strdup_printf("%.*f", dok_xy_int, pktTransf.getX());
	txtYt = g_strdup_printf("%.*f", dok_xy_int, pktTransf.getY());
 	
	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
		COL_THT_NRP, pktTransf.getNr(),
		COL_THT_XP, txtXt, COL_THT_XP_DOUBLE, pktTransf.getX(),
		COL_THT_YP, txtYt, COL_THT_YP_DOUBLE, pktTransf.getY(),
		COL_THT_NRW, txtNrNowy, COL_THT_DODAJ, s_dodaj, -1);
	if (s_dodaj == PKT_DODAJ)
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_THT_TOGGLE, TRUE, -1);
	else
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_THT_TOGGLE, FALSE, -1);
	
	g_free(txtXt);
	g_free(txtYt);
	g_free(txtNrNowy);
	
	// Czyszczenie wszystkich pol w zakladce Punkty do Transformacji
	geod_entry_clear(GTK_WIDGET(button), "entryTHelmertaTransfNr");
	geod_entry_clear(GTK_WIDGET(button), "entryTHelmertaTransfNrNowy");
	geod_entry_clear(GTK_WIDGET(button), "entryTHelmertaTransfY");
	geod_entry_clear(GTK_WIDGET(button), "entryTHelmertaTransfX");

	g_signal_emit_by_name(btnOblicz, "clicked");
	GtkWidget *focus = lookup_widget(GTK_WIDGET(button), "entryTHelmertaTransfNr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


gboolean on_entryTHelmertaTransfNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarTHelmerta");
	return FALSE;
}


gboolean on_entryTHelmertaTransfNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarTHelmerta");
	return FALSE;
}


/* dialogStatystyka */
void on_dialogStatystyka_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *entryProjekt = lookup_widget(GTK_WIDGET(widget), "entryDialogStatystykaProjekt");
	GtkWidget *entryXmin = lookup_widget(GTK_WIDGET(widget), "entryDialogStatystykaXmin");
	GtkWidget *entryXmax = lookup_widget(GTK_WIDGET(widget), "entryDialogStatystykaXmax");
	GtkWidget *entryYmin = lookup_widget(GTK_WIDGET(widget), "entryDialogStatystykaYmin");
	GtkWidget *entryYmax = lookup_widget(GTK_WIDGET(widget), "entryDialogStatystykaYmax");
	GtkWidget *entryNPkt = lookup_widget(GTK_WIDGET(widget), "entryDialogStatystykaNPkt");
	GtkWidget *entryDataMod = lookup_widget(GTK_WIDGET(widget), "entryDialogStatystykaDataMod");
	struct sProjekt projekt;
	unsigned int npoints;
	double xmin, xmax, ymin, ymax;
	char *txt_xmin = NULL, *txt_ymin = NULL, *txt_xmax = NULL, *txt_ymax = NULL, *txt_npoints = NULL;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	dbgeod_projekt_get(dbgeod_projekt(), projekt);
	dbgeod_points_info(npoints, xmin, xmax, ymin, ymax);
	txt_xmin = g_strdup_printf("%.*f", dok_xy_int, xmin);
	txt_ymin = g_strdup_printf("%.*f", dok_xy_int, ymin);
	txt_xmax = g_strdup_printf("%.*f", dok_xy_int, xmax);
	txt_ymax = g_strdup_printf("%.*f", dok_xy_int, ymax);
	txt_npoints = g_strdup_printf("%d", npoints);

	gtk_entry_set_text(GTK_ENTRY(entryProjekt), projekt.nazwa);
	gtk_entry_set_text(GTK_ENTRY(entryDataMod), projekt.data_mod);
	gtk_entry_set_text(GTK_ENTRY(entryXmin), txt_xmin);
	gtk_entry_set_text(GTK_ENTRY(entryYmin), txt_ymin);
	gtk_entry_set_text(GTK_ENTRY(entryXmax), txt_xmax);
	gtk_entry_set_text(GTK_ENTRY(entryYmax), txt_ymax);
	gtk_entry_set_text(GTK_ENTRY(entryNPkt), txt_npoints);
	
	g_free(txt_xmin);
	g_free(txt_ymin);
	g_free(txt_xmax);
	g_free(txt_ymax);
	g_free(txt_npoints);
}


/* dialogKonfiguracja */
void on_dialogKonfiguracja_realize (GtkWidget *widget, gpointer user_data)
{
	char konf_uklad[CONFIG_VALUE_MAX_LEN];
	char konf_auto_addpkt[CONFIG_VALUE_MAX_LEN];
	char konf_auto_num[CONFIG_VALUE_MAX_LEN];
	char konf_dpkt[CONFIG_VALUE_MAX_LEN];
	char konf_jkat[CONFIG_VALUE_MAX_LEN];
	char konf_jpow[CONFIG_VALUE_MAX_LEN];
	char konf_wnazwa[CONFIG_VALUE_MAX_LEN];
	char konf_wadres[CONFIG_VALUE_MAX_LEN];
	char konf_wnip[CONFIG_VALUE_MAX_LEN];
	char konf_wregon[CONFIG_VALUE_MAX_LEN];
	char konf_wnazwisko[CONFIG_VALUE_MAX_LEN];
	char konf_wupr[CONFIG_VALUE_MAX_LEN];
	char konf_web[CONFIG_VALUE_MAX_LEN];
	char konf_dok_xy[CONFIG_VALUE_MAX_LEN];
	char konf_dok_h[CONFIG_VALUE_MAX_LEN];
	char konf_dok_kat[CONFIG_VALUE_MAX_LEN];
	char konf_dok_par[CONFIG_VALUE_MAX_LEN];

	char konf_dok_kier[CONFIG_VALUE_MAX_LEN];
	char konf_dok_azym[CONFIG_VALUE_MAX_LEN];
	char konf_dok_odl1[CONFIG_VALUE_MAX_LEN];
	char konf_dok_odl2[CONFIG_VALUE_MAX_LEN];
	char konf_dok_centr[CONFIG_VALUE_MAX_LEN];
	char *txt_kat = NULL;
	
	// uklad wspolrzednych
	GtkWidget *rbtnUkladGeodezyjny = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigUkladGeodezyjny");
	GtkWidget *rbtnUkladMatematyczny = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigUkladMatematyczny");
	
	// dodawanie punktow do bazy
	GtkWidget *rbtnPktPozostaw = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigPktPozostaw");
	GtkWidget *rbtnPktNadpisz = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigPktNadpisz");
	GtkWidget *rbtnPktUsrednij = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigPktUsrednij");
	GtkWidget *rbtnPktZapytaj = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigPktZapytaj");
	
	// jednostka kata
	GtkWidget *rbtnJednKatGrady = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigJednKatGrady");
	GtkWidget *rbtnJednKatSt = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigJednKatSt");
	GtkWidget *rbtnJednKatStD = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigJednKatStD");
	GtkWidget *rbtnJednKatRad = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigJednKatRad");
	
	// jednostka powierzchni	
	GtkWidget *rbtnJednPowM2 = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigJednPowM2");
	GtkWidget *rbtnJednPowAr = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigJednPowAr");
	GtkWidget *rbtnJednPowHa = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigJednPowHa");	
	
	// dane o wykonawcy
	GtkWidget *entryWykonawcaNazwa = lookup_widget(GTK_WIDGET(widget), "entryDialogKonfigTabWykonawcaNazwa");
	GtkWidget *entryWykonawcaAdres = lookup_widget(GTK_WIDGET(widget), "entryDialogKonfigTabWykonawcaAdres");
	GtkWidget *entryWykonawcaNIP = lookup_widget(GTK_WIDGET(widget), "entryDialogKonfigTabWykonawcaNIP");
	GtkWidget *entryWykonawcaREGON = lookup_widget(GTK_WIDGET(widget), "entryDialogKonfigTabWykonawcaREGON");
	GtkWidget *entryWykonawcaNazwisko = lookup_widget(GTK_WIDGET(widget), "entryDialogKonfigTabWykonawcaNazwisko");
	GtkWidget *entryWykonawcaUprawnienia = lookup_widget(GTK_WIDGET(widget), "entryDialogKonfigTabWykonawcaUprawnienia");

	// dokladnosc
	GtkWidget *sbtnDokladnoscXY = lookup_widget(GTK_WIDGET(widget), "sbtnDialogKonfigDokladnoscXY");
	GtkWidget *sbtnDokladnoscH = lookup_widget(GTK_WIDGET(widget), "sbtnDialogKonfigDokladnoscH");
	GtkWidget *sbtnDokladnoscKat = lookup_widget(GTK_WIDGET(widget), "sbtnDialogKonfigDokladnoscKat");
	GtkWidget *sbtnDokladnoscParametr = lookup_widget(GTK_WIDGET(widget), "sbtnDialogKonfigDokladnoscParametr");

	GtkWidget *entryDokladnoscKier = lookup_widget(GTK_WIDGET(widget), "entryDialogKonfigDokladnoscKierunek");
	GtkWidget *entryDokladnoscOdl1 = lookup_widget(GTK_WIDGET(widget), "entryDialogKonfigDokladnoscOdleglosc1");
	GtkWidget *entryDokladnoscOdl2 = lookup_widget(GTK_WIDGET(widget), "entryDialogKonfigDokladnoscOdleglosc2");
	GtkWidget *entryDokladnoscAzym = lookup_widget(GTK_WIDGET(widget), "entryDialogKonfigDokladnoscAzymut");
	GtkWidget *entryDokladnoscCentr = lookup_widget(GTK_WIDGET(widget), "entryDialogKonfigDokladnoscCentrowanie");

	// jednostka powierzchni	
	GtkWidget *rbtnAutoNumNone = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigAutoNumNone");
	GtkWidget *rbtnAutoNumDec = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigAutoNumDec");
	GtkWidget *rbtnAutoNumInc = lookup_widget(GTK_WIDGET(widget), "rbtnDialogKonfigAutoNumInc");	

	// inne
	GtkWidget *filechooserWeb = lookup_widget(GTK_WIDGET(widget), "filechooserDialogKonfigTabInne");
	GtkWidget *cbtnAutoPktAdd = lookup_widget(GTK_WIDGET(widget), "cbtnDialogKonfigAddPkt");
	
	dbgeod_config_get("uklad", konf_uklad);
	if (g_str_equal(konf_uklad, "geodezyjny"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnUkladGeodezyjny), TRUE);
	else if (g_str_equal(konf_uklad, "matematyczny"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnUkladMatematyczny), TRUE);

	dbgeod_config_get("dodaj_pkt", konf_dpkt);
	if (g_str_equal(konf_dpkt, "pozostaw"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnPktPozostaw), TRUE);
	else if (g_str_equal(konf_dpkt, "nadpisz"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnPktNadpisz), TRUE);
	else if (g_str_equal(konf_dpkt, "usrednij"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnPktUsrednij), TRUE);
	else if (g_str_equal(konf_dpkt, "zapytaj"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnPktZapytaj), TRUE);

	dbgeod_config_get("jedn_kat", konf_jkat);
	if (g_str_equal(konf_jkat, "grad"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnJednKatGrady), TRUE);
	else if (g_str_equal(konf_jkat, "deg"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnJednKatSt), TRUE);
	else if (g_str_equal(konf_jkat, "degdec"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnJednKatStD), TRUE);
	else if (g_str_equal(konf_jkat, "rad"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnJednKatRad), TRUE);
	
	dbgeod_config_get("jedn_pow", konf_jpow);
	if (g_str_equal(konf_jpow, "m2"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnJednPowM2), TRUE);
	else if (g_str_equal(konf_jpow, "ar"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnJednPowAr), TRUE);
	else if (g_str_equal(konf_jpow, "ha"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnJednPowHa), TRUE);

	if (!dbgeod_config_get("wykonawca_nazwa", konf_wnazwa))
		gtk_entry_set_text(GTK_ENTRY(entryWykonawcaNazwa), konf_wnazwa);
	if (!dbgeod_config_get("wykonawca_adres", konf_wadres))
		gtk_entry_set_text(GTK_ENTRY(entryWykonawcaAdres), konf_wadres);
	if (!dbgeod_config_get("wykonawca_nip", konf_wnip))
		gtk_entry_set_text(GTK_ENTRY(entryWykonawcaNIP), konf_wnip);
	if (!dbgeod_config_get("wykonawca_regon", konf_wregon))
		gtk_entry_set_text(GTK_ENTRY(entryWykonawcaREGON), konf_wregon);
	if (!dbgeod_config_get("wykonawca_nazwisko", konf_wnazwisko))
		gtk_entry_set_text(GTK_ENTRY(entryWykonawcaNazwisko), konf_wnazwisko);
	if (!dbgeod_config_get("wykonawca_uprawnienia", konf_wupr))
		gtk_entry_set_text(GTK_ENTRY(entryWykonawcaUprawnienia), konf_wupr);

	if (!dbgeod_config_get("dok_centr", konf_dok_centr))
		gtk_entry_set_text(GTK_ENTRY(entryDokladnoscCentr), konf_dok_centr);
	else
		gtk_entry_set_text(GTK_ENTRY(entryDokladnoscCentr), "20");
	
	if (!dbgeod_config_get("dok_azym", konf_dok_azym))
		txt_kat = geod_kat_from_rad(g_strtod(konf_dok_azym, NULL), FALSE);
	else
		txt_kat = geod_kat_from_rad(0.01/RO_GR, FALSE);
	gtk_entry_set_text(GTK_ENTRY(entryDokladnoscAzym), txt_kat);
	g_free(txt_kat);
	txt_kat = NULL;

	if (!dbgeod_config_get("dok_kier", konf_dok_kier))
		txt_kat = geod_kat_from_rad(g_strtod(konf_dok_kier, NULL), FALSE);
	else
		txt_kat = geod_kat_from_rad(0.002/RO_GR, FALSE);
	gtk_entry_set_text(GTK_ENTRY(entryDokladnoscKier), txt_kat);
	g_free(txt_kat);
	txt_kat = NULL;
	
	if (!dbgeod_config_get("dok_odl1", konf_dok_odl1))
		gtk_entry_set_text(GTK_ENTRY(entryDokladnoscOdl1), konf_dok_odl1);
	else
		gtk_entry_set_text(GTK_ENTRY(entryDokladnoscOdl1), "20");

	if (!dbgeod_config_get("dok_odl2", konf_dok_odl2))
		gtk_entry_set_text(GTK_ENTRY(entryDokladnoscOdl2), konf_dok_odl2);
	else
		gtk_entry_set_text(GTK_ENTRY(entryDokladnoscOdl2), "20");

	if (!dbgeod_config_get("wykonawca_regon", konf_wregon))
		gtk_entry_set_text(GTK_ENTRY(entryWykonawcaREGON), konf_wregon);
	if (!dbgeod_config_get("wykonawca_nazwisko", konf_wnazwisko))
		gtk_entry_set_text(GTK_ENTRY(entryWykonawcaNazwisko), konf_wnazwisko);
	if (!dbgeod_config_get("wykonawca_uprawnienia", konf_wupr))
		gtk_entry_set_text(GTK_ENTRY(entryWykonawcaUprawnienia), konf_wupr);
	
	if (!dbgeod_config_get("przegladarka_www", konf_web))
		gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(filechooserWeb), konf_web);

	if (!dbgeod_config_get("dok_xy", konf_dok_xy))
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(sbtnDokladnoscXY), g_strtod(konf_dok_xy, NULL));
	if (!dbgeod_config_get("dok_h", konf_dok_h))
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(sbtnDokladnoscH), g_strtod(konf_dok_h, NULL));
	if (!dbgeod_config_get("dok_kat", konf_dok_kat))
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(sbtnDokladnoscKat), g_strtod(konf_dok_kat, NULL));
	if (!dbgeod_config_get("dok_par", konf_dok_par))
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(sbtnDokladnoscParametr), g_strtod(konf_dok_par, NULL));

	dbgeod_config_get("auto_addpkt", konf_auto_addpkt);
	if (g_str_equal(konf_auto_addpkt, "1"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cbtnAutoPktAdd), TRUE);
	else
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cbtnAutoPktAdd), FALSE);

	dbgeod_config_get("auto_num", konf_auto_num);
	if (g_str_equal(konf_auto_num, "+"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnAutoNumInc), TRUE);
	else if (g_str_equal(konf_auto_num, "-"))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnAutoNumDec), TRUE);
	else
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rbtnAutoNumNone), TRUE);
}

void on_dialogKonfiguracja_response (GtkDialog *dialog, gint response_id, gpointer user_data)
{
	if (response_id == GTK_RESPONSE_APPLY || response_id == GTK_RESPONSE_OK) {
		char *sbar_msg = NULL;
		char konfiguracja_kat[CONFIG_VALUE_MAX_LEN];
		double vdouble = 0.0;
		
		GtkWidget *sbarMain = lookup_widget(GTK_WIDGET(dialog), "sbarMain");
		
		// uklad wspolrzednych
		GtkWidget *rbtnUkladGeodezyjny = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigUkladGeodezyjny");
		GtkWidget *rbtnUkladMatematyczny = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigUkladMatematyczny");
	
		// dodawanie punktow do bazy
		GtkWidget *rbtnPktPozostaw = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigPktPozostaw");
		GtkWidget *rbtnPktNadpisz = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigPktNadpisz");
		GtkWidget *rbtnPktUsrednij = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigPktUsrednij");
		GtkWidget *rbtnPktZapytaj = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigPktZapytaj");
	
		// jednostka kata
		GtkWidget *rbtnJednKatGrady = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigJednKatGrady");
		GtkWidget *rbtnJednKatSt = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigJednKatSt");
		GtkWidget *rbtnJednKatStD = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigJednKatStD");
		GtkWidget *rbtnJednKatRad = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigJednKatRad");
	
		// jednostka powierzchni	
		GtkWidget *rbtnJednPowM2 = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigJednPowM2");
		GtkWidget *rbtnJednPowAr = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigJednPowAr");
		GtkWidget *rbtnJednPowHa = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigJednPowHa");	
	
		// dane o wykonawcy
		GtkWidget *entryWykonawcaNazwa = lookup_widget(GTK_WIDGET(dialog), "entryDialogKonfigTabWykonawcaNazwa");
		GtkWidget *entryWykonawcaAdres = lookup_widget(GTK_WIDGET(dialog), "entryDialogKonfigTabWykonawcaAdres");
		GtkWidget *entryWykonawcaNIP = lookup_widget(GTK_WIDGET(dialog), "entryDialogKonfigTabWykonawcaNIP");
		GtkWidget *entryWykonawcaREGON = lookup_widget(GTK_WIDGET(dialog), "entryDialogKonfigTabWykonawcaREGON");
		GtkWidget *entryWykonawcaNazwisko = lookup_widget(GTK_WIDGET(dialog), "entryDialogKonfigTabWykonawcaNazwisko");
		GtkWidget *entryWykonawcaUprawnienia = lookup_widget(GTK_WIDGET(dialog), "entryDialogKonfigTabWykonawcaUprawnienia");

		// jednostka powierzchni	
		GtkWidget *rbtnAutoNumNone = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigAutoNumNone");
		GtkWidget *rbtnAutoNumDec = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigAutoNumDec");
		GtkWidget *rbtnAutoNumInc = lookup_widget(GTK_WIDGET(dialog), "rbtnDialogKonfigAutoNumInc");	
	
		// inne
		GtkWidget *filechooserWeb = lookup_widget(GTK_WIDGET(dialog), "filechooserDialogKonfigTabInne");
		GtkWidget *sbtnDokladnoscXY = lookup_widget(GTK_WIDGET(dialog), "sbtnDialogKonfigDokladnoscXY");
		GtkWidget *sbtnDokladnoscH = lookup_widget(GTK_WIDGET(dialog), "sbtnDialogKonfigDokladnoscH");
		GtkWidget *sbtnDokladnoscKat = lookup_widget(GTK_WIDGET(dialog), "sbtnDialogKonfigDokladnoscKat");
		GtkWidget *sbtnDokladnoscParametr = lookup_widget(GTK_WIDGET(dialog), "sbtnDialogKonfigDokladnoscParametr");
		GtkWidget *cbtnAutoPktAdd = lookup_widget(GTK_WIDGET(dialog), "cbtnDialogKonfigAddPkt");

		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnUkladMatematyczny)))
			dbgeod_config_set("uklad", "matematyczny");
		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnUkladGeodezyjny)))
			dbgeod_config_set("uklad", "geodezyjny");

		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnAutoPktAdd)))
			dbgeod_config_set("auto_addpkt", "1");
		else
			dbgeod_config_set("auto_addpkt", "0");

		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnAutoNumNone)))
			dbgeod_config_set("auto_num", "0");
		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnAutoNumInc)))
			dbgeod_config_set("auto_num", "+");
		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnAutoNumDec)))
			dbgeod_config_set("auto_num", "-");
		
		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnPktPozostaw)))
			dbgeod_config_set("dodaj_pkt", "pozostaw");
		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnPktNadpisz)))
			dbgeod_config_set("dodaj_pkt", "nadpisz");
		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnPktUsrednij)))
			dbgeod_config_set("dodaj_pkt", "usrednij");
		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnPktZapytaj)))
			dbgeod_config_set("dodaj_pkt", "zapytaj");
	
		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnJednKatGrady)))
			dbgeod_config_set("jedn_kat", "grad");
		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnJednKatSt)))
			dbgeod_config_set("jedn_kat", "deg");
		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnJednKatStD)))
			dbgeod_config_set("jedn_kat", "degdec");
		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnJednKatRad)))
			dbgeod_config_set("jedn_kat", "rad");

		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnJednPowM2)))
			dbgeod_config_set("jedn_pow", "m2");
		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnJednPowAr)))
			dbgeod_config_set("jedn_pow", "ar");
		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnJednPowHa)))
			dbgeod_config_set("jedn_pow", "ha");
	
		dbgeod_config_set("wykonawca_nazwa", gtk_entry_get_text(GTK_ENTRY(entryWykonawcaNazwa)));
		dbgeod_config_set("wykonawca_adres", gtk_entry_get_text(GTK_ENTRY(entryWykonawcaAdres)));
		dbgeod_config_set("wykonawca_nip", gtk_entry_get_text(GTK_ENTRY(entryWykonawcaNIP)));
		dbgeod_config_set("wykonawca_regon", gtk_entry_get_text(GTK_ENTRY(entryWykonawcaREGON)));
		dbgeod_config_set("wykonawca_nazwisko", gtk_entry_get_text(GTK_ENTRY(entryWykonawcaNazwisko)));
		dbgeod_config_set("wykonawca_uprawnienia", gtk_entry_get_text(GTK_ENTRY(entryWykonawcaUprawnienia)));
		
		dbgeod_config_set("przegladarka_www", gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filechooserWeb)));
		dbgeod_config_set("dok_xy", g_strdup_printf("%.0f", gtk_spin_button_get_value(GTK_SPIN_BUTTON(sbtnDokladnoscXY))));
		dbgeod_config_set("dok_h", g_strdup_printf("%.0f", gtk_spin_button_get_value(GTK_SPIN_BUTTON(sbtnDokladnoscH))));
		dbgeod_config_set("dok_kat", g_strdup_printf("%.0f", gtk_spin_button_get_value(GTK_SPIN_BUTTON(sbtnDokladnoscKat))));
		dbgeod_config_set("dok_par", g_strdup_printf("%.0f", gtk_spin_button_get_value(GTK_SPIN_BUTTON(sbtnDokladnoscParametr))));

		geod_entry_get_kat(GTK_WIDGET(dialog), "entryDialogKonfigDokladnoscKierunek", vdouble);
		dbgeod_config_set("dok_kier", g_strdup_printf("%.10f", vdouble));
		geod_entry_get_kat(GTK_WIDGET(dialog), "entryDialogKonfigDokladnoscAzymut", vdouble);
		dbgeod_config_set("dok_azym", g_strdup_printf("%.10f", vdouble));
		geod_entry_get_double(GTK_WIDGET(dialog), "entryDialogKonfigDokladnoscCentrowanie", vdouble, FALSE);
		dbgeod_config_set("dok_centr", g_strdup_printf("%.0f", vdouble));
		geod_entry_get_double(GTK_WIDGET(dialog), "entryDialogKonfigDokladnoscOdleglosc1", vdouble, FALSE);
		dbgeod_config_set("dok_odl1", g_strdup_printf("%.0f", vdouble));
		geod_entry_get_double(GTK_WIDGET(dialog), "entryDialogKonfigDokladnoscOdleglosc2", vdouble, FALSE);
		dbgeod_config_set("dok_odl2", g_strdup_printf("%.0f", vdouble));	
		
		// ustawienie paska statusu w winMain
		dbgeod_config_get("jedn_kat", konfiguracja_kat);
		gtk_statusbar_pop(GTK_STATUSBAR(sbarMain), 2);
		if (g_str_equal(konfiguracja_kat, "rad"))
			sbar_msg = g_strdup(_("Jednostka kąta: RADIANY"));
		else if (g_str_equal(konfiguracja_kat, "grad"))
			sbar_msg = g_strdup(_("Jednostka kąta: GRADY"));
		else if (g_str_equal(konfiguracja_kat, "deg"))
			sbar_msg = g_strdup(_("Jednostka kąta: STOPNIE"));
		else if (g_str_equal(konfiguracja_kat, "degdec"))
			sbar_msg = g_strdup(_("Jednostka kąta: STOPNIE (dziesiętnie)"));
		else
			sbar_msg = g_strdup(_("Wybierz Plik->Preferencje aby dostosować program"));
		gtk_statusbar_push(GTK_STATUSBAR(sbarMain), 2, sbar_msg);
		g_free(sbar_msg);
	}

	if (response_id != GTK_RESPONSE_APPLY)
		gtk_widget_destroy(GTK_WIDGET(dialog));
}


/* dialogDodajPunkt */
void on_dialogDodajPunkt_response (GtkDialog *dialog, gint response_id, gpointer user_data)
{
	if (response_id == GTK_RESPONSE_OK) {
		cPunkt punkt;
		bool zmiana = FALSE;
		GtkWidget *entryH = lookup_widget(GTK_WIDGET(dialog), "entryDialogDodajPunktH");
		GtkWidget *entryTyp = lookup_widget(GTK_WIDGET(dialog), "entryDialogDodajPunktTyp");
		GtkWidget *focus = lookup_widget(GTK_WIDGET(dialog), "entryDialogDodajPunktNr");
		GtkWidget *eventEntry = (GtkWidget*)g_object_get_data (G_OBJECT(dialog), "eventEntry");
		const gchar *H = gtk_entry_get_text(GTK_ENTRY(entryH));
		int typ;
		
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(dialog), "entryDialogDodajPunktNr",
			"entryDialogDodajPunktX", "entryDialogDodajPunktY", "entryDialogDodajPunktKod", punkt))
		{
			punkt.setH(g_strtod(H, NULL));
			typ = gtk_combo_box_get_active(GTK_COMBO_BOX(entryTyp));
			if (typ < 0) typ = 0;
			punkt.setTyp(typ);
			dbgeod_point_add_config(punkt, &zmiana);
			if (zmiana) {
				geod_entry_inc(GTK_WIDGET(dialog), "entryDialogDodajPunktNr");
				geod_entry_clear(GTK_WIDGET(dialog), "entryDialogDodajPunktX");
				geod_entry_clear(GTK_WIDGET(dialog), "entryDialogDodajPunktY");
				geod_entry_clear(GTK_WIDGET(dialog), "entryDialogDodajPunktH");
				geod_entry_clear(GTK_WIDGET(dialog), "entryDialogDodajPunktKod");
				gtk_widget_grab_focus(GTK_WIDGET(focus));
				if (GTK_IS_ENTRY(eventEntry)) {
					gtk_widget_destroy(GTK_WIDGET(dialog));
					gtk_entry_set_text(GTK_ENTRY(eventEntry), punkt.getNr());
					gtk_cell_editable_editing_done(GTK_CELL_EDITABLE(eventEntry));
				}
			}
			else ; // blad dodawania ??
		}
		else ; // nie podano wszystkich danych ??
	}
	else
		gtk_widget_destroy(GTK_WIDGET(dialog));
}


/* dialogUsunPunkty */
void on_dialogUsunPunkty_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeDialogUsunPunkty");
	GtkTreeStore *punkty_model;
	
	punkty_model=gtk_tree_store_new(N_PKT_COLUMNS, G_TYPE_INT, G_TYPE_STRING, \
	G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, \
	G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_INT);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), \
	    GTK_TREE_MODEL(punkty_model));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;
	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);
	
	// kolumna nr
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Nr"), renderer, "text", COL_PKT_NR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);
	
	// kolumna x
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("X"), renderer, "text", COL_PKT_X, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 80);
	
	// kolumna y
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("Y"), renderer, "text", COL_PKT_Y, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 80);

	// kolumna h
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("H"), renderer, "text", COL_PKT_H, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 80);

	// kolumna kod
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("Kod"), renderer, "text", COL_PKT_KOD, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// kolumna typ
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("Typ"), renderer, "text", COL_PKT_TYP, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// ODCZYTANIE PUNKTOW Z BAZY
	treePunkty_update_view(treeview);
}


void on_entryDialogUsunPunktyFiltr_changed (GtkEditable *editable, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(editable), "treeDialogUsunPunkty");
	const char *nr_pkt = gtk_entry_get_text(GTK_ENTRY(editable));
	treePunkty_update_view(treeview, nr_pkt);
}


void on_tbtnDialogUsunPunktyZaznacz_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeDialogUsunPunkty");
	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	gtk_tree_selection_select_all(selection);

}


void on_tbtnDialogUsunPunktyOdznacz_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeDialogUsunPunkty");
	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	gtk_tree_selection_unselect_all(selection);
}


void on_dialogUsunPunkty_response (GtkDialog *dialog, gint response_id, gpointer user_data)
{
	if (response_id == GTK_RESPONSE_OK) {
		GList *sel_rows = NULL;
		GtkWidget *msg_dialog;
		GtkWidget *treeview = lookup_widget(GTK_WIDGET(dialog), "treeDialogUsunPunkty");
		GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
		GtkTreeModel *model = NULL;
		GtkTreePath *tpath = NULL;
		GtkTreeIter iter;
		guint i, pkt_id;
		char *msg_txt = NULL;
		gint nrows = gtk_tree_selection_count_selected_rows(selection);
	
		if (nrows == 0) {
			msg_dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, _("Zaznacz najpierw punkty do usunięcia"));
			gtk_dialog_run(GTK_DIALOG(msg_dialog));
			gtk_widget_destroy(msg_dialog);
			return;
		}
	
		msg_txt = g_strdup_printf(_("Czy na pewno usunąć %d punkty(ów)?"), nrows);
		msg_dialog = gtk_message_dialog_new(GTK_WINDOW(dialog), GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, msg_txt);
		if (gtk_dialog_run(GTK_DIALOG(msg_dialog)) == GTK_RESPONSE_YES) {
			sel_rows = gtk_tree_selection_get_selected_rows(selection, &model);
	   	for (i=0; i < g_list_length(sel_rows); i++) {
				tpath = (GtkTreePath*)g_list_nth_data(sel_rows, i);
				gtk_tree_model_get_iter(model, &iter, tpath);
				gtk_tree_model_get (model, &iter, COL_PKT_ID, &pkt_id, -1);
				dbgeod_point_del(pkt_id);
	    		gtk_tree_path_free(tpath);
			}
			g_list_free(sel_rows);
			treePunkty_update_view(treeview);
		}
		gtk_widget_destroy(msg_dialog);
		g_free(msg_txt);
	}
	else
		gtk_widget_destroy(GTK_WIDGET(dialog));
}


/* dialogProjektEdytuj */
void on_dialogProjektEdytuj_response (GtkDialog *dialog, gint response_id, gpointer user_data)
{
	if (response_id == GTK_RESPONSE_OK) {
		struct sProjekt projekt;
		GtkWidget *wdialog = NULL;
		GtkWidget *labelProjektId = lookup_widget(GTK_WIDGET(dialog), "labelDialogProjektEdytujIdV");
		GtkWidget *entryProjektNazwa = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujNazwa");
		GtkWidget *entryProjektWojewodztwo = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujWojewodztwo");
		GtkWidget *entryProjektPowiat = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujPowiat");
		GtkWidget *entryProjektGmina = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujGmina");
		GtkWidget *entryProjektMiejscowosc = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujMiejscowosc");
		GtkWidget *entryProjektUlica = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujUlica");
		GtkWidget *entryProjektOznNier = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujOznNier");
		GtkWidget *entryProjektCel = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujCel");
		GtkWidget *entryProjektUwagi = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujUwagi");
		GtkWidget *entryProjektDataPom = lookup_widget(GTK_WIDGET(dialog), "entryDialogProjektEdytujDataPom");

		projekt.id = atoi(gtk_label_get_text(GTK_LABEL(labelProjektId)));
		g_strlcpy(projekt.nazwa, gtk_entry_get_text(GTK_ENTRY(entryProjektNazwa)), PNAZWA_MAX_LEN);
		g_strlcpy(projekt.wojewodztwo, gtk_entry_get_text(GTK_ENTRY(entryProjektWojewodztwo)), PWOJEWODZTWO_MAX_LEN);
		g_strlcpy(projekt.powiat, gtk_entry_get_text(GTK_ENTRY(entryProjektPowiat)), PPOWIAT_MAX_LEN);
		g_strlcpy(projekt.gmina, gtk_entry_get_text(GTK_ENTRY(entryProjektGmina)), PGMINA_MAX_LEN);
		g_strlcpy(projekt.miejscowosc, gtk_entry_get_text(GTK_ENTRY(entryProjektMiejscowosc)), PMIEJSCOWOSC_MAX_LEN);
		g_strlcpy(projekt.ulica, gtk_entry_get_text(GTK_ENTRY(entryProjektUlica)), PULICA_MAX_LEN);
		g_strlcpy(projekt.ozn_nier, gtk_entry_get_text(GTK_ENTRY(entryProjektOznNier)), POZN_NIER_MAX_LEN);
		g_strlcpy(projekt.cel_pracy, gtk_entry_get_text(GTK_ENTRY(entryProjektCel)), PCEL_PRACY_MAX_LEN);
		g_strlcpy(projekt.uwagi, gtk_entry_get_text(GTK_ENTRY(entryProjektUwagi)), PUWAGI_MAX_LEN);
		g_strlcpy(projekt.data_pom, gtk_entry_get_text(GTK_ENTRY(entryProjektDataPom)), PDATA_POM_MAX_LEN);
	
		if (g_str_equal(projekt.nazwa,"")) {
			wdialog = gtk_message_dialog_new(GTK_WINDOW(dialog), GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, _("Musisz podać nazwę dla projektu."));
			gtk_dialog_run(GTK_DIALOG(wdialog));
			gtk_widget_destroy(wdialog);
		}
		else {
			if (projekt.id > 0)
				dbgeod_projekt_replace(projekt, &projekt.id);
			else
				dbgeod_projekt_replace(projekt, NULL);
			gtk_widget_destroy(GTK_WIDGET(dialog));
		}
	}
	else
		gtk_widget_destroy(GTK_WIDGET(dialog));
}


/* winObserwacje */
void on_winObserwacje_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeObserwacje");
	GtkTreeStore *obserwacje_model;
	
	obserwacje_model=gtk_tree_store_new(N_OBS_COLUMNS, G_TYPE_INT, \
		G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING,
		G_TYPE_STRING, G_TYPE_DOUBLE);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), \
	   GTK_TREE_MODEL(obserwacje_model));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;

	// kolumna typ
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Typ"), renderer, "text", COL_OBS_TYP, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 150);
	
	// kolumna L
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("L"), renderer, "text", COL_OBS_L, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);
	
	// kolumna Stanowisko
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("St"), renderer, "text", COL_OBS_C, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// kolumna P
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("P"), renderer, "text", COL_OBS_P, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// kolumna wartosc
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("wartość"), renderer, "text", COL_OBS_V, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 120);

	// ODCZYTANIE OBSERWACJI Z BAZY
	treeObserwacje_update_view(treeview);
}


void on_tbtnObserwacjeOdswiez_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeObserwacje");
	treeObserwacje_update_view(treeview);
}


void on_winObserwacje_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winObserwacje");
}


void on_tbtnObserwacjeWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeObserwacje");
	if (dbgeod_obs_del_all() == 0)
		treeObserwacje_update_view(treeview);		
}


void on_tbtnObserwacjeUsun_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *treeObserwacje = lookup_widget(GTK_WIDGET(toolbutton), "treeObserwacje");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeObserwacje));
	GtkTreePath *tpath;
	GtkTreeIter iter;
	unsigned int obs_id;
	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(treeObserwacje), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_OBS_ID, &obs_id, -1);
		dbgeod_obs_del(obs_id);
		treeObserwacje_update_view(treeObserwacje);
		gtk_tree_path_free(tpath);
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarObserwacje", _("Zaznacz najpierw obserwację w tabeli"));

}

void on_tbtnObserwacjeImport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeObserwacje");
	on_mnuMainImportujObserwacje_activate (NULL, NULL);
	treeObserwacje_update_view(treeview);
}


void on_tbtnObserwacjeEksport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	on_mnuMainEksportujObserwacje_activate (NULL, NULL);
}


void on_tbtnObserwacjeZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winObserwacje");
	gtk_widget_destroy(window);
}


/* dialogProjektWybor */
void on_dialogProjektWybor_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeDialogProjektWybor");
		
	GtkTreeStore *modelProjektWybor = gtk_tree_store_new (N_PRJ_COLUMNS, G_TYPE_INT, \
	G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, \
	G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING,
	G_TYPE_STRING);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), \
	    GTK_TREE_MODEL(modelProjektWybor));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;
	
	// kolumna id
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("ID"), renderer, "text", COL_PRJ_ID, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);
	
	// kolumna nazwa
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("Nazwa"), renderer, "text", COL_PRJ_NAZWA, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 150);
	
	// ODCZYTANIE PROJEKTOW Z BAZY
	treeProjekty_update_view(treeview);
}


void on_treeDialogProjektWybor_cursor_changed (GtkTreeView *treeview, gpointer user_data)
{
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	GtkWidget *labelProjektId = lookup_widget(GTK_WIDGET(treeview), "labelDialogProjektWyborIdV");
	GtkWidget *labelProjektNazwa = lookup_widget(GTK_WIDGET(treeview), "labelDialogProjektWyborNazwaV");
	GtkWidget *labelProjektWojewodztwo = lookup_widget(GTK_WIDGET(treeview), "labelDialogProjektWyborWojewodztwoV");
	GtkWidget *labelProjektPowiat = lookup_widget(GTK_WIDGET(treeview), "labelDialogProjektWyborPowiatV");
	GtkWidget *labelProjektGmina = lookup_widget(GTK_WIDGET(treeview), "labelDialogProjektWyborGminaV");
	GtkWidget *labelProjektMiejscowosc = lookup_widget(GTK_WIDGET(treeview), "labelDialogProjektWyborMiejscowoscV");
	GtkWidget *labelProjektUlica = lookup_widget(GTK_WIDGET(treeview), "labelDialogProjektWyborUlicaV");
	GtkWidget *labelProjektOznNier = lookup_widget(GTK_WIDGET(treeview), "labelDialogProjektWyborOznNierV");
	GtkWidget *labelProjektCel = lookup_widget(GTK_WIDGET(treeview), "labelDialogProjektWyborCelV");
	GtkWidget *labelProjektUwagi = lookup_widget(GTK_WIDGET(treeview), "labelDialogProjektWyborUwagiV");
	GtkWidget *labelProjektDataPom = lookup_widget(GTK_WIDGET(treeview), "labelDialogProjektWyborDataPomV");
	GtkWidget *labelProjektDataMod = lookup_widget(GTK_WIDGET(treeview), "labelDialogProjektWyborDataModV");

	GtkTreePath *tpath;
	int projekt_id;
	GtkTreeIter iter;
	struct sProjekt projekt;
	char *txt_id = NULL;

	gtk_tree_view_get_cursor(GTK_TREE_VIEW(treeview), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, 0, &projekt_id, -1);
		dbgeod_projekt_get(projekt_id, projekt);
		txt_id = g_strdup_printf("%d", projekt.id);
	
		gtk_label_set_text(GTK_LABEL(labelProjektId), txt_id);
		gtk_label_set_text(GTK_LABEL(labelProjektNazwa), projekt.nazwa);
		gtk_label_set_text(GTK_LABEL(labelProjektWojewodztwo), projekt.wojewodztwo);
		gtk_label_set_text(GTK_LABEL(labelProjektPowiat), projekt.powiat);
		gtk_label_set_text(GTK_LABEL(labelProjektGmina), projekt.gmina);
		gtk_label_set_text(GTK_LABEL(labelProjektMiejscowosc), projekt.miejscowosc);
		gtk_label_set_text(GTK_LABEL(labelProjektUlica), projekt.ulica);
		gtk_label_set_text(GTK_LABEL(labelProjektOznNier), projekt.ozn_nier);
		gtk_label_set_text(GTK_LABEL(labelProjektCel), projekt.cel_pracy);
		gtk_label_set_text(GTK_LABEL(labelProjektUwagi), projekt.uwagi);
		gtk_label_set_text(GTK_LABEL(labelProjektDataPom), projekt.data_pom);
		gtk_label_set_text(GTK_LABEL(labelProjektDataMod), projekt.data_mod);

		gtk_tree_path_free(tpath);
		g_free(txt_id);
	}
	else {
		gtk_label_set_text(GTK_LABEL(labelProjektId), "");
		gtk_label_set_text(GTK_LABEL(labelProjektNazwa), "");
		gtk_label_set_text(GTK_LABEL(labelProjektWojewodztwo), "");
		gtk_label_set_text(GTK_LABEL(labelProjektPowiat), "");
		gtk_label_set_text(GTK_LABEL(labelProjektGmina), "");
		gtk_label_set_text(GTK_LABEL(labelProjektMiejscowosc), "");
		gtk_label_set_text(GTK_LABEL(labelProjektUlica), "");
		gtk_label_set_text(GTK_LABEL(labelProjektOznNier), "");
		gtk_label_set_text(GTK_LABEL(labelProjektCel), "");
		gtk_label_set_text(GTK_LABEL(labelProjektUwagi), "");
		gtk_label_set_text(GTK_LABEL(labelProjektDataPom), "");
		gtk_label_set_text(GTK_LABEL(labelProjektDataMod), "");
	}
}


void on_btnDialogProjektWyborNowy_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeDialogProjektWybor");
	GtkWidget *dialog = create_dialogProjektEdytuj();
	
	gtk_dialog_run(GTK_DIALOG(dialog));
	treeProjekty_update_view(tree);
}


void on_btnDialogProjektWyborUsun_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *dialogProjektWybor = lookup_widget(GTK_WIDGET(button), "dialogProjektWybor");
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(button), "treeDialogProjektWybor");
	GtkWidget *dialog = NULL;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	GtkTreePath *tpath;
	int projekt_id;
	GtkTreeIter iter;

	gtk_tree_view_get_cursor(GTK_TREE_VIEW(treeview), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, 0, &projekt_id, -1);
		dbgeod_projekt_del(projekt_id);
		treeProjekty_update_view(treeview);
		gtk_tree_path_free(tpath);
	}
	else {
		dialog = gtk_message_dialog_new(GTK_WINDOW(dialogProjektWybor), GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, _("Zaznacz najpierw projekt w tabeli"));
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
	}
}

void on_dialogProjektWybor_response (GtkDialog *dialog, gint response_id, gpointer user_data)
{
	if (response_id == GTK_RESPONSE_OK) {
		GtkWidget *treeview = lookup_widget(GTK_WIDGET(dialog), "treeDialogProjektWybor");
		GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
		GtkWidget *winMain = lookup_widget(GTK_WIDGET(dialog), "winMain");
		GtkWidget *wdialog = NULL;
		GtkTreePath *tpath = NULL;
		GtkTreeIter iter;
		int projekt_id;
		char *txt_projekt_id = NULL;

		gtk_tree_view_get_cursor(GTK_TREE_VIEW(treeview), &tpath, NULL);
		if (tpath != NULL) {
			gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
			gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, 0, &projekt_id, -1);
			txt_projekt_id = g_strdup_printf("%d", projekt_id);
			dbgeod_projekt_data_mod();
			dbgeod_config_set("projekt", txt_projekt_id);
			g_free(txt_projekt_id);
			gtk_tree_path_free(tpath);
			gtk_widget_destroy(GTK_WIDGET(dialog));
			g_signal_emit_by_name(winMain, "realize");		
		}
		else {
			wdialog = gtk_message_dialog_new(GTK_WINDOW(dialog), GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, _("Zaznacz najpierw projekt w tabeli"));
			gtk_dialog_run(GTK_DIALOG(wdialog));
			gtk_widget_destroy(GTK_WIDGET(wdialog));
		}
	}
	else
		gtk_widget_destroy(GTK_WIDGET(dialog));
}


/* winImportTxt */
void on_btnImportTxtUp_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeImportTxt");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreePath *tpath;
	GtkTreeIter iter, iter_before;

	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		if (gtk_tree_path_prev (tpath) && 
-			gtk_tree_model_get_iter (GTK_TREE_MODEL(model), &iter_before, tpath))
			gtk_tree_store_swap(GTK_TREE_STORE(model), &iter_before, &iter);
		gtk_tree_path_free(tpath);
	}
}


void on_btnImportTxtDown_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeImportTxt");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreePath *tpath;
	GtkTreeIter iter, iter_before;
	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		iter_before = iter;
		if (gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter))
			gtk_tree_store_swap(GTK_TREE_STORE(model), &iter_before, &iter);
		gtk_tree_path_free(tpath);
	}
}


void on_dialogImportTxt_response (GtkDialog *dialog, gint response_id, gpointer user_data)
{
	GtkWidget *msgdialog;
	GtkWidget *filechooser = lookup_widget(GTK_WIDGET(dialog), "filechooserImportTxt");
	GtkWidget *cbtnSeparator = lookup_widget(GTK_WIDGET(dialog), "cbtnImportTxtSeparator");
	GtkWidget *cbtnHeader = lookup_widget(GTK_WIDGET(dialog), "cbtnImportTxtHeader");
	GtkWidget *cbtnIgnoruj = lookup_widget(GTK_WIDGET(dialog), "cbtnImportTxtIgnoruj");
	GtkWidget *tree = lookup_widget(GTK_WIDGET(dialog), "treeImportTxt");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));

	char addkonfig[CONFIG_VALUE_MAX_LEN];
	dbgeod_config_get("dodaj_pkt", addkonfig);

	if (response_id == GTK_RESPONSE_OK) {
		int uklad[N_FTXT];
		char *line_col[N_FTXT];
		GtkTreeIter iter;
		guint i, k, n_ipkt = 0, tree_id;
		gint col_nr;
		bool tree_active, zmiana, blad, naglowek;
		FILE *plik;
		cPunkt *pkt_insert = NULL;

		char bufor[80];
		char *file_name = NULL;
		char **bufor_split = NULL, **item = NULL;
		char *dialog_txt = NULL;
	
		// odczytanie ukladu kolumn
		for (i=0; i < N_FTXT; i++)
			uklad[i] = -1;

		gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
		k = 0; // k zamiast col_nr, bo w Win nie dziala
		for (i=0; i < N_FTXT; i++) {
			gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, 0, &tree_active, 3, &tree_id, -1);
			if (tree_active)
				uklad[k++] = tree_id;
			else if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnIgnoruj)))
				k++;
		
			gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
		}
	
		// odczytanie i sprawdzenie nazwy pliku
		file_name = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filechooser));
		if (file_name == NULL) {
			msgdialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Nie wybrano pliku."));
			gtk_dialog_run(GTK_DIALOG(msgdialog));
			gtk_widget_destroy(msgdialog);
			return;
		}
		if(g_file_test(file_name, G_FILE_TEST_IS_DIR)) {
			msgdialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Wybrano katalog zamiast pliku"));
			gtk_dialog_run(GTK_DIALOG(msgdialog));
			gtk_widget_destroy(msgdialog);
			g_free(file_name);
			return;
		}
	
		plik = fopen(file_name, "rt");
		
		if (plik == NULL)	{
			msgdialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Nie mogę otworzyć pliku, sprawdź uprawnienia."));
			gtk_dialog_run(GTK_DIALOG(msgdialog));
			gtk_widget_destroy(msgdialog);
			g_free(file_name);
			return;
		}
		else {
			naglowek = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnHeader));
			while (fgets(bufor, 80, plik) != NULL) {
				if (naglowek) naglowek = FALSE;
				else {
					bufor_split = g_strsplit_set(bufor, " :;|\t\n", -1);
			
					for (i=0; i < N_FTXT; i++) line_col[i] = NULL; // wyzerowanie tablicy wskaznikow
					for (col_nr = 0, item = bufor_split; *item != NULL && col_nr < N_FTXT; item++) {
						if (g_str_equal(*item, "") && gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnSeparator)));
						else if (uklad[col_nr] == -1)
							col_nr++;
						else {
							line_col[uklad[col_nr]] = *item;
							col_nr++;
						}
					}
					pkt_insert = new cPunkt;
					blad = FALSE;
					if (line_col[FTXT_NR] != NULL) pkt_insert->setNr(line_col[FTXT_NR]); else blad = TRUE;
					if (line_col[FTXT_KOD] != NULL) pkt_insert->setKod(line_col[FTXT_KOD]); else pkt_insert->setKod("");				
					if (line_col[FTXT_X] != NULL) pkt_insert->setX(g_strtod(line_col[FTXT_X], NULL)); else blad = TRUE;
					if (line_col[FTXT_Y] != NULL) pkt_insert->setY(g_strtod(line_col[FTXT_Y], NULL)); else blad = TRUE;
					if (line_col[FTXT_H] != NULL) pkt_insert->setH(g_strtod(line_col[FTXT_H], NULL));
					if (line_col[FTXT_TYP] != NULL) pkt_insert->setTyp(atoi(line_col[FTXT_TYP]));
			
					zmiana = FALSE;
					if (!blad) dbgeod_point_add_config(*pkt_insert, &zmiana, addkonfig); /// poprawic
					if (zmiana) n_ipkt++;
			
					delete pkt_insert;
					g_strfreev(bufor_split);
				}
			}
			fclose(plik);
		}
		g_free(file_name);
		
		//gtk_widget_destroy(GTK_WIDGET(dialog));
		dialog_txt = g_strdup_printf(_("Wczytano %d punktów.\n"), n_ipkt);
		msgdialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, dialog_txt);
		gtk_dialog_run(GTK_DIALOG(msgdialog));
		gtk_widget_destroy(msgdialog);
		g_free(dialog_txt);
	}
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "dialogImportTxt");
}


void on_dialogImportTxt_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeImportTxt");
	GtkTreeStore *model = gtk_tree_store_new(4, G_TYPE_BOOLEAN, G_TYPE_STRING, G_TYPE_BOOLEAN, G_TYPE_INT);
	GtkCellRenderer *renderer;
	GtkCellRenderer *rendererToggle;
	GtkTreeViewColumn *column, *toggleColumn;
	gint col_offset;
	GtkTreeIter iter;
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), GTK_TREE_MODEL(model));
	gtk_tree_view_set_reorderable(GTK_TREE_VIEW(treeview), FALSE);
	
	// wybór
	rendererToggle = gtk_cell_renderer_toggle_new();
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Wybór"), rendererToggle, "active", 0, NULL);
	toggleColumn = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(toggleColumn), 40);
	
	// nazwa kolumny
	renderer = gtk_cell_renderer_text_new();
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("Kolumna"), renderer, "text", 1, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);

	g_signal_connect (G_OBJECT (rendererToggle), "toggled",
		G_CALLBACK (cell_toggled_callback), treeview);

	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter, 0, TRUE, 1, _("Numer"), 2, FALSE, 3, FTXT_NR, -1);
	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter, 0, TRUE, 1, _("X"), 2, FALSE, 3, FTXT_X, -1);
	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter, 0, TRUE, 1, _("Y"), 2, FALSE, 3, FTXT_Y, -1);
	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter, 0, FALSE, 1, _("H"), 2, TRUE, 3, FTXT_H, -1);
	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter, 0, TRUE, 1, _("Kod"), 2, TRUE, 3, FTXT_KOD, -1);
	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter, 0, FALSE, 1, _("Typ"), 2, TRUE, 3, FTXT_TYP, -1);
}

void cell_toggled_callback (GtkCellRendererToggle *cell,
	gchar *path_string, gpointer treeview)
{
	gboolean value;
	gboolean can_change;
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	GtkTreePath *path = gtk_tree_path_new_from_string(path_string);

	if (gtk_tree_model_get_iter(model, &iter, path)) {
		gtk_tree_model_get (model, &iter, 0, &value, 2, &can_change, -1);
		if (can_change) {
			value = (!value);
			gtk_tree_store_set (GTK_TREE_STORE(model), &iter, 0, value, -1);
		}
	}
	gtk_tree_path_free(path);
}


/* winExportTxt */
void on_winEksportTxt_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeEksportTxt");
	GtkTreeStore *model = gtk_tree_store_new(4, G_TYPE_BOOLEAN, G_TYPE_STRING, G_TYPE_BOOLEAN, G_TYPE_INT);
	GtkCellRenderer *renderer;
	GtkCellRenderer *rendererToggle;
	GtkTreeViewColumn *column, *toggleColumn;
	gint col_offset;
	GtkTreeIter iter;
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), GTK_TREE_MODEL(model));
	gtk_tree_view_set_reorderable(GTK_TREE_VIEW(treeview), FALSE);
	
	// wybór
	rendererToggle = gtk_cell_renderer_toggle_new();
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Wybór"), rendererToggle, "active", 0, NULL);
	toggleColumn = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(toggleColumn), 40);
	
	// nazwa kolumny
	renderer = gtk_cell_renderer_text_new();
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("Kolumna"), renderer, "text", 1, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);

	g_signal_connect (G_OBJECT (rendererToggle), "toggled",
		G_CALLBACK (cell_toggled_callback), treeview);

	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter, 0, TRUE, 1, _("Numer"), 2, FALSE, 3, FTXT_NR, -1);
	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter, 0, TRUE, 1, _("X"), 2, FALSE, 3, FTXT_X, -1);
	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter, 0, TRUE, 1, _("Y"), 2, FALSE, 3, FTXT_Y, -1);
	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter, 0, FALSE, 1, _("H"), 2, TRUE, 3, FTXT_H, -1);
	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter, 0, TRUE, 1, _("Kod"), 2, TRUE, 3, FTXT_KOD, -1);
	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter, 0, FALSE, 1, _("Typ"), 2, TRUE, 3, FTXT_TYP, -1);
}


void on_winEksportTxt_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winEksportTxt");
}


void on_btnEksportTxtUp_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeEksportTxt");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreePath *tpath;
	GtkTreeIter iter, iter_before;

	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		if (gtk_tree_path_prev (tpath) && 
-			gtk_tree_model_get_iter (GTK_TREE_MODEL(model), &iter_before, tpath))
			gtk_tree_store_swap(GTK_TREE_STORE(model), &iter_before, &iter);
		gtk_tree_path_free(tpath);
	}
}


void on_btnEksportTxtDown_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeEksportTxt");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreePath *tpath;
	GtkTreeIter iter, iter_before;
	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		iter_before = iter;
		if (gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter))
			gtk_tree_store_swap(GTK_TREE_STORE(model), &iter_before, &iter);
		gtk_tree_path_free(tpath);
	}
}


void on_btnEksportTxtCancel_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(button), "winEksportTxt");
	gtk_widget_destroy(window);
}


void on_btnEksportTxtOk_clicked (GtkButton *button, gpointer user_data)
{
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	char dok_h[CONFIG_VALUE_MAX_LEN];
	int dok_h_int = 3;
	if (!dbgeod_config_get("dok_h", dok_h)) dok_h_int = atoi(dok_h);


	GtkWidget *filechooser = lookup_widget(GTK_WIDGET(button), "filechooserEksportTxt");
	GtkWidget *cbtnHeader = lookup_widget(GTK_WIDGET(button), "cbtnEksportTxtHeader");
	GtkWidget *cbtnSKropka = lookup_widget(GTK_WIDGET(button), "cbtnEksportTxtSKropka");
	
	GtkWidget *rbtnSpacja = lookup_widget(GTK_WIDGET(button), "rbtnEksportTxtSeparatorSpacja");
	GtkWidget *rbtnSrednik = lookup_widget(GTK_WIDGET(button), "rbtnEksportTxtSeparatorSrednik");
	GtkWidget *rbtnTab = lookup_widget(GTK_WIDGET(button), "rbtnEksportTxtSeparatorTab");
	GtkWidget *rbtnKreska = lookup_widget(GTK_WIDGET(button), "rbtnEksportTxtSeparatorKreska");

	GtkWidget *window = lookup_widget(GTK_WIDGET(button), "winEksportTxt");
	GtkWidget *dialog = NULL;

	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeEksportTxt");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreeIter iter;

	GSList *punkty = NULL;
	GSList *punkty_id = NULL;

	char bufor[G_ASCII_DTOSTR_BUF_SIZE];

	int uklad[N_FTXT];	
	char *line_col[N_FTXT];

	char separator;
	bool tree_active;
	guint i, j, tree_id, npkt;
	gint col_nr;
	FILE *plik;
	cPunkt *punkt = NULL;
	char *file_name = NULL;
	char *dialog_txt = NULL;
	
	// sprawdzenie nazwy i obecnosci pliku
	file_name = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filechooser));
	if (file_name == NULL) {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Nie wybrano pliku."));
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
		return;
	}
	if(g_file_test(file_name, G_FILE_TEST_IS_DIR)) {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Wybrano katalog zamiast pliku"));
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
		g_free(file_name);
		return;
	}
	if(g_file_test(file_name, G_FILE_TEST_EXISTS)) {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_WARNING, GTK_BUTTONS_OK_CANCEL, _("Uwaga podany plik istnieje.\nEksport spowoduje usunięcie istniejącego pliku.\nKontynuować?"));
		if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_CANCEL) {
			gtk_widget_destroy(dialog);
			g_free(file_name);
			return;
		}
		else
			gtk_widget_destroy(dialog);
	}

	// odczytanie ukladu kolumn
	for (i=0; i < N_FTXT; i++) {
		uklad[i] = -1;
	}
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	col_nr = 0;
	for (i=0; i < N_FTXT; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, 0, &tree_active, 1, &line_col[i], 3, &tree_id, -1);
		if (tree_active) {
			uklad[col_nr] = tree_id;
			col_nr++;
		}
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}

	// ustalenie separatora
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnSpacja)))
		separator = ' ';
	else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnSrednik)))
		separator = ';';
	else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnTab)))
		separator = '\t';
	else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbtnKreska)))
		separator = '|';
	else // separator domyslny
		separator = ' ';


	plik = fopen(file_name, "wt");
	if (plik == NULL)	{
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Nie mogę utworzyć pliku, sprawdź uprawnienia."));
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
		for (j=0; j < N_FTXT; j++) g_free(line_col[j]); // usuniecie z pamieci nazw kolumn
		g_free(file_name);
		return;
	}
	else {
		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnHeader))) {
			// zapisywanie nazw kolumn
			fprintf(plik, line_col[uklad[0]]);
			for (j=1; j < N_FTXT; j++)
				if (uklad[j] > -1)
					fprintf(plik, "%c%s", separator, line_col[uklad[j]]);
			fprintf(plik, "\n");
		}
		for (j=0; j < N_FTXT; j++) g_free(line_col[j]); // usuniecie z pamieci nazw kolumn
		
		// wyszukanie punktow
		dbgeod_points_find(NULL, &punkty, &punkty_id);
		npkt = g_slist_length(punkty);

		for (i=0; i < npkt; i++) {
			punkt = (cPunkt*)g_slist_nth_data(punkty, i);
			
			for (j=0; j < N_FTXT; j++) line_col[j] = NULL; // wyzerowanie tablicy wskaznikow
			line_col[FTXT_NR] = g_strdup(punkt->getNr());
			line_col[FTXT_KOD] = g_strdup(punkt->getKod());
			line_col[FTXT_TYP] = g_strdup_printf("%d", punkt->getTyp());
			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnSKropka))) {
				line_col[FTXT_X] = g_strdup(g_ascii_formatd(bufor, sizeof(bufor), "%.4f", punkt->getX()));
				line_col[FTXT_Y] = g_strdup(g_ascii_formatd(bufor, sizeof(bufor), "%.4f", punkt->getY()));
				line_col[FTXT_H] = g_strdup(g_ascii_formatd(bufor, sizeof(bufor), "%.5f", punkt->getH()));
			}
			else {
				line_col[FTXT_X] = g_strdup_printf("%.*f", dok_xy_int, punkt->getX());
				line_col[FTXT_Y] = g_strdup_printf("%.*f", dok_xy_int, punkt->getY());
				line_col[FTXT_H] = g_strdup_printf("%.*f", dok_h_int, punkt->getH());
			}			

			// zapisywanie
			fprintf(plik, line_col[uklad[0]]);
			for (j=1; j < N_FTXT; j++)
				if (uklad[j] > -1)
					fprintf(plik, "%c%s", separator, line_col[uklad[j]]);
			fprintf(plik, "\n");
			for (j=0; j < N_FTXT; j++) g_free(line_col[j]); // usuniecie z pamieci tablicy
		}
		g_slist_free(punkty);
		g_slist_free(punkty_id);
		fclose(plik);
	}
	g_free(file_name);
	gtk_widget_destroy(window);

	dialog_txt = g_strdup_printf(_("Zapisano %d punktów.\n"), npkt);
	dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, dialog_txt);
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
	g_free(dialog_txt);
}


/* dialogRaport */
void on_dialogRaport_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *textview = lookup_widget(GTK_WIDGET(widget), "textviewDialogRaport");
	GtkTextBuffer *textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
	PangoFontDescription *font_desc;

	gtk_text_buffer_create_tag (textbuffer, "bold", "weight", PANGO_WEIGHT_BOLD, NULL);
	gtk_text_buffer_create_tag (textbuffer, "italic", "style", PANGO_STYLE_ITALIC, NULL);
	gtk_text_buffer_create_tag (textbuffer, "blue", "foreground", "blue", NULL);
	gtk_text_buffer_create_tag (textbuffer, "left", "justification", GTK_JUSTIFY_LEFT, NULL);
	gtk_text_buffer_create_tag (textbuffer, "right", "justification", GTK_JUSTIFY_RIGHT, NULL);
	gtk_text_buffer_create_tag (textbuffer, "center", "justification", GTK_JUSTIFY_CENTER, NULL);

	/* zmiana domyslnej czcionki dla widgetu */
	font_desc = pango_font_description_from_string ("courier 10");
	gtk_widget_modify_font (textview, font_desc);
	pango_font_description_free (font_desc);
	/* ustawienie lewego marginesu */
	gtk_text_view_set_left_margin (GTK_TEXT_VIEW (textview), 10);
}


void on_mnuDialogRaportZapisz_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	char *file_name = NULL;
	GtkWidget *dialog = NULL;
	FILE *plik;
	GtkWidget *filechooser = gtk_file_chooser_dialog_new (_("Zapisz raport"),
		NULL, GTK_FILE_CHOOSER_ACTION_SAVE,
		GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
		GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT, NULL);
	GtkWidget *textview = lookup_widget(GTK_WIDGET(menuitem), "textviewDialogRaport");
	GtkTextBuffer *textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
	GtkTextIter start_iter, end_iter;
	gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(textbuffer), &start_iter, &end_iter);

	
	if (gtk_dialog_run (GTK_DIALOG (filechooser)) == GTK_RESPONSE_ACCEPT) {
		file_name = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (filechooser));

		if (file_name == NULL) {
			dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Nie wybrano pliku."));
			gtk_dialog_run(GTK_DIALOG(dialog));
			g_free(file_name);
			gtk_widget_destroy(dialog);
			gtk_widget_destroy (filechooser);
			return;
		}
		if (g_file_test(file_name, G_FILE_TEST_IS_DIR)) {
			dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Wybrano katalog zamiast pliku"));
			gtk_dialog_run(GTK_DIALOG(dialog));
			g_free(file_name);
			gtk_widget_destroy(dialog);
			gtk_widget_destroy (filechooser);
			return;
		}
		if (g_file_test(file_name, G_FILE_TEST_EXISTS)) {
			dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_WARNING, GTK_BUTTONS_OK_CANCEL, _("Uwaga podany plik istnieje.\nZapis spowoduje usunięcie istniejącego pliku.\nKontynuować?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_CANCEL) {
				g_free(file_name);
				gtk_widget_destroy(dialog);
				gtk_widget_destroy (filechooser);
				return;
			}
			else
				gtk_widget_destroy(dialog);
		}

		plik = fopen(file_name, "wt");
		if (plik == NULL)	{
			dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("Nie mogę utworzyć pliku, sprawdź uprawnienia."));
			gtk_dialog_run(GTK_DIALOG(dialog));
			gtk_widget_destroy(dialog);
			g_free(file_name);
			gtk_widget_destroy (filechooser);
			return;
		}
		else {
			// zapisywanie
			fprintf(plik, gtk_text_buffer_get_text(GTK_TEXT_BUFFER(textbuffer), &start_iter, &end_iter, FALSE));
		}
		fclose(plik);
	}
	g_free(file_name);
	gtk_widget_destroy(filechooser);
}


void on_mnuDialogRaportZakoncz_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	GtkWidget *dialog = lookup_widget(GTK_WIDGET(menuitem), "dialogRaport");
	gtk_widget_destroy(dialog);
}


void on_btnDialogRaportOk_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *dialog = lookup_widget(GTK_WIDGET(button), "dialogRaport");
	gtk_widget_destroy(dialog);
}


/* winAbout */
void on_winAbout_response (GtkDialog *dialog, gint response_id, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winAbout");
	gtk_widget_destroy(GTK_WIDGET(dialog));
}


/* winHistoria */
void on_winHistoria_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeHistoria");
	GtkTreeStore *historia_model;
	
	historia_model=gtk_tree_store_new(N_HIS_COLUMNS, G_TYPE_INT, \
		G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), \
	   GTK_TREE_MODEL(historia_model));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;

	// kolumna id
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("ID"), renderer, "text", COL_HIS_ID, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 40);

	// kolumna kolejnosc
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Kolejność"), renderer, "text", COL_HIS_ORDER, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 40);

	// kolumna polecenie
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Polecenie"), renderer, "text", COL_HIS_FUNCTION, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 150);

	// kolumna dane
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Dane (pkt)"), renderer, "text", COL_HIS_DATA, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);
	
	// kolumna wynik
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Wynik (pkt)"), renderer, "text", COL_HIS_REZULT, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 80);

	// kolumna data
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Obliczono"), renderer, "text", COL_HIS_MOD, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 80);

	// ODCZYTANIE ZADAN Z BAZY
	treeHistoria_update_view(treeview);
}


void on_winHistoria_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winHistoria");
}


void on_tbtnHistoriaPonow_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeHistoria");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreeIter iter;
	guint n_pkt = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	guint i;
	int zadanie_id;
	char *nfunc;
	double parametr[7] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0};
	double az_k1, az_k2, az0, dh;
	char nr_pkt[NR_MAX_LEN];
	cPunkt *pktDane = NULL;
	cPunkt *pktWynik = NULL;
	bool blad;

	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (i=0; i<n_pkt; i++) {
		blad=FALSE;
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_HIS_ID, &zadanie_id, COL_HIS_FUNCTION, &nfunc, -1);

		if (g_str_equal(nfunc, "ZadanieMareka")) {
			pktDane = new cPunkt[4];
			pktWynik = new cPunkt[2];
			if (dbgeod_hist_point_get(zadanie_id, "St1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "St2Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "L1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "P1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "L2Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[2])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "P2Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[3])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "KatAlfa", parametr[1])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "KatBeta", parametr[0])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "KatGamma", parametr[3])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "KatDelta", parametr[2])!=0) blad=TRUE;
			if (zadanie_Mareka(pktDane[0], pktDane[1], pktDane[2], pktDane[3],
				parametr[0], parametr[1], parametr[2], parametr[3],
				pktWynik[0], pktWynik[1]) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, pktWynik[0], TRUE);
				dbgeod_point_update(-1, pktWynik[1], TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete [] pktWynik;
		}
		else if (g_str_equal(nfunc, "WciecieKatoweOgolne")) {
			pktDane = new cPunkt[4];
			pktWynik = new cPunkt;

			if (dbgeod_hist_point_get(zadanie_id, "LNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "L1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[2])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "P1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[3])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, pktWynik)!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "KatL", parametr[0])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "KatP", parametr[1])!=0) blad=TRUE;

			if (wciecie_w_przod_ogolne(pktDane[0], pktDane[1], parametr[0],
				pktDane[2], pktDane[3], parametr[1], *pktWynik) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, *pktWynik, TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete pktWynik;
		}
		else if (g_str_equal(nfunc, "WciecieKatowe")) {
			pktDane = new cPunkt[2];
			pktWynik = new cPunkt;

			if (dbgeod_hist_point_get(zadanie_id, "LNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, pktWynik)!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "KatL", parametr[0])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "KatP", parametr[1])!=0) blad=TRUE;

			if (wciecie_katowe_w_przod(pktDane[0], pktDane[1], parametr[0],
				parametr[1], *pktWynik) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, *pktWynik, TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete pktWynik;
		}
		else if (g_str_equal(nfunc, "WciecieLiniowe")) {
			pktDane = new cPunkt[2];
			pktWynik = new cPunkt;

			if (dbgeod_hist_point_get(zadanie_id, "LNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, pktWynik)!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "DlugoscLW", parametr[0])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "DlugoscPW", parametr[1])!=0) blad=TRUE;

			if (wciecie_liniowe(pktDane[0], pktDane[1], parametr[0],
				parametr[1], *pktWynik) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, *pktWynik, TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete pktWynik;
		}
		else if (g_str_equal(nfunc, "WciecieWstecz")) {
			pktDane = new cPunkt[3];
			pktWynik = new cPunkt;

			if (dbgeod_hist_point_get(zadanie_id, "LNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "CNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[2])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, pktWynik)!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "kier", parametr[0])!=0) blad=TRUE;
			if (!blad and (int)(parametr[0]+0.5)) {
				if (dbgeod_hist_par_get(zadanie_id, "KierL", parametr[1])!=0) blad=TRUE;
				if (dbgeod_hist_par_get(zadanie_id, "KierC", parametr[2])!=0) blad=TRUE;
				if (dbgeod_hist_par_get(zadanie_id, "KierP", parametr[3])!=0) blad=TRUE;
			}
			else {
				parametr[1]=0.0;
				if (dbgeod_hist_par_get(zadanie_id, "KatAlfa", parametr[2])!=0) blad=TRUE;
				if (dbgeod_hist_par_get(zadanie_id, "KatBeta", parametr[3])!=0) blad=TRUE;
			}
			if (wciecie_wstecz(pktDane[0], pktDane[1], pktDane[2], parametr[2]-parametr[1],
				parametr[3]-parametr[1], *pktWynik) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, *pktWynik, TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete pktWynik;
		}
		else if (g_str_equal(nfunc, "WciecieAzymutalneWPrzod")) {
			pktDane = new cPunkt[2];
			pktWynik = new cPunkt;

			if (dbgeod_hist_point_get(zadanie_id, "LNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, pktWynik)!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "AzymutLW", parametr[0])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "AzymutPW", parametr[1])!=0) blad=TRUE;

			if (wciecie_azymutalne_w_przod(pktDane[0], pktDane[1], parametr[0],
				parametr[1], *pktWynik) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, *pktWynik, TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete pktWynik;
		}
		else if (g_str_equal(nfunc, "WciecieAzymutalneWstecz")) {
			pktDane = new cPunkt[2];
			pktWynik = new cPunkt;

			if (dbgeod_hist_point_get(zadanie_id, "LNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, pktWynik)!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "AzymutWL", parametr[0])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "AzymutWP", parametr[1])!=0) blad=TRUE;

			if (wciecie_azymutalne_wstecz(pktDane[0], pktDane[1], parametr[0],
				parametr[1], *pktWynik) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, *pktWynik, TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete pktWynik;
		}
		else if (g_str_equal(nfunc, "OkragStycznaProstaRownolegla")) {
			pktDane = new cPunkt[3];
			pktWynik = new cPunkt[2];

			if (dbgeod_hist_point_get(zadanie_id, "SNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PrNr1", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PrNr2", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[2])!=0) blad=TRUE;

			if (dbgeod_hist_point_get(zadanie_id, "W1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "W2Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[1])!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "Promien", parametr[0])!=0) blad=TRUE;

			if (okrag_styczna_prosta_rownolegla(pktDane[0], parametr[0],
				pktDane[1], pktDane[2], pktWynik[0], pktWynik[1]) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, pktWynik[0], TRUE);
				dbgeod_point_update(-1, pktWynik[1], TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete [] pktWynik;
		}
		else if (g_str_equal(nfunc, "OkragStycznaProstaProstopadla")) {
			pktDane = new cPunkt[3];
			pktWynik = new cPunkt[2];

			if (dbgeod_hist_point_get(zadanie_id, "SNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PrNr1", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PrNr2", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[2])!=0) blad=TRUE;

			if (dbgeod_hist_point_get(zadanie_id, "W1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "W2Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[1])!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "Promien", parametr[0])!=0) blad=TRUE;

			if (okrag_styczna_prosta_prostopadla(pktDane[0], parametr[0],
				pktDane[1], pktDane[2], pktWynik[0], pktWynik[1]) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, pktWynik[0], TRUE);
				dbgeod_point_update(-1, pktWynik[1], TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete [] pktWynik;
		}
		else if (g_str_equal(nfunc, "OkragStyczna2Okregi")) {
			pktDane = new cPunkt[2];
			pktWynik = new cPunkt[4];

			if (dbgeod_hist_point_get(zadanie_id, "S1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "S2Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;

			if (dbgeod_hist_point_get(zadanie_id, "W1aNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "W1bNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "W2aNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[2])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "W2bNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[3])!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "PromienOkr1", parametr[0])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "PromienOkr2", parametr[1])!=0) blad=TRUE;
			
			if (okrag_styczna_2okregi(pktDane[0], parametr[0], pktDane[1], parametr[1],
				pktWynik[0], pktWynik[1], pktWynik[2], pktWynik[3]) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, pktWynik[0], TRUE);
				dbgeod_point_update(-1, pktWynik[1], TRUE);
				dbgeod_point_update(-1, pktWynik[2], TRUE);
				dbgeod_point_update(-1, pktWynik[3], TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete [] pktWynik;
		}
		else if (g_str_equal(nfunc, "OkragStycznaPunkt")) {
			pktDane = new cPunkt[2];
			pktWynik = new cPunkt[2];

			if (dbgeod_hist_point_get(zadanie_id, "SNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PktNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;

			if (dbgeod_hist_point_get(zadanie_id, "W1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "W2Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[1])!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "Promien", parametr[0])!=0) blad=TRUE;
			
			if (okrag_styczna_punkt(pktDane[0], parametr[0], pktDane[1],
				pktWynik[0], pktWynik[1]) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, pktWynik[0], TRUE);
				dbgeod_point_update(-1, pktWynik[1], TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete [] pktWynik;
		}
		else if (g_str_equal(nfunc, "OkragWpasowanie2Proste")) {
			pktDane = new cPunkt[4];
			pktWynik = new cPunkt;

			if (dbgeod_hist_point_get(zadanie_id, "PLNr1", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PLNr2", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PPNr1", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[2])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PPNr2", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[3])!=0) blad=TRUE;

			if (dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, pktWynik)!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "Promien", parametr[0])!=0) blad=TRUE;
			
			if (okrag_wpasowanie_2proste(pktDane[0], pktDane[1], pktDane[2], pktDane[3],
				parametr[0], *pktWynik) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, *pktWynik, TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete pktWynik;
		}
		else if (g_str_equal(nfunc, "OkragWpasowanie3Punkty")) {
			pktDane = new cPunkt[3];
			pktWynik = new cPunkt;

			if (dbgeod_hist_point_get(zadanie_id, "Pkt1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "Pkt2Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "Pkt3Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[2])!=0) blad=TRUE;

			if (dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, pktWynik)!=0) blad=TRUE;

			if (okrag_wpasowanie_3punkty(pktDane[0], pktDane[1], pktDane[2],
				*pktWynik, parametr[0]) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, *pktWynik, TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete pktWynik;
		}
		else if (g_str_equal(nfunc, "PrzeciecieProste")) {
			pktDane = new cPunkt[4];
			pktWynik = new cPunkt;

			if (dbgeod_hist_point_get(zadanie_id, "Pr1Nr1", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "Pr1Nr2", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "Pr2Nr1", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[2])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "Pr2Nr2", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[3])!=0) blad=TRUE;

			if (dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, pktWynik)!=0) blad=TRUE;

			if (przeciecie_prostych(pktDane[0], pktDane[1], pktDane[2], pktDane[3],
				*pktWynik) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, *pktWynik, TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete pktWynik;
		}
		else if (g_str_equal(nfunc, "PrzeciecieOkragProsta")) {
			pktDane = new cPunkt[3];
			pktWynik = new cPunkt[2];

			if (dbgeod_hist_point_get(zadanie_id, "OkrNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PrNr1", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PrNr2", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[2])!=0) blad=TRUE;

			if (dbgeod_hist_point_get(zadanie_id, "W1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "W2Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[1])!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "Promien", parametr[0])!=0) blad=TRUE;

			if (przeciecie_okrag_prosta(pktDane[0], parametr[0],
				pktDane[1], pktDane[2], pktWynik[0], pktWynik[1]) >= 0 and !blad) {
				dbgeod_point_update(-1, pktWynik[0], TRUE);
				dbgeod_point_update(-1, pktWynik[1], TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete [] pktWynik;
		}
		else if (g_str_equal(nfunc, "PrzeciecieOkregi")) {
			pktDane = new cPunkt[2];
			pktWynik = new cPunkt[2];

			if (dbgeod_hist_point_get(zadanie_id, "Okr1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "Okr2Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "PromienOkr1", parametr[0])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "PromienOkr2", parametr[1])!=0) blad=TRUE;

			if (dbgeod_hist_point_get(zadanie_id, "W1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "W2Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[1])!=0) blad=TRUE;


			if (wciecie_liniowe(pktDane[0], pktDane[1], parametr[0],
				parametr[1], pktWynik[0]) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, pktWynik[0], TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			if (wciecie_liniowe(pktDane[1], pktDane[0], parametr[1],
				parametr[0], pktWynik[1]) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, pktWynik[1], TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete [] pktWynik;
		}
		else if (g_str_equal(nfunc, "RzutProsta")) {
			pktDane = new cPunkt[3];
			pktWynik = new cPunkt;

			if (dbgeod_hist_point_get(zadanie_id, "PrPPNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PrPKNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PktRzutowanyNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[2])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "PktRzutNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktWynik[0])!=0) blad=TRUE;
			if (rzut_prosta(pktDane[0], pktDane[1], pktDane[2],
				parametr[0], parametr[1]) == GEODERR_OK and !blad)
				if (domiar_prostokatny(pktDane[0], pktDane[1], *pktWynik,
					parametr[0]) == GEODERR_OK and !blad) {
					dbgeod_point_update(-1, *pktWynik, TRUE);
					dbgeod_hist_modtime(zadanie_id);
				}
			delete [] pktDane;
			delete pktWynik;
		}
		else if (g_str_equal(nfunc, "OkragTyczenie")) {
			pktDane = new cPunkt[2];
			pktWynik = new cPunkt;

			if (dbgeod_hist_point_get(zadanie_id, "OkragONr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "OkragPNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "biezaca", parametr[0])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "domiar", parametr[1])!=0) blad=TRUE;

			if (dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, pktWynik)!=0) blad=TRUE;

			if (!blad) {
				parametr[2] = dlugosc(pktDane[0], pktDane[1]);
				azymut(pktDane[0], pktDane[1], az0);
				if (parametr[2] == 0.0)
					pktWynik->setXY(pktDane[0].getX(), pktDane[0].getY());
				else
					punkt_biegun(pktDane[0], az0+(parametr[0]/parametr[2]), parametr[2]-parametr[1], *pktWynik);

				dbgeod_point_update(-1, *pktWynik, TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete pktWynik;
		}
		else if (g_str_equal(nfunc, "Domiary")) {
			pktDane = new cPunkt[2];
			pktWynik = new cPunkt;

			if (dbgeod_hist_point_get(zadanie_id, "BazaPPNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "BazaPKNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "bazaPomierzona", parametr[0])!=0) parametr[0]=0.0;
			if (dbgeod_hist_par_get(zadanie_id, "biezaca", parametr[1])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "domiar", parametr[2])!=0) blad=TRUE;

			if (dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, pktWynik)!=0) blad=TRUE;

			if (domiar_prostokatny(pktDane[0], pktDane[1], *pktWynik, parametr[1],
				parametr[2], parametr[0]) == GEODERR_OK and !blad) {
				dbgeod_point_update(-1, *pktWynik, TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete pktWynik;
		}
		else if (g_str_equal(nfunc, "Tachimetria_DsV")) {
			pktDane = new cPunkt[3];
			pktWynik = new cPunkt;

			if (dbgeod_hist_point_get(zadanie_id, "StNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "N1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "N2Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[2])!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "Kier1", parametr[0])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "Kier2", parametr[1])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "i_st", parametr[2])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "i_cel", parametr[3])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "odleglosc", parametr[4])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "KierHz", parametr[5])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "KatV", parametr[6])!=0) blad=TRUE;
				
			if (dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, pktWynik)!=0) blad=TRUE;

			// obliczenie azymutu miejsca 0
			azymut(pktDane[0], pktDane[1], az_k1);
			azymut(pktDane[0], pktDane[2], az_k2);
			az_k1 = norm_kat(az_k1-parametr[0]);
			az_k2 = norm_kat(az_k2-parametr[1], az_k1);
			az0 = 0.5*(az_k1+az_k2);

			dh = dh_odl_skosna(parametr[4], parametr[6]); //dsv
			pktWynik->setH(H_cel(pktDane[0].getH(), dh, parametr[2], parametr[3]));

			// redukcja odleglosci
			parametr[4] = ds_na_poz_odniesienia(parametr[4], pktDane[0].getH(), parametr[2], pktWynik->getH(), parametr[3]);
			punkt_biegun(pktDane[0], az0+parametr[5], parametr[4], *pktWynik);
			if (!blad) {
				dbgeod_point_update(-1, *pktWynik, TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete pktWynik;
		}
		else if (g_str_equal(nfunc, "Tachimetria_DpV")) {
			pktDane = new cPunkt[3];
			pktWynik = new cPunkt;

			if (dbgeod_hist_point_get(zadanie_id, "StNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "N1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "N2Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[2])!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "Kier1", parametr[0])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "Kier2", parametr[1])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "i_st", parametr[2])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "i_cel", parametr[3])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "odleglosc", parametr[4])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "KierHz", parametr[5])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "KatV", parametr[6])!=0) blad=TRUE;
				
			if (dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, pktWynik)!=0) blad=TRUE;
			// obliczenie azymutu miejsca 0
			azymut(pktDane[0], pktDane[1], az_k1);
			azymut(pktDane[0], pktDane[2], az_k2);
			az_k1 = norm_kat(az_k1-parametr[0]);
			az_k2 = norm_kat(az_k2-parametr[1], az_k1);
			az0 = 0.5*(az_k1+az_k2);

			dh = dh_odl_pozioma(parametr[4], parametr[6]); //dpv
			pktWynik->setH(H_cel(pktDane[0].getH(), dh, parametr[2], parametr[3]));

			// redukcja odleglosci
			parametr[4] = d_na_poz_odniesienia(parametr[4], pktDane[0].getH(), pktWynik->getH());
			punkt_biegun(pktDane[0], az0+parametr[5], parametr[4], *pktWynik);
			if (!blad) {
				dbgeod_point_update(-1, *pktWynik, TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete pktWynik;
		}
		else if (g_str_equal(nfunc, "Tachimetria_DpH")) {
			pktDane = new cPunkt[3];
			pktWynik = new cPunkt;

			if (dbgeod_hist_point_get(zadanie_id, "StNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[0])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "N1Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[1])!=0) blad=TRUE;
			if (dbgeod_hist_point_get(zadanie_id, "N2Nr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, &pktDane[2])!=0) blad=TRUE;

			if (dbgeod_hist_par_get(zadanie_id, "Kier1", parametr[0])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "Kier2", parametr[1])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "i_st", parametr[2])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "i_cel", parametr[3])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "odleglosc", parametr[4])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "KierHz", parametr[5])!=0) blad=TRUE;
			if (dbgeod_hist_par_get(zadanie_id, "dH", parametr[6])!=0) blad=TRUE;
				
			if (dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt)!=0) blad=TRUE;
			if (dbgeod_pointt_find(nr_pkt, pktWynik)!=0) blad=TRUE;

			// obliczenie azymutu miejsca 0
			azymut(pktDane[0], pktDane[1], az_k1);
			azymut(pktDane[0], pktDane[2], az_k2);
			az_k1 = norm_kat(az_k1-parametr[0]);
			az_k2 = norm_kat(az_k2-parametr[1], az_k1);
			az0 = 0.5*(az_k1+az_k2);

			dh = parametr[6];
			pktWynik->setH(H_cel(pktDane[0].getH(), dh, parametr[2], parametr[3]));

			// redukcja odleglosci
			parametr[4] = d_na_poz_odniesienia(parametr[4], pktDane[0].getH(), pktWynik->getH());
			punkt_biegun(pktDane[0], az0+parametr[5], parametr[4], *pktWynik);
			if (!blad) {
				dbgeod_point_update(-1, *pktWynik, TRUE);
				dbgeod_hist_modtime(zadanie_id);
			}
			delete [] pktDane;
			delete pktWynik;
		}
		g_free(nfunc);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	treeHistoria_update_view(tree);
}


void on_tbtnHistoriaUsun_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_WARNING, GTK_BUTTONS_YES_NO,
		_("Czy na pewno usunąć ostatni wpis w historii?"));
	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES) {
		dbgeod_hist_del(-1);
		GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeHistoria");
			treeHistoria_update_view(GTK_WIDGET(treeview));
	}
	gtk_widget_destroy(dialog);
}


void on_tbtnHistoriaEdytuj_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *treeHistoria = lookup_widget(GTK_WIDGET(toolbutton), "treeHistoria");
	GtkTreeModel *modelHistoria = gtk_tree_view_get_model(GTK_TREE_VIEW(treeHistoria));
	GtkWidget *dialog = NULL;
	GtkTreePath *tpath;
	GtkTreeIter iter;
	int zadanie_id, hist_kolejnosc=0;
	char hist_nazwa[255];
	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(treeHistoria), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(modelHistoria), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(modelHistoria), &iter, COL_HIS_ID, &zadanie_id, -1);
		dialog = create_dialogHistoriaEdytuj();
		gtk_widget_show(GTK_WIDGET(dialog));
		GtkWidget *treeHistoriaEdytuj = lookup_widget(GTK_WIDGET(dialog), "treeDialogHistoriaEdytuj");
		GtkWidget *labelHistoriaEdytujFunkcja = lookup_widget(GTK_WIDGET(dialog), "labelDialogHistoriaEdytujFunkcja");
		GtkWidget *labelHistoriaEdytujId = lookup_widget(GTK_WIDGET(dialog), "labelDialogHistoriaEdytujId");
		GtkWidget *labelHistoriaEdytujKolejnosc = lookup_widget(GTK_WIDGET(dialog), "labelDialogHistoriaEdytujKolejnosc");
		treeDialogHistoriaEdytuj_update_view(treeHistoriaEdytuj, zadanie_id);
		dbgeod_hist_get(zadanie_id, hist_nazwa, hist_kolejnosc);
		gtk_label_set_label(GTK_LABEL(labelHistoriaEdytujFunkcja), g_strdup_printf("<big>%s</big>", hist_nazwa));
		gtk_label_set_label(GTK_LABEL(labelHistoriaEdytujId), g_strdup_printf("%d", zadanie_id));
		gtk_label_set_label(GTK_LABEL(labelHistoriaEdytujKolejnosc), g_strdup_printf("%d", hist_kolejnosc));
		gtk_tree_path_free(tpath);
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarHistoria", _("Zaznacz najpierw wpis w tabeli"));
}


void on_tbtnHistoriaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winHistoria");
	gtk_widget_destroy(window);
}


void on_btnTHelmertaDostDodajWszystkie_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeTHelmertaDost");
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(button), "tbtnTHelmertaOblicz");
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	char *txtXp = NULL, *txtYp = NULL, *txtXw = NULL, *txtYw = NULL;

	GSList *punkty = NULL;
	guint ile_pkt, i;
	cPunkt *pktDostP = NULL, *pktDostW = NULL;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	char konf_lokalny[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	dbgeod_config_get("lokalny", konf_lokalny);

	
	if(!dbgeod_points_transf_find(&punkty))
		ile_pkt = g_slist_length(punkty);
	else ile_pkt = 0;
	
	for (i=0;i<ile_pkt;i++) {
		pktDostP = new cPunkt;
		pktDostW = new cPunkt;
		if (g_str_equal(konf_lokalny, "1")) {
			dbgeod_point_find((char*)g_slist_nth_data(punkty, i), pktDostP, GEODPKT_OSNOWA);
			dbgeod_point_find((char*)g_slist_nth_data(punkty, i), pktDostW, GEODPKT_OSNOWAL);
		}
		else {
			dbgeod_point_find((char*)g_slist_nth_data(punkty, i), pktDostP, GEODPKT_OSNOWAL);
			dbgeod_point_find((char*)g_slist_nth_data(punkty, i), pktDostW, GEODPKT_OSNOWA);
		}

		txtXp = g_strdup_printf("%.*f", dok_xy_int, pktDostP->getX());
		txtYp = g_strdup_printf("%.*f", dok_xy_int, pktDostP->getY());
		txtXw = g_strdup_printf("%.*f", dok_xy_int, pktDostW->getX());
		txtYw = g_strdup_printf("%.*f", dok_xy_int, pktDostW->getY());

		gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
			COL_THD_NRP, pktDostP->getNr(),
			COL_THD_XP, txtXp, COL_THD_XP_DOUBLE, pktDostP->getX(),
			COL_THD_YP, txtYp, COL_THD_YP_DOUBLE, pktDostP->getY(),
			COL_THD_NRW, pktDostW->getNr(),
			COL_THD_XW, txtXw, COL_THD_XW_DOUBLE, pktDostW->getX(),
			COL_THD_YW, txtYw, COL_THD_YW_DOUBLE, pktDostW->getY(), -1);
		
		g_free(txtXp);
		g_free(txtYp);
		g_free(txtXw);
		g_free(txtYw);
		delete pktDostP;
		delete pktDostW;
	}

	g_signal_emit_by_name(btnOblicz, "clicked");
	
	g_slist_free(punkty);
}


void on_btnTHelmertaTransfDodajWszystkie_clicked (GtkButton *button, gpointer user_data)
{
	GSList *punkty = NULL;
	GSList *punkty_id = NULL;
	guint ile_pkt, i;
	
	cPunkt *pktTransf = NULL;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeTHelmertaTransf");
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(button), "tbtnTHelmertaOblicz");
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	char *txtXt = NULL, *txtYt = NULL;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if(!dbgeod_points_find(NULL, &punkty, &punkty_id))
		ile_pkt = g_slist_length(punkty);
	else ile_pkt = 0;
	
	for (i=0;i<ile_pkt;i++) {	
		pktTransf = (cPunkt*)g_slist_nth_data(punkty, i);
		txtXt = g_strdup_printf("%.*f", dok_xy_int, pktTransf->getX());
		txtYt = g_strdup_printf("%.*f", dok_xy_int, pktTransf->getY());
 	
		gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
			COL_THT_NRP, pktTransf->getNr(), COL_THT_TOGGLE, FALSE,
			COL_THT_XP, txtXt, COL_THT_XP_DOUBLE, pktTransf->getX(),
			COL_THT_YP, txtYt, COL_THT_YP_DOUBLE, pktTransf->getY(),
			COL_THT_DODAJ, PKT_NIE_DODAWAJ, COL_THT_NRW, pktTransf->getNr(), -1);
	
		g_free(txtXt);
		g_free(txtYt);
	}
	g_signal_emit_by_name(btnOblicz, "clicked");
	g_slist_free(punkty_id);
	g_slist_free(punkty);
}


void on_btnDomiaryMiaryDodajWszystkie_clicked (GtkButton *button, gpointer user_data)
{
	GSList *punkty = NULL;
	guint ile_pkt, i;

	GtkWidget *cbtnNoAdd = lookup_widget(GTK_WIDGET(button), "cbtnDomiaryMiaryDodajLista");
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeDomiary");
	GtkWidget *entryMiaryKod = lookup_widget(GTK_WIDGET(button), "entryDomiaryMiaryKod");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreeIter iter;
	cPunkt pktBazaP, pktBazaK;

	double biezaca, domiar;
	char *txtB = NULL, *txtD = NULL, *txtNr = NULL, *txtKod = NULL;
	int s_dodaj;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryDomiaryBazaPPNr",
		"entryDomiaryBazaPPX", "entryDomiaryBazaPPY", NULL, pktBazaP));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarDomiary", _("Musisz podać Nr, X, Y pierwszego punktu bazy"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryDomiaryBazaPKNr",
		"entryDomiaryBazaPKX", "entryDomiaryBazaPKY", NULL, pktBazaK));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarDomiary", _("Musisz podać Nr, X, Y drugiego punktu bazy"));
		return;
	}


	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if(!dbgeod_obs_domiary_find(&punkty, pktBazaP.getNr(), pktBazaK.getNr()))
		ile_pkt = g_slist_length(punkty);
	else ile_pkt = 0;

	for (i=0;i<ile_pkt;i++) {	
		if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnNoAdd)))
			s_dodaj = PKT_NIE_DODAWAJ;
		else 
			s_dodaj = PKT_DODAJ;

		dbgeod_obs_get((char*)g_slist_nth_data(punkty, i),
			g_strdup_printf("$%s", pktBazaP.getNr()),
			g_strdup_printf("$%s", pktBazaK.getNr()), biezaca);
		dbgeod_obs_get((char*)g_slist_nth_data(punkty, i),
			g_strdup_printf("$%s", pktBazaP.getNr()),
			pktBazaK.getNr(), domiar);
		
		txtB = g_strdup_printf("%.*f", dok_xy_int, biezaca);
		txtD = g_strdup_printf("%.*f", dok_xy_int, domiar);
		txtNr = geod_prefix_rem((char*)g_slist_nth_data(punkty, i));
		txtKod = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryMiaryKod)));

		if (!dbgeod_point_find(txtNr)) s_dodaj = PKT_NIE_DODAWAJ;
	
		gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
			COL_DOM_NR, txtNr,
			COL_DOM_B, txtB, COL_DOM_B_DOUBLE, biezaca,
			COL_DOM_D, txtD, COL_DOM_D_DOUBLE, domiar, 
			COL_DOM_KOD, txtKod, COL_DOM_DODAJ, s_dodaj, -1);

		if (s_dodaj == PKT_DODAJ)
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_DOM_TOGGLE, TRUE, -1);
		else
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_DOM_TOGGLE, FALSE, -1);
	
		g_free(txtB);
		g_free(txtD);
		g_free(txtNr);
		g_free(txtKod);
	}
	
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(button), "tbtnDomiaryOblicz");
	g_signal_emit_by_name(btnOblicz, "clicked");
	g_slist_free(punkty);
}


void on_btnRzutProstaPunktDodajWszystkie_clicked (GtkButton *button, gpointer user_data)
{
	GSList *punkty = NULL;
	GSList *punkty_id = NULL;
	guint ile_pkt, i;

	cPunkt *pktRzutowany;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeRzutProsta");
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	char *txtX = NULL, *txtY = NULL;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if(!dbgeod_points_find(NULL, &punkty, &punkty_id))
		ile_pkt = g_slist_length(punkty);
	else ile_pkt = 0;
	
	for (i=0;i<ile_pkt;i++) {	
		pktRzutowany = (cPunkt*)g_slist_nth_data(punkty, i);

		txtX = g_strdup_printf("%.*f", dok_xy_int, pktRzutowany->getX());
		txtY = g_strdup_printf("%.*f", dok_xy_int, pktRzutowany->getY());
 	
		gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
			COL_RZP_NR, pktRzutowany->getNr(), COL_RZP_NRR, pktRzutowany->getNr(),
			COL_RZP_X, txtX, COL_RZP_X_DOUBLE, pktRzutowany->getX(),
			COL_RZP_Y, txtY, COL_RZP_Y_DOUBLE, pktRzutowany->getY(),
			COL_RZP_DODAJ, PKT_NIE_DODAWAJ, COL_RZP_TOGGLE, FALSE,-1);

		g_free(txtX);
		g_free(txtY);
	}
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(button), "tbtnRzutProstaOblicz");
	g_signal_emit_by_name(btnOblicz, "clicked");
	g_slist_free(punkty_id);
	g_slist_free(punkty);
}

void on_btnBiegunowePktTyczonyDodajWszystkie_clicked (GtkButton *button, gpointer user_data)
{
	GSList *punkty = NULL;
	GSList *punkty_id = NULL;
	guint ile_pkt, i;

	cPunkt *pktTyczony;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeBiegunowe");
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	char *txtX = NULL, *txtY = NULL;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if(!dbgeod_points_find(NULL, &punkty, &punkty_id))
		ile_pkt = g_slist_length(punkty);
	else ile_pkt = 0;
	
	for (i=0;i<ile_pkt;i++) {	
		pktTyczony = (cPunkt*)g_slist_nth_data(punkty, i);
		txtX = g_strdup_printf("%.*f", dok_xy_int, pktTyczony->getX());
		txtY = g_strdup_printf("%.*f", dok_xy_int, pktTyczony->getY());
	
		gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
			COL_BIE_NR, pktTyczony->getNr(),
			COL_BIE_X, txtX, COL_BIE_X_DOUBLE, pktTyczony->getX(),
			COL_BIE_Y, txtY, COL_BIE_Y_DOUBLE, pktTyczony->getY(), -1);

		g_free(txtX);
		g_free(txtY);
	}
	g_signal_emit_by_name(lookup_widget(GTK_WIDGET(button), "tbtnBiegunoweOblicz"), "clicked");
	g_slist_free(punkty_id);
	g_slist_free(punkty);
}

void on_cbtnMainUkladLokalny_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
	GtkWidget *dialog;
	
	if (g_slist_length(otwarte_okna) > 0) {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_INFO, GTK_BUTTONS_OK, _("Zalecane jest zamknięcie wszystkich okien z obliczeniami."));
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
	}
	if (gtk_toggle_button_get_active(togglebutton))
		dbgeod_config_set("lokalny", "1");
	else
		dbgeod_config_set("lokalny", "0");
}


/* winTachimetria */
void on_mnuMainTachimetria_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winTachimetria, "winTachimetria") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}


void on_winTachimetria_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winTachimetria");
}


void on_winTachimetria_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeTachimetria");
	GtkTreeStore *tachimetria_model;
	
	tachimetria_model=gtk_tree_store_new(N_TACH_COLUMNS, \
	G_TYPE_STRING, G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, // nr,hz, odl
	G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, // vdh, hc
	G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, // x,y,h
	G_TYPE_STRING, G_TYPE_INT, G_TYPE_BOOLEAN);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), \
	    GTK_TREE_MODEL(tachimetria_model));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;
	
	// kolumna Nr
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Nr"), renderer, "text", COL_TACH_NR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// kolumna kier hz
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Kier. Hz"), renderer, "text", COL_TACH_HZ, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna odleglosc
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Odległość"), renderer, "text", COL_TACH_ODL, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 60);

	// kolumna kat v lub dh
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Kąt V"), renderer, "text", COL_TACH_VDH, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna kat H celu
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("H cel"), renderer, "text", COL_TACH_HC, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// kolumna wsp. X
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("X"), renderer, "text", COL_TACH_X, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna wsp. Y
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Y"), renderer, "text", COL_TACH_Y, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna wsp. H
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("H"), renderer, "text", COL_TACH_H, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 60);

	// kolumna Kod
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Kod"), renderer, "text", COL_TACH_KOD, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// kolumna Toggle
	GtkCellRenderer *rendererToggle = gtk_cell_renderer_toggle_new();
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("D"), rendererToggle, "active", COL_TACH_TOGGLE, NULL);
	GtkTreeViewColumn *toggleColumn = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(toggleColumn), 10);
}


void on_tbtnTachimetriaOblicz_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt pktSt, pktN1, pktN2;
	cPunkt *pktTachimetria = NULL;
	GtkWidget *toggleDsV = lookup_widget(GTK_WIDGET(toolbutton), "toggleTachimetriaDaneDsV");
	GtkWidget *toggleDpV = lookup_widget(GTK_WIDGET(toolbutton), "toggleTachimetriaDaneDpV");
	GtkWidget *toggleDpH = lookup_widget(GTK_WIDGET(toolbutton), "toggleTachimetriaDaneDpH");
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeTachimetria");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	guint l_wierszy = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	guint i, s_dodaj;
	int zadanie_id;
	enum TypPomiar {TACH_DSV, TACH_DPV, TACH_DPH} tpomiar=TACH_DSV;

	double i_st, i_cel, kier1, kier2, hz, odl, odlzr, vdh, dh=0.0;
	double az0, az_k1, az_k2;
	char *pkt_nr = NULL, *pkt_kod = NULL, *txtX = NULL, *txtY = NULL, *txtH = NULL;
	char dok_xy[CONFIG_VALUE_MAX_LEN], dok_h[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int=3, dok_h_int=3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	if (!dbgeod_config_get("dok_h", dok_h)) dok_h_int = atoi(dok_h);

	if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toggleDsV)))
		tpomiar = TACH_DSV;
	else if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toggleDpV)))
		tpomiar = TACH_DPV;
	else if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toggleDpH)))
		tpomiar = TACH_DPH;
	
	if (!geod_entry_get_pkt_xyh(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazanieStNr",
		"entryTachimetriaNawiazanieStX", "entryTachimetriaNawiazanieStY",
		"entryTachimetriaNawiazanieStH",	NULL, pktSt));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarTachimetria", _("Musisz podać Nr, X, Y, H stanowiska"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN1Nr",
		"entryTachimetriaNawiazaniePktN1X", "entryTachimetriaNawiazaniePktN1Y", NULL, pktN1));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarTachimetria", _("Musisz podać Nr, X, Y pierwszego punktu nawiązania"));
		return;
	}

	if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN1Kier", kier1));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarTachimetria", _("Musisz podać kierunek do pierwszego punktu nawiązania"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN2Nr",
		"entryTachimetriaNawiazaniePktN2X", "entryTachimetriaNawiazaniePktN2Y", NULL, pktN2));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarTachimetria", _("Musisz podać Nr, X, Y drugiego punktu nawiązania"));
		return;
	}

	if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN2Kier", kier2));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarTachimetria", _("Musisz podać kierunek do drugiego punktu nawiązania"));
		return;
	}
	
	if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazanieHi", i_st));
	else i_st = 0.0; // gdy nie godano wysokosci instr. przyjmujemy 0
	
	// obliczenie azymutu miejsca 0
	azymut(pktSt, pktN1, az_k1);
	azymut(pktSt, pktN2, az_k2);
	az_k1 = norm_kat(az_k1-kier1);
	az_k2 = norm_kat(az_k2-kier2);
	az_k2 = norm_kat(az_k2-kier2, az_k1);
	az0 = 0.5*(az_k1+az_k2);

	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (i = 0; i < l_wierszy; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_TACH_NR, &pkt_nr,
			COL_TACH_HZ_DOUBLE, &hz, COL_TACH_ODL_DOUBLE, &odl,
			COL_TACH_VDH_DOUBLE, &vdh, COL_TACH_HC_DOUBLE, &i_cel,
			COL_TACH_KOD, &pkt_kod, COL_TACH_DODAJ, &s_dodaj, -1);

		pktTachimetria = new cPunkt;
		pktTachimetria->setNr(pkt_nr);
		pktTachimetria->setKod(pkt_kod);
		
		// obliczenie przewyzszenia
		if (tpomiar==TACH_DPV)
			dh = dh_odl_pozioma(odl, vdh);
		else if (tpomiar==TACH_DSV)
			dh = dh_odl_skosna(odl, vdh);
		else if (tpomiar==TACH_DPH)
			dh = vdh;
		pktTachimetria->setH(H_cel(pktSt.getH(), dh, i_st, i_cel));

		// redukcja odleglosci
		if (tpomiar==TACH_DSV)
			odlzr = ds_na_poz_odniesienia(odl, pktSt.getH(), i_st, pktTachimetria->getH(), i_cel);
		else
			odlzr = d_na_poz_odniesienia(odl, pktSt.getH(), pktTachimetria->getH());
		punkt_biegun(pktSt, az0+hz, odlzr, *pktTachimetria);

		// dodanie wynikow do tabeli
		txtX = g_strdup_printf("%.*f", dok_xy_int, pktTachimetria->getX());
		txtY = g_strdup_printf("%.*f", dok_xy_int, pktTachimetria->getY());
		txtH = g_strdup_printf("%.*f", dok_h_int, pktTachimetria->getH());
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
			COL_TACH_X, txtX, COL_TACH_X_DOUBLE, pktTachimetria->getX(),
			COL_TACH_Y, txtY, COL_TACH_Y_DOUBLE, pktTachimetria->getY(),
			COL_TACH_H, txtH, COL_TACH_H_DOUBLE, pktTachimetria->getH(), -1);

		switch (s_dodaj) {
			case PKT_DODAJ:
				if (dbgeod_point_add_config(*pktTachimetria) != GEODERR_OK) {
					geod_sbar_set(GTK_WIDGET(toolbutton), "sbarTachimetria", _("Nie mogę dodać punktu do bazy danych!"));
					gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_TACH_DODAJ, PKT_ERROR, -1);
				}
				else {
					gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_TACH_DODAJ, PKT_DODANY, -1);
					if (tpomiar==TACH_DSV) {
						dbgeod_hist_add("Tachimetria_DsV", zadanie_id);
						dbgeod_hist_par_set(zadanie_id, "KatV", vdh, 0);
					}
					if (tpomiar==TACH_DPV) {
						dbgeod_hist_add("Tachimetria_DpV", zadanie_id);
						dbgeod_hist_par_set(zadanie_id, "KatV", vdh, 0);
					}
					if (tpomiar==TACH_DPH) {
						dbgeod_hist_add("Tachimetria_DpH", zadanie_id);
						dbgeod_hist_par_set(zadanie_id, "dH", vdh, 0);
					}
					
					dbgeod_hist_point_set(zadanie_id, "StNr", pktSt.getNr(), 0);
					dbgeod_hist_point_set(zadanie_id, "N1Nr", pktN1.getNr(), 0);
					dbgeod_hist_point_set(zadanie_id, "N2Nr", pktN2.getNr(), 0);
					dbgeod_hist_point_set(zadanie_id, "WNr", pktTachimetria->getNr(), 1);
					
					dbgeod_hist_par_set(zadanie_id, "Kier1", kier1, 0);
					dbgeod_hist_par_set(zadanie_id, "Kier2", kier2, 0);
					dbgeod_hist_par_set(zadanie_id, "i_st", i_st, 0);
					dbgeod_hist_par_set(zadanie_id, "i_cel", i_cel, 0);
					dbgeod_hist_par_set(zadanie_id, "odleglosc", odl, 0);
					dbgeod_hist_par_set(zadanie_id, "KierHz", hz, 0);

				}
			break;
				
			case PKT_DODANY:
				dbgeod_point_update(-1, *pktTachimetria, TRUE);
			break;
		}
		g_free(txtX);
		g_free(txtY);
		g_free(txtH);	
		g_free(pkt_nr);
		g_free(pkt_kod);
		delete pktTachimetria;
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
}


void on_tbtnTachimetriaWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	// Czyszczenie listy
	GtkWidget *treeTachimetria = lookup_widget(GTK_WIDGET(toolbutton), "treeTachimetria");
	GtkTreeModel *modelTachimetria = gtk_tree_view_get_model(GTK_TREE_VIEW(treeTachimetria));
	gtk_tree_store_clear(GTK_TREE_STORE(modelTachimetria));
	
	// Czyszczenie wszystkich pol
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazanieStNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazanieStX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazanieStY");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazanieStH");

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN1Nr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN1X");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN1Y");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN1Kier");

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN2Nr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN2X");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN2Y");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN2Kier");

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazanieHi");

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaPktNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaPktHz");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaPktOdl");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaPktVdH");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaPktKod");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryTachimetriaPktHc");

	GtkWidget *focus = lookup_widget(GTK_WIDGET(toolbutton), "entryTachimetriaPktNr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


void on_tbtnTachimetriaUsun_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeTachimetria");
	GtkWidget *winTachimetria = lookup_widget(GTK_WIDGET(toolbutton), "winTachimetria");
	GtkWidget *dialog;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreePath *tpath;
	GtkTreeIter iter;
	char *pkt_nr = NULL;
	int s_dodaj;
	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);

	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_TACH_NR, &pkt_nr,
				COL_TACH_DODAJ, &s_dodaj, -1);
		if (s_dodaj == PKT_DODANY) {
			dialog = gtk_message_dialog_new(GTK_WINDOW(winTachimetria), GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Usunąć także punkt z bazy danych?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES) dbgeod_point_del(0, pkt_nr);
			gtk_widget_destroy(dialog);
		}
		g_free(pkt_nr);
		gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarTachimetria", _("Zaznacz najpierw pozycję w tabeli"));

	gtk_tree_path_free(tpath);
}


void on_tbtnTachimetriaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *toggleDsV = lookup_widget(GTK_WIDGET(toolbutton), "toggleTachimetriaDaneDsV");
	GtkWidget *toggleDpH = lookup_widget(GTK_WIDGET(toolbutton), "toggleTachimetriaDaneDpH");
	const guint NCOLS = 8;
	GSList *lpunkty = NULL;
	cPunkt *punkt = NULL;
	double kierunek, hi;
	char *txt_kier = NULL, *txt_hi = NULL;
	guint i;

	GtkWidget *dialogRaport = create_dialogRaport();
	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");
	gtk_widget_show(dialogRaport);

	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeTachimetria");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	const guint NROWS = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	
	guint colsize[NCOLS] = { 10, 15, 11, 15, 10, 12, 12, 10 };
	char **tab_cell = new char*[(NROWS+1)*NCOLS];

	geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\ntachimetria"));

	// punkty nawiazania
	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xyh(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazanieStNr",
		"entryTachimetriaNawiazanieStX", "entryTachimetriaNawiazanieStY",
		"entryTachimetriaNawiazanieStH", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		geod_raport_error(textview, _("Dla stanowiska nie podano wszystkich wymaganych danych"));
		delete punkt;
	}
	
	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN1Nr",
		"entryTachimetriaNawiazaniePktN1X", "entryTachimetriaNawiazaniePktN1Y", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
	else
		delete punkt;

	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN2Nr",
		"entryTachimetriaNawiazaniePktN2X", "entryTachimetriaNawiazaniePktN2Y", NULL, *punkt))
			lpunkty = g_slist_append(lpunkty, punkt);
	else
		delete punkt;

	geod_raport_table_pkt(textview, lpunkty, TRUE, _("Punkty nawiązania"));
	g_slist_free(lpunkty);
	lpunkty = NULL;
	
	// kierunek nawiazania 1
	if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN1Kier", kierunek)) {
		txt_kier = geod_kat_from_rad(kierunek);
		geod_raport_line(textview, _("Kierunek nazwiązania 1"), txt_kier, FALSE);
		g_free(txt_kier);
	}
	else
		geod_raport_error(textview, _("Brak kierunku naw. 1!"));

	// kierunek nawiazania 2
	if (!geod_entry_get_kat(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN2Kier", kierunek)) {
		txt_kier = geod_kat_from_rad(kierunek);
		geod_raport_line(textview, _("Kierunek nazwiązania 2"), txt_kier, FALSE);
		g_free(txt_kier);
	}
	else
		geod_raport_error(textview, _("Brak kierunku naw. 2!"));

	// wysokość instrumentu
	if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazanieHi", hi)) {
		txt_hi = g_strdup_printf("%.3f", hi);
		geod_raport_line(textview, _("Wysokość instrumentu"), txt_hi, FALSE);
		g_free(txt_hi);
	}
	else
		geod_raport_error(textview, _("Nie podano wysokości instrumentu"));
	
	// mierzone punkty
	tab_cell[0] = g_strdup(_("Nr"));
	tab_cell[1] = g_strdup(_("Kir. Hz"));
	if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toggleDsV)))
		tab_cell[2] = g_strdup(_("d skośna"));
	else
		tab_cell[2] = g_strdup(_("d poz."));
	if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toggleDpH)))
		tab_cell[3] = g_strdup(_("\316\224h"));
	else
		tab_cell[3] = g_strdup(_("Kąt V"));
	tab_cell[4] = g_strdup(_("ic"));
	tab_cell[5] = g_strdup(_("X"));
	tab_cell[6] = g_strdup(_("Y"));
	tab_cell[7] = g_strdup(_("H"));

	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);	
	for (i=1; i<=NROWS; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_TACH_NR, &tab_cell[i*NCOLS+0],
			COL_TACH_HZ, &tab_cell[i*NCOLS+1],
			COL_TACH_ODL, &tab_cell[i*NCOLS+2],
			COL_TACH_VDH, &tab_cell[i*NCOLS+3],		
			COL_TACH_HC, &tab_cell[i*NCOLS+4],	
			COL_TACH_X, &tab_cell[i*NCOLS+5],
			COL_TACH_Y, &tab_cell[i*NCOLS+6],
			COL_TACH_H, &tab_cell[i*NCOLS+7], -1);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	
	geod_raport_table(textview, tab_cell, NCOLS, NROWS, colsize, _("\nPunkty pomierzone"));
	geod_raport_stopka(textview);

	for (i=0; i<(NROWS+1)*NCOLS; i++)
		g_free(tab_cell[i]);

}


void on_tbtnTachimetriaWidok_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	int nawiazanie = 2;
	cPunkt *pktDane = NULL;
	GdkPoint *pktWidok = NULL;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeTachimetria");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	guint l_wierszy = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	guint i;
	char *pkt_nr = NULL;
	double pkt_x, pkt_y;

	GtkWidget *winWidok = create_winWidok();
	gtk_widget_show(winWidok);
	GtkWidget *darea = lookup_widget(GTK_WIDGET(winWidok), "drawingAreaWidok");
	
	pktDane = new cPunkt[l_wierszy+3];
	pktWidok = new GdkPoint[l_wierszy+4];
	
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazanieStNr",
		"entryTachimetriaNawiazanieStX", "entryTachimetriaNawiazanieStY", NULL, pktDane[0]));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarTachimetria", _("Musisz podać Nr, X, Y stanowiska"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN1Nr",
		"entryTachimetriaNawiazaniePktN1X", "entryTachimetriaNawiazaniePktN1Y", NULL, pktDane[l_wierszy+1]));
	else
		--nawiazanie;

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryTachimetriaNawiazaniePktN2Nr",
		"entryTachimetriaNawiazaniePktN2X", "entryTachimetriaNawiazaniePktN2Y", NULL, pktDane[l_wierszy+nawiazanie]));
	else
		--nawiazanie;

	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (i = 0; i < l_wierszy; i++)
	{
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_TACH_NR, &pkt_nr,
		COL_TACH_X_DOUBLE, &pkt_x, COL_TACH_Y_DOUBLE, &pkt_y, -1);
		pktDane[i+1].setNr(pkt_nr);
		pktDane[i+1].setXY(pkt_x, pkt_y);
		g_free(pkt_nr);

		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, l_wierszy+1+nawiazanie);
	
	// rysowane polnocy
	pktWidok[l_wierszy+3].x = pktWidok[0].x;
	pktWidok[l_wierszy+3].y = 0;
	if (nawiazanie>0)
		geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[l_wierszy+1], LINE_BAZA);
	if (nawiazanie>1)
		geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[l_wierszy+2], LINE_BAZA);

	geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[l_wierszy+3], LINE_POLNOC);
	// rysowanie punktow
	for (i = 0; i < l_wierszy; i++)
	{
		geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[i+1], LINE_KIERUNEK);
		geod_draw_point(GTK_WIDGET(darea), pktWidok[i+1], pktDane[i+1].getNr(), PKT_WYNIK);
	}
	// rysowanie punktow nawiazania
	geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
	if (nawiazanie>0)
		geod_draw_point(GTK_WIDGET(darea), pktWidok[l_wierszy+1], pktDane[l_wierszy+1].getNr(), PKT_OSNOWA);
	if (nawiazanie>1)
		geod_draw_point(GTK_WIDGET(darea), pktWidok[l_wierszy+2], pktDane[l_wierszy+2].getNr(), PKT_OSNOWA);

	gtk_widget_queue_draw(darea);
	delete [] pktDane;
	delete [] pktWidok;

}


void on_tbtnTachimetriaZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winTachimetria");
	gtk_widget_destroy(window);
}


gboolean on_entryTachimetriaNawiazanieStNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarTachimetria");
	return FALSE;
}


gboolean on_entryTachimetriaNawiazanieStNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarTachimetria");
	return FALSE;
}


gboolean on_entryTachimetriaNawiazaniePktN1Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarTachimetria");
	return FALSE;
}


gboolean on_entryTachimetriaNawiazaniePktN1Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarTachimetria");
	return FALSE;
}


gboolean on_entryTachimetriaNawiazaniePktN2Nr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarTachimetria");
	return FALSE;
}


gboolean on_entryTachimetriaNawiazaniePktN2Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarTachimetria");
	return FALSE;
}


gboolean on_entryTachimetriaPktNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	GtkWidget *entryPktHz = lookup_widget(GTK_WIDGET(widget), "entryTachimetriaPktHz");
	GtkWidget *entryPktOdl = lookup_widget(GTK_WIDGET(widget), "entryTachimetriaPktOdl");
	GtkWidget *entryPktVdH = lookup_widget(GTK_WIDGET(widget), "entryTachimetriaPktVdH");
	GtkWidget *toggleDsV = lookup_widget(GTK_WIDGET(widget), "toggleTachimetriaDaneDsV");
	GtkWidget *toggleDpV = lookup_widget(GTK_WIDGET(widget), "toggleTachimetriaDaneDpV");
	GtkWidget *toggleDpH = lookup_widget(GTK_WIDGET(widget), "toggleTachimetriaDaneDpH");
	
	geod_update_entry_obs(GTK_WIDGET(entryPktHz), "$hz", "entryTachimetriaNawiazanieStNr",
		"entryTachimetriaPktNr", NULL);

	if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toggleDsV))) {
		geod_update_entry_obs(GTK_WIDGET(entryPktOdl), "$ds",
			"entryTachimetriaNawiazanieStNr", "entryTachimetriaPktNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryPktVdH), "$v",
			"entryTachimetriaNawiazanieStNr", "entryTachimetriaPktNr", NULL);
	}
	else if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toggleDpV))) {
		geod_update_entry_obs(GTK_WIDGET(entryPktOdl), "$dl",
			"entryTachimetriaNawiazanieStNr", "entryTachimetriaPktNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryPktVdH), "$v",
			"entryTachimetriaNawiazanieStNr", "entryTachimetriaPktNr", NULL);
	}
	else if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toggleDpH))) {
		geod_update_entry_obs(GTK_WIDGET(entryPktOdl), "$dl",
			"entryTachimetriaNawiazanieStNr", "entryTachimetriaPktNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryPktVdH), "$dz",
			"entryTachimetriaNawiazanieStNr", "entryTachimetriaPktNr", NULL);
	}
	return FALSE;
}


gboolean on_entryTachimetriaPktKod_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Kod_set(widget, "sbarTachimetria");
	return FALSE;
}


gboolean on_entryTachimetriaPktKod_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarTachimetria");
	return FALSE;
}


void on_btnTachimetriaPktDodaj_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *entryNr = lookup_widget(GTK_WIDGET(button), "entryTachimetriaPktNr");
	GtkWidget *entryKod = lookup_widget(GTK_WIDGET(button), "entryTachimetriaPktKod");
	GtkWidget *cbtnNoAdd = lookup_widget(GTK_WIDGET(button), "cbtnTachimetriaPktDodajLista");
	GtkWidget *toggleDpH = lookup_widget(GTK_WIDGET(button), "toggleTachimetriaDaneDpH");
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeTachimetria");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreeIter iter;

	double hz, vdh, hc, odl;
	char *txtHz=NULL, *txtVdH=NULL, *txtHc=NULL, *txtOdl=NULL, *txtNr=NULL, *txtKod=NULL;
	int s_dodaj;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	char dok_h[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	int dok_h_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	if (!dbgeod_config_get("dok_h", dok_h)) dok_h_int = atoi(dok_h);
	
	if (!geod_entry_get_double(GTK_WIDGET(button), "entryTachimetriaPktHc", hc));
	else hc=0.0;

	if (!geod_entry_get_double(GTK_WIDGET(button), "entryTachimetriaPktOdl", odl));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarTachimetria", _("Musisz podać odległość"));
		return;
	}

	if (!geod_entry_get_kat(GTK_WIDGET(button), "entryTachimetriaPktHz", hz));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarTachimetria", _("Musisz podać kierunek Hz"));
		return;
	}

	if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toggleDpH))) {
		if (!geod_entry_get_double(GTK_WIDGET(button), "entryTachimetriaPktVdH", vdh))
			txtVdH = g_strdup_printf("%.*f", dok_h_int, vdh);
		else vdh = 0.0;
	}
	else {
		if (!geod_entry_get_kat(GTK_WIDGET(button), "entryTachimetriaPktVdH", vdh))
			txtVdH = geod_kat_from_rad(vdh);
		else {
			geod_sbar_set(GTK_WIDGET(button), "sbarTachimetria", _("Musisz podać kąt V"));
			return;
		}
	}
	
	txtHc = g_strdup_printf("%.*f", dok_h_int, hc);
	txtOdl = g_strdup_printf("%.*f", dok_xy_int, odl);
	txtHz = geod_kat_from_rad(hz);
	txtNr = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryNr)));
	txtKod = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryKod)));

	if (g_str_equal(txtNr, "")) {
		geod_sbar_set(GTK_WIDGET(button), "sbarTachimetria", _("Musisz podać Nr punktu"));
		gtk_widget_grab_focus(GTK_WIDGET(entryNr));
		return;
	}
	else if (!dbgeod_point_find(txtNr) && !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnNoAdd))) {
		geod_sbar_set(GTK_WIDGET(button), "sbarTachimetria", _("Punkt o takim numerze znajduje się już w bazie"));
		gtk_widget_grab_focus(GTK_WIDGET(entryNr));
		return;
	}
	
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnNoAdd)))
		s_dodaj = PKT_NIE_DODAWAJ;
	else 
		s_dodaj = PKT_DODAJ;

	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
		COL_TACH_NR, txtNr,
		COL_TACH_HZ, txtHz, COL_TACH_HZ_DOUBLE, hz,
		COL_TACH_VDH, txtVdH, COL_TACH_VDH_DOUBLE, vdh,
		COL_TACH_ODL, txtOdl, COL_TACH_ODL_DOUBLE, odl,
		COL_TACH_HC, txtHc, COL_TACH_HC_DOUBLE, hc,
		COL_TACH_KOD, txtKod, COL_TACH_DODAJ, s_dodaj, -1);

	if (s_dodaj == PKT_DODAJ)
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_TACH_TOGGLE, TRUE, -1);
	else
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_TACH_TOGGLE, FALSE, -1);
	
	g_free(txtHc);
	g_free(txtVdH);
	g_free(txtOdl);
	g_free(txtHz);
	g_free(txtNr);
	g_free(txtKod);
	
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(button), "tbtnTachimetriaOblicz");
	g_signal_emit_by_name(btnOblicz, "clicked");
	
	// wyczyszczenie wszystkich pol
	geod_entry_inc(GTK_WIDGET(button), "entryTachimetriaPktNr");
	geod_entry_clear(GTK_WIDGET(button), "entryTachimetriaPktHz");
	geod_entry_clear(GTK_WIDGET(button), "entryTachimetriaPktOdl");
	geod_entry_clear(GTK_WIDGET(button), "entryTachimetriaPktKod");
	geod_entry_clear(GTK_WIDGET(button), "entryTachimetriaPktVdH");
	GtkWidget *focus = lookup_widget(GTK_WIDGET(button), "entryTachimetriaPktNr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}

void on_toggleTachimetriaDaneDsV_toggled (GtkToggleToolButton *toggletoolbutton,
	gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toggletoolbutton), "treeTachimetria");
	GtkWidget *labelPktVdH = lookup_widget(GTK_WIDGET(toggletoolbutton), "labelTachimetriaPktVdH");
	GtkTreeViewColumn *vdhColumn = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), 3);

	if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toggletoolbutton)))
	{
		gtk_tree_view_column_set_title (GTK_TREE_VIEW_COLUMN(vdhColumn), _("Kąt V"));
		gtk_label_set_text(GTK_LABEL(labelPktVdH), _("Kąt V:"));
	}
}


void on_toggleTachimetriaDaneDpV_toggled (GtkToggleToolButton *toggletoolbutton,
	gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toggletoolbutton), "treeTachimetria");
	GtkWidget *labelPktVdH = lookup_widget(GTK_WIDGET(toggletoolbutton), "labelTachimetriaPktVdH");
	GtkTreeViewColumn *vdhColumn = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), 3);

	if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toggletoolbutton)))
	{
		gtk_tree_view_column_set_title (GTK_TREE_VIEW_COLUMN(vdhColumn), _("Kąt V"));
		gtk_label_set_text(GTK_LABEL(labelPktVdH), _("Kąt V:"));
	}
}


void on_toggleTachimetriaDaneDpH_toggled (GtkToggleToolButton *toggletoolbutton,
	gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toggletoolbutton), "treeTachimetria");
	GtkWidget *labelPktVdH = lookup_widget(GTK_WIDGET(toggletoolbutton), "labelTachimetriaPktVdH");
	GtkTreeViewColumn *vdhColumn = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), 3);
	
	if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toggletoolbutton)))
	{
		gtk_tree_view_column_set_title (GTK_TREE_VIEW_COLUMN(vdhColumn), _("\316\224H"));
		gtk_label_set_text(GTK_LABEL(labelPktVdH), _("\316\224H:"));
	}
}

void on_dialogHistoriaEdytuj_response (GtkDialog *dialog, gint response_id,
	gpointer user_data)
{
	if (response_id == GTK_RESPONSE_APPLY) {
		GtkWidget *treeview = lookup_widget(GTK_WIDGET(dialog), "treeDialogHistoriaEdytuj");
		GtkWidget *label = lookup_widget(GTK_WIDGET(dialog), "labelDialogHistoriaEdytujId");
		GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
		guint l_wierszy = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
		guint i;
		int par_id;
		double wartosc;
		char *nazwa = NULL;
		bool edited;

		GtkTreeIter iter;
	
		gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
		for (i = 0; i < l_wierszy; i++) {
			gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_HE_ID, &par_id,
				COL_HE_NAZWA, &nazwa, COL_HE_VALUE_DOUBLE, &wartosc,
				COL_HE_EDITED, &edited, -1);
			if (edited)
				dbgeod_hist_par_set(atoi(gtk_label_get_text(GTK_LABEL(label))), nazwa, wartosc, 0);
			g_free(nazwa);
			gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
		}
	}
	gtk_widget_destroy(GTK_WIDGET(dialog));
}


void on_dialogHistoriaEdytuj_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeDialogHistoriaEdytuj");
	GtkTreeStore *punkty_model;
	
	punkty_model=gtk_tree_store_new(N_HE_COLUMNS, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, \
		G_TYPE_DOUBLE, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN); // id, nazwa, wartosc, wartosc double, kat, edytowany
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), GTK_TREE_MODEL(punkty_model));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;
	
	// kolumna id
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Id"), renderer, "text", COL_HE_ID, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);
	
	// kolumna nazwa
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("Parametr"), renderer, "text", COL_HE_NAZWA, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);
	
	// kolumna wartosc
	renderer = gtk_cell_renderer_text_new();
	g_object_set(renderer, "editable", TRUE, NULL);
	g_signal_connect(renderer, "edited", (GCallback) dialogHistoriaEdytuj_cell_edited_callback, (gpointer)treeview);
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview),
	   -1, _("Wartość"), renderer, "text", COL_HE_VALUE, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
}

void dialogHistoriaEdytuj_cell_edited_callback (GtkCellRendererText *cell, gchar *path_string,
	gchar *new_text, gpointer treeview)
{
	gboolean is_angle;
	double new_value;
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	GtkTreePath *path = gtk_tree_path_new_from_string(path_string);

	if (gtk_tree_model_get_iter(model, &iter, path)) {
		gtk_tree_model_get (model, &iter, COL_HE_ANGLE, &is_angle, -1);
		if (is_angle) {
			geod_get_kat(new_text, new_value);
			gtk_tree_store_set (GTK_TREE_STORE(model), &iter, COL_HE_VALUE_DOUBLE, new_value,
				COL_HE_VALUE, geod_kat_from_rad(new_value), -1);
		}
		else {
			new_value = g_strtod(new_text, NULL);
			gtk_tree_store_set (GTK_TREE_STORE(model), &iter, COL_HE_VALUE_DOUBLE, new_value,
				COL_HE_VALUE, g_strdup_printf("%.11f", new_value), -1);
		}
		gtk_tree_store_set (GTK_TREE_STORE(model), &iter, COL_HE_EDITED, TRUE, -1);
	}
	gtk_tree_path_free(path);
}

void on_tbtnHistoriaRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GSList *lpunkty = NULL;
	cPunkt *punkt;
	double obs_kat, parametr;
	
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	char dok_h[CONFIG_VALUE_MAX_LEN];
	int dok_h_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	if (!dbgeod_config_get("dok_h", dok_h)) dok_h_int = atoi(dok_h);
	
	GtkWidget *dialogRaport = create_dialogRaport();
	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");
	gtk_widget_show(dialogRaport);

	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeHistoria");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreeIter iter;
	guint i=0, k, n_pkt = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	guint NCOLS, NROWS;
	guint *colsize = NULL;
	char **tab_cell = NULL;

	int zadanie_id;
	char *nfunc;
	char nr_pkt[NR_MAX_LEN];

	geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ"));
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (k=0; k<n_pkt; k++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_HIS_ID, &zadanie_id, COL_HIS_FUNCTION, &nfunc, -1);
		printf("%d\n", k);
		if (g_str_equal(nfunc, "ZadanieMareka")) {
			geod_raport_error(textview, _("\nZADANIE MAREKA"), TRUE);
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "L1Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla L1 nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "P1Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla P1 nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "L2Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla L2 nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "P2Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla P2 nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty nawiązania"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// mierzaone katy
			if (!dbgeod_hist_par_get(zadanie_id, "KatAlfa", obs_kat))
				geod_raport_line(textview, _("\nKąt alfa"), geod_kat_from_rad(obs_kat));
			else
				geod_raport_error(textview, _("Nie podano kąta alfa!"));

			if (!dbgeod_hist_par_get(zadanie_id, "KatBeta", obs_kat))
				geod_raport_line(textview, _("Kąt beta"), geod_kat_from_rad(obs_kat));
			else
				geod_raport_error(textview, _("Nie podano kąta beta!"));

			if (!dbgeod_hist_par_get(zadanie_id, "KatGamma", obs_kat))
				geod_raport_line(textview, _("Kąt gamma"), geod_kat_from_rad(obs_kat));
			else
				geod_raport_error(textview, _("Nie podano kąta gamma!"));

			if (!dbgeod_hist_par_get(zadanie_id, "KatDelta", obs_kat))
				geod_raport_line(textview, _("Kąt delta"), geod_kat_from_rad(obs_kat));
			else
				geod_raport_error(textview, _("Nie podano kąta delta!"));

			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "St1Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "St2Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty wyznaczane"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else if (g_str_equal(nfunc, "WciecieKatoweOgolne")) {
			geod_raport_error(textview, _("\nWCIĘCIE KĄTOWE - przypadek ogólny"), TRUE);
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "LNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla L nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "L1Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla L1 nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla P nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "P1Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla P1 nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty osnowy"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// mierzaone katy
			if (!dbgeod_hist_par_get(zadanie_id, "KatL", obs_kat))
				geod_raport_line(textview, _("\nKąt L"), geod_kat_from_rad(obs_kat));
			else
				geod_raport_error(textview, _("Nie podano kąta alfa!"));

			if (!dbgeod_hist_par_get(zadanie_id, "KatP", obs_kat))
				geod_raport_line(textview, _("Kąt P"), geod_kat_from_rad(obs_kat));
			else
				geod_raport_error(textview, _("Nie podano kąta beta!"));

			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkt wyznaczany"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else if (g_str_equal(nfunc, "WciecieKatowe")) {
			geod_raport_error(textview, _("\nWCIĘCIE KĄTOWE"), TRUE);
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "LNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla L nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla P nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty osnowy"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// mierzaone katy
			if (!dbgeod_hist_par_get(zadanie_id, "KatL", obs_kat))
				geod_raport_line(textview, _("\nKąt L"), geod_kat_from_rad(obs_kat));
			else
				geod_raport_error(textview, _("Nie podano kąta alfa!"));

			if (!dbgeod_hist_par_get(zadanie_id, "KatP", obs_kat))
				geod_raport_line(textview, _("Kąt P"), geod_kat_from_rad(obs_kat));
			else
				geod_raport_error(textview, _("Nie podano kąta beta!"));

			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkt wyznaczany"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else if (g_str_equal(nfunc, "WciecieLiniowe")) {
			geod_raport_error(textview, _("\nWCIĘCIE LINIOWE"), TRUE);
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "LNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla L nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla P nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty osnowy"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// mierzaone dlugosci
			if (!dbgeod_hist_par_get(zadanie_id, "DlugoscLW", obs_kat))
				geod_raport_line(textview, _("\nDługość LW"), g_strdup_printf("%.*f", dok_xy_int, obs_kat));
			else
				geod_raport_error(textview, _("Nie podano długości LW!"));

			if (!dbgeod_hist_par_get(zadanie_id, "DlugoscPW", obs_kat))
				geod_raport_line(textview, _("Długość PW"), g_strdup_printf("%.*f", dok_xy_int, obs_kat));
			else
				geod_raport_error(textview, _("Nie podano długości PW!"));

			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkt wyznaczany"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else if (g_str_equal(nfunc, "WciecieWstecz")) {
			geod_raport_error(textview, _("\nWCIĘCIE WSTECZ"), TRUE);
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "LNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla L nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			if (!dbgeod_hist_point_get(zadanie_id, "CNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla C nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla P nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty osnowy"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			if (!dbgeod_hist_par_get(zadanie_id, "kier", parametr) && (int)(parametr+0.5)) {
				// mierzaone kierunki
				if (!dbgeod_hist_par_get(zadanie_id, "KierL", obs_kat))
					geod_raport_line(textview, _("\nKierunek L"), geod_kat_from_rad(obs_kat));
				else
					geod_raport_error(textview, _("Nie podano kierunku L!"));

				if (!dbgeod_hist_par_get(zadanie_id, "KierC", obs_kat))
					geod_raport_line(textview, _("Kierunek C"), geod_kat_from_rad(obs_kat));
				else
					geod_raport_error(textview, _("Nie podano kierunku C!"));

				if (!dbgeod_hist_par_get(zadanie_id, "KierP", obs_kat))
					geod_raport_line(textview, _("Kierunek P"), geod_kat_from_rad(obs_kat));
				else
					geod_raport_error(textview, _("Nie podano kierunku P!"));
			}
			else {
				// mierzaone katy
				if (!dbgeod_hist_par_get(zadanie_id, "KatAlfa", obs_kat))
					geod_raport_line(textview, _("\nKąt alfa"), geod_kat_from_rad(obs_kat));
				else
					geod_raport_error(textview, _("Nie podano kąta alfa!"));

				if (!dbgeod_hist_par_get(zadanie_id, "KatBeta", obs_kat))
					geod_raport_line(textview, _("Kąt beta"), geod_kat_from_rad(obs_kat));
				else
					geod_raport_error(textview, _("Nie podano kąta beta!"));
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkt wyznaczany"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else if (g_str_equal(nfunc, "WciecieAzymutalneWPrzod")) {
			geod_raport_error(textview, _("\nWCIĘCIE AZYMUTALNE W PRZÓD"), TRUE);
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "LNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla L nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla P nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty osnowy"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// mierzaone azymuty
			if (!dbgeod_hist_par_get(zadanie_id, "AzymutLW", obs_kat))
				geod_raport_line(textview, _("\nAzymut LW"), geod_kat_from_rad(obs_kat));
			else
				geod_raport_error(textview, _("Nie podano azymutu LW!"));

			if (!dbgeod_hist_par_get(zadanie_id, "AzymutPW", obs_kat))
				geod_raport_line(textview, _("Azymut PW"), geod_kat_from_rad(obs_kat));
			else
				geod_raport_error(textview, _("Nie podano azymutu PW!"));

			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			geod_raport_table_pkt(textview, lpunkty, FALSE, "Punkt wyznaczany");
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else if (g_str_equal(nfunc, "WciecieAzymutalneWstecz")) {
			geod_raport_error(textview, _("\nWCIĘCIE AZYMUTALNE WSTECZ"), TRUE);
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "LNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla L nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla P nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, "Punkty osnowy");
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// mierzaone azymuty
			if (!dbgeod_hist_par_get(zadanie_id, "AzymutWL", obs_kat))
				geod_raport_line(textview, _("\nAzymut WL"), geod_kat_from_rad(obs_kat));
			else
				geod_raport_error(textview, _("Nie podano azymutu WL!"));

			if (!dbgeod_hist_par_get(zadanie_id, "AzymutWP", obs_kat))
				geod_raport_line(textview, _("Azymut WP"), geod_kat_from_rad(obs_kat));
			else
				geod_raport_error(textview, _("Nie podano azymutu WP!"));

			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkt wyznaczany"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else if (g_str_equal(nfunc, "OkragStycznaProstaRownolegla") || g_str_equal(nfunc, "OkragStycznaProstaProstopadla")) {
			if (g_str_equal(nfunc, "OkragStycznaProstaProstopadla"))
				geod_raport_error(textview, _("\nSTYCZNA DO OKRĘGU prostopadła do prostej"), TRUE);
			else
				geod_raport_error(textview, _("\nSTYCZNA DO OKRĘGU równoległa do prostej"), TRUE);
			// okrag
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "SNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla środką okręgu nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Okrąg"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			if (!dbgeod_hist_par_get(zadanie_id, "Promien", obs_kat))
				geod_raport_line(textview, _("Promień"), g_strdup_printf("%.*f", dok_xy_int, obs_kat));
			else
				geod_raport_error(textview, _("Nie podano promienia okręgu!"));

			// prosta
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PrNr1", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla pierwszego punktu prostej nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PrNr2", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla drugiego punktu prostej nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Prosta"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// wynik
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "W1Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "W2Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty styczności"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else if (g_str_equal(nfunc, "OkragStyczna2Okregi")) {
			geod_raport_error(textview, _("\nSTYCZNA DO DWÓCH OKRĘGÓW"), TRUE);
			// okrag 1
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "S1Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla środką pierwszego okręgu nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Okrąg 1"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			if (!dbgeod_hist_par_get(zadanie_id, "PromienOkr1", obs_kat))
				geod_raport_line(textview, _("Promień 1"), g_strdup_printf("%.*f", dok_xy_int, obs_kat));
			else
				geod_raport_error(textview, _("Nie podano promienia pierwszego okręgu!"));

			// okrag 2
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "S2Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla środką drugiego okręgu nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Okrąg 2"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			if (!dbgeod_hist_par_get(zadanie_id, "PromienOkr2", obs_kat))
				geod_raport_line(textview, _("Promień 2"), g_strdup_printf("%.*f", dok_xy_int, obs_kat));
			else
				geod_raport_error(textview, _("Nie podano promienia drugiego okręgu!"));

			// wynik
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "W1aNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "W1bNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			if (!dbgeod_hist_point_get(zadanie_id, "W2aNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "W2bNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty styczności"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else if (g_str_equal(nfunc, "OkragStycznaPunkt")) {
			geod_raport_error(textview, _("\nSTYCZNA DO OKRĘGU przechodząca przez punkt"), TRUE);
			// okrag
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "SNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla środką okręgu nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, "Okrąg");
			g_slist_free(lpunkty);
			lpunkty = NULL;

			if (!dbgeod_hist_par_get(zadanie_id, "Promien", obs_kat))
				geod_raport_line(textview, _("Promień"), g_strdup_printf("%.*f", dok_xy_int, obs_kat));
			else
				geod_raport_error(textview, _("Nie podano promienia okręgu!"));

			// punkt 
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PktNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla punktu stycznej nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkt na stycznej"));
			g_slist_free(lpunkty);
			lpunkty = NULL;


			// wynik
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "W1Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "W2Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty styczności"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else if (g_str_equal(nfunc, _("OkragWpasowanie2Proste"))) {
			geod_raport_error(textview, _("\nWYZNACZENIE ŚRODKA OKRĘGU stycznego do prostych"), TRUE);

			if (!dbgeod_hist_par_get(zadanie_id, "Promien", obs_kat))
				geod_raport_line(textview, _("Promień"), g_strdup_printf("%.*f", dok_xy_int, obs_kat));
			else
				geod_raport_error(textview, _("Nie podano promienia okręgu!"));

			// prosta L
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PLNr1", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla pierwszego punktu prostej L nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PLNr2", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla drugiego punktu prostej L nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Prosta L"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// prosta P
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PPNr1", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla pierwszego punktu prostej P nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PPNr2", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla drugiego punktu prostej P nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Prosta P"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// wynik
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Wyznaczony środek okręgu"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else if (g_str_equal(nfunc, "OkragWpasowanie3Punkty")) {
			geod_raport_error(textview, _("\nWYZNACZENIE ŚRODKA OKRĘGU\nna podstawie 3 punktów do niego należących"), TRUE);

			// punkty
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "Pkt1Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla pierwszego punktu nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "Pkt2Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla drugiego punktu nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "Pkt3Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla trzeciego punktu pnie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty należące do okręgu"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// wynik
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Wyznaczony środek okręgu"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			if (!dbgeod_hist_par_get(zadanie_id, "Promien", obs_kat))
				geod_raport_line(textview, _("Promień"), g_strdup_printf("%.*f", dok_xy_int, obs_kat));
			else
				geod_raport_error(textview, _("Nie obliczono promienia okręgu!"));
		}
		else if (g_str_equal(nfunc, "PrzeciecieProste")) {
			geod_raport_error(textview, _("\nPRZECIĘCIE PROSTYCH"), TRUE);

			// prosta 1
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "Pr1Nr1", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla pierwszego punktu prostej 1 nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "Pr1Nr2", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla drugiego punktu prostej 1 nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Prosta 1"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// prosta P
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "Pr2Nr1", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla pierwszego punktu prostej 2 nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "Pr2Nr2", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla drugiego punktu prostej 2 nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Prosta 2"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// wynik
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Wyznaczony punkt przecięcia"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		} 
		else if (g_str_equal(nfunc, "PrzeciecieOkragProsta")) {
			geod_raport_error(textview, _("\nPRZECIĘCIE PROSTEJ Z OKRĘGIEM"), TRUE);

			// prosta
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PrNr1", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla pierwszego punktu prostej nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PrNr2", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla drugiego punktu prostej nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Prosta"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// okrag
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "OkrNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla środka okręgu nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Okrąg"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			if (!dbgeod_hist_par_get(zadanie_id, "Promien", obs_kat))
				geod_raport_line(textview, _("Promień"), g_strdup_printf("%.*f", dok_xy_int, obs_kat));
			else
				geod_raport_error(textview, _("Nie podano promienia okręgu!"));

			// wynik
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "W1Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			if (!dbgeod_hist_point_get(zadanie_id, "W2Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Wyznaczone punkty przecięcia"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else if (g_str_equal(nfunc, "PrzeciecieOkregi")) {
			geod_raport_error(textview, _("\nPRZECIĘCIE OKRĘGÓW"), TRUE);

			// okrag 1
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "Okr1Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla środka okręgu 1 nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Okrąg 1"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			if (!dbgeod_hist_par_get(zadanie_id, "PromienOkr1", obs_kat))
				geod_raport_line(textview, _("Promień 1"), g_strdup_printf("%.*f", dok_xy_int, obs_kat));
			else
				geod_raport_error(textview, _("Nie podano promienia pierwszego okręgu!"));

			// okrag 2
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "Okr2Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla środka okręgu 2 nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Okrąg 2"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			if (!dbgeod_hist_par_get(zadanie_id, "PromienOkr2", obs_kat))
				geod_raport_line(textview, _("Promień 2"), g_strdup_printf("%.*f", dok_xy_int, obs_kat));
			else
				geod_raport_error(textview, _("Nie podano promienia drugiego okręgu!"));

			// wynik
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "W1Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}
			if (!dbgeod_hist_point_get(zadanie_id, "W2Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Brak obliczeń!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Wyznaczone punkty przecięcia"));
			g_slist_free(lpunkty);
			lpunkty = NULL;
		}
		else if (g_str_equal(nfunc, "RzutProsta")) {
			geod_raport_error(textview, _("\nRZUT PUNKTU NA PROSTĄ"), TRUE);

			// baza
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PrPPNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla początku bazy nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PrPKNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla końca bazy nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Prosta"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			NCOLS = 6;
			NROWS = 1;
			colsize = new guint[NCOLS];
			colsize[0] = 10;
			colsize[1] = 15;
			colsize[2] = 15;
			colsize[3] = 10;
			colsize[4] = 15;
			colsize[5] = 15;
			tab_cell = new char*[(NROWS+1)*NCOLS];


			// domiary	
			tab_cell[0] = g_strdup(_("Nr"));
			tab_cell[1] = g_strdup(_("X"));
			tab_cell[2] = g_strdup(_("Y"));
			tab_cell[3] = g_strdup(_("Nr rzut"));
			tab_cell[4] = g_strdup(_("X rzut"));
			tab_cell[5] = g_strdup(_("Y rzut"));

			// punkt rzutowany
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PktRzutowanyNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt)) {
				tab_cell[NCOLS+0] = g_strdup(punkt->getNr());
				tab_cell[NCOLS+1] = g_strdup_printf("%.*f", dok_xy_int, punkt->getX());
				tab_cell[NCOLS+2] = g_strdup_printf("%.*f", dok_xy_int, punkt->getY());	
			}	
			else {
				tab_cell[NCOLS+0] = g_strdup(punkt->getNr());
				tab_cell[NCOLS+1] = g_strdup_printf("%.*f", dok_xy_int, punkt->getX());
				tab_cell[NCOLS+2] = g_strdup_printf("%.*f", dok_xy_int, punkt->getY());	
				geod_raport_error(textview, _("Brak wszystkich danych dotyczących punktu rzutowanego!"));
			}
			delete punkt;

			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "PktRzutNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt)) {
				tab_cell[NCOLS+3] = g_strdup(punkt->getNr());
				tab_cell[NCOLS+4] = g_strdup_printf("%.*f", dok_xy_int, punkt->getX());
				tab_cell[NCOLS+5] = g_strdup_printf("%.*f", dok_xy_int, punkt->getY());	
			}	
			else {
				tab_cell[NCOLS+3] = g_strdup(punkt->getNr());
				tab_cell[NCOLS+4] = g_strdup_printf("%.*f", dok_xy_int, punkt->getX());
				tab_cell[NCOLS+5] = g_strdup_printf("%.*f", dok_xy_int, punkt->getY());	
				geod_raport_error(textview, _("Brak obliczeń!"));
			}
			delete punkt;
			
			geod_raport_table(textview, tab_cell, NCOLS, NROWS, colsize, _("\nRzutowane punkty"));
			for (i=0; i<(NROWS+1)*NCOLS; i++)
				g_free(tab_cell[i]);
			g_free(colsize);
		}
		else if (g_str_equal(nfunc, "Domiary")) {
			geod_raport_error(textview, _("\nDOMIARY PROSTOKĄTNE"), TRUE);

			// baza
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "BazaPPNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla początku bazy nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "BazaPKNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla końca bazy nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Baza pomiarowa"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			if (!dbgeod_hist_par_get(zadanie_id, "bazaPomierzona", obs_kat))
				geod_raport_line(textview, _("Pomierzona długość bazy"), g_strdup_printf("%.*f", dok_xy_int, obs_kat));

			NCOLS = 6;
			NROWS = 1;
			colsize = new guint[NCOLS];
			colsize[0] = 10;
			colsize[1] = 13;
			colsize[2] = 13;
			colsize[3] = 15;
			colsize[4] = 15;
			colsize[5] = 4;
			tab_cell = new char*[(NROWS+1)*NCOLS];

			// wynik
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt));
			else
				geod_raport_error(textview, _("\nBrak obliczeń!"));

			// domiary	
			tab_cell[0] = g_strdup(_("Nr"));
			tab_cell[1] = g_strdup(_("Bieżąca"));
			tab_cell[2] = g_strdup(_("Domiar"));
			tab_cell[3] = g_strdup(_("X"));
			tab_cell[4] = g_strdup(_("Y"));
			tab_cell[5] = g_strdup(_("Kod"));
	
			tab_cell[NCOLS+0] = g_strdup(punkt->getNr());
			if (!dbgeod_hist_par_get(zadanie_id, "biezaca", obs_kat))
				tab_cell[NCOLS+1] = g_strdup_printf("%.*f", dok_xy_int, obs_kat);
			else
				tab_cell[NCOLS+1] = g_strdup("b.d.");
			if (!dbgeod_hist_par_get(zadanie_id, "domiar", obs_kat))
				tab_cell[NCOLS+2] = g_strdup_printf("%.*f", dok_xy_int, obs_kat);
			else
				tab_cell[NCOLS+2] = g_strdup("b.d.");
			tab_cell[NCOLS+3] = g_strdup_printf("%.*f", dok_xy_int, punkt->getX());
			tab_cell[NCOLS+4] = g_strdup_printf("%.*f", dok_xy_int, punkt->getY());
			tab_cell[NCOLS+5] = g_strdup(punkt->getKod());

			geod_raport_table(textview, tab_cell, NCOLS, NROWS, colsize, _("\nMierzone punkty"));

			for (i=0; i<(NROWS+1)*NCOLS; i++)
				g_free(tab_cell[i]);
			g_free(colsize);
			delete punkt;
		}
		else if (g_str_equal(nfunc, "OkragTyczenie")) {
			geod_raport_error(textview, _("\nTYCZENIE PUNKTÓW OKRĘGU"), TRUE);

			// okrag
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "OkragONr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla środka okręgu nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "OkragPNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla punktu początkowego nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Środek i początek tyczenia"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			NCOLS = 6;
			NROWS = 1;
			colsize = new guint[NCOLS];
			colsize[0] = 10;
			colsize[1] = 13;
			colsize[2] = 13;
			colsize[3] = 15;
			colsize[4] = 15;
			colsize[5] = 4;
			tab_cell = new char*[(NROWS+1)*NCOLS];

			// wynik
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt));
			else
				geod_raport_error(textview, _("\nBrak obliczeń!"));

			// domiary	
			tab_cell[0] = g_strdup(_("Nr"));
			tab_cell[1] = g_strdup(_("Bieżąca"));
			tab_cell[2] = g_strdup(_("Domiar"));
			tab_cell[3] = g_strdup(_("X"));
			tab_cell[4] = g_strdup(_("Y"));
			tab_cell[5] = g_strdup(_("Kod"));
	
			tab_cell[NCOLS+0] = g_strdup(punkt->getNr());
			if (!dbgeod_hist_par_get(zadanie_id, "biezaca", obs_kat))
				tab_cell[NCOLS+1] = g_strdup_printf("%.*f", dok_xy_int, obs_kat);
			else
				tab_cell[NCOLS+1] = g_strdup("b.d.");
			if (!dbgeod_hist_par_get(zadanie_id, "domiar", obs_kat))
				tab_cell[NCOLS+2] = g_strdup_printf("%.*f", dok_xy_int, obs_kat);
			else
				tab_cell[NCOLS+2] = g_strdup("b.d.");
			tab_cell[NCOLS+3] = g_strdup_printf("%.*f", dok_xy_int, punkt->getX());
			tab_cell[NCOLS+4] = g_strdup_printf("%.*f", dok_xy_int, punkt->getY());
			tab_cell[NCOLS+5] = g_strdup(punkt->getKod());

			geod_raport_table(textview, tab_cell, NCOLS, NROWS, colsize, _("\nTyczone punkty"));

			for (i=0; i<(NROWS+1)*NCOLS; i++)
				g_free(tab_cell[i]);
			g_free(colsize);
			delete punkt;
		}
		else if (g_str_equal(nfunc, "Tachimetria_DsV") || g_str_equal(nfunc, "Tachimetria_DpV") || g_str_equal(nfunc, "Tachimetria_DpH" )) {
			if (g_str_equal(nfunc, "Tachimetria_DsV"))
				geod_raport_error(textview, _("\nTACHIMETRIA\nodległość skośna i kąt V"), TRUE);
			else if (g_str_equal(nfunc, "Tachimetria_DpV"))
				geod_raport_error(textview, _("\nTACHIMETRIA\nodległość pozioma i kąt V"), TRUE);
			else if (g_str_equal(nfunc, "Tachimetria_DpH"))
				geod_raport_error(textview, _("\nTACHIMETRIA\nodległość pozioma i przewyższenie"), TRUE);

			// stanowisko
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "StNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla stanowiska nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Stanowisko"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// punkty nawiazania
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "N1Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla pierwszego nawiązania nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "N2Nr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt))
				lpunkty = g_slist_append(lpunkty, punkt);
			else {
				geod_raport_error(textview, _("Dla drugiego nawiązania nie podano wszystkich wymaganych danych!"));
				delete punkt;
			}

			geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty nawiązania"));
			g_slist_free(lpunkty);
			lpunkty = NULL;

			// kierunki nawiazania
			if (!dbgeod_hist_par_get(zadanie_id, "Kier1", obs_kat))
				geod_raport_line(textview, _("\nKierunek nawiązania N1"), geod_kat_from_rad(obs_kat));
			else
				geod_raport_error(textview, _("Nie podano kierunku nawiązania do N1!"));

			if (!dbgeod_hist_par_get(zadanie_id, "Kier2", obs_kat))
				geod_raport_line(textview, _("Kierunek nawiązania N2"), geod_kat_from_rad(obs_kat));
			else
				geod_raport_error(textview, _("Nie podano kierunku nawiązania do N2!"));

			if (!dbgeod_hist_par_get(zadanie_id, "i_st", obs_kat))
				geod_raport_line(textview, _("Wysokość instrumentu"), g_strdup_printf("%.*f", dok_h_int, obs_kat));
			else
				geod_raport_line(textview, _("Wysokość instrumentu"), g_strdup_printf("%.*f", dok_h_int, 0.0));

			NCOLS = 8;
			NROWS = 1;
			colsize = new guint[NCOLS];
			colsize[0] = 10;
			colsize[1] = 15;
			colsize[2] = 11;
			colsize[3] = 15;
			colsize[4] = 10;
			colsize[5] = 12;
			colsize[6] = 12;
			colsize[7] = 10;
			tab_cell = new char*[(NROWS+1)*NCOLS];

			// tabela - naglowki
			tab_cell[0] = g_strdup(_("Nr"));
			tab_cell[1] = g_strdup(_("Kir. Hz"));
			if (g_str_equal(nfunc, "Tachimetria_DsV"))
				tab_cell[2] = g_strdup(_("d skośna"));
			else
				tab_cell[2] = g_strdup(_("d poz."));
			if (g_str_equal(nfunc, "Tachimetria_DpH"))
				tab_cell[3] = g_strdup(_("\316\224h"));
			else
				tab_cell[3] = g_strdup(_("Kąt V"));
			tab_cell[4] = g_strdup(_("ic"));
			tab_cell[5] = g_strdup(_("X"));
			tab_cell[6] = g_strdup(_("Y"));
			tab_cell[7] = g_strdup(_("H"));

			// wynik
			punkt = new cPunkt;
			if (!dbgeod_hist_point_get(zadanie_id, "WNr", nr_pkt) && !dbgeod_pointt_find(nr_pkt, punkt));
			else
				geod_raport_error(textview, _("\nBrak obliczeń!"));

			// tabela - dane
			tab_cell[NCOLS+0] = g_strdup(punkt->getNr());
			if (!dbgeod_hist_par_get(zadanie_id, "KierHz", obs_kat))
				tab_cell[NCOLS+1] = geod_kat_from_rad(obs_kat);
			else
				tab_cell[NCOLS+1] = g_strdup(_("b.d."));
			if (!dbgeod_hist_par_get(zadanie_id, "odleglosc", obs_kat))
				tab_cell[NCOLS+2] = g_strdup_printf("%.*f", dok_xy_int, obs_kat);
			else
				tab_cell[NCOLS+2] = g_strdup(_("b.d."));
			if (g_str_equal(nfunc, "Tachimetria_DpH")) {
				if (!dbgeod_hist_par_get(zadanie_id, "dH", obs_kat))
					tab_cell[NCOLS+3] = g_strdup_printf("%.*f", dok_h_int, obs_kat);
				else
					tab_cell[NCOLS+3] = g_strdup_printf("%.*f", dok_h_int, 0.0);
			}
			else {
				if (!dbgeod_hist_par_get(zadanie_id, "KatV", obs_kat))
					tab_cell[NCOLS+3] = geod_kat_from_rad(obs_kat);
				else
					tab_cell[NCOLS+3] = g_strdup(_("b.d."));			
			}
			if (!dbgeod_hist_par_get(zadanie_id, "i_cel", obs_kat))
				tab_cell[NCOLS+4] = g_strdup_printf("%.*f", dok_h_int, obs_kat);
			else
				tab_cell[NCOLS+4] = g_strdup_printf("%.*f", dok_h_int, 0.0);
			tab_cell[NCOLS+5] = g_strdup_printf("%.*f", dok_xy_int, punkt->getX());
			tab_cell[NCOLS+6] = g_strdup_printf("%.*f", dok_xy_int, punkt->getY());
			tab_cell[NCOLS+7] = g_strdup_printf("%.*f", dok_h_int, punkt->getH());
			geod_raport_table(textview, tab_cell, NCOLS, NROWS, colsize, _("\nMierzone punkty"));

			for (i=0; i<(NROWS+1)*NCOLS; i++)
				g_free(tab_cell[i]);
			g_free(colsize);
			delete punkt;
		}
		g_free(nfunc);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	geod_raport_stopka(textview);
}

// winPoligon
void on_mnuMainPoligon_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winPoligon, "winPoligon") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}


void on_winPoligon_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winPoligon");
}


void on_winPoligon_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treePoligon");
	GtkTreeStore *poligon_model;
	
	poligon_model=gtk_tree_store_new(N_POL_COLUMNS, \
	G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_DOUBLE, // nr,kod,kat
	G_TYPE_STRING, G_TYPE_DOUBLE, // dlugosc
	G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_INT); // x, y, dodaj
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), \
	    GTK_TREE_MODEL(poligon_model));

	GtkTreeViewColumn *column;
	gint col_offset;
	GtkCellRenderer *renderer;
	
	// kolumna Nr
	renderer = gtk_cell_renderer_text_new();
	g_object_set(renderer, "editable", FALSE, NULL);

	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Nr"), renderer, "text", COL_POL_NR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna kat
	renderer = gtk_cell_renderer_text_new();
	g_object_set(renderer, "editable", TRUE, NULL);
	g_signal_connect(renderer, "edited", (GCallback) cellPoligon_edited_callback, (gpointer)treeview);
	g_object_set_data(G_OBJECT(renderer), "my_column_num", GUINT_TO_POINTER(COL_POL_KAT));

	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Kąt"), renderer, "text", COL_POL_KAT, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);

	// kolumna dlugosc boku
	renderer = gtk_cell_renderer_text_new();
	g_object_set(renderer, "editable", TRUE, NULL);
	g_signal_connect(renderer, "edited", (GCallback) cellPoligon_edited_callback, (gpointer)treeview);
	g_object_set_data(G_OBJECT(renderer), "my_column_num", GUINT_TO_POINTER(COL_POL_DL));

	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Długość"), renderer, "text", COL_POL_DL, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);

	// kolumna wsp. X
	renderer = gtk_cell_renderer_text_new();
	g_object_set(renderer, "editable", FALSE, NULL);

	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("X"), renderer, "text", COL_POL_X, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);

	// kolumna wsp. Y
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Y"), renderer, "text", COL_POL_Y, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 100);

	// kolumna Kod
	renderer = gtk_cell_renderer_text_new();
	g_object_set(renderer, "editable", TRUE, NULL);
	g_signal_connect(renderer, "edited", (GCallback) cellPoligon_edited_callback, (gpointer)treeview);
	g_object_set_data(G_OBJECT(renderer), "my_column_num", GUINT_TO_POINTER(COL_POL_KOD));
	
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Kod"), renderer, "text", COL_POL_KOD, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);
}


void on_tbtnPoligonOblicz_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treePoligon");
	GtkWidget *tbtnKLewe = lookup_widget(GTK_WIDGET(toolbutton), "togglePoligonKLewe");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	guint i, n = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;

	char *pkt_nr = NULL, *pkt_kod = NULL;
	double cp_odl, cp_kat, azymP = 0.0, azymK = 0.0;
	cPunkt *pktPoligon = new cPunkt[n];
	cPunkt pktP1, pktP2, pktK1, pktK2;
	double *cp_katy = new double[n];
	double *cp_boki = new double[n];
	double *cp_azym = new double[n];
	double fk=0.0, fx=0.0, fy=0.0, fl=0.0, fkmax=0.0, flmax=0.0, fk_popr;
	double sum_kt = 0.0, sum_xt = 0.0, sum_yt = 0.0, cp_dlugosc = 0.0;
	gboolean klewe = gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(tbtnKLewe));
	double waga = 0.0;
	int s_dodaj;

	char dok_xy[CONFIG_VALUE_MAX_LEN], dok_kat[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	double dok_kat_double = 0.009/RO_GR;
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	if (!dbgeod_config_get("dok_kier", dok_xy)) dok_kat_double = g_strtod(dok_kat, NULL);
	
	enum tPoligon {
		GEOD_POLIGON_WISZACY,
		GEOD_POLIGON_ZAMKNIETY,
		GEOD_POLIGON_ZAMKNIETYD,
		GEOD_POLIGON_NAWIAZANY
	} cp_typ;

	// odczytanie punktów nawiazania
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieP1Nr",
		"entryPoligonNawiazanieP1X", "entryPoligonNawiazanieP1Y", NULL, pktP1));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPoligon",
			_("Dla P1 musisz obowiązkowo podać Nr, X, Y"));
		return;
	}
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieP2Nr",
		"entryPoligonNawiazanieP2X", "entryPoligonNawiazanieP2Y", NULL, pktP2));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPoligon",
			_("Dla P2 musisz obowiązkowo podać Nr, X, Y"));
		return;
	}
	azymut(pktP1, pktP2, azymP);
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieK1Nr",
		"entryPoligonNawiazanieK1X", "entryPoligonNawiazanieK1Y", NULL, pktK1));
	else pktK1.setNr("");


	// odczytanie katow i dlugosci bokow
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (i = 0; i < n; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_POL_NR, &pkt_nr,
			COL_POL_KOD, &pkt_kod, COL_POL_DL_DOUBLE, &cp_odl,
			COL_POL_KAT_DOUBLE, &cp_kat, -1);
		pktPoligon[i].setNr(pkt_nr);
		pktPoligon[i].setKod(pkt_kod);
		cp_katy[i] = cp_kat;
		fk += cp_kat;
		cp_boki[i] = cp_odl;
		cp_dlugosc += cp_odl;
		g_free(pkt_nr);
		g_free(pkt_kod);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	
	// typ ciagu poligonowego
	if (g_str_equal(pktPoligon[n-1].getNr(), pktP1.getNr())) {
		if (cp_boki[n-1] > 0)
			cp_typ = GEOD_POLIGON_ZAMKNIETYD;
		else
			cp_typ = GEOD_POLIGON_ZAMKNIETY;
	}
	else if (g_str_equal(pktPoligon[n-1].getNr(), pktK1.getNr())) {
		cp_typ = GEOD_POLIGON_NAWIAZANY;
		if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieK2Nr",
			"entryPoligonNawiazanieK2X", "entryPoligonNawiazanieK2Y", NULL, pktK2));
		else {
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPoligon",
				_("Dla K2 musisz obowiązkowo podać Nr, X, Y"));
			delete [] pktPoligon;
			delete [] cp_katy;
			delete [] cp_boki;
			return;
		}
	}
	else
		cp_typ = GEOD_POLIGON_WISZACY;

	// obliczenie odchylki katowej i azymutu
	switch(cp_typ) {
		case GEOD_POLIGON_WISZACY:
			fk = 0.0;
			break;
		case GEOD_POLIGON_NAWIAZANY:
			azymut(pktK1, pktK2, azymK);
			klewe ? sum_kt=azymK-azymP+(n-2)*M_PI : sum_kt=azymP-azymK+(n-2)*M_PI;
			fk -= sum_kt;
			break;
		default:
			azymK=azymP;
			sum_kt=(n-2)*M_PI;
			fk -= sum_kt;
	}
	if (cp_typ == GEOD_POLIGON_WISZACY)
		fk_popr = fk/(double)(n-1);
	else
		fk_popr = fk/(double)(n);
	
	cp_katy[0] -= fk_popr;
	klewe ? cp_azym[0]=azymP+cp_katy[0]-M_PI : cp_azym[0]=azymP+M_PI-cp_katy[0];
	pktPoligon[0].setXY(pktP2.getX(), pktP2.getY());
	for (i=1; i<n; i++) {
		cp_katy[i] -= fk_popr;
		klewe ? cp_azym[i]=cp_azym[i-1]+cp_katy[i]-M_PI : cp_azym[i]=cp_azym[i-1]+M_PI-cp_katy[i];
		punkt_biegun(pktPoligon[i-1], cp_azym[i-1], cp_boki[i-1], pktPoligon[i]);
	}
	
	// obliczenie przyrostow odchylki liniowej
	switch(cp_typ) {
		case GEOD_POLIGON_WISZACY:
			fx = fy = 0.0;
			break;
		case GEOD_POLIGON_NAWIAZANY:
			sum_xt = pktK1.getX()-pktP2.getX();
			sum_yt = pktK1.getY()-pktP2.getY();
			fx = (pktPoligon[n-1].getX()-pktPoligon[0].getX()) - sum_xt;
			fy = (pktPoligon[n-1].getY()-pktPoligon[0].getY()) - sum_yt;
			break;
		default:
			sum_xt = pktP1.getX()-pktP2.getX();
			sum_yt = pktP1.getY()-pktP2.getY();
			fx = (pktPoligon[n-1].getX()-pktPoligon[0].getX()) - sum_xt;
			fy = (pktPoligon[n-1].getY()-pktPoligon[0].getY()) - sum_yt;
	}
	fl = sqrt(fx*fx + fy*fy);
	for (i=1; i<n; i++) {
		waga += cp_boki[i-1];
		pktPoligon[i].setX( pktPoligon[i].getX()-(waga/cp_dlugosc)*fx );
		pktPoligon[i].setY( pktPoligon[i].getY()-(waga/cp_dlugosc)*fy );
	}

	// uzupelnienie tabeli w oknie
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (i = 0; i < n; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_POL_DODAJ, &s_dodaj, -1);

		switch (s_dodaj) {
		case PKT_DODAJ:
			if (dbgeod_point_add_config(pktPoligon[i]) != GEODERR_OK) {
				geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPoligon", _("Nie mogę dodać punktu do bazy danych!"));
				s_dodaj = PKT_ERROR;
			}
			else
				s_dodaj = PKT_DODANY;
		break;
		case PKT_DODANY:
			dbgeod_point_update(-1, pktPoligon[i], TRUE);
			break;
		}
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
			COL_POL_X, g_strdup_printf("%.3f", pktPoligon[i].getX()), COL_POL_X_DOUBLE, pktPoligon[i].getX(),
			COL_POL_Y, g_strdup_printf("%.3f", pktPoligon[i].getY()), COL_POL_Y_DOUBLE, pktPoligon[i].getY(),
			COL_POL_DODAJ, s_dodaj, -1);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}

	GtkWidget *entryFx = lookup_widget(GTK_WIDGET(toolbutton), "entryPoligonFx");
	GtkWidget *entryFy = lookup_widget(GTK_WIDGET(toolbutton), "entryPoligonFy");
	GtkWidget *entryFl = lookup_widget(GTK_WIDGET(toolbutton), "entryPoligonFl");
	GtkWidget *entryFlmax = lookup_widget(GTK_WIDGET(toolbutton), "entryPoligonFlmax");
	GtkWidget *entryFk = lookup_widget(GTK_WIDGET(toolbutton), "entryPoligonFk");
	GtkWidget *entryFkmax = lookup_widget(GTK_WIDGET(toolbutton), "entryPoligonFkmax");
	GtkWidget *entryDlugosc = lookup_widget(GTK_WIDGET(toolbutton), "entryPoligonDlugosc");

	GdkColor color;
	gdk_color_parse ("red", &color);
	gtk_widget_modify_bg (entryFl, GTK_STATE_NORMAL, &color);
	
	flmax = cp_dlugosc/2000.0;
	fkmax = 2*sqrt(n)*dok_kat_double;
	
	if (fabs(fl) > flmax)
		gtk_widget_modify_base (entryFl, GTK_STATE_NORMAL, &color);
	else
		gtk_widget_modify_base (entryFl, GTK_STATE_NORMAL, NULL);

	if (fabs(fk) > fkmax)
		gtk_widget_modify_base (entryFk, GTK_STATE_NORMAL, &color);
	else
		gtk_widget_modify_base (entryFk, GTK_STATE_NORMAL, NULL);

	gtk_entry_set_text(GTK_ENTRY(entryFk), geod_kat_from_rad(fk, TRUE));
	gtk_entry_set_text(GTK_ENTRY(entryFkmax), geod_kat_from_rad(fkmax, TRUE));
	gtk_entry_set_text(GTK_ENTRY(entryFl), g_strdup_printf("%.*f", dok_xy_int, fl));
	gtk_entry_set_text(GTK_ENTRY(entryFlmax), g_strdup_printf("%.*f", dok_xy_int, flmax));
	gtk_entry_set_text(GTK_ENTRY(entryFx), g_strdup_printf("%.*f", dok_xy_int, fx));
	gtk_entry_set_text(GTK_ENTRY(entryFy), g_strdup_printf("%.*f", dok_xy_int, fy));
	gtk_entry_set_text(GTK_ENTRY(entryDlugosc), g_strdup_printf("%.*f", dok_xy_int, cp_dlugosc));

	delete [] pktPoligon;
	delete [] cp_katy;
	delete [] cp_boki;
}


void on_tbtnPoligonWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	// Czyszczenie listy
	GtkWidget *treePoligon = lookup_widget(GTK_WIDGET(toolbutton), "treePoligon");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treePoligon));
	gtk_tree_store_clear(GTK_TREE_STORE(model));
	
	// Czyszczenie wszystkich pol
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieP1Nr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieP1X");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieP1Y");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieP2Nr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieP2X");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieP2Y");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieAzymP");

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieK1Nr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieK1X");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieK1Y");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieK2Nr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieK2X");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieK2Y");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieAzymK");

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonFk");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonFkmax");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonFl");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonFlmax");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonFx");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonFy");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonDlugosc");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonTyp");

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonMiaryNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonMiaryKod");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonMiaryKat");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryPoligonMiaryDlugosc");

	GtkWidget *focus = lookup_widget(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieP1Nr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


void on_tbtnPoligonUsun_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treePoligon");
	GtkWidget *winPoligon = lookup_widget(GTK_WIDGET(toolbutton), "winPoligon");
	GtkWidget *dialog = NULL;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreePath *tpath = NULL, *tpath_first = NULL;
	GtkTreeIter iter;
	int s_dodaj;
	char *pkt_nr = NULL;
	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		tpath_first = gtk_tree_path_new_first();
		
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_POL_NR, &pkt_nr,
				COL_POL_DODAJ, &s_dodaj, -1);
		if (gtk_tree_path_compare(tpath, tpath_first) != 0) {
			if (s_dodaj == PKT_DODANY) {
				dialog = gtk_message_dialog_new(GTK_WINDOW(winPoligon), GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Usunąć także punkt z bazy danych?"));
				if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES) dbgeod_point_del(0, pkt_nr);
				gtk_widget_destroy(dialog);
			}
			g_free(pkt_nr);
			gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
		}
		else
			geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPoligon", _("Nie można usunąć pierwszego stanowiska"));
	
		gtk_tree_path_free(tpath_first);
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPoligon", _("Zaznacz najpierw pozycję w tabeli"));

	gtk_tree_path_free(tpath);
}


void on_tbtnPoligonRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	const guint NCOLS = 6;
	GSList *lpunkty = NULL;
	cPunkt *punkt = NULL;
	guint i;

	GtkWidget *dialogRaport = create_dialogRaport();
	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");
	gtk_widget_show(dialogRaport);

	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treePoligon");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	const guint NROWS = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;

	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	
	guint colsize[NCOLS] = { 10, 13, 13, 15, 15, 4 };
	char **tab_cell = new char*[(NROWS+1)*NCOLS];

	geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nCiąg poligonowy"));
	
	// punkty nawiązania
	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieP1Nr",
		"entryPoligonNawiazanieP1X", "entryPoligonNawiazanieP1Y", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		geod_raport_error(textview, _("Dla punktu nawiązania P1 nie podano wszystkich wymaganych danych"));
		delete punkt;
	}

	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieP2Nr",
		"entryPoligonNawiazanieP2X", "entryPoligonNawiazanieP2Y", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		geod_raport_error(textview, _("Dla punktu nawiązania P2 nie podano wszystkich wymaganych danych"));
		delete punkt;
	}

	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieK1Nr",
		"entryPoligonNawiazanieK1X", "entryPoligonNawiazanieK1Y", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		delete punkt;
	}

	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieK2Nr",
		"entryPoligonNawiazanieK2X", "entryPoligonNawiazanieK2Y", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		delete punkt;
	}

	geod_raport_table_pkt(textview, lpunkty, FALSE, _("Punkty nawiązania"));
	g_slist_free(lpunkty);
	lpunkty = NULL;	

	// odchylki
	GtkWidget *entryFx = lookup_widget(GTK_WIDGET(toolbutton), "entryPoligonFx");
	GtkWidget *entryFy = lookup_widget(GTK_WIDGET(toolbutton), "entryPoligonFy");
	GtkWidget *entryFl = lookup_widget(GTK_WIDGET(toolbutton), "entryPoligonFl");
	GtkWidget *entryFlmax = lookup_widget(GTK_WIDGET(toolbutton), "entryPoligonFlmax");
	GtkWidget *entryFk = lookup_widget(GTK_WIDGET(toolbutton), "entryPoligonFk");
	GtkWidget *entryFkmax = lookup_widget(GTK_WIDGET(toolbutton), "entryPoligonFkmax");
	GtkWidget *entryDlugosc = lookup_widget(GTK_WIDGET(toolbutton), "entryPoligonDlugosc");

	geod_raport_line(textview, _("Odchyłka kątowa"), gtk_entry_get_text(GTK_ENTRY(entryFk)), FALSE);
	geod_raport_line(textview, _("Dopuszczalna odchyłka kątowa"), gtk_entry_get_text(GTK_ENTRY(entryFkmax)), FALSE);
	geod_raport_line(textview, _("Fx"), gtk_entry_get_text(GTK_ENTRY(entryFx)), FALSE);
	geod_raport_line(textview, _("Fy"), gtk_entry_get_text(GTK_ENTRY(entryFy)), FALSE);
	geod_raport_line(textview, _("Odchyłka liniowa"), gtk_entry_get_text(GTK_ENTRY(entryFl)), FALSE);
	geod_raport_line(textview, _("Dopuszczalna odchyłka liniowa"), gtk_entry_get_text(GTK_ENTRY(entryFlmax)), FALSE);
	geod_raport_line(textview, _("Długość ciągu"), gtk_entry_get_text(GTK_ENTRY(entryDlugosc)), FALSE);
	
	// ciag poligonowy	
	tab_cell[0] = g_strdup(_("Nr"));
	tab_cell[1] = g_strdup(_("Kąt"));
	tab_cell[2] = g_strdup(_("Dł. boku"));
	tab_cell[3] = g_strdup(_("X"));
	tab_cell[4] = g_strdup(_("Y"));
	tab_cell[5] = g_strdup(_("Kod"));
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);	
	for (i=1; i<=NROWS; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_POL_NR, &tab_cell[i*NCOLS+0],
			COL_POL_KAT, &tab_cell[i*NCOLS+1],
			COL_POL_DL, &tab_cell[i*NCOLS+2],
			COL_POL_X, &tab_cell[i*NCOLS+3],
			COL_POL_Y, &tab_cell[i*NCOLS+4],
			COL_POL_KOD, &tab_cell[i*NCOLS+5], -1);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}

	geod_raport_table(textview, tab_cell, NCOLS, NROWS, colsize, _("\nPunkty ciągu poligonowego"));
	geod_raport_stopka(textview);

	for (i=0; i<(NROWS+1)*NCOLS; i++)
		g_free(tab_cell[i]);
}


void on_tbtnPoligonWidok_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt *pktDane = NULL;
	GdkPoint *pktWidok = NULL;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treePoligon");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	guint l_wierszy = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	guint i, npoints = l_wierszy+4;
	char *pkt_nr = NULL;
	double pkt_x, pkt_y;

	GtkWidget *winWidok = create_winWidok();
	gtk_widget_show(winWidok);
	GtkWidget *darea = lookup_widget(GTK_WIDGET(winWidok), "drawingAreaWidok");
	
	pktDane = new cPunkt[4+l_wierszy];
	
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieP1Nr",
		"entryPoligonNawiazanieP1X", "entryPoligonNawiazanieP1Y", NULL, pktDane[0]));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPoligon", _("Musisz podać Nr, X, Y punktu P1"));
		delete [] pktDane;
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieP2Nr",
		"entryPoligonNawiazanieP2X", "entryPoligonNawiazanieP2Y", NULL, pktDane[1]));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarPoligon", _("Musisz podać Nr, X, Y punktu P2"));
		delete [] pktDane;
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieK1Nr",
		"entryPoligonNawiazanieK1X", "entryPoligonNawiazanieK1Y", NULL, pktDane[l_wierszy+2]));
	else
		npoints--;

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryPoligonNawiazanieK2Nr",
		"entryPoligonNawiazanieK2X", "entryPoligonNawiazanieK2Y", NULL, pktDane[l_wierszy+3]));
	else
		npoints--;

	pktWidok = new GdkPoint[npoints];
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (i = 0; i < l_wierszy; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_POL_NR, &pkt_nr,
		COL_POL_X_DOUBLE, &pkt_x, COL_POL_Y_DOUBLE, &pkt_y, -1);
		pktDane[2+i].setNr(pkt_nr);
		pktDane[2+i].setXY(pkt_x, pkt_y);
		g_free(pkt_nr);

		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, npoints);
	
	// rysowanie linii
	geod_draw_line(GTK_WIDGET(darea), pktWidok[0], pktWidok[1], LINE_BAZA);
	for (i=1; i<l_wierszy; i++)
		geod_draw_line(GTK_WIDGET(darea), pktWidok[i+1], pktWidok[i+2], LINE_DLUGOSC);
	if (npoints == l_wierszy+4)
		geod_draw_line(GTK_WIDGET(darea), pktWidok[npoints-2], pktWidok[npoints-1], LINE_BAZA);
	
	//  rysowanie punktow
	for (i=0; i<l_wierszy; i++)
		geod_draw_point(GTK_WIDGET(darea), pktWidok[i+2], pktDane[i+2].getNr(), PKT_WYNIK);
	for (i=l_wierszy+2; i<npoints; i++)
		geod_draw_point(GTK_WIDGET(darea), pktWidok[i], pktDane[i].getNr(), PKT_OSNOWA);
	geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
	geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[1].getNr(), PKT_OSNOWA);

	gtk_widget_queue_draw(darea);
	delete [] pktDane;
	delete [] pktWidok;
}


void on_tbtnPoligonZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winPoligon");
	gtk_widget_destroy(window);
}


void on_framePoligonNawiazanie_set_focus_child (GtkContainer *container,
	GtkWidget *widget, gpointer user_data)
{
	GtkWidget *entryAzymP = lookup_widget(GTK_WIDGET(container), "entryPoligonNawiazanieAzymP");
	GtkWidget *entryAzymK = lookup_widget(GTK_WIDGET(container), "entryPoligonNawiazanieAzymK");
	GtkWidget *entryP1X = lookup_widget(GTK_WIDGET(container), "entryPoligonNawiazanieP1X");
	GtkWidget *entryP1Y = lookup_widget(GTK_WIDGET(container), "entryPoligonNawiazanieP1Y");
	GtkWidget *entryP2X = lookup_widget(GTK_WIDGET(container), "entryPoligonNawiazanieP2X");
	GtkWidget *entryP2Y = lookup_widget(GTK_WIDGET(container), "entryPoligonNawiazanieP2Y");
	GtkWidget *entryK1X = lookup_widget(GTK_WIDGET(container), "entryPoligonNawiazanieK1X");
	GtkWidget *entryK1Y = lookup_widget(GTK_WIDGET(container), "entryPoligonNawiazanieK1Y");
	GtkWidget *entryK2X = lookup_widget(GTK_WIDGET(container), "entryPoligonNawiazanieK2X");
	GtkWidget *entryK2Y = lookup_widget(GTK_WIDGET(container), "entryPoligonNawiazanieK2Y");

	const char *txtP1X = gtk_entry_get_text(GTK_ENTRY(entryP1X));
	const char *txtP1Y = gtk_entry_get_text(GTK_ENTRY(entryP1Y));
	const char *txtP2X = gtk_entry_get_text(GTK_ENTRY(entryP2X));
	const char *txtP2Y = gtk_entry_get_text(GTK_ENTRY(entryP2Y));
	const char *txtK1X = gtk_entry_get_text(GTK_ENTRY(entryK1X));
	const char *txtK1Y = gtk_entry_get_text(GTK_ENTRY(entryK1Y));
	const char *txtK2X = gtk_entry_get_text(GTK_ENTRY(entryK2X));
	const char *txtK2Y = gtk_entry_get_text(GTK_ENTRY(entryK2Y));
	double azym = 0.0;
	char *txtAzym = NULL;

	if (g_str_equal(txtP1X, "") || g_str_equal(txtP1Y, "") || g_str_equal(txtP2X, "") ||
		g_str_equal(txtP2Y, ""))
		gtk_editable_delete_text(GTK_EDITABLE(entryAzymP), 0, -1);
	else {
		azymut(g_strtod(txtP2Y, NULL)-g_strtod(txtP1Y, NULL), g_strtod(txtP2X, NULL)-g_strtod(txtP1X, NULL), azym);
		txtAzym = geod_kat_from_rad(azym, TRUE);
		gtk_entry_set_text(GTK_ENTRY(entryAzymP), txtAzym);
		g_free(txtAzym);
	}

	if (g_str_equal(txtK1X, "") || g_str_equal(txtK1Y, "") || g_str_equal(txtK2X, "") ||
		g_str_equal(txtK2Y, ""))
		gtk_editable_delete_text(GTK_EDITABLE(entryAzymK), 0, -1);
	else {
		azymut(g_strtod(txtK2Y, NULL)-g_strtod(txtK1Y, NULL), g_strtod(txtK2X, NULL)-g_strtod(txtK1X, NULL), azym);
		txtAzym = geod_kat_from_rad(azym, TRUE);
		gtk_entry_set_text(GTK_ENTRY(entryAzymK), txtAzym);
		g_free(txtAzym);
	}
}


gboolean on_entryPoligonNawiazanieP1Nr_focus_in_event
	(GtkWidget *widget, GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarPoligon");
	return FALSE;
}


gboolean on_entryPoligonNawiazanieP1Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPoligon");
	return FALSE;
}


gboolean on_entryPoligonNawiazanieP2Nr_focus_in_event
	(GtkWidget *widget, GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarPoligon");
	return FALSE;
}


gboolean on_entryPoligonNawiazanieP2Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPoligon");
	return FALSE;
}


gboolean on_entryPoligonNawiazanieK1Nr_focus_in_event
	(GtkWidget *widget, GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarPoligon");
	return FALSE;
}


gboolean on_entryPoligonNawiazanieK1Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPoligon");
	return FALSE;
}


gboolean on_entryPoligonNawiazanieK2Nr_focus_in_event
	(GtkWidget *widget, GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarPoligon");
	return FALSE;
}


gboolean on_entryPoligonNawiazanieK2Nr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPoligon");
	return FALSE;
}


gboolean on_entryPoligonMiaryKod_focus_in_event
	(GtkWidget *widget, GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Kod_set(widget, "sbarPoligon");
	return FALSE;
}


gboolean on_entryPoligonMiaryKod_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarPoligon");
	return FALSE;
}

void on_btnPoligonMiaryDodaj_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *entryNr = lookup_widget(GTK_WIDGET(button), "entryPoligonMiaryNr");
	GtkWidget *entryKod = lookup_widget(GTK_WIDGET(button), "entryPoligonMiaryKod");
	GtkWidget *entryTyp = lookup_widget(GTK_WIDGET(button), "entryPoligonTyp");
	GtkWidget *tbtnOblicz = lookup_widget(GTK_WIDGET(button), "tbtnPoligonOblicz");
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treePoligon");
	GtkWidget *dialog = NULL;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreeIter iter, iter_sel;
	GtkTreePath *tpath;
	int s_dodaj = PKT_DODAJ;
	int response = GTK_RESPONSE_YES;

	double pol_kat, pol_dl;
	char *txtNr=NULL, *txtKod=NULL, *txtKat=NULL, *txtDlugosc=NULL;
	cPunkt pktP1, pktP2, pktK1, pktK2;
	gboolean dlgOblicz = FALSE;

	// odczytanie punktów nawiazania
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryPoligonNawiazanieP1Nr",
		"entryPoligonNawiazanieP1X", "entryPoligonNawiazanieP1Y", NULL, pktP1));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarPoligon",
			_("Dla P1 musisz obowiązkowo podać Nr, X, Y"));
		return;
	}
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryPoligonNawiazanieP2Nr",
		"entryPoligonNawiazanieP2X", "entryPoligonNawiazanieP2Y", NULL, pktP2));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarPoligon",
			_("Dla P2 musisz obowiązkowo podać Nr, X, Y"));
		return;
	}
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryPoligonNawiazanieK1Nr",
		"entryPoligonNawiazanieK1X", "entryPoligonNawiazanieK1Y", NULL, pktK1));
	else pktK1.setNr("");
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryPoligonNawiazanieK2Nr",
		"entryPoligonNawiazanieK2X", "entryPoligonNawiazanieK2Y", NULL, pktK2));
	else pktK2.setNr("");

	
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	txtNr = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryNr)));
	txtKod = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryKod)));
	// spr. czy podano nr
	if (g_str_equal(txtNr, "")) {
		geod_sbar_set(GTK_WIDGET(button), "sbarPoligon", _("Musisz podać Nr punktu"));
		gtk_widget_grab_focus(GTK_WIDGET(entryNr));
		return;
	}
	// spr. czy pierwszy (wtedy nr = P2)
	else if(gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL)==0 && !g_str_equal(txtNr, pktP2.getNr())) {
		geod_sbar_set(GTK_WIDGET(button), "sbarPoligon", _("Nr pierwszego stanowiska nie jest równy P2"));
		gtk_widget_grab_focus(GTK_WIDGET(entryNr));
		return;
	}
	// spr. czy istnieje punkt o takim samym numerze
	else if (!dbgeod_point_find(txtNr)) {
		if (gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL)==0 && g_str_equal(txtNr, pktP2.getNr()))
			s_dodaj = PKT_NIE_DODAWAJ;
		else if (gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL)>1 && (g_str_equal(txtNr, pktK1.getNr()) || g_str_equal(txtNr, pktP1.getNr())))
			s_dodaj = PKT_NIE_DODAWAJ;
		else {
			dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Punkt o takim numerze już istnieje.\nCzy na pewno nadpisać istniejący punkt?"));
			response = gtk_dialog_run(GTK_DIALOG(dialog));
			gtk_widget_destroy(dialog);
			if (response == GTK_RESPONSE_YES)
				s_dodaj = PKT_DODANY; // aktualizuj istniejacy punkt
			else {
				geod_sbar_set(GTK_WIDGET(button), "sbarPoligon", _("Punkt o takim numerze znajduje się już w bazie"));
				gtk_widget_grab_focus(GTK_WIDGET(entryNr));
				return;
			}
		}
	}

	if (g_str_equal(txtNr, pktP1.getNr())) { // pierwszy punkt ciagu
		if (!geod_entry_get_kat(GTK_WIDGET(button), "entryPoligonMiaryKat", pol_kat))
			txtKat = geod_kat_from_rad(pol_kat);
		else {
			geod_sbar_set(GTK_WIDGET(button), "sbarPoligon", _("Musisz podać kąt"));
			return;
		}
 		if (!geod_entry_get_double(GTK_WIDGET(button), "entryPoligonMiaryDlugosc", pol_dl, FALSE))
			txtDlugosc = g_strdup_printf("%.*f", dok_xy_int, pol_dl);	 
		else
			txtDlugosc = g_strdup("");
		dlgOblicz = TRUE;
		gtk_entry_set_text(GTK_ENTRY(entryTyp), "CIĄG ZAMKNIĘTY");
	}
	else if (g_str_equal(txtNr, pktK1.getNr())) { // ostatni punkt ciagu nawiazanego
		if (!geod_entry_get_kat(GTK_WIDGET(button), "entryPoligonMiaryKat", pol_kat))
			txtKat = geod_kat_from_rad(pol_kat);
		else {
			geod_sbar_set(GTK_WIDGET(button), "sbarPoligon", _("Musisz podać kąt"));
			return;
		}
		pol_dl = 0.0;
		txtDlugosc = g_strdup("");
		dlgOblicz = TRUE;
		gtk_entry_set_text(GTK_ENTRY(entryTyp), "CIĄG NAWIĄZANY");

	}
	// stanowisko ciagu poligonowego
	else if (!geod_entry_get_kat(GTK_WIDGET(button), "entryPoligonMiaryKat", pol_kat)) {
		if (!geod_entry_get_double(GTK_WIDGET(button), "entryPoligonMiaryDlugosc", pol_dl)){
			txtKat = geod_kat_from_rad(pol_kat);
			txtDlugosc = g_strdup_printf("%.*f", dok_xy_int, pol_dl);
			gtk_entry_set_text(GTK_ENTRY(entryTyp), "CIĄG WISZĄCY");
		}
		else {
			geod_sbar_set(GTK_WIDGET(button), "sbarPoligon", _("Musisz podać długość boku"));
			return;
		}
	}
	else { // brak kata na stanowisku, zakladamy ze ostatni punkt ciagu wiszacego
		dlgOblicz = TRUE;
		txtDlugosc = g_strdup("");
		txtKat = g_strdup("");
		pol_dl = 0.0;
		pol_kat = 0.0;
		gtk_entry_set_text(GTK_ENTRY(entryTyp), "CIĄG WISZĄCY");
	}

	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);
	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter_sel, tpath);
		gtk_tree_selection_unselect_all(gtk_tree_view_get_selection(GTK_TREE_VIEW(tree)));
		gtk_tree_store_insert_after(GTK_TREE_STORE(model), &iter, NULL, &iter_sel);
		gtk_tree_path_free(tpath);
	}
	else
		gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);

	gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
		COL_POL_NR, txtNr,
		COL_POL_KAT, txtKat, COL_POL_KAT_DOUBLE, pol_kat,
		COL_POL_DL, txtDlugosc, COL_POL_DL_DOUBLE, pol_dl,
		COL_POL_KOD, txtKod, COL_POL_DODAJ, s_dodaj, -1);

	g_free(txtNr);
	g_free(txtKod);
	g_free(txtDlugosc);
	g_free(txtKat);
	
	// wyczyszczenie wszystkich pol
	geod_entry_inc(GTK_WIDGET(button), "entryPoligonMiaryNr");
	geod_entry_clear(GTK_WIDGET(button), "entryPoligonMiaryKod");
	geod_entry_clear(GTK_WIDGET(button), "entryPoligonMiaryKat");
	geod_entry_clear(GTK_WIDGET(button), "entryPoligonMiaryDlugosc");

	// jezeli stwierdzo podanie ostatniego punktu zapytaj czy obliczyc
	if (dlgOblicz) {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Czy obliczyć współrzędne punktów poligonowych?"));
		response = gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
		if (response == GTK_RESPONSE_YES)
			g_signal_emit_by_name(tbtnOblicz, "clicked");
	}

	GtkWidget *focus = lookup_widget(GTK_WIDGET(button), "entryPoligonMiaryNr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}

gboolean on_entryPoligonMiaryNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	/// dodac uzupelnianie danych
	return FALSE;
}


/* winOkragTyczenie */
void on_mnuMainTyczenieOkrag_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if(geod_open_window(create_winOkragTyczenie, "winOkragTyczenie") == NULL)
		geod_sbar_set_error(GTK_WIDGET(menuitem), "sbarMain", GEODERR_WINDOW_OPEN);
}


void on_winOkragTyczenie_realize (GtkWidget *widget, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(widget), "treeOkragTyczenie");
	GtkTreeStore *model;
	
	model=gtk_tree_store_new(N_DOM_COLUMNS, \
	G_TYPE_STRING, G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, \
	G_TYPE_STRING, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_DOUBLE, \
	G_TYPE_DOUBLE, G_TYPE_DOUBLE, G_TYPE_STRING, G_TYPE_INT, G_TYPE_BOOLEAN);
	
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), GTK_TREE_MODEL(model));

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *column;
	gint col_offset;
	
	// kolumna Nr
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Nr"), renderer, "text", COL_DOM_NR, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// kolumna miara biezaca
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Bieżąca"), renderer, "text", COL_DOM_B, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna domiar
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Domiar"), renderer, "text", COL_DOM_D, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 70);

	// kolumna wsp. X
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("X"), renderer, "text", COL_DOM_X, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 80);

	// kolumna wsp. Y
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Y"), renderer, "text", COL_DOM_Y, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 80);

	// kolumna Kod
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("Kod"), renderer, "text", COL_DOM_KOD, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(column), TRUE);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(column), 50);

	// kolumna Toggle
	GtkCellRenderer *rendererToggle = gtk_cell_renderer_toggle_new();
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW(treeview), \
	   -1, _("D"), rendererToggle, "active", COL_DOM_TOGGLE, NULL);
	GtkTreeViewColumn *toggleColumn = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), col_offset -1);
	gtk_tree_view_column_set_min_width (GTK_TREE_VIEW_COLUMN(toggleColumn), 10);
}


void on_tbtnOkragTyczenieOblicz_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt *pktTyczony = NULL;
	cPunkt pktOkragO, pktOkragP;

	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeOkragTyczenie");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	guint l_wierszy = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	guint i, s_dodaj;
	double biezaca, domiar, az0;
	char *pkt_nr = NULL, *pkt_kod = NULL, *txtX = NULL, *txtY = NULL;
	int zadanie_id;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragTyczenieOkragONr",
		"entryOkragTyczenieOkragOX", "entryOkragTyczenieOkragOY", NULL, pktOkragO));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragTyczenie", _("Musisz podać Nr, X, Y środka okręgu"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragTyczenieOkragPNr",
		"entryOkragTyczenieOkragPX", "entryOkragTyczenieOkragPY", NULL, pktOkragP));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragTyczenie", _("Musisz podać Nr, X, Y początkowego punktu tyczenia"));
		return;
	}

	double okragR = dlugosc(pktOkragO, pktOkragP);
	azymut(pktOkragO, pktOkragP, az0);
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (i = 0; i < l_wierszy; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_DOM_NR, &pkt_nr,
			COL_DOM_B_DOUBLE, &biezaca, COL_DOM_D_DOUBLE, &domiar, COL_DOM_KOD, &pkt_kod,
			COL_DOM_DODAJ, &s_dodaj, -1);

		pktTyczony = new cPunkt;
		pktTyczony->setNr(pkt_nr);
		pktTyczony->setKod(pkt_kod);

		if (okragR == 0.0)
			pktTyczony->setXY(pktOkragO.getX(), pktOkragO.getY());
		else
			punkt_biegun(pktOkragO, az0+(biezaca/okragR), okragR-domiar, *pktTyczony);
		
		txtX = g_strdup_printf("%.*f", dok_xy_int, pktTyczony->getX());
		txtY = g_strdup_printf("%.*f", dok_xy_int, pktTyczony->getY());
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
				COL_DOM_X, txtX, COL_DOM_X_DOUBLE, pktTyczony->getX(),
				COL_DOM_Y, txtY, COL_DOM_Y_DOUBLE, pktTyczony->getY(), -1);

		switch (s_dodaj) {
			case PKT_DODAJ:
				if (dbgeod_point_add_config(*pktTyczony) != GEODERR_OK) {
					geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragTyczenie", _("Nie mogę dodać punktu do bazy danych!"));
					gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_DOM_DODAJ, PKT_ERROR, -1);
				}
				else {
					gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_DOM_DODAJ, PKT_DODANY, -1);
					dbgeod_hist_add("OkragTyczenie", zadanie_id);
					dbgeod_hist_point_set(zadanie_id, "OkragONr", pktOkragO.getNr(), 0);
					dbgeod_hist_point_set(zadanie_id, "OkragPNr", pktOkragP.getNr(), 0);
					dbgeod_hist_point_set(zadanie_id, "WNr", pktTyczony->getNr(), 1);
					dbgeod_hist_par_set(zadanie_id, "biezaca", biezaca, 0);
					dbgeod_hist_par_set(zadanie_id, "domiar", domiar, 0);
				}
				break;
				
			case PKT_DODANY:
				dbgeod_point_update(-1, *pktTyczony, TRUE);
			break;
		}
		g_free(txtX);
		g_free(txtY);
			
		g_free(pkt_nr);
		g_free(pkt_kod);
		delete pktTyczony;
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
}


void on_tbtnOkragTyczenieWyczysc_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	// Czyszczenie listy
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeOkragTyczenie");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	gtk_tree_store_clear(GTK_TREE_STORE(model));
	
	// Czyszczenie wszystkich pol
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragTyczenieOkragPNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragTyczenieOkragPX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragTyczenieOkragPY");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragTyczenieOkragONr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragTyczenieOkragOX");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragTyczenieOkragOY");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragTyczenieOkragR");

	geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragTyczeniePunktNr");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragTyczeniePunktB");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragTyczeniePunktD");
	geod_entry_clear(GTK_WIDGET(toolbutton), "entryOkragTyczeniePunktKod");

	GtkWidget *focus = lookup_widget(GTK_WIDGET(toolbutton), "entryOkragTyczenieOkragONr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


void on_tbtnOkragTyczenieUsun_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeOkragTyczenie");
	GtkWidget *winOkragTyczenie = lookup_widget(GTK_WIDGET(toolbutton), "winOkragTyczenie");
	GtkWidget *dialog;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreePath *tpath;
	GtkTreeIter iter;
	char *pkt_nr = NULL;
	int s_dodaj;
	
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);

	if (tpath != NULL) {
		gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter, tpath);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_DOM_NR, &pkt_nr,
				COL_DOM_DODAJ, &s_dodaj, -1);
		if (s_dodaj == PKT_DODANY) {
			dialog = gtk_message_dialog_new(GTK_WINDOW(winOkragTyczenie), GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, _("Usunąć także punkt z bazy danych?"));
			if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES) dbgeod_point_del(0, pkt_nr);
			gtk_widget_destroy(dialog);
		}
		g_free(pkt_nr);
		gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
	}
	else
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragTyczenie", _("Zaznacz najpierw pozycję w tabeli"));

	gtk_tree_path_free(tpath);
}


void on_tbtnOkragTyczenieRaport_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	const guint NCOLS = 6;
	GSList *lpunkty = NULL;
	cPunkt *punkt = NULL;
	guint i;
	char *txtPromien = NULL;
	double promien;

	GtkWidget *dialogRaport = create_dialogRaport();
	GtkWidget *textview = lookup_widget(GTK_WIDGET(dialogRaport), "textviewDialogRaport");
	gtk_widget_show(dialogRaport);

	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeOkragTyczenie");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	const guint NROWS = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;

	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	
	guint colsize[NCOLS] = { 10, 13, 13, 15, 15, 4 };
	char **tab_cell = new char*[(NROWS+1)*NCOLS];

	geod_raport_naglowek(textview, _("RAPORT Z OBLICZEŃ\nTyczenie punktów okręgu"));
	
	// promien
	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragTyczenieOkragONr",
		"entryOkragTyczenieOkragOX", "entryOkragTyczenieOkragOY", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		geod_raport_error(textview, _("Dla środka okręgu nie podano wszystkich wymaganych danych"));
		delete punkt;
	}

	punkt = new cPunkt;
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragTyczenieOkragPNr",
		"entryOkragTyczenieOkragPX", "entryOkragTyczenieOkragPY", NULL, *punkt))
		lpunkty = g_slist_append(lpunkty, punkt);
	else {
		geod_raport_error(textview, _("Dla punktu początkowego nie podano wszystkich wymaganych danych"));
		delete punkt;
	}
	
	geod_raport_table_pkt(textview, lpunkty, FALSE, _("Środek i początek tyczenia"));
	g_slist_free(lpunkty);
	lpunkty = NULL;	

	if (!geod_entry_get_double(GTK_WIDGET(toolbutton), "entryOkragTyczenieOkragR", promien)) {
		txtPromien = g_strdup_printf("%.*f", dok_xy_int, promien);
		geod_raport_line(textview, _("Promień okręgu"), txtPromien, TRUE);
		g_free(txtPromien);
	}

	// tyczone punkty
	tab_cell[0] = g_strdup(_("Nr"));
	tab_cell[1] = g_strdup(_("Bieżąca"));
	tab_cell[2] = g_strdup(_("Domiar"));
	tab_cell[3] = g_strdup(_("X"));
	tab_cell[4] = g_strdup(_("Y"));
	tab_cell[5] = g_strdup(_("Kod"));
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);	
	for (i=1; i<=NROWS; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_DOM_NR, &tab_cell[i*NCOLS+0],
			COL_DOM_B, &tab_cell[i*NCOLS+1],
			COL_DOM_D, &tab_cell[i*NCOLS+2],
			COL_DOM_X, &tab_cell[i*NCOLS+3],
			COL_DOM_Y, &tab_cell[i*NCOLS+4],
			COL_DOM_KOD, &tab_cell[i*NCOLS+5], -1);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}

	geod_raport_table(textview, tab_cell, NCOLS, NROWS, colsize, _("\nTyczone punkty"));
	geod_raport_stopka(textview);

	for (i=0; i<(NROWS+1)*NCOLS; i++)
		g_free(tab_cell[i]);
}


void on_tbtnOkragTyczenieWidok_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	cPunkt *pktDane = NULL;
	GdkPoint *pktWidok = NULL;
	GtkWidget *tree = lookup_widget(GTK_WIDGET(toolbutton), "treeOkragTyczenie");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	guint l_wierszy = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	guint i;
	char *pkt_nr = NULL;
	double pkt_x, pkt_y;

	GtkWidget *winWidok = create_winWidok();
	gtk_widget_show(winWidok);
	GtkWidget *darea = lookup_widget(GTK_WIDGET(winWidok), "drawingAreaWidok");
	
	pktDane = new cPunkt[2+l_wierszy];
	
	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragTyczenieOkragONr",
		"entryOkragTyczenieOkragOX", "entryOkragTyczenieOkragOY", NULL, pktDane[0]));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragTyczenie", _("Musisz podać Nr, X, Y środka okręgu"));
		delete [] pktDane;
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(toolbutton), "entryOkragTyczenieOkragPNr",
		"entryOkragTyczenieOkragPX", "entryOkragTyczenieOkragPY", NULL, pktDane[1]));
	else {
		geod_sbar_set(GTK_WIDGET(toolbutton), "sbarOkragTyczenie", _("Musisz podać Nr, X, Y punktu początkowego"));
		delete [] pktDane;
		return;
	}

	pktWidok = new GdkPoint[2+l_wierszy];
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	for (i = 0; i < l_wierszy; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_DOM_NR, &pkt_nr,
		COL_DOM_X_DOUBLE, &pkt_x, COL_DOM_Y_DOUBLE, &pkt_y, -1);
		pktDane[2+i].setNr(pkt_nr);
		pktDane[2+i].setXY(pkt_x, pkt_y);
		g_free(pkt_nr);

		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	double skala = geod_view_transform(GTK_WIDGET(darea), pktDane, pktWidok, 2+l_wierszy);
	
	// rysowanie okręgu
	geod_draw_circle(GTK_WIDGET(darea), pktWidok[0], (int)round(dlugosc(pktDane[0], pktDane[1])*skala));

	geod_draw_point(GTK_WIDGET(darea), pktWidok[0], pktDane[0].getNr(), PKT_OSNOWA);
	geod_draw_point(GTK_WIDGET(darea), pktWidok[1], pktDane[1].getNr(), PKT_OSNOWA);

	// rysowanie punktow
	for (i = 0; i < l_wierszy; i++)
		geod_draw_point(GTK_WIDGET(darea), pktWidok[i+2], pktDane[i+2].getNr(), PKT_WYNIK);

	gtk_widget_queue_draw(darea);
	delete [] pktDane;
	delete [] pktWidok;
}


void on_tbtnOkragTyczenieZamknij_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *window = lookup_widget(GTK_WIDGET(toolbutton), "winOkragTyczenie");
	gtk_widget_destroy(window);
}


void on_frameOkragTyczenieOkrag_set_focus_child (GtkContainer *container,
	GtkWidget *widget, gpointer user_data)
{
	char *txtRWyl = NULL;
	GtkWidget *entryRWyl = lookup_widget(GTK_WIDGET(container), "entryOkragTyczenieOkragR");
	GtkWidget *entryOkragOX = lookup_widget(GTK_WIDGET(container), "entryOkragTyczenieOkragOX");
	GtkWidget *entryOkragOY = lookup_widget(GTK_WIDGET(container), "entryOkragTyczenieOkragOY");
	GtkWidget *entryOkragPX = lookup_widget(GTK_WIDGET(container), "entryOkragTyczenieOkragPX");
	GtkWidget *entryOkragPY = lookup_widget(GTK_WIDGET(container), "entryOkragTyczenieOkragPY");
	
	const char *txtOX = gtk_entry_get_text(GTK_ENTRY(entryOkragOX));
	const char *txtOY = gtk_entry_get_text(GTK_ENTRY(entryOkragOY));
	const char *txtPX = gtk_entry_get_text(GTK_ENTRY(entryOkragPX));
	const char *txtPY = gtk_entry_get_text(GTK_ENTRY(entryOkragPY));
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if (g_str_equal(txtOX, "") || g_str_equal(txtOY, "") || g_str_equal(txtPX, "") ||
		g_str_equal(txtPY, ""))
		gtk_editable_delete_text(GTK_EDITABLE(entryRWyl), 0, -1);
	else {
		txtRWyl = g_strdup_printf("%.*f",  dok_xy_int, dlugosc(g_strtod(txtOX, NULL), g_strtod(txtOY, NULL), g_strtod(txtPX, NULL), g_strtod(txtPY, NULL)));
		gtk_entry_set_text(GTK_ENTRY(entryRWyl), txtRWyl);
		g_free(txtRWyl);
	}
}


gboolean on_entryOkragTyczenieOkragONr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragTyczenie");
	return FALSE;
}


gboolean on_entryOkragTyczenieOkragONr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragTyczenie");
	return FALSE;
}


gboolean on_entryOkragTyczenieOkragPNr_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Punkt_set(widget, "sbarOkragTyczenie");
	return FALSE;
}


gboolean on_entryOkragTyczenieOkragPNr_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragTyczenie");
	return FALSE;
}


void on_btnOkragTyczeniePunktDodaj_clicked (GtkButton *button, gpointer user_data)
{
	GtkWidget *entryNr = lookup_widget(GTK_WIDGET(button), "entryOkragTyczeniePunktNr");
	GtkWidget *entryKod = lookup_widget(GTK_WIDGET(button), "entryOkragTyczeniePunktKod");
	GtkWidget *cbtnNoAdd = lookup_widget(GTK_WIDGET(button), "cbtnOkragTyczeniePunktDodajLista");
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeOkragTyczenie");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreeIter iter;
	double biezaca, domiar;
	char *txtB = NULL, *txtD = NULL, *txtNr = NULL, *txtKod = NULL;
	int s_dodaj;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if (!geod_entry_get_double(GTK_WIDGET(button), "entryOkragTyczeniePunktB", biezaca))
		geod_entry_get_double(GTK_WIDGET(button), "entryOkragTyczeniePunktD", domiar, FALSE);
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragTyczenie", _("Musisz podać miarę bieżącą"));
		return;
	}
	
	txtB = g_strdup_printf("%.*f", dok_xy_int, biezaca);
	txtD = g_strdup_printf("%.*f", dok_xy_int, domiar);
	txtNr = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryNr)));
	txtKod = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryKod)));

	if (g_str_equal(txtNr, "")) {
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragTyczenie", _("Musisz podać Nr tyczonego punktu"));
		gtk_widget_grab_focus(GTK_WIDGET(entryNr));
		return;
	}
	else if (!dbgeod_point_find(txtNr) && !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnNoAdd))) {
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragTyczenie", _("Punkt o takim numerze znajduje się już w bazie"));
		gtk_widget_grab_focus(GTK_WIDGET(entryNr));
		return;
	}
	
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnNoAdd)))
		s_dodaj = PKT_NIE_DODAWAJ;
	else 
		s_dodaj = PKT_DODAJ;

	gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
		COL_DOM_NR, txtNr,
		COL_DOM_B, txtB, COL_DOM_B_DOUBLE, biezaca,
		COL_DOM_D, txtD, COL_DOM_D_DOUBLE, domiar, 
		COL_DOM_KOD, txtKod, COL_DOM_DODAJ, s_dodaj, -1);

	if (s_dodaj == PKT_DODAJ)
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_DOM_TOGGLE, TRUE, -1);
	else
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_DOM_TOGGLE, FALSE, -1);
	
	g_free(txtB);
	g_free(txtD);
	g_free(txtNr);
	g_free(txtKod);
	
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(button), "tbtnOkragTyczenieOblicz");
	g_signal_emit_by_name(btnOblicz, "clicked");
	
	// wyczyszczenie wszystkich pol
	geod_entry_clear(GTK_WIDGET(button), "entryOkragTyczeniePunktD");
	geod_entry_clear(GTK_WIDGET(button), "entryOkragTyczeniePunktB");
	geod_entry_inc(GTK_WIDGET(button), "entryOkragTyczeniePunktNr");
	geod_entry_clear(GTK_WIDGET(button), "entryOkragTyczeniePunktKod");
	GtkWidget *focus = lookup_widget(GTK_WIDGET(button), "entryOkragTyczeniePunktNr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


void on_winOkragTyczenie_destroy (GtkObject *object, gpointer user_data)
{
	otwarte_okna = geod_slist_remove_string(otwarte_okna, "winOkragTyczenie");
}


gboolean on_entryOkragTyczeniePunktKod_focus_in_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1Kod_set(widget, "sbarOkragTyczenie");
	return FALSE;
}


gboolean on_entryOkragTyczeniePunktKod_focus_out_event (GtkWidget *widget,
	GdkEventFocus *event, gpointer user_data)
{
	geod_sbarF1_clear(widget, "sbarOkragTyczenie");
	return FALSE;
}

void on_btnOkragTyczeniePunktWszystkie_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktOkragO, pktOkragP;
	GtkWidget *entryNr = lookup_widget(GTK_WIDGET(button), "entryOkragTyczeniePunktNr");
	GtkWidget *entryKod = lookup_widget(GTK_WIDGET(button), "entryOkragTyczeniePunktKod");
	GtkWidget *cbtnNoAdd = lookup_widget(GTK_WIDGET(button), "cbtnOkragTyczeniePunktDodajLista");
	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treeOkragTyczenie");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreeIter iter;
	double biezaca, domiar;
	char *txtB = NULL, *txtD = NULL, *txtNr = NULL, *txtNrInc = NULL, *txtKod = NULL;
	int s_dodaj;
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	double kat_max = 0, b_max;
	
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryOkragTyczenieOkragONr",
		"entryOkragTyczenieOkragOX", "entryOkragTyczenieOkragOY", NULL, pktOkragO));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragTyczenie", _("Musisz podać Nr, X, Y środka okręgu"));
		return;
	}

	if (!geod_entry_get_pkt_xy(GTK_WIDGET(button), "entryOkragTyczenieOkragPNr",
		"entryOkragTyczenieOkragPX", "entryOkragTyczenieOkragPY", NULL, pktOkragP));
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragTyczenie", _("Musisz podać Nr, X, Y początkowego punktu tyczenia"));
		return;
	}

	if (!geod_entry_get_double(GTK_WIDGET(button), "entryOkragTyczeniePunktB", biezaca))
		geod_entry_get_double(GTK_WIDGET(button), "entryOkragTyczeniePunktD", domiar, FALSE);
	else {
		geod_sbar_set(GTK_WIDGET(button), "sbarOkragTyczenie", _("Musisz podać miarę bieżącą"));
		return;
	}
	
	txtD = g_strdup_printf("%.*f", dok_xy_int, domiar);
	txtKod = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryKod)));
	txtNr = g_strdup(gtk_entry_get_text(GTK_ENTRY(entryNr)));
	geod_entry_get_kat(GTK_WIDGET(button), "entryOkragTyczenieKat", kat_max);
	
	if (g_str_equal(txtNr, "")) {
		g_free(txtNr);
		txtNr = geod_inc(pktOkragP.getNr());
	}
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnNoAdd)))
		s_dodaj = PKT_NIE_DODAWAJ;
	else 
		s_dodaj = PKT_DODAJ;

	if (biezaca*kat_max < 0) biezaca*=-1; // znak miary biezacej musi byc zgodny ze znakiem kata
	if (fabs(biezaca) < pow(10,-dok_xy_int)) kat_max = 0.0;
	
	b_max = kat_max * dlugosc(pktOkragO, pktOkragP);
	
	for (double b = 0.0; fabs(b)<=fabs(b_max); b += biezaca) {
		txtB = g_strdup_printf("%.*f", dok_xy_int, b);
		do {
			txtNrInc = geod_inc(txtNr);
			g_free(txtNr);
			txtNr = txtNrInc;
		} while (!dbgeod_point_find(txtNr));
		
		gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
		gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
			COL_DOM_NR, txtNr,
			COL_DOM_B, txtB, COL_DOM_B_DOUBLE, b,
			COL_DOM_D, txtD, COL_DOM_D_DOUBLE, domiar, 
			COL_DOM_KOD, txtKod, COL_DOM_DODAJ, s_dodaj, -1);
		
		if (s_dodaj == PKT_DODAJ)
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_DOM_TOGGLE, TRUE, -1);
		else
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter, COL_DOM_TOGGLE, FALSE, -1);

		g_free(txtB);
	}

	g_free(txtD);
	g_free(txtNr);
	g_free(txtKod);
	
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(button), "tbtnOkragTyczenieOblicz");
	g_signal_emit_by_name(btnOblicz, "clicked");
	
	// wyczyszczenie wszystkich pol
	geod_entry_clear(GTK_WIDGET(button), "entryOkragTyczeniePunktD");
	geod_entry_clear(GTK_WIDGET(button), "entryOkragTyczeniePunktB");
	geod_entry_clear(GTK_WIDGET(button), "entryOkragTyczeniePunktNr");
	geod_entry_clear(GTK_WIDGET(button), "entryOkragTyczeniePunktKod");
	GtkWidget *focus = lookup_widget(GTK_WIDGET(button), "entryOkragTyczeniePunktNr");
	gtk_widget_grab_focus(GTK_WIDGET(focus));
}


/* Edntry editing done */
void on_entryZMarekaP1Nr_editing_done (GtkCellEditable *celleditable, gpointer user_data)
{
	GtkWidget *entryAlfa = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaAlfa");
	GtkWidget *entryBeta = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaBeta");
	GtkWidget *entryGamma = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaGamma");
	GtkWidget *entryDelta = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaDelta");

	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryZMarekaP1X", "entryZMarekaP1Y", "entryZMarekaL1Nr");

	geod_update_entry_obs(GTK_WIDGET(entryAlfa), "entryZMarekaSt2Nr", \
		"entryZMarekaSt1Nr", "entryZMarekaP1Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryBeta), "entryZMarekaL1Nr", \
		"entryZMarekaSt1Nr", "entryZMarekaSt2Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryGamma), "entryZMarekaSt1Nr", \
		"entryZMarekaSt2Nr", "entryZMarekaP2Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryDelta), "entryZMarekaL2Nr", \
		"entryZMarekaSt2Nr", "entryZMarekaSt1Nr", NULL);
}


void on_entryZMarekaL1Nr_activate (GtkCellEditable *celleditable, gpointer user_data)
{
	GtkWidget *entryAlfa = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaAlfa");
	GtkWidget *entryBeta = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaBeta");
	GtkWidget *entryGamma = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaGamma");
	GtkWidget *entryDelta = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaDelta");

  	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryZMarekaL1X", "entryZMarekaL1Y", "entryZMarekaP2Nr");

	geod_update_entry_obs(GTK_WIDGET(entryAlfa), "entryZMarekaSt2Nr", \
		"entryZMarekaSt1Nr", "entryZMarekaP1Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryBeta), "entryZMarekaL1Nr", \
		"entryZMarekaSt1Nr", "entryZMarekaSt2Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryGamma), "entryZMarekaSt1Nr", \
		"entryZMarekaSt2Nr", "entryZMarekaP2Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryDelta), "entryZMarekaL2Nr", \
		"entryZMarekaSt2Nr", "entryZMarekaSt1Nr", NULL);
}


void on_entryZMarekaP2Nr_editing_done (GtkCellEditable *celleditable, gpointer user_data)
{
	GtkWidget *entryAlfa = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaAlfa");
	GtkWidget *entryBeta = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaBeta");
	GtkWidget *entryGamma = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaGamma");
	GtkWidget *entryDelta = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaDelta");

	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryZMarekaP2X", "entryZMarekaP2Y", "entryZMarekaL2Nr");

	geod_update_entry_obs(GTK_WIDGET(entryAlfa), "entryZMarekaSt2Nr", \
		"entryZMarekaSt1Nr", "entryZMarekaP1Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryBeta), "entryZMarekaL1Nr", \
		"entryZMarekaSt1Nr", "entryZMarekaSt2Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryGamma), "entryZMarekaSt1Nr", \
		"entryZMarekaSt2Nr", "entryZMarekaP2Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryDelta), "entryZMarekaL2Nr", \
		"entryZMarekaSt2Nr", "entryZMarekaSt1Nr", NULL);
}


void on_entryZMarekaL2Nr_editing_done (GtkCellEditable *celleditable, gpointer user_data)
{
	GtkWidget *entryAlfa = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaAlfa");
	GtkWidget *entryBeta = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaBeta");
	GtkWidget *entryGamma = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaGamma");
	GtkWidget *entryDelta = lookup_widget(GTK_WIDGET(celleditable), "entryZMarekaDelta");

	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryZMarekaL2X", "entryZMarekaL2Y", "entryZMarekaSt1Nr");

	geod_update_entry_obs(GTK_WIDGET(entryAlfa), "entryZMarekaSt2Nr", \
		"entryZMarekaSt1Nr", "entryZMarekaP1Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryBeta), "entryZMarekaL1Nr", \
		"entryZMarekaSt1Nr", "entryZMarekaSt2Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryGamma), "entryZMarekaSt1Nr", \
		"entryZMarekaSt2Nr", "entryZMarekaP2Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryDelta), "entryZMarekaL2Nr", \
		"entryZMarekaSt2Nr", "entryZMarekaSt1Nr", NULL);
}


void on_entryWcieciaKatowePktLNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryKatL = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaKatoweKatL");
	GtkWidget *entryKatP = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaKatoweKatP");
	GtkWidget *cbtnOgolne = lookup_widget(GTK_WIDGET(celleditable), "cbtnWcieciaKatoweOgolne");

  	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryWcieciaKatowePktLX", \
		"entryWcieciaKatowePktLY", "entryWcieciaKatowePktPNr");

	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnOgolne))) {
		geod_update_entry_obs(GTK_WIDGET(entryKatL), "entryWcieciaKatoweWynikNr", \
			"entryWcieciaKatowePktLNr", "entryWcieciaKatowePktL1Nr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKatP), "entryWcieciaKatowePktP1Nr", \
			"entryWcieciaKatowePktPNr", "entryWcieciaKatoweWynikNr", NULL);
	}
	else {
		geod_update_entry_obs(GTK_WIDGET(entryKatL), "entryWcieciaKatoweWynikNr", \
			"entryWcieciaKatowePktLNr", "entryWcieciaKatowePktPNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKatP), "entryWcieciaKatowePktLNr", \
			"entryWcieciaKatowePktPNr", "entryWcieciaKatoweWynikNr", NULL);
	}

}


void on_entryWcieciaKatowePktPNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *cbtnOgolne = lookup_widget(GTK_WIDGET(celleditable), "cbtnWcieciaKatoweOgolne");
	GtkWidget *entryKatL = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaKatoweKatL");
	GtkWidget *entryKatP = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaKatoweKatP");


	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnOgolne))) {
  		geod_update_entry_xy(GTK_WIDGET(celleditable), "entryWcieciaKatowePktPX", \
			"entryWcieciaKatowePktPY", "entryWcieciaKatowePktL1Nr");
		geod_update_entry_obs(GTK_WIDGET(entryKatL), "entryWcieciaKatoweWynikNr", \
			"entryWcieciaKatowePktLNr", "entryWcieciaKatowePktL1Nr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKatP), "entryWcieciaKatowePktP1Nr", \
			"entryWcieciaKatowePktPNr", "entryWcieciaKatoweWynikNr", NULL);
	}
	else {
  		geod_update_entry_xy(GTK_WIDGET(celleditable), "entryWcieciaKatowePktPX", \
			"entryWcieciaKatowePktPY", "entryWcieciaKatoweWynikNr");
		geod_update_entry_obs(GTK_WIDGET(entryKatL), "entryWcieciaKatoweWynikNr", \
			"entryWcieciaKatowePktLNr", "entryWcieciaKatowePktPNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKatP), "entryWcieciaKatowePktLNr", \
			"entryWcieciaKatowePktPNr", "entryWcieciaKatoweWynikNr", NULL);
	}
}


void on_entryWcieciaKatowePktL1Nr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryKatL = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaKatoweKatL");
	GtkWidget *entryKatP = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaKatoweKatP");

  	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryWcieciaKatowePktL1X", \
		"entryWcieciaKatowePktL1Y", "entryWcieciaKatowePktP1Nr");
	geod_update_entry_obs(GTK_WIDGET(entryKatL), "entryWcieciaKatoweWynikNr", \
		"entryWcieciaKatowePktLNr", "entryWcieciaKatowePktL1Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryKatP), "entryWcieciaKatoweWynikNr", \
		"entryWcieciaKatowePktPNr", "entryWcieciaKatowePktP1Nr", NULL);
}


void on_entryWcieciaKatowePktP1Nr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryKatL = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaKatoweKatL");
	GtkWidget *entryKatP = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaKatoweKatP");

  	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryWcieciaKatowePktP1X", \
		"entryWcieciaKatowePktP1Y", "entryWcieciaKatoweKatL");
	geod_update_entry_obs(GTK_WIDGET(entryKatL), "entryWcieciaKatoweWynikNr", \
		"entryWcieciaKatowePktLNr", "entryWcieciaKatowePktL1Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryKatP), "entryWcieciaKatoweWynikNr", \
		"entryWcieciaKatowePktPNr", "entryWcieciaKatowePktP1Nr", NULL);
}


void on_entryWcieciaLiniowePktLNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryDlugoscLW = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaLinioweDlugoscLW");
	GtkWidget *entryDlugoscPW = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaLinioweDlugoscPW");

	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryWcieciaLiniowePktLX", \
		"entryWcieciaLiniowePktLY", "entryWcieciaLiniowePktPNr");

	if (!geod_update_entry_obs(GTK_WIDGET(entryDlugoscLW), "$dl",
		"entryWcieciaLiniowePktLNr", "entryWcieciaLinioweWynikNr", NULL))
		geod_update_entry_obs(GTK_WIDGET(entryDlugoscLW), "$dl",
			"entryWcieciaLinioweWynikNr", "entryWcieciaLiniowePktLNr", NULL);

	if (!geod_update_entry_obs(GTK_WIDGET(entryDlugoscPW), "$dl",
		"entryWcieciaLiniowePktPNr", "entryWcieciaLinioweWynikNr", NULL))
		geod_update_entry_obs(GTK_WIDGET(entryDlugoscPW), "$dl",
			"entryWcieciaLinioweWynikNr", "entryWcieciaLiniowePktPNr", NULL);
}


void on_entryWcieciaLiniowePktPNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryDlugoscLW = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaLinioweDlugoscLW");
	GtkWidget *entryDlugoscPW = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaLinioweDlugoscPW");

	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryWcieciaLiniowePktPX", \
		"entryWcieciaLiniowePktPY", "entryWcieciaLinioweWynikNr");

	if (!geod_update_entry_obs(GTK_WIDGET(entryDlugoscLW), "$dl",
		"entryWcieciaLiniowePktLNr", "entryWcieciaLinioweWynikNr", NULL))
		geod_update_entry_obs(GTK_WIDGET(entryDlugoscLW), "$dl",
			"entryWcieciaLinioweWynikNr", "entryWcieciaLiniowePktLNr", NULL);

	if (!geod_update_entry_obs(GTK_WIDGET(entryDlugoscPW), "$dl",
		"entryWcieciaLiniowePktPNr", "entryWcieciaLinioweWynikNr", NULL))
		geod_update_entry_obs(GTK_WIDGET(entryDlugoscPW), "$dl",
			"entryWcieciaLinioweWynikNr", "entryWcieciaLiniowePktPNr", NULL);

}


void on_entryWcieciaWsteczPktLNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *cbtnKierunki = lookup_widget(GTK_WIDGET(celleditable), "cbtnWcieciaWsteczKierunki");
	GtkWidget *entryKierL = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaWsteczKierL");
	GtkWidget *entryKierC = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaWsteczKierC");
	GtkWidget *entryKierP = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaWsteczKierP");

	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryWcieciaWsteczPktLX", \
		"entryWcieciaWsteczPktLY", "entryWcieciaWsteczPktCNr");

	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnKierunki))) {
		geod_update_entry_obs(GTK_WIDGET(entryKierL), "$hz",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktLNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKierC), "$hz",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktCNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKierP), "$hz",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktPNr", NULL);	
	}
	else {
		geod_update_entry_obs(GTK_WIDGET(entryKierC), "entryWcieciaWsteczPktLNr",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktCNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKierP), "entryWcieciaWsteczPktLNr",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktPNr", NULL);	
	}
}


void on_entryWcieciaWsteczPktCNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *cbtnKierunki = lookup_widget(GTK_WIDGET(celleditable), "cbtnWcieciaWsteczKierunki");
	GtkWidget *entryKierL = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaWsteczKierL");
	GtkWidget *entryKierC = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaWsteczKierC");
	GtkWidget *entryKierP = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaWsteczKierP");

	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryWcieciaWsteczPktCX", \
		"entryWcieciaWsteczPktCY", "entryWcieciaWsteczPktPNr");

	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cbtnKierunki))) {
		geod_update_entry_obs(GTK_WIDGET(entryKierL), "$hz",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktLNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKierC), "$hz",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktCNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKierP), "$hz",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktPNr", NULL);	
	}
	else {
		geod_update_entry_obs(GTK_WIDGET(entryKierC), "entryWcieciaWsteczPktLNr",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktCNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKierP), "entryWcieciaWsteczPktLNr",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktPNr", NULL);	
	}
}


void on_entryWcieciaWsteczPktPNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *togglebutton = lookup_widget(GTK_WIDGET(celleditable), "cbtnWcieciaWsteczKierunki");
	GtkWidget *entryKierL = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaWsteczKierL");
	GtkWidget *entryKierC = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaWsteczKierC");
	GtkWidget *entryKierP = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaWsteczKierP");

	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryWcieciaWsteczPktPX", \
		"entryWcieciaWsteczPktPY", "entryWcieciaWsteczWynikNr");

	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(togglebutton))) {
		geod_update_entry_obs(GTK_WIDGET(entryKierL), "$hz",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktLNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKierC), "$hz",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktCNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKierP), "$hz",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktPNr", NULL);	
	}
	else {
		geod_update_entry_obs(GTK_WIDGET(entryKierC), "entryWcieciaWsteczPktLNr",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktCNr", NULL);
		geod_update_entry_obs(GTK_WIDGET(entryKierP), "entryWcieciaWsteczPktLNr",
			"entryWcieciaWsteczWynikNr", "entryWcieciaWsteczPktPNr", NULL);	
	}
}


void on_entryWcieciaAWPrzodPktLNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryAzLW = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaAWPrzodAzLW");
	GtkWidget *entryAzPW = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaAWPrzodAzPW");

	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryWcieciaAWPrzodPktLX", \
		"entryWcieciaAWPrzodPktLY", "entryWcieciaAWPrzodPktPNr");

	geod_update_entry_obs(GTK_WIDGET(entryAzLW), "$az",
		"entryWcieciaAWPrzodPktLNr", "entryWcieciaAWPrzodWynikNr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryAzPW), "$az",
		"entryWcieciaAWPrzodPktPNr", "entryWcieciaAWPrzodWynikNr", NULL);	
}


void on_entryWcieciaAWPrzodPktPNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryAzLW = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaAWPrzodAzLW");
	GtkWidget *entryAzPW = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaAWPrzodAzPW");

	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryWcieciaAWPrzodPktPX", \
		"entryWcieciaAWPrzodPktPY", "entryWcieciaAWPrzodWynikNr");

	geod_update_entry_obs(GTK_WIDGET(entryAzLW), "$az",
		"entryWcieciaAWPrzodPktLNr", "entryWcieciaAWPrzodWynikNr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryAzPW), "$az",
		"entryWcieciaAWPrzodPktPNr", "entryWcieciaAWPrzodWynikNr", NULL);	
}


void on_entryWcieciaAWsteczPktLNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryAzWL = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaAWsteczAzWL");
	GtkWidget *entryAzWP = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaAWsteczAzWP");

	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryWcieciaAWsteczPktLX", \
		"entryWcieciaAWsteczPktLY", "entryWcieciaAWsteczPktPNr");

	geod_update_entry_obs(GTK_WIDGET(entryAzWL), "$az",
		"entryWcieciaAWsteczWynikNr", "entryWcieciaAWsteczPktLNr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryAzWP), "$az",
		"entryWcieciaAWsteczWynikNr", "entryWcieciaAWsteczPktPNr", NULL);	
}


void on_entryWcieciaAWsteczPktPNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryAzWL = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaAWsteczAzWL");
	GtkWidget *entryAzWP = lookup_widget(GTK_WIDGET(celleditable), "entryWcieciaAWsteczAzWP");

	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryWcieciaAWsteczPktPX", \
		"entryWcieciaAWsteczPktPY", "entryWcieciaAWsteczWynikNr");
	geod_update_entry_obs(GTK_WIDGET(entryAzWL), "$az",
		"entryWcieciaAWsteczWynikNr", "entryWcieciaAWsteczPktLNr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryAzWP), "$az",
		"entryWcieciaAWsteczWynikNr", "entryWcieciaAWsteczPktPNr", NULL);	
}


void on_entryTHelmertaDostPNr_editing_done (GtkCellEditable *celleditable, gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryTHelmertaDostPX", \
		"entryTHelmertaDostPY", "entryTHelmertaDostWNr");
}


void on_entryTHelmertaDostWNr_editing_done (GtkCellEditable *celleditable, gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryTHelmertaDostWX", \
		"entryTHelmertaDostWY", "btnTHelmertaDostDodaj");
}


void on_entryTHelmertaTransfNr_editing_done (GtkCellEditable *celleditable, gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryTHelmertaTransfX", \
		"entryTHelmertaTransfY", "entryTHelmertaTransfNrNowy");
}


void on_entryAzymutPPNr_editing_done (GtkCellEditable *celleditable, gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryAzymutPPX", \
		"entryAzymutPPY", "entryAzymutPKNr");
}


void on_entryAzymutPKNr_editing_done (GtkCellEditable *celleditable, gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryAzymutPKX", \
		"entryAzymutPKY", "btnAzymutDodaj");
}


void on_entryKatCNr_editing_done (GtkCellEditable *celleditable, gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryKatCX", \
		"entryKatCY", "entryKatLNr");
}


void on_entryKatLNr_editing_done (GtkCellEditable *celleditable, gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryKatLX", \
		"entryKatLY", "entryKatPNr");
}


void on_entryKatPNr_editing_done (GtkCellEditable *celleditable, gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryKatPX", \
		"entryKatPY", "btnKatDodaj");
}


void on_entryDomiaryBazaPPNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryBazaPom = lookup_widget(GTK_WIDGET(celleditable), "entryDomiaryBazaPom");

	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryDomiaryBazaPPX", \
		"entryDomiaryBazaPPY", "entryDomiaryBazaPKNr");

	if (!geod_update_entry_obs(GTK_WIDGET(entryBazaPom), "$dl", \
		"entryDomiaryBazaPPNr", "entryDomiaryBazaPKNr", "entryDomiaryMiaryNr"))
		geod_update_entry_obs(GTK_WIDGET(entryBazaPom), "$dl", \
		"entryDomiaryBazaPKNr", "entryDomiaryBazaPPNr", "entryDomiaryMiaryNr");
}


void on_entryDomiaryBazaPKNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryBazaPom = lookup_widget(GTK_WIDGET(celleditable), "entryDomiaryBazaPom");
	
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryDomiaryBazaPKX", \
		"entryDomiaryBazaPKY", "entryDomiaryBazaPom");

	if (!geod_update_entry_obs(GTK_WIDGET(entryBazaPom), "$dl", \
			"entryDomiaryBazaPPNr", "entryDomiaryBazaPKNr", "entryDomiaryMiaryNr"))
		geod_update_entry_obs(GTK_WIDGET(entryBazaPom), "$dl", \
			"entryDomiaryBazaPKNr", "entryDomiaryBazaPPNr", "entryDomiaryMiaryNr");
}


void on_entryRzutProstaBazaPPNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryRzutProstaBazaPPX", \
		"entryRzutProstaBazaPPY", "entryRzutProstaBazaPKNr");
}


void on_entryRzutProstaBazaPKNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryRzutProstaBazaPKX", \
		"entryRzutProstaBazaPKY", "entryRzutProstaPunktNr");
}


void on_entryRzutProstaPunktNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryRzutProstaPunktX", \
		"entryRzutProstaPunktY", "entryRzutProstaPunktNrRzut");
}


void on_entryPowierzchniaPunktNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryPowierzchniaPunktX", \
		"entryPowierzchniaPunktY", "btnPowierzchniaPunktDodaj");
}


void on_entryBiegunoweNawiazanieStNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryKier = lookup_widget(GTK_WIDGET(celleditable), "entryBiegunoweNawiazanieKier");
	
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryBiegunoweNawiazanieStX", \
		"entryBiegunoweNawiazanieStY", "entryBiegunoweNawiazaniePktNNr");
	geod_update_entry_obs(GTK_WIDGET(entryKier), "$hz", "entryBiegunoweNawiazanieStNr", \
		"entryBiegunoweNawiazaniePktNNr", "entryBiegunowePktTyczonyNr");
}


void on_entryBiegunoweNawiazaniePktNNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryKier = lookup_widget(GTK_WIDGET(celleditable), "entryBiegunoweNawiazanieKier");
	const char* entry_txt =gtk_entry_get_text(GTK_ENTRY(celleditable));
	GtkWidget *entry_next = lookup_widget(GTK_WIDGET(celleditable), "entryBiegunowePktTyczonyNr");
	
	if (g_str_equal(entry_txt, ""))
		gtk_widget_grab_focus(GTK_WIDGET(entry_next));
	else {
		geod_update_entry_xy(GTK_WIDGET(celleditable), "entryBiegunoweNawiazaniePktNX", \
			"entryBiegunoweNawiazaniePktNY", "entryBiegunoweNawiazanieKier");
		geod_update_entry_obs(GTK_WIDGET(entryKier), "$hz", "entryBiegunoweNawiazanieStNr", \
			"entryBiegunoweNawiazaniePktNNr", "entryBiegunowePktTyczonyNr");
	}
}


void on_entryBiegunowePktTyczonyNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryBiegunowePktTyczonyX", \
		"entryBiegunowePktTyczonyY", "btnBiegunowePktTyczonyDodaj");
}


void on_entryPrzecieciaProstePr1Nr1_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryPrzecieciaProstePr1X1", \
		"entryPrzecieciaProstePr1Y1", "entryPrzecieciaProstePr1Nr2");
}


void on_entryPrzecieciaProstePr1Nr2_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryPrzecieciaProstePr1X2", \
		"entryPrzecieciaProstePr1Y2", "entryPrzecieciaProstePr2Nr1");
}


void on_entryPrzecieciaProstePr2Nr1_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryPrzecieciaProstePr2X1", \
		"entryPrzecieciaProstePr2Y1", "entryPrzecieciaProstePr2Nr2");
}


void on_entryPrzecieciaProstePr2Nr2_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryPrzecieciaProstePr2X2", \
		"entryPrzecieciaProstePr2Y2", "entryPrzecieciaProsteWynikNr");
}


void on_entryPrzecieciaOkragProstaOkrNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryPrzecieciaOkragProstaOkrX", \
		"entryPrzecieciaOkragProstaOkrY", "entryPrzecieciaOkragProstaOkrR");
}


void on_entryPrzecieciaOkragProstaPrNr1_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryPrzecieciaOkragProstaPrX1", \
		"entryPrzecieciaOkragProstaPrY1", "entryPrzecieciaOkragProstaPrNr2");
}


void on_entryPrzecieciaOkragProstaPrNr2_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryPrzecieciaOkragProstaPrX2", \
		"entryPrzecieciaOkragProstaPrY2", "entryPrzecieciaOkragProstaWynikNr1");
}


void on_entryPrzecieciaOkregiOkr1Nr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryPrzecieciaOkregiOkr1X", \
		"entryPrzecieciaOkregiOkr1Y", "entryPrzecieciaOkregiOkr1R");
}


void on_entryPrzecieciaOkregiOkr2Nr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryPrzecieciaOkregiOkr2X", \
		"entryPrzecieciaOkregiOkr2Y", "entryPrzecieciaOkregiOkr2R");
}


void on_entryOkragWpasowanie2ProstePLNr1_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragWpasowanie2ProstePLX1", \
		"entryOkragWpasowanie2ProstePLY1", "entryOkragWpasowanie2ProstePLNr2");
}


void on_entryOkragWpasowanie2ProstePLNr2_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragWpasowanie2ProstePLX2", \
		"entryOkragWpasowanie2ProstePLY2", "entryOkragWpasowanie2ProstePPNr1");
}


void on_entryOkragWpasowanie2ProstePPNr1_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragWpasowanie2ProstePPX1", \
		"entryOkragWpasowanie2ProstePPY1", "entryOkragWpasowanie2ProstePPNr2");
}


void on_entryOkragWpasowanie2ProstePPNr2_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragWpasowanie2ProstePPX2", \
		"entryOkragWpasowanie2ProstePPY2", "entryOkragWpasowanie2ProsteWynikNr");
}


void on_entryOkragWpasowanie3PktPNr1_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragWpasowanie3PktPX1", \
		"entryOkragWpasowanie3PktPY1", "entryOkragWpasowanie3PktPNr2");
}


void on_entryOkragWpasowanie3PktPNr2_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragWpasowanie3PktPX2", \
		"entryOkragWpasowanie3PktPY2", "entryOkragWpasowanie3PktPNr3");
}


void on_entryOkragWpasowanie3PktPNr3_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragWpasowanie3PktPX3", \
		"entryOkragWpasowanie3PktPY3", "entryOkragWpasowanie3PktWynikNr");
}


void on_entryOkragStycznaPunktOkrNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragStycznaPunktOkrX", \
		"entryOkragStycznaPunktOkrY", "entryOkragStycznaPunktOkrR");
}


void on_entryOkragStycznaPunktPktNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragStycznaPunktPktX", \
		"entryOkragStycznaPunktPktY", "entryOkragStycznaPunktWynikNr1");
}


void on_entryOkragStycznaProstaOkrNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragStycznaProstaOkrX", \
		"entryOkragStycznaProstaOkrY", "entryOkragStycznaProstaOkrR");
}


void on_entryOkragStycznaProstaPrNr1_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragStycznaProstaPrX1", \
		"entryOkragStycznaProstaPrY1", "entryOkragStycznaProstaPrNr2");
}


void on_entryOkragStycznaProstaPrNr2_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragStycznaProstaPrX2", \
		"entryOkragStycznaProstaPrY2", "rbtnOkragStycznaProstaStycznaRownolegla");
}


void on_entryOkragStyczna2OkregiOkr1Nr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragStyczna2OkregiOkr1X", \
		"entryOkragStyczna2OkregiOkr1Y", "entryOkragStyczna2OkregiOkr1R");
}


void on_entryOkragStyczna2OkregiOkr2Nr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragStyczna2OkregiOkr2X", \
		"entryOkragStyczna2OkregiOkr2Y", "entryOkragStyczna2OkregiOkr2R");
}


void on_entryTachimetriaNawiazanieStNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryKier1 = lookup_widget(GTK_WIDGET(celleditable), "entryTachimetriaNawiazaniePktN1Kier");
	GtkWidget *entryKier2 = lookup_widget(GTK_WIDGET(celleditable), "entryTachimetriaNawiazaniePktN2Kier");

  	geod_update_entry_xyh(GTK_WIDGET(celleditable), "entryTachimetriaNawiazanieStX", \
		"entryTachimetriaNawiazanieStY", "entryTachimetriaNawiazanieStH",
		"entryTachimetriaNawiazaniePktN1Nr");

	geod_update_entry_obs(GTK_WIDGET(entryKier1), "$hz", \
		"entryTachimetriaNawiazanieStNr", "entryTachimetriaNawiazaniePktN1Nr", NULL);
	geod_update_entry_obs(GTK_WIDGET(entryKier2), "$hz", \
		"entryTachimetriaNawiazanieStNr", "entryTachimetriaNawiazaniePktN2Nr", NULL);
}


void on_entryTachimetriaNawiazaniePktN1Nr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryKier1 = lookup_widget(GTK_WIDGET(celleditable), "entryTachimetriaNawiazaniePktN1Kier");

  	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryTachimetriaNawiazaniePktN1X", \
		"entryTachimetriaNawiazaniePktN1Y", "entryTachimetriaNawiazaniePktN2Nr");
	geod_update_entry_obs(GTK_WIDGET(entryKier1), "$hz", \
		"entryTachimetriaNawiazanieStNr", "entryTachimetriaNawiazaniePktN1Nr", NULL);
}


void on_entryTachimetriaNawiazaniePktN2Nr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	GtkWidget *entryKier2 = lookup_widget(GTK_WIDGET(celleditable), "entryTachimetriaNawiazaniePktN2Kier");
	
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryTachimetriaNawiazaniePktN2X",
		"entryTachimetriaNawiazaniePktN2Y", "entryTachimetriaNawiazaniePktN2Kier");
	geod_update_entry_obs(GTK_WIDGET(entryKier2), "$hz", "entryTachimetriaNawiazanieStNr",
		"entryTachimetriaNawiazaniePktN2Nr", "entryTachimetriaNawiazanieHi");
}


void on_entryPoligonNawiazanieP1Nr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryPoligonNawiazanieP1X", \
		"entryPoligonNawiazanieP1Y", "entryPoligonNawiazanieP2Nr");
}


void on_entryPoligonNawiazanieP2Nr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryPoligonNawiazanieP2X", \
		"entryPoligonNawiazanieP2Y", "entryPoligonNawiazanieK1Nr");
}


void on_entryPoligonNawiazanieK1Nr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	const char* entry_txt =gtk_entry_get_text(GTK_ENTRY(celleditable));
	GtkWidget *entry_next = lookup_widget(GTK_WIDGET(celleditable), "entryPoligonMiaryNr");
	
	if (g_str_equal(entry_txt, ""))
		gtk_widget_grab_focus(GTK_WIDGET(entry_next));
	else
		geod_update_entry_xy(GTK_WIDGET(celleditable), "entryPoligonNawiazanieK1X", \
			"entryPoligonNawiazanieK1Y", "entryPoligonNawiazanieK2Nr");
}


void on_entryPoligonNawiazanieK2Nr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	const char* entry_txt =gtk_entry_get_text(GTK_ENTRY(celleditable));
	GtkWidget *entry_next = lookup_widget(GTK_WIDGET(celleditable), "entryPoligonMiaryNr");
	
	if (g_str_equal(entry_txt, ""))
		gtk_widget_grab_focus(GTK_WIDGET(entry_next));
	else
		geod_update_entry_xy(GTK_WIDGET(celleditable), "entryPoligonNawiazanieK2X", \
			"entryPoligonNawiazanieK2Y", "entryPoligonMiaryNr");
}

void on_entryOkragTyczenieOkragONr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragTyczenieOkragOX", \
		"entryOkragTyczenieOkragOY", "entryOkragTyczenieOkragPNr");
}


void on_entryOkragTyczenieOkragPNr_editing_done (GtkCellEditable *celleditable,
	gpointer user_data)
{
	geod_update_entry_xy(GTK_WIDGET(celleditable), "entryOkragTyczenieOkragPX", \
		"entryOkragTyczenieOkragPY", "entryOkragTyczeniePunktNr");
}

void on_tbtnPowierzchniaOdswiez_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(toolbutton), "tbtnPowierzchniaOblicz");
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treePowierzchniaPunkty");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	const guint NROWS = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	char *nrPktGraniczny, *txtX = NULL, *txtY = NULL;
	cPunkt pktGraniczny;

	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);	
	for (guint i=1; i<=NROWS; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COL_PWP_NR, &nrPktGraniczny, -1);
		
		if (dbgeod_pointt_find(nrPktGraniczny, &pktGraniczny) == 0) {
			txtX = g_strdup_printf("%.*f", dok_xy_int, pktGraniczny.getX());
			txtY = g_strdup_printf("%.*f", dok_xy_int, pktGraniczny.getY());
			
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
				COL_PWP_NR, pktGraniczny.getNr(),
				COL_PWP_X, txtX, COL_PWP_X_DOUBLE, pktGraniczny.getX(),
				COL_PWP_Y, txtY, COL_PWP_Y_DOUBLE, pktGraniczny.getY(), -1);
			
			g_free(txtX);
			g_free(txtY);
		}
		g_free(nrPktGraniczny);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
	g_signal_emit_by_name(btnOblicz, "clicked");
}


void on_tbtnAzymutOdswiez_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeAzymut");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	const guint NROWS = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	char *nrP, *nrK;
	cPunkt pktP, pktK;
	double x, y, obl_azymut, obl_dlugosc;
	char *txtAzymut = NULL, *txtDlugosc = NULL;
	
	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);	
	for (guint i=1; i<=NROWS; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_AZ_PPNR, &nrP, COL_AZ_PKNR, &nrK, -1);
		
		if (dbgeod_pointt_find(nrP, &pktP) == 0)
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
				COL_AZ_PPNR, pktP.getNr(), COL_AZ_PPX, pktP.getX(), COL_AZ_PPY, pktP.getY(), -1);
		else {
			gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
				COL_AZ_PPNR, &nrP, COL_AZ_PPX, &x, COL_AZ_PPY, &y, -1);
			pktP.setXY(x, y);
			pktP.setNr(nrP);
		}
		
		if (dbgeod_pointt_find(nrK, &pktK) == 0)
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
				COL_AZ_PKNR, pktK.getNr(), COL_AZ_PKX, pktK.getX(), COL_AZ_PKY, pktK.getY(), -1);
		else {
			gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
				COL_AZ_PKNR, &nrK, COL_AZ_PKX, &x, COL_AZ_PKY, &y, -1);
			pktK.setXY(x, y);
			pktK.setNr(nrK);
		}

		if(azymut(pktP, pktK, obl_azymut) == GEODERR_OK)
			txtAzymut = geod_kat_from_rad(obl_azymut);
		else
			txtAzymut = g_strdup("");

 		obl_dlugosc = dlugosc(pktP, pktK);
		txtDlugosc = g_strdup_printf("%.*f", dok_xy_int, obl_dlugosc);

		gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
			COL_AZ_AZYM, txtAzymut, COL_AZ_DL_DOUBLE, obl_dlugosc, COL_AZ_DL, txtDlugosc, -1);
	
		g_free(txtAzymut);
		g_free(txtDlugosc);
		g_free(nrP);
		g_free(nrK);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
}


void on_tbtnKatOdswiez_clicked (GtkToolButton *toolbutton, gpointer user_data)
{
	GtkWidget *treeview = lookup_widget(GTK_WIDGET(toolbutton), "treeKat");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	const guint NROWS = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
	GtkTreeIter iter;
	char *nrStanowisko, *nrLewy, *nrPrawy;
	cPunkt pktStanowisko, pktLewy, pktPrawy;
	double x, y;
	char *txtObliczonyKat = NULL;
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);	
	for (guint i=1; i<=NROWS; i++) {
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
			COL_KAT_CNR, &nrStanowisko, COL_KAT_LNR, &nrLewy, COL_KAT_PNR, &nrPrawy, -1);
		
		if (dbgeod_pointt_find(nrStanowisko, &pktStanowisko) == 0)
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
				COL_KAT_CNR, pktStanowisko.getNr(), COL_KAT_CX, pktStanowisko.getX(),
				COL_KAT_CY, pktStanowisko.getY(), -1);
		else {
			gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
				COL_KAT_CNR, &nrStanowisko, COL_KAT_CX, &x, COL_KAT_CY, &y, -1);
			pktStanowisko.setXY(x, y);
			pktStanowisko.setNr(nrStanowisko);
		}
		
		if (dbgeod_pointt_find(nrLewy, &pktLewy) == 0) {
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
				COL_KAT_LNR, pktLewy.getNr(), COL_KAT_LX, pktLewy.getX(), COL_KAT_LY, pktLewy.getY(), -1);
		}
		else {
			gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
				COL_KAT_LNR, &nrLewy, COL_KAT_LX, &x, COL_KAT_LY, &y, -1);
			pktLewy.setXY(x, y);
			pktLewy.setNr(nrLewy);
		}
		
		if (dbgeod_pointt_find(nrPrawy, &pktPrawy) == 0)
			gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
				COL_KAT_PNR, pktPrawy.getNr(), COL_KAT_PX, pktPrawy.getX(), COL_KAT_PY, pktPrawy.getY(), -1);
		else {
			gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
				COL_KAT_PNR, &nrPrawy, COL_KAT_PX, &x, COL_KAT_PY, &y, -1);
			pktPrawy.setXY(x, y);
			pktPrawy.setNr(nrPrawy);
		}

		txtObliczonyKat = geod_kat_from_rad(kat(pktStanowisko, pktLewy, pktPrawy));
 		gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
			COL_KAT_KAT, txtObliczonyKat, -1);
		delete [] txtObliczonyKat;

		g_free(nrLewy);
		g_free(nrPrawy);
		g_free(nrStanowisko);
		gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
}


void on_treePowierzchniaPunkty_row_activated (GtkTreeView *treeview,
	GtkTreePath *path, GtkTreeViewColumn *column, gpointer user_data)
{
	gtk_tree_selection_unselect_path (gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview)), path);
}


gboolean on_winPoligon_delete_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{

  return FALSE;
}

gboolean on_winAzymut_delete_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{

  return FALSE;
}


gboolean on_winKat_delete_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{

  return FALSE;
}


gboolean on_winPowierzchnia_delete_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{

  return FALSE;
}

void cellPoligon_edited_callback (GtkCellRendererText *cell, gchar *path_string,
	gchar *new_text, gpointer treeview)
{
	guint col_number = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(cell), "my_column_num"));
	double new_value;
	char *old_text = NULL;

	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	GtkTreePath *path = gtk_tree_path_new_from_string(path_string);

	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	
	if (!gtk_tree_model_get_iter(model, &iter, path))
		return;
	
	gtk_tree_model_get (model, &iter, col_number, &old_text, -1);
	if (g_str_equal(old_text, new_text)) {
		g_free(old_text);
		gtk_tree_path_free(path);
		return;
	}	
	
	switch (col_number) {
		case COL_POL_KOD:
			gtk_tree_store_set (GTK_TREE_STORE(model), &iter, COL_POL_KOD, new_text, -1);
			break;
		case COL_POL_DL:
			new_value = g_strtod(new_text, NULL);
			gtk_tree_store_set (GTK_TREE_STORE(model), &iter,
				COL_POL_DL, g_strdup_printf("%.*f", dok_xy_int, new_value), COL_POL_DL_DOUBLE, new_value,
				COL_POL_X, "", COL_POL_Y, "", -1);
			break;
		case COL_POL_KAT:
			geod_get_kat(new_text, new_value);
			gtk_tree_store_set (GTK_TREE_STORE(model), &iter,
				COL_POL_KAT, geod_kat_from_rad(new_value), COL_POL_KAT_DOUBLE, new_value,
				COL_POL_X, "", COL_POL_Y, "", -1);
			break;
	}
	gtk_tree_path_free(path);
}

void on_treePoligon_row_activated  (GtkTreeView *treeview,
	GtkTreePath *path, GtkTreeViewColumn *column, gpointer user_data)
{
	gtk_tree_selection_unselect_path (gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview)), path);
}

void on_btnPowierzchniaPunktDodajZakres_clicked (GtkButton *button, gpointer user_data)
{
	cPunkt pktGraniczny;
	GtkWidget *btnOblicz = lookup_widget(GTK_WIDGET(button), "tbtnPowierzchniaOblicz");
	GtkWidget *entryDzialka = lookup_widget(GTK_WIDGET(button), "entryPowierzchniaPunktDzialka");
	const char *entry_dz_nr = gtk_entry_get_text(GTK_ENTRY(entryDzialka));

	GtkWidget *tree = lookup_widget(GTK_WIDGET(button), "treePowierzchniaPunkty");
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	GtkTreeIter iter, iter_i;
	GtkTreePath *tpath;

	bool GEOD_INC, nastepny = FALSE, istnieje = FALSE;
	char *tree_dz_nr, *txtX = NULL, *txtY = NULL;

	char dok_xy[CONFIG_VALUE_MAX_LEN];
	int dok_xy_int = 3;
	if (!dbgeod_config_get("dok_xy", dok_xy)) dok_xy_int = atoi(dok_xy);
	
	char pkt_od[NR_MAX_LEN], pkt_do[NR_MAX_LEN];
	char *txt_nr = NULL, *txt_old = NULL;
	GtkWidget *dialog = create_dialogPunktyZakres();

	int response = gtk_dialog_run(GTK_DIALOG(dialog));
	GtkWidget *entry_od = lookup_widget(GTK_WIDGET(dialog), "entryDialogPunktyZakresOd");
	GtkWidget *entry_do = lookup_widget(GTK_WIDGET(dialog), "entryDialogPunktyZakresDo");

	g_strlcpy(pkt_od, gtk_entry_get_text(GTK_ENTRY(entry_od)), NR_MAX_LEN);
	g_strlcpy(pkt_do, gtk_entry_get_text(GTK_ENTRY(entry_do)), NR_MAX_LEN);
	gtk_widget_destroy(GTK_WIDGET(dialog));

	if (response == GTK_RESPONSE_OK && !g_str_equal(pkt_od,""), !g_str_equal(pkt_do,"")) {
		int typ = MAX(geod_nr_type(pkt_od), geod_nr_type(pkt_do));
		GEOD_INC = (geod_nr_cmp(pkt_od, pkt_do) < 0) ? TRUE : FALSE;
		txt_nr =  g_strdup(pkt_od);

		gtk_tree_view_get_cursor(GTK_TREE_VIEW(tree), &tpath, NULL);
		if (tpath != NULL) {
			gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &iter_i, tpath);
			gtk_tree_model_get(GTK_TREE_MODEL(model), &iter_i, COL_PWP_DZ, &tree_dz_nr, -1);
			if (g_str_equal(tree_dz_nr, entry_dz_nr)) {
				istnieje = TRUE;
				nastepny = TRUE;
				gtk_tree_selection_unselect_all(gtk_tree_view_get_selection(GTK_TREE_VIEW(tree)));
			}
		}
		else {
			nastepny = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter_i);
			while (nastepny)
			{
				gtk_tree_model_get(GTK_TREE_MODEL(model), &iter_i, COL_PWP_DZ, &tree_dz_nr, -1);
				if (g_str_equal(tree_dz_nr, entry_dz_nr))
					istnieje = TRUE;

				if (istnieje && !g_str_equal(tree_dz_nr, entry_dz_nr)) {
					g_free(tree_dz_nr);
					break;
				}

				nastepny = gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter_i);
				g_free(tree_dz_nr);
			}
		}

		do {
			if (!dbgeod_pointt_find(txt_nr, &pktGraniczny)) {
				txtX = g_strdup_printf("%.*f", dok_xy_int, pktGraniczny.getX());
				txtY = g_strdup_printf("%.*f", dok_xy_int, pktGraniczny.getY());

				if (istnieje && nastepny)
					gtk_tree_store_insert_before(GTK_TREE_STORE(model), &iter, NULL, &iter_i);
				else
					gtk_tree_store_append(GTK_TREE_STORE(model), &iter, NULL);
	
				gtk_tree_store_set(GTK_TREE_STORE(model), &iter,
					COL_PWP_DZ, entry_dz_nr, COL_PWP_NR, pktGraniczny.getNr(),
					COL_PWP_X, txtX, COL_PWP_X_DOUBLE, pktGraniczny.getX(),
					COL_PWP_Y, txtY, COL_PWP_Y_DOUBLE, pktGraniczny.getY(), -1);

				while (gtk_events_pending ())
  					gtk_main_iteration ();
				
				g_free(txtX);
				g_free(txtY);
			}
			g_free(txt_old);
			txt_old = txt_nr;
			txt_nr = GEOD_INC ?	geod_inc(txt_old, typ) : geod_dec(txt_old, typ);
		} while (!g_str_equal(txt_old, pkt_do) && strlen(txt_nr)<NR_MAX_LEN && !g_str_equal(txt_nr, ""));
		
		g_free(txt_old);
		g_free(txt_nr);
		gtk_tree_path_free(tpath);
		g_signal_emit_by_name(G_OBJECT(btnOblicz), "clicked");
	}
}


gboolean event_get_pkt_dpz (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	GtkWidget *dialog = NULL;
	GtkWidget *dialog_entry = NULL;
	
	switch (event->keyval) {
		case GDK_F1:
			dialog = create_dialogPunkty();
			g_object_set_data_full (G_OBJECT (dialog), "eventEntry",
				g_object_ref (widget), (GDestroyNotify) g_object_unref);
			gtk_dialog_run(GTK_DIALOG(dialog));
			break;
		case GDK_Insert:
			dialog = create_dialogDodajPunkt();
			g_object_set_data_full (G_OBJECT (dialog), "eventEntry",
				g_object_ref (widget), (GDestroyNotify) g_object_unref);
			dialog_entry = lookup_widget(GTK_WIDGET(dialog), "entryDialogDodajPunktNr");
			gtk_entry_set_text(GTK_ENTRY(dialog_entry), g_strdup(gtk_entry_get_text(GTK_ENTRY(widget))));
			gtk_dialog_run(GTK_DIALOG(dialog));
			break;
	}
	return FALSE;
}
