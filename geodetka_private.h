/* THIS FILE WILL BE OVERWRITTEN BY DEV-C++ */
/* DO NOT EDIT ! */

#ifndef GEODETKA_PRIVATE_H
#define GEODETKA_PRIVATE_H

/* VERSION DEFINITIONS */
#define VER_STRING	"1.2.2.0"
#define VER_MAJOR	1
#define VER_MINOR	2
#define VER_RELEASE	1
#define VER_BUILD	0
#define COMPANY_NAME	"Politechnika Warszawska"
#define FILE_VERSION	""
#define FILE_DESCRIPTION	"Developed using the Dev-C++ IDE"
#define INTERNAL_NAME	"Kalkulator Geodezyjny napisany w GTK"
#define LEGAL_COPYRIGHT	"Bogus�aw Ciastek"
#define LEGAL_TRADEMARKS	""
#define ORIGINAL_FILENAME	"geodetka"
#define PRODUCT_NAME	"GeodeTKa"
#define PRODUCT_VERSION	""

#endif /*GEODETKA_PRIVATE_H*/
