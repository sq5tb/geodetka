# geodetka.spec
#
# Copyright (c) 2005-2006 Boguslaw Ciastek ciacho@z.pl
#
%define name geodetka
%define version 1.2.3
%define release 1

######################################################################
%define is_mandrake %(test -e /etc/mandrake-release && echo 1 || echo 0)
%define is_suse %(test -e /etc/SuSE-release && echo 1 || echo 0)
%define is_fedora %(test -e /etc/fedora-release && echo 1 || echo 0)
%define is_dist 0

%if %is_mandrake
%define dist mandrake
%define disttag mdk
%define is_dist 1
%endif
%if %is_suse
%define dist suse
%define disttag suse
%define is_dist 1
%define kde_path /opt/kde3
%endif
%if %is_fedora
%define dist fedora
%define disttag fc
%define is_dist 1
%endif

%define distver %(release="`rpm -q --queryformat='%{VERSION}' %{dist}-release 2> /dev/null | tr . : | sed s/://g`" ; if test $? != 0 ; then release="" ; fi ; echo "$release")

Name: %{name}
Version: %{version}
%if %is_dist
Release: %{release}.%{disttag}%{distver}
%else
Release: %{release}
%endif
License: GPL
Group: Applications/Engineering
Prefix: %{_prefix}
Summary: Program for basic geodetic computations
Summary(pl): Program do podstawowych obliczeń geodezyjnych
Vendor: Boguslaw Ciastek <ciacho@z.pl>
URL: http://geodetka.prv.pl/
Packager: Boguslaw Ciastek <ciacho@z.pl>
Source0: %{name}-%{version}.tar.gz

BuildRoot: %{_tmppath}/root-%{name}-%{version}

BuildRequires: autoconf
BuildRequires: libstdc++-devel
BuildRequires: gtk2-devel >= 2.4.0
BuildRequires: sqlite-devel >= 3.1.0

Requires: gtk2 >= 2.4.0
Requires: sqlite >= 3.1.0

%description
Program for basic geodetic and land surveying computations written in GTK.

%description -l pl
Program do podstawowych obliczeń geodezyjnych napisany za pomocą biblioteki GTK.

%prep
%setup

%build
autoconf
%configure
%{__make}

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall
%find_lang %{name}

%clean
if [ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ]; then
rm -rf $RPM_BUILD_ROOT
fi
rm -rf %{_builddir}/%{name}-%{version}


%post

%postun


%files -f %{name}.lang
%defattr(-,root,root)
%{_bindir}/%{name}
%{_datadir}/icons/%{name}-icon.png
%{_datadir}/%{name}/*
%{_datadir}/applications/*.desktop
%{_datadir}/doc/%{name}/*


%changelog
* Sat May 26 2006 Buguslaw Ciastek
- define dist and disttag

* Thu Aug 09 2005 Buguslaw Ciastek
- Initial spec file
